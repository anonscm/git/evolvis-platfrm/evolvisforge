#! /bin/sh

test -f /root/dump || {
	echo "Couldn't restore the database: No /root/dump found"
	exit 2
}

if [ $# -eq 1 ]
then
	database=$1
else
	export PATH=$PATH:/usr/share/gforge/bin/:/usr/share/gforge/utils:/usr/share/gforge/src/utils:/opt/gforge/utils
	database=`FUSIONFORGE_NO_PLUGINS=true forge_get_config database_name`
fi
if [ "x$database" = "x" ]
then
	echo "Forge database name not found"
	exit 1
else
	echo "Forge database is $database"
fi

echo "Stopping apache"
if type invoke-rc.d 2>/dev/null
then
	invoke-rc.d apache2 stop
else
	service httpd stop
fi

echo "Starting the database"
if type invoke-rc.d 2>/dev/null
then
	invoke-rc.d postgresql restart
else
	service postgresql restart
fi

echo "Dropping database $database"
su - postgres -c "dropdb -e $database"

echo "Restore database from dump file: psql -f- < /root/dump"
su - postgres -c "psql -f-" < /root/dump > /var/log/pg_restore.log 2>/var/log/pg_restore.err

echo "Starting apache"
if type invoke-rc.d 2>/dev/null
then
	invoke-rc.d apache2 start
else
	service httpd start
fi
