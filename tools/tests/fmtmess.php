#!/usr/bin/env php
<?php
/*-
 * Stand-alone message formatter
 *
 * Copyright © 2011
 *	Thorsten “mirabilos” Glaser <t.glaser@tarent.de>
 * All rights reserved.
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

function incfile($file, $paths) {
	foreach ($paths as $apath) {
		$f = $apath . '/' . $file;
		if (file_exists($f)) {
			require_once($f);
			return;
		}
	}
}

incfile('env.inc.php', array(
	dirname(__FILE__).'/../../src/www',
	'/usr/share/gforge/www',
    ));
require_once $gfwww.'include/squal_pre.php';
require_once $gfwww.'include/html.php';
incfile('descriptive.php', array('.', $gfcommon.'include'));

$group_id = 1;

$mess = file_get_contents('php://stdin');
echo showmess_fmt($mess) . "\n";
