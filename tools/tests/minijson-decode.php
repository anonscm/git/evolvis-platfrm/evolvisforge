#!/usr/bin/env php
<?php
/*-
 * Minimal JSON decoding command line utility, for test purposes
 *
 * Copyright © 2011
 *	Thorsten “mirabilos” Glaser <t.glaser@tarent.de>
 * All rights reserved.
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

require_once(dirname(__FILE__).'/../../src/common/include/minijson.php');

$eingabe = file_get_contents('php://stdin');
$resultat = '';
$code = minijson_decode($eingabe, $resultat);
print_r(array('eingabe' => $eingabe, 'code' => $code, 'resultat' => $resultat));
echo "\n";
if ($code) {
	echo 'JSON => ' . minijson_encode($resultat) . "\n";
} else {
	echo "Not trying to re-encode due to failures.\n";
}
