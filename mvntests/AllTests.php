<?php
if (!defined('PHPUnit_MAIN_METHOD')) {
	define('PHPUnit_MAIN_METHOD', 'AllTests::main');
}

require_once 'PHPUnit/Framework.php';
require_once 'PHPUnit/TextUI/TestRunner.php';

// Code tests
require_once 'syntax/AllTests.php';

class AllTests
{
	public static function main()
	{
		try {
			$res = PHPUnit_TextUI_TestRunner::run(self::suite());
		} catch (Exception $e) {
			echo "Caught exception: " . $e->getMessage() . "\n";
			exit(255);
		}
		exit($res->wasSuccessful() ? 0 : 1);
	}

	public static function suite()
	{
		$suite = new PHPUnit_Framework_TestSuite('PHPUnit');

		// Code tests
		$suite->addTest(Syntax_AllTests::suite());

		return $suite;
	}
}

if (PHPUnit_MAIN_METHOD == 'AllTests::main') {
	AllTests::main();
}
?>
