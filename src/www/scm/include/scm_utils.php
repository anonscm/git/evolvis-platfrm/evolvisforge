<?php
/**
 * FusionForge SCM Library
 *
 * Copyright 2004-2005 (c) GForge LLC, Tim Perdue
 * Copyright 2010 (c), Franck Villaume
 * Copyright (C) 2010-2011 Alain Peyrat - Alcatel-Lucent
 * Copyright © 2012
 *	Thorsten Glaser <t.glaser@tarent.de>
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

$scm_instance_cache = false;
function &scm_get_instance($group) {
	global $gfcommon, $scm_instance_cache;
	require_once $gfcommon.'scm/SCMFactory.class.php';

	if ($scm_instance_cache === false) {
		$SCMFactory = new SCMFactory();
		$scm_plugins = $SCMFactory->getSCMs();
		$usedplugin = false;
		if (count($scm_plugins) >= 1) {
			foreach ($scm_plugins as $plugin) {
				$myPlugin = plugin_get_object($plugin);
				if ($group->usesPlugin($myPlugin->name)) {
					if ($usedplugin === false) {
						$usedplugin = $myPlugin;
					} else {
						exit_error(_('More than one SCM plugin in use.'), 'scm');
					}
				}
			}
		}
		$scm_instance_cache = $usedplugin;
	}
	return $scm_instance_cache;
}

function scm_header($params) {
	global $HTML;

	if (!forge_get_config('use_scm')) {
		exit_disabled();
	}

	$project = group_get_object($params['group']);
	if (!$project || !is_object($project)) {
		exit_no_group();
	} elseif ($project->isError()) {
		exit_error($project->getErrorMessage(),'scm');
	}

	if (!$project->usesSCM()) {
		exit_disabled();
	}

	$submenu = array(array(), array(), array());
	$submenu[0][] = _('Access Source Code');
	$submenu[1][] = '/scm/?group_id=' . $params['group'];
	$submenu[2][] = _('-tooltip:scm:view');

	$scmplugin = scm_get_instance($project);
	if ($scmplugin && $scmplugin->browserDisplayable($project)) {
		$submenu[0][] = _('Browse Source Code');
		$submenu[1][] = '/scm/browser.php?group_id=' .
		    $params['group'];
		$submenu[2][] = _('Look at the source code repository using your webbrowser!');
	}

	if (session_loggedin() &&
	    forge_check_perm('project_admin', $project->getID())) {
		$submenu[0][] = _('Reporting');
		$submenu[1][] = '/scm/reporting/?group_id=' . $params['group'];
		$submenu[2][] = _('-tooltip:scm:reporting');

		$submenu[0][] = _('Administration');
		$submenu[1][] = '/scm/admin/?group_id=' . $params['group'];
		$submenu[2][] = _('-tooltip:scm:admin');
	}

	$params['submenu'] = $submenu;
	$params['toptab'] = 'scm';
	site_project_header($params);
	echo '<div id="scm" class="scm">';
}

function scm_footer() {
	echo '</div>';
	site_project_footer(array());
}

// Local Variables:
// mode: php
// c-file-style: "bsd"
// End:
