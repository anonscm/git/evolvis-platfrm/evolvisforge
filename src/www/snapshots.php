<?php
/*-
 * FusionForge SCM snapshots download page
 *
 * Copyright 1999-2001 (c) VA Linux Systems
 * Copyright 2003-2004 (c) GForge
 * Copyright 2010 (c) Franck Villaume
 * Copyright (C) 2011 Alain Peyrat - Alcatel-Lucent
 * Copyright © 2012
 *	Thorsten Glaser <t.glaser@tarent.de>
 * All rights reserved.
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

define('ev_outbuf', false);

require_once('env.inc.php');
require_once $gfcommon.'include/pre.php';

/* get current information */
if (!($group_id = getIntFromGet('group_id'))) {
	exit_no_group();
}
$group = group_get_object($group_id);
if (!$group || !is_object($group)) {
	exit_error(_('Error creating group'), 'scm');
} elseif ($group->isError()) {
	exit_error($group->getErrorMessage(), 'scm');
}

/* SCM snapshot download requires SCM read access permissions */
if (!forge_check_perm('scm', $group->getID(), 'read')) {
	exit_permission_denied('', 'scm');
}

$fn = $group->getUnixName() . '-scm-latest.tar' .
    util_get_compressed_file_extension();
$fp = forge_get_config('scm_snapshots_path') . '/' . $fn;

if (!file_exists($fp)) {
	session_redirect('/404.php');
}

sysdebug_off('Content-type: application/force-download');
header('Content-Length: ' . filesize($fp));
header('Content-Disposition: attachment; filename="' .
    preg_replace('/[\x00-\x08\x0A-\x1F\x7F]/', '',
    preg_replace('/[\x09\x20\x22\x5C]/', "\\\\$0",
    $fn)) . '"');
header('X-Content-Type-Options: nosniff');
readfile_chunked($fp);
exit(0);
