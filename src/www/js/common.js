/*-
 * common ECMAscript functions for FusionForge
 *
 * Copyright © 2011, 2012
 *	Thorsten Glaser <t.glaser@tarent.de>
 * Copyright © 2012
 *	Patrick Apel <p.apel@tarent.de>
 * (and others)
 * All rights reserved.
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

function checkboxHandle100(boxname, curidx) {
	try {
		var boxes = document.getElementsByName(boxname);
		var imax = boxes.length;
		var cnt = 0;

		for (var i = 1; i < imax; i++) {
			if (boxes[i].checked) {
				cnt++;
			}
		}

		if (cnt == 0) {
			boxes[0].checked = true;
			return true;
		}

		if (curidx == 0 && boxes[0].checked) {
			for (var i = 1; i < imax; i++) {
				boxes[i].checked = false;
			}
			return true;
		}

		boxes[0].checked = false;
		return true;
	} catch (e) {
		return false;
	}
}

function MM_goToURL() { //v3.0
	var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
	for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}

function toggledisplay(a,list) {
  var elem=document.getElementById(list)
  var open='/images/folderArrowOpen.png'
  var close='/images/folderArrowClosed.png'
  if (elem.style.display=='none') {
    elem.style.display='block'
    a.title='Click to display only admins'
    a.src = open
  } else {
    elem.style.display='none';
    a.title='Click to display all members'
    a.src = close
  }
}

function switch2edit (a,show,edit) {
  var elemshow=document.getElementById(show)
  var elemedit=document.getElementById(edit)
  if (elemedit.style.display=='none') {
    elemedit.style.display='block'
    elemshow.style.display='none'
    a.style.display='none'
  }
 }

 function switch2display (a,bt,disp,i) {
  var elembt1=document.getElementById(bt+'1_'+i)
  var elemdisp1=document.getElementById(disp+'1_'+i)
  var elembt2=document.getElementById(bt+'2_'+i)
  var elemdisp2=document.getElementById(disp+'2_'+i)

  if (elemdisp1.style.display=='none') {
    elembt1.style.display='inline'
    elemdisp1.style.display='block'
    elembt2.style.display='none'
    elemdisp2.style.display='none'
  }
  else {
    elembt1.style.display='none'
    elemdisp1.style.display='none'
    elembt2.style.display='inline'
    elemdisp2.style.display='block'
  }
}

function flipAll(formObj) {
  var isFirstSet = -1;
  for (var i=0; i < formObj.length; i++) {
      fldObj = formObj.elements[i];
      if ((fldObj.type == 'checkbox') && (fldObj.name.substring(0,2) == 'p[')) {
         if (isFirstSet == -1)
           isFirstSet = (fldObj.checked) ? true : false;
         fldObj.checked = (isFirstSet) ? false : true;
       }
   }
}


function evcollapse(iid, eid, init, cid) {
	var eicon = document.getElementById(iid);
	var eelem = document.getElementById(eid);
	var dopen = 0;
	if (eelem.style.display == "none") {
		dopen = 1;
	}
	if (init == 1) {
		eicon.style.display = "block";
		dopen = 1;
		if (cid && getCookie("evcol_" + cid)) {
			dopen = 0;
		}
	}
	if (dopen == 1) {
		eicon.getElementsByTagName("img")[0].getAttributeNode("src").nodeValue = icon_minimise;
		eelem.style.display = "table-row";
		if (cid) {
			deleteCookie("evcol_" + cid);
		}
	} else {
		eicon.getElementsByTagName("img")[0].getAttributeNode("src").nodeValue = icon_maximise;
		eelem.style.display = "none";
		if (cid) {
			/* to make cookies persistent, set an expiry */
			var expdate = new Date();
			expdate.setYear(expdate.getFullYear() + 1);

			setCookie("evcol_" + cid, 1, expdate);
		}
	}
}


function setCookie(name, value, expires, path, domain, secure) {
	document.cookie = name + "=" + escape(value) +
	    ((expires) ? "; expires=" + expires.toGMTString() : "") +
	    ((path) ? "; path=" + path : "") +
	    ((domain) ? "; domain=" + domain : "") +
	    ((secure) ? "; secure" : "");
}

function getCookie(name) {
	var dc = document.cookie;
	var prefix = name + "=";
	var begin = dc.indexOf("; " + prefix);
	if (begin == -1) {
		begin = dc.indexOf(prefix);
		if (begin != 0) {
			return null;
		}
	} else {
		begin += 2;
	}
	var end = document.cookie.indexOf(";", begin);
	if (end == -1) {
		end = dc.length;
	}
	return unescape(dc.substring(begin + prefix.length, end));
}

function deleteCookie(name, path, domain) {
	if (getCookie(name)) {
		document.cookie = name + "=" +
		    ((path) ? "; path=" + path : "") +
		    ((domain) ? "; domain=" + domain : "") +
		    "; expires=Thu, 01-Jan-70 00:00:01 GMT";
	}
}

function notepad_window(form, kind) {
	var notepad_url = sys_url_base + "notepad.php?form=" + form +
	    "&kind=" + kind + "&rows=32&cols=80";
	var notepad_title = "FusionForge Notepad";
	var notepad_winopt = "scrollbars=yes,resizable=yes,toolbar=no," +
	    "height=700,width=620";
	NotepadWin = window.open(notepad_url, notepad_title, notepad_winopt);
}

function update_tracker_status() {
	$('efstatusmap').update('(' + status_map[$F('tracker-status_id')] + ')');
}
