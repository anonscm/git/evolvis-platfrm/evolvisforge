<?php
/*-
 * FusionForge Projects Redirector
 *
 * Copyright 1999-2001 (c) VA Linux Systems
 * Copyright 2002-2004 (c) GForge Team
 * Copyright 2010 (c) FusionForge Team
 * http://fusionforge.org/
 * Copyright © 2012
 *	Thorsten Glaser <t.glaser@tarent.de>
 * All rights reserved.
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

require_once('../env.inc.php');
require_once $gfcommon.'include/pre.php';

//
//	IMPORTANT NOTE!!
//	Setting up the $project object is all being
//	handled in the logger now
//	This was done so the logger would accurately record these pages
//

//
//	test to see if the logger was successful in setting up the objects
//
if (!$group_id) {
	/* prevent information leak */
	if (!session_get_user()) {
		/* handles the redirect for us */
		include $gfwww.'include/project_home.php';
		exit;
	}

	/* we are logged in for sure */
	exit_no_group();
}

include $gfwww.'include/project_home.php';
