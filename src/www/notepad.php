<?php
/**
 * FusionForge Notepad Facility
 *
 * Copyright 2003 Hidenari Miwa, FUJITSU PRIME SOFTWARE TECHNOLOGIES LIMITED
 * http://www.pst.fujitsu.com/
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

require_once('env.inc.php');
require_once $gfcommon.'include/pre.php';

/*
 *  form:       Input form name
 *  rows:       Rows
 *  cols:       Colums
 */

$form = getStringFromRequest('form');
$rows = getIntFromRequest('rows');
$cols = getIntFromRequest('cols');

if (!is_int($rows)) {
        $rows = 30;
}
if (!is_int($cols)) {
        $cols = 75;
}
$pform = '';
if ($form) {
        $pform = "opener.".$form;
}

html_generic_fileheader(_('FusionForge Notepad'));
?>
<script type="text/javascript"><!--//--><![CDATA[//><!--
    function load_initial_value() {
        try {
            aform = <?php echo $pform ?>;
            document.forms[0].details.value = aform.value;
        } catch (e) {
        }
    }

    function set_value() {
         try {
             aform = eval("<?php echo $pform ?>");
             aform.value = document.forms[0].details.value;
         } catch (e) {
         }
    }

    function set_and_close() {
         set_value();
         window.close();
    }

//--><!]]></script>
  </head>
  <body onload="load_initial_value();" >
    <form name="form1" action="">
      <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
          <td><input type="button" name="ok" value="OK"
                     onclick="set_and_close()"/>
          <input type="reset" value="Clear" />
          <input type="button" value="Cancel"
                     onclick="window.close()" />
          </td>
        </tr>
        <tr>
          <td>
            <textarea name="details" rows="<?php echo $rows ?>"
                      cols="<?php echo $cols ?>"></textarea>
          </td>
        </tr>
        <tr>
          <td><input type="button" name="ok" value="OK"
                     onclick="set_and_close()"/>
          <input type="reset" value="Clear" />
          <input type="button" value="Cancel"
                     onclick="window.close()" />
          </td>
        </tr>
      </table>
    </form>
<?php
$kind = getStringFromRequest("kind");
if ($kind == "messformat") {
	echo "<hr />\n<p>" .
	    _('You can apply basic formatting to the messages: by default, they are displayed as running text with automatic line breaks; manual line breaks (one newline) and paragraph breaks (two or more newlines) are also supported. Furthermore, the following formatting is supported:') .
	    "</p><ul>\n" .
		"<li>" . _('<tt>{{{</tt> on a line by itself switches to preformatted text') . "</li>\n" .
		"<li>" . _('<tt>----</tt> on a line by itself (outside of preformatted text) displays a horizontal rule') . "</li>\n" .
		"<li>" . _('<tt>&lt;&lt;&lt;b:</tt><i>sometext</i><tt>&gt;&gt;&gt;</tt> displays “sometext” in a <b>boldface</b> font') . "</li>\n" .
		"<li>" . _('<tt>&lt;&lt;&lt;i:</tt><i>sometext</i><tt>&gt;&gt;&gt;</tt> displays “sometext” in an <i>italic</i> font') . "</li>\n" .
		"<li>" . _('<tt>&lt;&lt;&lt;u:</tt><i>sometext</i><tt>&gt;&gt;&gt;</tt> displays “sometext” in an <span style="text-decoration:underline;">underlined</span> manner') . "</li>\n" .
		"<li>" . _('<tt>&lt;&lt;&lt;tt:</tt><i>sometext</i><tt>&gt;&gt;&gt;</tt> displays “sometext” in <tt>monospace</tt> (teletype font)') . "</li>\n" .
		"<li>" . _('<tt>&lt;&lt;&lt;ins:</tt><i>sometext</i><tt>&gt;&gt;&gt;</tt> displays “sometext” as <ins>insertion</ins> (with browser-defined rendering)') . "</li>\n" .
		"<li>" . _('<tt>&lt;&lt;&lt;del:</tt><i>sometext</i><tt>&gt;&gt;&gt;</tt> displays “sometext” as <del>deletion</del> (with browser-defined rendering)') . "</li>\n" .
	    "</ul><p>\n" .
	    _('If the message contains three spaces in a row or a horizontal tabulator, it’s also assumed to be preformatted text unless it contains other formatting instructions, i.e. ‘&gt;&gt;&gt;’ or ‘}}}’. You can switch out of preformatted text by ‘}}}’ on a line by itself. The following formatting instructions are recognised in both preformatted and running text:') .
	    "</p><ul>\n" .
		"<li>" . _('<b>[#<i>123</i>]</b> links to the Tracker Item with the number <i>123</i> (or, if unique, Task Item)') . "</li>\n" .
		"<li>" . _('<b>[T<i>123</i>]</b> links to the Task Item with the number <i>123</i>') . "</li>\n" .
		"<li>" . _('<b>[forum:<i>123</i>]</b> links to the Forum Message with the ID <i>123</i>') . "</li>\n" .
		"<li>" . _('<b>[wiki:<i>Some Page</i>]</b> links to the Wiki Page with the name <i>Some_Page</i>') . "</li>\n" .
		"<li>" . _('Almost any valid URI by itself is converted into a hyperlink') . "</li>\n" .
	    "</ul>\n";
}
?>
  </body>
</html>
<?php

// Local Variables:
// mode: php
// c-file-style: "bsd"
// End:
