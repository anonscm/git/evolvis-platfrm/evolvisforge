<?php
/*-
 * List of all groups on the forge.
 *
 * Copyright © 2012
 *	Thorsten Glaser <t.glaser@tarent.de>
 * Copyright © 1999-2000 The SourceForge Crew
 * All rights reserved.
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

require_once('../env.inc.php');
require_once $gfcommon.'include/pre.php';
require_once $gfwww.'admin/admin_utils.php';

$tcolnames = array(
	_('Project Name (click to edit)'),
	_('Register Time'),
	_('Unix name'),
	_('Status'),
	_('Public?'),
	_('Licence'),
	_('Members#'),
	_('Template?'),
    );

$tcolids = array(
	'group_name',
	'register_time',
	'unix_group_name',
	'status',
	'is_public',
	'license_name',
	'members',
	'is_template',
    );

$group_name_search = getStringFromRequest('group_name_search');
$status = getStringFromRequest('status');
$sortorder = getStringFromRequest('sortorder');
$sortorder = in_array($sortorder, $tcolids) ? $sortorder : 'unix_group_name';
$sortrev = getIntFromRequest('sortrev');

$ra = RoleAnonymous::getInstance();
$qpa = db_construct_qpa(false, '
	SELECT group_name, register_time, unix_group_name,
	    groups.group_id, groups.is_template, status, license_name,
	    COUNT(DISTINCT(pfo_user_role.user_id)) AS members
	FROM groups
	LEFT OUTER JOIN pfo_role
		ON pfo_role.home_group_id = groups.group_id
	LEFT OUTER JOIN pfo_user_role
		ON pfo_user_role.role_id = pfo_role.role_id
	    , licenses
	WHERE
		license_id = license
    ');
if ($group_name_search) {
	$qpa = db_construct_qpa($qpa, '
		AND lower(group_name) LIKE $1
	    ', array(
		strtolower($group_name_search . '%'),
	    ));
} else if ($status) {
	$qpa = db_construct_qpa($qpa, '
		AND status = $1
	    ', array(
		$status,
	    ));
}
$qpa = db_construct_qpa($qpa, '
	GROUP BY group_name, register_time, unix_group_name,
	    groups.group_id,
	    groups.is_template, status, license_name
    ');
$res = db_query_qpa($qpa);

$rows = array();
while (($grp = db_fetch_array($res))) {
	$grp['is_public'] = $ra->hasPermission('project_read',
	    $grp['group_id']) ? 1 : 0;
	$rows[] = $grp;
}

usort($rows, function ($a, $b) {
	global $sortorder, $sortrev;

	$av = $a[$sortorder];
	$bv = $b[$sortorder];

	switch ($sortorder) {
	case "register_time":
	case "is_public":
	case "members":
	case "is_template":
		$rv = $av === $bv ? 0 : ($av > $bv ? 1 : -1);
		break;

	case "group_name":
	case "status":
	case "license_name":
		$rv = strcasecmp($av, $bv);
		break;

	case "unix_group_name":
	default:
		$rv = 0;
	}

	/* second order sort is by unix group id */
	$rv = $rv ? $rv : strcmp($a["unix_group_name"], $b["unix_group_name"]);

	return ($sortrev ? -$rv : $rv);
    });

$t = new EvolvisTable($tcolnames, array_map(function ($v) {
	global $sortorder, $sortrev;

	$rv = "/admin/grouplist.php?sortorder=" . $v;
	if ($v == $sortorder && !$sortrev) {
		$rv .= "&sortrev=1";
	}

	return ($rv);
    }, $tcolids));

foreach ($rows as $grp) {
	switch ($grp['status']) {
	case 'A':
		$status = "active";
		break;
	case 'D':
		$status = "deleted";
		break;
	case 'P':
		$status = "pending";
		break;
	default:
		$status = "";
	}

	$r = $t->tr();
	$r->td()->setraw(html_e('a', array(
		'href' => 'groupedit.php?group_id=' . $grp['group_id'],
	    ), util_html_secure($grp['group_name'])));
	$x = $r->td();
	if ($grp['register_time']) {
		$x->set(datepick_format($grp['register_time'], true));
	}
	$r->td()->set($grp['unix_group_name']);
	$r->td(array(
		'class' => $status,
	    ), -1, $grp['status']);
	$r->td()->set($grp['is_public'] ? _('Yes') : _('No'));
	$r->td()->set($grp['license_name']);
	$r->td()->set($grp['members']);
	$r->td()->set($grp['is_template'] ? _('Yes') : _('No'));
}

site_admin_header(array(
	'title' => _('Project List'),
    ));

if ($group_name_search) {
	echo html_e('p', array(
	    ), util_html_encode(sprintf(_('Projects that begin with "%s"'),
	    $group_name_search)));
}
echo $t->emit();

site_admin_footer(array());
