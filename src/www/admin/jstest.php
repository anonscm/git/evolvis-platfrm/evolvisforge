<?php
/*-
 * FusionForge ECMAscript Library test
 *
 * Copyright © 2011, 2012
 *	Thorsten “mirabilos” Glaser <t.glaser@tarent.de>
 * All rights reserved.
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *-
 * Test embedding both ECMAscript libraries we use in FusionForge at
 * the same time (jQuery in noConflice mode, plus Prototype), as well
 * as their plugins (jQuery-Tipsy, jQuery UI, Scriptaculous, Lightbox).
 */

require_once('../env.inc.php');
require_once $gfcommon.'include/pre.php';

/* test jQuery */
html_use_jquery();

/* test jQuery-Tipsy */
html_use_tooltips();

/* test jQuery-UI */
html_use_jqueryui();

/* test Prototype */
use_javascript('/js/datepicker.js');
/* and script.aculo.us and Lightbox2 */
html_use_lightbox();

/* display site header */
site_header(array('title' => 'ECMAscript Test'));

/* provide basic DOM for ECMAscript to manipulate */
?>

<h1>JavaScript Library Test Page</h1>

<h2>jQuery<span id="jquery-vsn" /></h2>
<ul id="orderedlist">
 <li>one</li>
 <li>two</li>
</ul>
<p>The above list should be manipulated using jQuery.noConflict</p>
<script type="text/javascript"><!--//--><![CDATA[//><!--
jQuery(document).ready(function() {
	jQuery("#orderedlist").find("li").each(function(i) {
		jQuery(this).append(" BAM! " + i);
	});
	jQuery("#jquery-vsn").append(" " + jQuery().jquery);
});
//--><!]]></script>

<h2>jQuery-Tipsy <small id="jquery-tipsy-vsn">(no version info available)</small></h2>
<p id="tipsytest" title="Hello, ECMA 262 World!">You should see some
 kind of fancy tooltip when hovering here. If you see the standard
 webbrowser one, jQuery-Tipsy is not working properly.</p>
<script type="text/javascript"><!--//--><![CDATA[//><!--
jQuery(function() {
	if (typeof(jQuery(window).tipsy) == 'function') {
		jQuery('#tipsytest').tipsy({
			"gravity": "s",
			"html": true,
			"delayIn": 1000,
			"delayOut": 500,
			"fade": true
		});
	} else {
		jQuery('#tipsytest').append(" In fact, it is not working, " +
		    "the jQuery-Tipsy library seems to not have been loaded.");
	}
});
//--><!]]></script>

<h2>jQuery UI<span id="jquery-ui-vsn" /></h2>
<div id="patentmyll"></div>
<p>You should see a progress bar above.</p>

<script type="text/javascript"><!--//--><![CDATA[//><!--
jQuery(function() {
	jQuery('#patentmyll').progressbar({ "value": 37 });
	jQuery("#jquery-ui-vsn").append(" " + jQuery.ui.version);
});
//--><!]]></script>

<h2>Prototype<span id="prototype-vsn" /></h2>
<p style="display:none;">You should not see this; if you do, CSS
 hiding is broken for you. (#1)</p>
<p id="protoon" style="display:none;">You should see this. If
 you don’t, Prototype failed to unhide. (#2)</p>
<p id="protooff">You should not see this. If you do, Prototype
 failed to hide. (#3)</p>
<p>You should see this. There are three paragraphs above, but
 you should <em>not</em> see all of them, only one (#2) and
 this. (#4)</p>
<script type="text/javascript"><!--//--><![CDATA[//><!--
$("protoon").show();
$("protooff").hide();
$("prototype-vsn").insert(" " + Prototype.Version);
//--><!]]></script>

<h3>DatePicker</h3>
<form action="/" method="get">
<input type="text" name="dptest" value="2011-10-31" class="date_ymd_de" />
</form>
<p>Click on the above field to see a DatePicker pop in.</p>
<script type="text/javascript"><!--//--><![CDATA[//><!--
function createTestPicker() {
	$(document.body).select('input.date_ymd_de').each(function(e) {
		new Control.DatePicker(e, {
			"locale": "ymd_de",
			"icon": "/images/office-calendar.png"
		});
	});
}
Event.observe(window, "load", createTestPicker);
//--><!]]></script>

<h3>Lightbox2 with script.aculo.us<span id="scriptaculous-vsn" /></h3>
<p>Show the <a href="/images/logo_internal_repo.png"
 rel="lightbox" title="Unser tolles Logo">Evolvis Repository logo</a>
 image in a lightbox.</p>
<script type="text/javascript"><!--//--><![CDATA[//><!--
$("scriptaculous-vsn").insert(" " + Scriptaculous.Version);
//--><!]]></script>

<?php

/* finish the XHTML and output */
site_footer(array());
