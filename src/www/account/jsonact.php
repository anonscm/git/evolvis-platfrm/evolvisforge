<?php
/*-
 * JSON export for information about the currently logged-in Forge user
 *
 * Copyright © 2012
 *	Thorsten “mirabilos” Glaser <t.glaser@tarent.de>
 * All rights reserved.
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *-
 * Short description of the module or comments or whatever
 */

require_once('../env.inc.php');
require_once $gfcommon.'include/pre.php';
require_once $gfcommon.'pm/ProjectTask.class.php';
require_once $gfcommon.'pm/ProjectGroupFactory.class.php';
require_once $gfcommon.'tracker/ArtifactTypeFactory.class.php';
require_once $gfwww.'tracker/include/ArtifactHtml.class.php';

session_require_login();
if (!session_issecure()) {
	exit_error(_('Session is not secure'), 'my');
}

if (!($u =& user_get_object(user_getid())) || !is_object($u)) {
	exit_error(_('Could Not Get User'), 'my');
} elseif ($u->isError()) {
	exit_error($u->getErrorMessage(), 'my');
}

/*XXX TODO: nur Tracker- und Taskitems, wo man selber was mit zu
	tun hatte (Submitter, Assignee, Kommentator), exportieren? */

function jsonact_add_trackers(&$ra, &$g) {
	$atf = new ArtifactTypeFactory($g);
	if (!$atf || !is_object($atf) || $atf->isError()) {
		return;
	}
	if (!($at_arr = $atf->getArtifactTypes())) {
		return;
	}
	$t = array();
	foreach ($at_arr as $at) {
		if (!$at || !is_object($at) || $at->isError()) {
			/* just skip it */
			continue;
		}
		$d = array(
			'atid' => (int)$at->getID(),
			'artifact_type_id' => (int)$at->getID(),
			'group_artifact_id' => (int)$at->getID(),
			'open_count' => (int)$at->getOpenCount(),
			'count' => (int)$at->getTotalCount(),
			'allow_anon' => !!$at->allowsAnon(),
			'name' => $at->getName(),
			'formatted_name' => $at->getFormattedName(),
			'unixname' => $at->getUnixName(),
			'description' => $at->getDescription(),
			'due_period' => (int)$at->getDuePeriod(),
			'status_timeout' => (int)$at->getStatusTimeout(),
			'custom_status_field' => (int)$at->getCustomStatusField(),
			'datatype' => (int)$at->getDataType(),
			'~items' => array(),
		    );
		$res = db_query_params('SELECT *
			FROM artifact_vw
			WHERE group_artifact_id=$1',
		    array($at->getID()));
		if ($res) while (($row = db_fetch_array($res))) {
			$ah =& artifact_get_object($row['artifact_id'], $row);
			if (!$ah || !is_object($ah) || $ah->isError()) {
				continue;
			}
			$a = array(
				'_permalink' => util_make_url('/tracker/t_follow.php/' .
				    $ah->getID()),
			    );
			foreach ($ah->data_array as $k => $v) {
				/* skip numeric fields */
				if (!preg_match('/^[a-z]/', $k)) {
					continue;
				}

				/* distinguish actions for specific fields */
				switch ($k) {
				case 'details':
				case 'summary':
					/* fix mistake of how stuff is stored in the DB */
					$v = util_unconvert_htmlspecialchars($v);
					/* fix issue with how stuff may be stored in the DB */
					$v = util_sanitise_multiline_submission($v);
					/* but export using logical newlines */
					$v = str_replace("\r\n", "\n", $v);
					/* now we’ve got something we can use */
					$a[$k] = $v;
					break;

				case 'artifact_id':
				case 'assigned_to':
				case 'close_date':
				case 'group_artifact_id':
				case 'group_id':
				case 'last_modified_date':
				case 'open_date':
				case 'priority':
				case 'status_id':
				case 'submitted_by':
					$a[$k] = (int)$v;
					break;

				default:
					$a[$k] = $v;
					break;
				}
			}

			$a['_votes'] = array_combine(array(
				'votes',
				'voters',
				'votage_percent',
			    ), $ah->getVotes());
			$a['_related_tasks'] = array();
			if ($g->usesPM()) {
				$taskcount = db_numrows($ah->getRelatedTasks());
				if ($taskcount >= 1) for ($i = 0; $i < $taskcount; ++$i) {
					$taskinfo = db_fetch_array($ah->relatedtasks, $i);
					$a['_related_tasks'][] =
					    (int)$taskinfo['project_task_id'];
				}
				sort($a['_related_tasks'], SORT_NUMERIC);
			}

			$res = $ah->getMessages();
			$c = array();
			if ($res) while (($row = db_fetch_array($res))) {
				$cu = util_ifsetor($row['user_id']) ? : 100;
				$ca = util_ifsetor($c[$cu], array());
				$ca[] = (int)util_ifsetor($row['adddate'], 0);
				$c[(int)$cu] = $ca;
			}
			$a['~comments'] = $c;

			if (util_ifsetor($a['assigned_unixname']))
				unset($a['assigned_email']);
			if (util_ifsetor($a['submitted_unixname']))
				unset($a['submitted_email']);

			$d['~items'][(int)$ah->getID()] = $a;
		}
		$t[(int)$at->getID()] = $d;
	}
	$ra['~tracker'] = $t;
}

function jsonact_add_tasks(&$ra, &$g) {
	$pgf = new ProjectGroupFactory($g);
	if (!$pgf || !is_object($pgf) || $pgf->isError()) {
		return;
	}
	if (!($pg_arr = $pgf->getProjectGroups()) || $pgf->isError()) {
		return;
	}
	$t = array();
	foreach ($pg_arr as $at) {
		if (!$at || !is_object($at) || $at->isError()) {
			/* just skip it */
			continue;
		}
		$d = array(
			'group_project_id' => (int)$at->getID(),
			'open_count' => (int)$at->getOpenCount(),
			'count' => (int)$at->getTotalCount(),
			'project_name' => $at->getName(),
			'description' => $at->getDescription(),
			'~items' => array(),
		    );
		$res = db_query_params('SELECT *
			FROM project_task_vw
			WHERE group_project_id=$1',
		    array($at->getID()));
		if ($res) while (($row = db_fetch_array($res))) {
			$pt = projecttask_get_object($row['project_task_id'], $row);
			if (!$pt || !is_object($pt) || $pt->isError()) {
				continue;
			}
			$a = array(
				'_permalink' => util_make_url('/pm/t_follow.php/' .
				    $pt->getID()),
			    );
			foreach ($pt->data_array as $k => $v) {
				/* skip numeric fields */
				if (!preg_match('/^[a-z]/', $k)) {
					continue;
				}

				/* distinguish actions for specific fields */
				switch ($k) {
				case 'details':
				case 'summary':
					/* fix mistake of how stuff is stored in the DB */
					$v = util_unconvert_htmlspecialchars($v);
					/* fix issue with how stuff may be stored in the DB */
					$v = util_sanitise_multiline_submission($v);
					/* but export using logical newlines */
					$v = str_replace("\r\n", "\n", $v);
					/* now we’ve got something we can use */
					$a[$k] = $v;
					break;

				case 'category_id':
				case 'duration':
				case 'end_date':
				case 'last_modified_date':
				case 'parent_id':
				case 'percent_complete':
				case 'priority':
				case 'project_task_id':
				case 'start_date':
				case 'status_id':
					$a[$k] = (int)$v;
					break;

				/* dropped */
				case 'group_project_id':
					break;

				/* renamed */
				case 'hours':
					$a['hours_estimated'] = (float)$v;
					break;
				case 'created_by':
					$a['submitted_by'] = (int)$v;
					break;
				case 'realname':
					$a['submitted_realname'] = $v;
					break;
				case 'user_name':
					$a['submitted_unixname'] = $v;
					break;

				default:
					$a[$k] = $v;
					break;
				}
			}
			$a['_linked_tracker_items'] = array();
			if (($res = $pt->getRelatedArtifacts()) &&
			    ($rows = db_numrows($res)) >= 1) {
				for ($i = 0; $i < $rows; ++$i) {
					$a['_linked_tracker_items'][] =
					    (int)db_result($res, $i, 'artifact_id');
				}
				sort($a['_linked_tracker_items'], SORT_NUMERIC);
			}
			$a['_depends_on'] = $pt->getDependentOn(true);
			$ass = $pt->getAssignedTo();
			sort($ass, SORT_NUMERIC);
			$a['assigned_realname'] = array();
			$a['assigned_to'] = array();
			$a['assigned_unixname'] = array();
			foreach ($ass as $au) {
				if (!($u = user_get_object($au))) {
					$a['assigned_realname'][] = '?' . $au;
					$a['assigned_unixname'][] = '?' . $au;
				} else {
					$a['assigned_realname'][] = $u->getRealName();
					$a['assigned_unixname'][] = $u->getUnixName();
				}
				$a['assigned_to'][] = (int)$au;
			}
			$a['hours_worked'] = $pt->getHoursWorked();
			$res = $pt->getMessages();
			$c = array();
			if ($res) while (($row = db_fetch_array($res))) {
				$cu = util_ifsetor($row['posted_by']) ? : 100;
				$ca = util_ifsetor($c[$cu], array());
				$ca[] = (int)util_ifsetor($row['postdate'], 0);
				$c[(int)$cu] = $ca;
			}
			$a['~comments'] = $c;

			$d['~items'][(int)$pt->getID()] = $a;
		}
		$t[(int)$at->getID()] = $d;
	}
	$ra['~tasks'] = $t;
}

$i = array(
	'user_id' => (int)$u->getID(),
	'user_name' => $u->getUnixName(),
	'email' => $u->getEmail(),
	'realname' => $u->getRealName(),
	'add_date' => (int)$u->getAddDate(),
	'timezone' => $u->getTimeZone(),
	'ccode' => $u->getCountryCode(),
	'shell' => $u->getShell(),
	'language' => (int)$u->getLanguage(),
	'language_name' => lang_id_to_language_name($u->getLanguage()),
	'language_code' => language_name_to_locale_code(lang_id_to_language_name($u->getLanguage())),
	'_forge-admin' => !!forge_check_global_perm('forge_admin'),
	'~groups' => array(),
    );

$roles = $u->getRoles();
$groups = array();
$groles = array();
foreach ($roles as $r) {
	foreach ($r->getLinkedProjects() as $p) {
		if (!isset($groups[$p->getID()])) {
			$groups[$p->getID()] = array($p, 1);
		}
		$groles[$p->getID()][$r->getID()] = $r;
	}
	if ($r instanceof RoleExplicit &&
	    ($p = $r->getHomeProject()) != NULL) {
		$groups[$p->getID()] = array($p, 2);
		$groles[$p->getID()][$r->getID()] = $r;
	}
}

foreach ($groups as $gid => $gtype) {
	$g = $gtype[0];
	$j = array('_explicit' => $gtype[1] == 2);
	if ($g->isTemplate() && !$j['_explicit']) {
		/* skip implicit template groups */
		continue;
	}

	foreach ($g->data_array as $k => $v) {
		/* skip numeric fields */
		if (!preg_match('/^[a-z]/', $k)) {
			continue;
		}

		/* skip use_* fields */
		if (preg_match('/^use_/', $k)) {
			continue;
		}

		/* distinguish actions for specific fields */
		switch ($k) {
		/* skip these */
		case 'built_from_template':
		case 'enable_anonscm':
		case 'enable_pserver':
		case 'force_docman_reindex':
		case 'is_public':
		case 'is_template':
		case 'license':
		case 'license_other':
		case 'new_doc_address':
		case 'rand_hash':
		case 'send_all_docs':
		case 'type_id':
		/* these are not really used */
		case 'scm_box':
		case 'unix_box':
			break;

		case 'group_name':
		case 'short_description':
		case 'register_purpose':
			/* fix mistake of how stuff is stored in the DB */
			$v = util_unconvert_htmlspecialchars($v);
			/* fix issue with how stuff may be stored in the DB */
			$v = util_sanitise_multiline_submission($v);
			/* but export using logical newlines */
			$v = str_replace("\r\n", "\n", $v);
			/* now we’ve got something we can use */
			$j[$k] = $v;
			break;

		case 'group_id':
		case 'register_time':
			$j[$k] = (int)$v;
			break;

		case 'http_domain':
		case 'sys_state':
			$j[$k] = $v ? : "";
			break;

		default:
			$j[$k] = $v;
			break;
		}
	}
	$j['_project-admin'] = !!forge_check_perm('project_admin', $gid);
	$j['~roles'] = array();
	foreach ($groles[$gid] as $rid => $r) {
		$j['~roles'][(int)$rid] = array(
			'role_id' => (int)$r->getID(),
			'role_name' => $r->getName(),
			'home_project_id' => $r->getHomeProject() ?
			    (int)$r->getHomeProject()->getID() : NULL,
			'home_project_name' => $r->getHomeProject() ?
			    $r->getHomeProject()->getPublicName(true) : NULL,
		    );
	}
	if ($g->usesTracker()) {
		jsonact_add_trackers($j, $g);
	}
	if ($g->usesPm()) {
		jsonact_add_tasks($j, $g);
	}
	$i['~groups'][(int)$gid] = $j;
}

if (getIntFromRequest('html')) {
	$params = array();
	$HTML->header($params);
	echo html_e('pre', array(),
	    util_html_encode(minijson_encode($i)));
	$HTML->footer($params);
	exit;
} elseif (getIntFromRequest('text')) {
	$asformat = "text/plain; charset=\"UTF-8\"";
	$jsonindent = "";
} else {
	$asformat = "application/json; charset=\"UTF-8\"";
	$jsonindent = false;
}

sysdebug_off("Content-type: " . $asformat);
echo minijson_encode($i, $jsonindent) . "\n";
exit;
