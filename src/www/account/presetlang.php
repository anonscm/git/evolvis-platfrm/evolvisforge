<?php
/*-
 * Set default language for the current and future sessions
 *
 * Copyright © 2018, 2019
 *	mirabilos <t.glaser@tarent.de>
 * All rights reserved.
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

require_once('../env.inc.php');
require_once $gfcommon.'include/pre.php';

site_header(array(
	'title' => _('Change Language'),
    ));

?>

<ul>
 <li><a href="#en"><img
  src="<?php echo util_make_url("/images/english.png"); ?>"
  title="Switch to English language texts" border="0" height="18" width="27"
  alt="English Language flag" /> English / Englisch</a>
  (<?php echo _('English'); ?>)</li>
 <li><a href="#de"><img
  src="<?php echo util_make_url("/images/deutsch.png"); ?>"
  title="Schalte auf deutsche Texte um" border="0" height="18" width="27"
  alt="German Language flag" /> German / Deutsch</a>
  (<?php echo _('German'); ?>)</li>
</ul>

<?php
 /*
  * The extra _() call in the parenthesēs above have as argument
  * precisely supported_languages.name, so “Your new language is”
  * in setlang.php can translate it to the chosen language.
  */
?>

<hr />
<h2 id="en"><img
 src="<?php echo util_make_url("/images/english.png"); ?>"
 title="English language change intro" border="0" height="18" width="27"
 alt="English Language flag" /> Language Change</h2>

<p>By submitting the form below, you can change the language used on this
 site. If you are logged in, this will change the language preference; if
 you are not logged in, the chosen language will be stored in a cookie for
 30 days; you need to agree with this cookie being stored. This cookie will
 not be used for any other purpose, specifically not for user tracking.</p>

<form action="<?php
 echo util_make_url("/account/setlang.php?force=1&amp;language_id=" .
    language_name_to_lang_id('English')); ?>" method="post">
 <fieldset>
  <legend>Switch site language to English</legend>

  <p><input id="acc_en" type="checkbox" name="acceptcookie" value="yes" />
   <label for="acc_en">I accept the storage of the language cookie.</label>
  </p>

  <p><input type="submit" /></p>
 </fieldset>
</form>

<hr />
<h2 id="de"><img
 src="<?php echo util_make_url("/images/deutsch.png"); ?>"
 title="German language change intro" border="0" height="18" width="27"
 alt="German Language flag" /> Sprachenwechsel</h2>

<p>Durch Absenden des untenstehenden Formulars wird die Sprache, die diese
 Plattform verwendet, geändert. Falls Du eingeloggt bist wird die Einstellung
 in Deinem Benutzerprofil angepaßt, andernfalls wird sie für 30 Tage in einem
 Cookie gespeichert; Du mußt dieser Speicherung zustimmen. Der Cookie wird für
 keinen anderen Zweck verwendet, insbesondere nicht zu Trackingzwecken.</p>

<form action="<?php
 echo util_make_url("/account/setlang.php?force=1&amp;language_id=" .
    language_name_to_lang_id('German')); ?>" method="post">
 <fieldset>
  <legend>Plattformsprache auf Deutsch umstellen</legend>

  <p><input id="acc_de" type="checkbox" name="acceptcookie" value="ja" />
   <label for="acc_de">Ich stimme der Speicherung des Sprachcookies zu.</label>
  </p>

  <p><input type="submit" /></p>
 </fieldset>
</form>

<?php
site_footer(array());
