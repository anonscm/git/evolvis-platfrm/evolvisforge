<?php
/*-
 * Set default language for the current and future sessions
 *
 * Copyright © 2012, 2018, 2019
 *	mirabilos <t.glaser@tarent.de>
 * All rights reserved.
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

require_once('../env.inc.php');
require_once $gfcommon.'include/pre.php';

$force = getIntFromRequest('force');
$language_id = getIntFromRequest('language_id');
$acceptcookie = getStringFromRequest('acceptcookie');
$acceptcookie = ($acceptcookie === 'ja' || $acceptcookie === 'yes');
if (getStringFromServer('REQUEST_METHOD') !== 'POST') {
	/* cannot accept explicitly in a GET request */
	$acceptcookie = false;
}

/* validate */
$language_name = lang_id_to_language_name($language_id);
if (!$language_id || !$language_name ||
    !language_name_to_lang_id($language_name)) {
	$language_name = 'English';
	$language_id = language_name_to_lang_id($language_name);
	$language_id = $language_id ? $language_id : 1;
}
$language_name = lang_id_to_language_name($language_id);
if (!$language_id || !$language_name ||
    !language_name_to_lang_id($language_name)) {
	exit_error('Cannot find a working language or English fallback');
}

/* switch to new language now already */
setup_gettext_from_langname($language_name);

/* not logged-in users get a cookie */
if ($acceptcookie) {
	/* 2592000: “30 days” in presetlang.php */
	setcookie('cookie_language_id', $language_id,
	    (time() + 2592000), '/', '', 0);
}
$cookie_language_id = $language_id;

/* logged-in users can get their preferences forcibly changed */
if ($force == 1 && session_loggedin()) {
	$res = db_query_params('UPDATE users SET language=$1 WHERE user_id=$2',
	    array(
		$language_id,
		user_getid(),
	    ));
	$force = (db_affected_rows($res) == 1) ? 2 : 1;
} else {
	$force = 0;
}

site_header(array(
	'title' => _('Change Language'),
    ));

echo '<h2>' . _('Language Updated') . "</h2>\n";

echo '<p>' . ($acceptcookie ?
    _('Your language preference has been saved in a cookie and will be remembered next time you visit the site.') :
    _('You did not agree to cookie storage; therefore, your language choice will not be remembered unless you are logged in:')) .
    "</p>\n";

switch ($force) {
case 0:
	echo '<p>' . _('Your account preferences — if you are a registered user on the site — have not been changed because you are not currently logged in.') . "</p>\n";
	break;
case 2:
	echo '<p>' . _('Your account settings have also been changed to the new language.') . "</p>\n";
	break;
case 1:
	echo '<p>' . _('Your account settings could not be changed due to an error.') . "</p>\n";
	echo "<hr />\n";
	$account_from_setlang = true;
	include $gfwww.'account/index.php';
	die;
}

echo '<p>' . sprintf(_('Your new language is #%1$d %3$s (%2$s).'),
    $language_id, $language_name, _($language_name)) . "</p>\n";

site_footer(array());
