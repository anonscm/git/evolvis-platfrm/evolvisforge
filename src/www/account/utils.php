<?php
/*-
 * www/account utils for FusionForge
 *
 * Copyright 1999-2001 (c) VA Linux Systems
 * Copyright 2010 (c) FusionForge Team
 * Copyright (C) 2010 Alain Peyrat - Alcatel-Lucent
 * Copyright © 2011, 2012
 *	Thorsten “mirabilos” Glaser <t.glaser@tarent.de>
 * All rights reserved.
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/**
 * html_get_language_popup() - Pop up box of supported languages.
 *
 * @param		string	The title of the popup box.
 * @param		string	Which element of the box is to be selected.
 * @return	string	The html select box.
 */
function html_get_language_popup ($title='language_id',$selected='xzxz') {
	$res = db_query_params('SELECT language_id, name
		FROM supported_languages ORDER BY name ASC', array());
	$keys = util_result_column_to_array($res, 0);
	$values = util_result_column_to_array($res, 1);
	for ($i = 0; $i < count($values); ++$i) {
		/* mask out all unsupported languages */
		if (!in_array($values[$i], array(
			'English',
			'German',
		    ))) {
			$values[$i] = '(' . $values[$i] . ')';
		}
	}
	return html_build_select_box_from_arrays($keys, $values, $title,
	    $selected, false);
}
