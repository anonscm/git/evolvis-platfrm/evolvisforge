<?php
require_once $gfcommon.'include/FusionForge.class.php';

return function () {
	global $HTML;
?>
<!-- whole page table -->
<table id="bd" class="width-100p100" summary="">
<tr>
<td id="bd-col1">
<?php
echo $HTML->boxTop(_('Latest News'), 'Latest_News', false, 'toplev_news');
echo news_show_latest(forge_get_config('news_group'),5,true,false,false,5);
echo $HTML->boxBottom();

echo $HTML->boxTop(_('About the Evolvis software'), 'about_the_software', false, 'toplev_about');
?>
<h3 id="title-home-page"><?php print _('Evolvis &ndash; Make it into a project!'); ?></h3>
<p>
<?php print _('evolvis stems from the latin verb “evolvere” and means “you evolve”. “Evolution” as well traces back to this word.<br />evolvis.org is a project platform for open source software / free software projects, the publicly available instance of the Evolvis platform that used to be provided by tarent solutions GmbH and is currently transitioning to a private individual as owner. Here, developers and other project participants can find many things needed for developing a free software project:<br />source code management, bug/issue tracking, discussion board (forum), task tracking, release and document management, downloads and more. (Mailing lists are, unfortunately, currently unavailable because Python has reached end of life.)'); ?>
</p>
<p>For the avoidance of doubt, let it be spelled out explicitly:
 Evolvis is an <i>open source developing and sharing platform</i>
 and thus <strong>not</strong> an <i>online content sharing service
 provider</i> per Directive 52016PC0593.</p>
<h3><?php print _('EvolvisForge helps you manage the entire development life cycle'); ?></h3>
<p>
<?php
print _('EvolvisForge has tools to help your team collaborate, like message forums and mailing lists; tools to create and control access to git. EvolvisForge automatically creates a repository and controls access to it depending on the role-based access control settings of the project.');
?>
</p>
<p><?php print _('Additional Features:'); ?></p>
<ul>
<li><?php print _('Manage File Releases.'); ?></li>
<li><?php print _('Document Management.'); ?></li>
<li><?php print _('News announcements.'); ?></li>
<li><?php print _('Issue tracking with "unlimited" numbers of categories, text fields, etc.'); ?></li>
<li><?php print _('Task management.'); ?></li>
<li><?php print _('A powerful plugin system to add new features.'); ?></li>
</ul>

<h3><?php print _("What's new in EvolvisForge"); ?></h3>
<ul>
<li><?php printf(_('Mailing lists disabled, but the <a href="%s">old mailing list archives</a> are still available.'), util_make_uri('/pipermail/')); ?></li>
<li><?php print _('Standardised JSON format access to information about users, their roles in groups, tasks and tracker items, for interfacing with other systems.'); ?></li>
<li><?php print _('Based on FusionForge 5.1.'); ?></li>
<li><?php print _('New Evolvis Theme.'); ?></li>
<li><?php print _('New UI and features for the document manager (download as zip, locking, referencing documents by URL) (Capgemini).'); ?></li>
<li><?php print _('New progress bar displaying completion state of trackers using a custom status field.'); ?></li>
<li><?php print _('Improved sorting in trackers (Alcatel-Lucent).'); ?></li>
<li><?php print _('More flexible and more powerful role-based access control system (Coclico).'); ?></li>
<li><?php print _('New unobtrusive tooltip system based on jQuery and tipsy to replace old help window (Alcatel-Lucent).'); ?></li>
<li><?php print _('scmgit plugin: Personal Git repositories for project members (AdaCore).'); ?></li>
<li><?php print _('Template projects: there can be several of them, and users registering new projects can pick which template to clone from for their new projects (Coclico).'); ?></li>
<li><?php print _('Simplified configuration system, using standard *.ini files.'); ?></li>
<li><?php print _('Reorganised, modular Apache configuration.'); ?></li>
<!-- FF 5.0 -->
<li><?php print _('Many improvements to the trackers: configurable display, workflow management, links between artifacts, better searches, and more.'); ?></li>
<li><?php print _('New extratabs plugin.'); ?></li>
</ul>

<?php
echo $HTML->boxBottom();
?>
</td>

<td id="bd-col2">
<?php
	echo show_features_boxes();
?>

</td></tr></table>

<?php
};
