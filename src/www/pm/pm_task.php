<?php
/**
 * FusionForge : Project Management Facility
 *
 * Copyright 1999/2000, Sourceforge.net Tim Perdue
 * Copyright 2002 GForge, LLC, Tim Perdue
 * Copyright 2010, FusionForge Team
 * Copyright (C) 2011 Alain Peyrat - Alcatel-Lucent
 * Copyright (C) 2011 Patrick Apel - tarent solutions GmbH
 *
 * http://fusionforge.org
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

//require_once('../env.inc.php'); //Page gets included from task.php. The files are available in task.php
//require_once $gfcommon.'include/pre.php'; //Page gets included from task.php. The files are available in task.php
require_once $gfcommon.'reporting/report_utils.php';
require_once $gfcommon.'reporting/Report.class.php';

require_once $gfcommon.'pm/ProjectController.class.php';
require_once $gfcommon.'pm/ProjectModel.class.php';

datepick_prepare();

$pageView = getStringFromRequest('func');

$related_artifact_id = getIntFromRequest('related_artifact_id');
$related_artifact_summary = getStringFromRequest('related_artifact_summary');

$headerArray = array(
	'pagename' => 'pm_modtask',
	'group_project_id' => $group_project_id
    );

if ($pageView == 'addtask') {
	$pageView = 'postaddtask';
	$headerArray['title'] = '';
} else {
	$headerArray['title'] = sprintf('[#%d] %s', $pt->getID(), $pt->getSummary());
}

pm_header($headerArray);

session_require_login();

$boolManager = false;
if (forge_check_perm ('pm', $pg->getID(), 'manager')) {
	$boolManager = true;
}

/*
 Method forge_check_perm and objects $pg and $pt provided by task.php
 */

$uri = getStringFromServer('PHP_SELF');
$showPageView = getStringFromRequest('showFunc');

if (($pageView == 'detailtask' && $boolManager == true) ||
		($pageView == 'postaddtask' && $showPageView == 'postmodtask' && $boolManager == true)) {
	$pageView = 'postmodtask';
}

$pmController = new ProjectController($uri, $pg, $pt, $pageView, $boolManager, $HTML, $sort_comments_chronologically, $related_artifact_id, $related_artifact_summary);
echo $pmController->display();

/* Implements the theme and includes the pm_controller.php that implements the view in this file */

if (forge_get_config('pm_timetracking') &&
    /* place to figure out access rights, too */
    $pageView != /*XXX Patrick, why post? */ 'postaddtask') {
/* time_tracking.php should moved in the MVC pattern if more time is available */
	echo '<div id="trackingTaskContainer">';
	echo $HTML->boxTop(_('Time tracking'), 'tracking', false, 'pm_item_ttrack');
	echo'<div class="bigTaskContainer bigTrackingTaskContainer">';
	echo '<div class="smallTaskContainer">';
	include $gfwww.'pm/templates/time_tracking.php';
	echo '</div>';
	echo'</div>';
	echo $HTML->boxBottom();
	echo '</div>';
}

pm_footer(array());
