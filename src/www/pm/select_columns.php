<?php
/*-
 * Preferences for browse_tasks
 *
 * Copyright © 2010
 *	Umer Kayani <u.kayani@tarent.de>
 * Copyright © 2010, 2011
 *	Thorsten Glaser <t.glaser@tarent.de>
 * All rights reserved.
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *-
 * This module nables the current user to select/deselect column
 * preferences for browse_task.php. Preferences are stored using
 * GFUser's class setPreference, getPreference and deletePreference
 * methods. It was conceived for Evolvis.
 */

require_once('../env.inc.php');
require_once $gfwww.'include/pre.php';

$group_id = getIntFromRequest('group_id');
$subid = getIntFromRequest($subidname);

$u =& session_get_user();
if (!$u) {
	require_once $gfcommon.'include/NotUser.class.php';
	$u = new NotUser();
}

pm_columns_header();

?>
<script type="text/javascript"><!--//--><![CDATA[//><!--
	function selectDeselectAllCheckFields(selectDeselect) {
<?php
	foreach ($pm_columns_list as $v)
		echo "		document.forms.scform." . $v[0] .
		    ".checked = selectDeselect.checked;\n";
?>
	}

	function areAllCheckFieldsSelected(){
		if (<?php
			$i = "";
			foreach ($pm_columns_list as $v) {
				echo $i . "document.forms.scform." . $v[0] .
				    ".checked";
				$i = " &&\n	    ";
			}
		?>) {
			return true;
		} else {
			return false;
		}
	}
	document.forms.scform.selectDeselect.checked = areAllCheckFieldsSelected();
//--><!]]></script>
<p><?php echo _('Select which columns to display.'); ?></p>
<form name="scform" action="<?php echo util_make_url($pm_columns_retf . "group_id=$group_id&amp;$subidname=$subid"); ?>" method="post">
<?php
	echo "<input type=\"hidden\" name=\"group_id\" value=\"$group_id\" />\n";
	echo "<input type=\"hidden\" name=\"$subidname\" value=\"$subid\" />\n";
?>
	<table>
		<tr><th><?php echo _('Columns'); ?></th><th><?php echo _('Add'); ?></th></tr>
		<tr><td colspan="2">&nbsp;</td></tr>
<?php
	foreach ($pm_columns_list as $v) {
		echo "		<tr><td>" . $v[2] . "</td><td>" .
		    '<input type="checkbox" name="' . $v[0] .
		    '" id="' . $v[0] . '" ';
		if (pm_ispref($v[0]))
			echo 'checked="checked" ';
		echo "/></td></tr>\n";
	}
?>
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr>
			<td><?php echo _('Select/Deselect All'); ?></td>
			<td><input type="checkbox" name="selectDeselect" onclick="selectDeselectAllCheckFields(this);" /></td>
		</tr><tr>
			<td><?php echo _('Reset to Defaults'); ?></td>
			<td><input type="checkbox" name="resetval" /></td>
		</tr>
	</table>
	<br />

	<input type="submit" name="SaveColumns" value="<?php echo _('Save'); ?>" />
	<input type="submit" name="cancel" value="<?php echo _('Abort'); ?>" />
</form>

<?php pm_columns_footer(); ?>
