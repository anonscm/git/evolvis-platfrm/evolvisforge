<?php
/**
 * Task UUID implementation for FusionForge
 *
 * Copyright © 2010, 2012, 2018
 *	Thorsten “mirabilos” Glaser <t.glaser@tarent.de>
 * All rights reserved.
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *-
 * Locate task information by UUID (project_task_id) and return as JSON.
 */

require_once('../env.inc.php');
require_once $gfcommon.'include/pre.php';
require_once $gfcommon.'pm/ProjectTaskSqlQueries.php';

/*-
 * We have things like protected properties. We have abstract methods.
 * We have all this stuff that your computer science teacher told you
 * you should be using. I don't care about this crap at all.
 * -- Rasmus Lerdorf
 */

$tid = getIntFromRequest('tid');
if (!$tid)
	$tid = util_path_info_last_numeric_component();
if (!$tid) {
	exit_missing_param('', array(_('Task ID')), 'pm');
}

$tinfo = getGroupProjectIdGroupId($tid);

if (!$tinfo) {
	exit_error(_('No Task with ID: ') . $tid, 'pm');
}

$asuser = getStringFromRequest('asuser');

if (getIntFromRequest('text')) {
	$asformat = "text/plain; charset=\"UTF-8\"";
	$jsonindent = "";
} else {
	$asformat = "application/json; charset=\"UTF-8\"";
	$jsonindent = false;
}

$islogin = session_loggedin();
$isadmin = forge_check_global_perm ('forge_admin');
$ishttps = session_issecure();
$ispublic = isProjectTaskInfoPublic($tid);

if (!$ishttps) {
	$islogin = false;
	$isadmin = false;
}

if ($ispublic) {
	$showall = true;
} else if ($islogin) {
	if (!$isadmin || !$asuser) {
		/* operate as ourselves */
		$asuser = session_get_user()->getUnixName();
	}

	if (isUserAndTaskinSameGroup($tid, $asuser))
		$showall = true;
	else
		$showall = false;
} else {
	$showall = false;
}

if ($showall) {
	$tinfo = getAllFromProjectTask($tid);
	require_once $gfcommon.'pm/ProjectTask.class.php';
}

$tinfo['public'] = $ispublic;
$tinfo['forge_base'] = forge_get_config ('web_host') ;
$tinfo['forge_name'] = forge_get_config ('forge_name') ;
$tinfo['_permalink'] = util_make_url('/pm/t_follow.php/' . $tid);

if ($showall && ($pt = projecttask_get_object($tid))) {
	$tinfo['_linked_tracker_items'] = array();
	if (($res = $pt->getRelatedArtifacts()) &&
	    ($rows = db_numrows($res)) >= 1) {
		for ($i = 0; $i < $rows; ++$i) {
			$tinfo['_linked_tracker_items'][] =
			    (int)db_result($res, $i, 'artifact_id');
		}
		sort($tinfo['_linked_tracker_items'], SORT_NUMERIC);
	}
	$tinfo['duration'] = (int)$pt->getDuration();
	$tinfo['parent_id'] = (int)$pt->getParentID();
	$tinfo['percent_complete'] = (int)$pt->getPercentComplete();
	$tinfo['hours_estimated'] = (float)$pt->getHours();
	$tinfo['start_date'] = (int)$pt->getStartDate();
	$tinfo['end_date'] = (int)$pt->getEndDate();
	$tinfo['last_modified_date'] = (int)$pt->getLastModifiedDate();
	$tinfo['external_id'] = $pt->getExternalID();
	$tinfo['_depends_on'] = $pt->getDependentOn(true);
	$ass = $pt->getAssignedTo();
	sort($ass, SORT_NUMERIC);
	$tinfo['assigned_realname'] = array();
	$tinfo['assigned_to'] = array();
	$tinfo['assigned_unixname'] = array();
	foreach ($ass as $a) {
		if (!($u = user_get_object($a))) {
			$tinfo['assigned_realname'][] = '?' . $a;
			$tinfo['assigned_unixname'][] = '?' . $a;
		} else {
			$tinfo['assigned_realname'][] = $u->getRealName();
			$tinfo['assigned_unixname'][] = $u->getUnixName();
		}
		$tinfo['assigned_to'][] = (int)$a;
	}
	$tinfo['hours_worked'] = $pt->getHoursWorked();
	$tinfo['details'] = $pt->getDetails();
}
foreach (array('summary', 'details') as $a) {
	if (!($v = util_ifsetor($tinfo[$a]))) {
		continue;
	}
	/* fix mistake of how stuff is stored in the DB */
	$v = util_unconvert_htmlspecialchars($v);
	/* fix issue with how stuff may be stored in the DB */
	$v = util_sanitise_multiline_submission($v);
	/* but export using logical newlines */
	$v = str_replace("\r\n", "\n", $v);
	/* now we’ve got something we can use */
	$tinfo[$a] = $v;
}

sysdebug_off("Content-type: " . $asformat);
echo minijson_encode($tinfo, $jsonindent) . "\n";
exit;
