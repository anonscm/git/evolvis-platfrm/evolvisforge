<div id="taskContainer">

<?php echo $this->showStartForm(); ?>
<?php echo $this->showHiddenBox(); ?>

<div id="detailTaskContainer">
	<?php echo $this->getHTML()->boxTop(_('Details'), 'details', false, 'pm_item_details'); ?>
		<div class="bigTaskContainer bigDetailTaskContainer">
			<div class="smallTaskContainer">
				<span class="taskHeading"><?php echo _('Percent Complete'); ?>:</span>
					<br />
				<?php echo $this->showPercentComplete(); ?>
			</div>
			<div class="smallTaskContainer">
				<span class="taskHeading"><?php echo _('Status'); ?>:</span>
					<br />
				<?php echo $this->showStatus(); ?>
			</div>
			<div class="smallTaskContainer">
				<span class="taskHeading"><?php echo _('Priority'); ?>:</span>
					<br />
				<?php echo $this->showPriority(); ?>
			</div>
			<div class="smallTaskContainer">
				<span class="taskHeading"><?php echo _('Category'); ?>:</span>
					<br />
				<?php echo $this->showCategory();?>&nbsp;<?php echo $this->showAddCategory();?>
			</div>
			<div class="smallTaskContainer">
				<span class="taskHeading"><?php echo _('Assigned to'); ?>:</span>
					<br />
				<div class="checkBoxTaskContainer">
					<div class="checkBoxInnerContainer">
						<?php echo $this->showAssignedTo(); ?>
					</div>
				</div>
			</div>
		</div>

		<div class="bigTaskContainer bigDetailTaskContainer">
			<div class="smallTaskContainer">
				<span class="taskHeading"><?php echo _('Start Date'); ?>:</span>
					<br />
				<?php echo $this->showStartDate(); ?>
			</div>
			<div class="smallTaskContainer">
				<span class="taskHeading"><?php echo _('End Date'); ?>:</span>
					<br />
				<?php echo $this->showEndDate(); ?>
			</div>
			<div class="smallTaskContainer">
				<?php echo $this->showCalendar(); ?>
			</div>
			<div class="smallTaskContainer">
				<span class="taskHeading"><?php echo _('Estimated Hours'); ?>:</span>
					<br />
				<?php echo $this->showEstimatedHours()?>
			</div>
		</div>

		<?php if(!$this->isPostAddTask()) {?>
			<div class="bigTaskContainer bigDetailTaskContainer">
				<div class="smallTaskContainer">
					<span class="taskHeading"><?php echo _('Submitted by'); ?>:</span>
						<br />
					<?php echo $this->showSubmittedBy(); ?>
				</div>
				<div class="smallTaskContainer">
					<span class="taskHeading"><a href="<?php echo $this->getPermalink(); ?>"><?php echo _('Permalink'); ?></a>:</span>
						<br />
					<span class="smaller gentium"><?php echo $this->getPermalink(); ?></span>
				</div>
				<div class="smallTaskContainer">
					<span class="taskHeading"><?php echo _('Item Detail Information'); ?>:</span>
						<br />
					<?php echo $this->showTaskDetailInfo(); ?>
				</div>
				<div class="smallTaskContainer">
					<span class="taskHeading"><?php echo _('Subproject'); ?>:</span>
						<br />
					<?php echo $this->showSubproject(); ?>
				</div>
				<div class="smallTaskContainer">
					<?php echo $this->showSubscribeLink(); ?>
				</div>
				<div class="smallTaskContainer">
					<?php echo $this->showDeleteLink(); ?>
				</div>
			</div>
		<?php }?>
		<div class="taskSubmitButtonContainer"><?php echo $this->showSubmitButton(); ?></div>
	<?php echo $this->getHTML()->boxBottom(); ?>
</div>

<div id="descriptionTaskContainer">
	<?php echo $this->getHTML()->boxTop(_('Description'), 'description', false, 'pm_item_descr'); ?>
		<div class="bigTaskContainer bigDescriptionTaskContainer">
			<div class="smallTaskContainer">
				<span class="taskHeading"><?php echo _('Task Summary') ?>:</span>
					<br />
				<?php echo $this->showSummary(); ?>
			</div>
			<div class="smallTaskContainer">
				<?php echo $this->showDetails(); ?>
			</div>
		</div>

	<?php if (!$this->isPostAddTask()) { ?>
		<div class="bigTaskContainer bigDescriptionTaskContainer">
			<div class="smallTaskContainer">
				<?php $this->renderComments(); ?>
			</div>

			<div class="smallTaskContainer">
				<?php echo $this->showAddCommentBox(); ?>
			</div>
		</div>
	<?php } ?>

		<div class="taskSubmitButtonContainer"><?php echo $this->showSubmitButton(); ?></div>
	<?php echo $this->getHTML()->boxBottom(); ?>
</div>

<div id="linkTaskContainer">
	<?php echo $this->getHTML()->boxTop(_('Link'), 'link', false, 'pm_item_link'); ?>
		<?php if(!$this->isPostAddTask()) {?>
			<div class="bigTaskContainer bigLinkTaskContainer">
				<div class="smallTaskContainer">
					<span class="taskHeading"><?php echo _('Related Commits'); ?>:</span>
						<br /><br />
						<table cellspacing="0" cellpadding="0" width="100%">
							<tr>
								<td class="taskHeading"><?php echo _('Group name'); ?></td>
								<td class="taskHeading"><?php echo _('Revision'); ?></td>
							</tr>
						<?php foreach($this->_['commits'] as $commit) { ?>
							<tr>
								<td><?php echo $commit['group_name']; ?></td>
								<td><a href="<?php echo util_html_encode($commit['href']); ?>" target="_blank" title="<?php echo _('Link to version control repository'); ?>"><?php echo $commit['revision']; ?></a></td>
							</tr>
						<?php } ?>
					</table>
				</div>
			</div>
		<?php }?>

		<div class="bigTaskContainer bigLinkTaskContainer">
			<div class="smallTaskContainer">
				<span class="taskHeading"><?php echo _('Dependent on task'); ?>:</span>
					<br /><br />
					<div class="checkBoxTaskContainer dependentContainer">
						<div class="checkBoxInnerContainer dependentContainer">
							<?php echo $this->showDependentTaskBox(); ?>
					</div>
				</div>
			</div>
		</div>

	<?php if(!$this->isPostAddTask()) {?>
		<div class="bigTaskContainer bigLinkTaskContainer">
			<div class="smallTaskContainer">
				<span class="taskHeading"><?php echo _('Related Tracker Items'); ?>:</span>
					<br /><br />
				<?php echo $this->showRelatedTrackerItems(); ?>
			</div>
		</div>
	<?php }?>

<?php if ($this->getPageView() == 'postmodtask') { ?>
		<div class="bigTaskContainer bigLinkTaskContainer">
			<div class="smallTaskContainer">
						<span class="taskHeading"><?php echo _('Copy to another Subproject'); ?>:</span>
							<br />
						<?php echo $this->showCopyTaskFunc(); ?>
			</div>
		</div>
<?php } ?>

		<div class="taskSubmitButtonContainer"><?php echo $this->showSubmitButton(); ?></div>
	<?php echo $this->getHTML()->boxBottom(); ?>
</div>

<?php if(!$this->isPostAddTask()) {?>
	<div id="protocolTaskContainer">
		<?php echo $this->getHTML()->boxTop(_('Change protocol'), 'protocol', false, 'pm_item_history'); ?>
			<div class="bigTaskContainer bigProtocolTaskContainer">
				<div class="smallTaskContainer">
					<span class="taskHeading"><?php echo _('Task Change History'); ?>:</span>
						<br /><br />
					<?php echo $this->showProtocol(); ?>
				</div>
			</div>
		<?php echo $this->getHTML()->boxBottom(); ?>
	</div>
<?php }?>

<?php echo $this->showEndForm(); ?>
</div>
