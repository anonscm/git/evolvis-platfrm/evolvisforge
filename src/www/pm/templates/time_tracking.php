<h3><?php echo _('Time tracking'); ?></h3>

<?php
$title_arr = array();
$title_arr[] = _('Day');
$title_arr[] = _('Hours worked');
$title_arr[] = _('Category');
$title_arr[] = _('User');

$report = new Report();
if ($report->isError()) {
	exit_error($report->getErrorMessage(), 'pm');
}

$project_task_id = (int)util_ifsetor($project_task_id);

echo html_eo('form', array(
	'action' => getStringFromServer('PHP_SELF') .
	    '?func=detailtask&project_task_id=' . $project_task_id .
	    '&group_id=' . $group_id .
	    '&group_project_id=' . $group_project_id,
	'method' => 'post')) . "\n" . html_e('input', array(
		'type' => 'hidden',
		'name' => 'project_task_id',
		'value' => $project_task_id,
	    )) . "\n" . html_e('input', array(
		'type' => 'hidden',
		'name' => 'submit',
		'value' => 1,
	    )) . "\n" . $HTML->listTableTop($title_arr);
$xi = 0;
	echo '<tr '.$HTML->boxGetAltRowStyle($xi++).'>
		<td>';
datepick_emit('report_dt', datepick_format(time()));
echo '</td>
		<td><input type="text" name="hours" value="" size="6" maxlength="6" /></td>
		<td>' . report_time_category_box('time_code', false) . '</td>
		<td>
			<input type="submit" name="add" value="' . _('Add') . '" />
			<input type="submit" name="cancel" value="' . _('Cancel') . '" />
		</td>
	</tr>';

//
//	Display Time Recorded for this task
//
$sql = "SELECT users.realname, rep_time_tracking.report_date, rep_time_tracking.hours, rep_time_category.category_name
	FROM users,rep_time_tracking,rep_time_category
	WHERE users.user_id=rep_time_tracking.user_id
		AND rep_time_tracking.time_code=rep_time_category.time_code
		AND rep_time_tracking.project_task_id=$project_task_id";
$res = db_query($sql);
$total_hours = 0;
for ($i = 0; $i < db_numrows($res); $i++) {
	echo '
	<tr ' . $HTML->boxGetAltRowStyle($xi++) . '>
	<td>' . datepick_format(db_result($res, $i, 'report_date')) . '</td>
	<td>' . db_result($res, $i, 'hours') . '</td>
	<td>' . db_result($res, $i, 'category_name') . '</td>
	<td>' . db_result($res, $i, 'realname') . '</td></tr>';
	$total_hours += db_result($res, $i, 'hours');
}

echo '
<tr ' . $HTML->boxGetAltRowStyle($xi++) . '>
<td><strong>' . _('Total') . ':</strong></td>
<td>' . $total_hours . '</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>';
echo $HTML->listTableBottom();
echo '</form>
<h3>' . _('Time tracking list') . '</h3>
<form action="/reporting/timeadd.php" method="post">
	<input type="hidden" name="project_task_id" value="' . $project_task_id . '" />
	<input type="hidden" name="submit" value="1" />
<p>' . report_weeks_box($report, 'week') . '
<input type="submit" name="showTasks" value="' . _('Edit') . '" />
<span style="visibility:hidden;">
' . report_day_adjust_box($report, 'days_adjust') . '
<input type="text" name="hours" value="" size="3" maxlength="3" />
' . report_time_category_box('time_code',false) . '
</span>
</p>
</form>
';
