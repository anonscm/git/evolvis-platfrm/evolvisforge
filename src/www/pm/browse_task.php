<?php
/**
 * FusionForge : Project Management Facility
 *
 * Copyright 1999-2001 (c) VA Linux Systems, Tim Perdue
 * Copyright 2002 GForge, LLC, Tim Perdue
 * Copyright 2010, FusionForge Team
 * Copyright 2010, Umer Kayani <u.kayani@tarent.de>
 * Copyright 2010 Mike Esser <m.esser@tarent.de>
 * Copyright 2010, 2011 Thorsten Glaser <t.glaser@tarent.de>
 * Copyright 2010, 2011 Patrick Apel <p.apel@tarent.de>
 * http://fusionforge.org
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

require_once $gfcommon.'include/UserManager.class.php';
require_once $gfcommon.'pm/ProjectTaskFactory.class.php';

use_javascript('/js/sortable.js');

//Preselect columns (added in session) only done on first loading of page.
require_once $gfwww.'pm/include/pm_columns.php';
$u =& session_get_user();
if (!$u) {
	require_once $gfcommon.'include/NotUser.class.php';
	$u = new NotUser();
}
pm_columns_initprefs();

//build page title to make bookmarking easier
//if a user was selected, add the user_name to the title
//same for status

$pagename = "pm_browse_custom";

$showAll = getStringFromRequest('showAll');

$_order = getStringFromRequest('_order');
$set = getStringFromRequest('set');
$_assigned_to = getIntFromRequest('_assigned_to');
$_status = getStringFromRequest('_status');
$_category_id = getIntFromRequest('_category_id');
$_view = getStringFromRequest('_view');

//showAll url parameter decides whether one subproject is shown
//or the union of subprojects (decided by All option in dropdownlist)
if ($showAll == 'showAllSubProjects') {
	//Code to show all sub-projects on the page

	$groupId = getIntFromRequest('group_id');
	if (!$groupId) {
		exit_no_group();
	}

	$gg =& group_get_object($groupId);
	if (!$gg || !is_object($gg)) {
		exit_no_group();
	} elseif ($gg->isError()) {
		exit_error('Error: ' . $gg->getErrorMessage(), 'pm');
	}

	$pgff = new ProjectGroupFactory($gg);
	if (!$pgff || !is_object($pgff)) {
		exit_error('Error: Could Not Get Factory', 'pm');
	} elseif ($pgff->isError()) {
		exit_error('Error: ' . $pgff->getErrorMessage(), 'pm');
	}


	$pgArr =& $pgff->getProjectGroups();
	if ($pgArr && $pgff->isError()) {
		exit_error('Error: ' . $pgff->getErrorMessage(), 'pm');
	}

	$pt_arr = array();

	//Loop through all subprojects and accumulate all project tasks
	for ($jj = 0; $jj < count($pgArr); $jj++ ) {
		$ptf = new ProjectTaskFactory($pgArr[$jj]);
		if (!$ptf || !is_object($ptf)) {
			exit_error('Error: Could Not Get ProjectTaskFactory', 'pm');
		} elseif ($ptf->isError()) {
			exit_error('Error: ' . $ptf->getErrorMessage(), 'pm');
		}

		$ptf->setup(0,$_order,-1,$set,$_assigned_to,$_status,$_category_id,$_view);
		if ($ptf->isError()) {
			exit_error('Error: ' . $ptf->getErrorMessage(), 'pm');
		}
		$pt_arr = array_merge((array)$pt_arr,  $ptf->getTasks(true));

		if ($ptf->isError()) {
			exit_error('Error: ' . $ptf->getErrorMessage(), 'pm');
		}
	}
} else {
	//Code to show only one sub-project on the page

	$ptf = new ProjectTaskFactory($pg);
	if (!$ptf || !is_object($ptf)) {
		exit_error('Error: Could Not Get ProjectTaskFactory', 'pm');
	} elseif ($ptf->isError()) {
		exit_error('Error: ' . $ptf->getErrorMessage(), 'pm');
	}

	$ptf->setup(0,$_order,-1,$set,$_assigned_to,$_status,$_category_id,$_view);
	if ($ptf->isError()) {
		exit_error('Error : ' . $ptf->getErrorMessage(), 'pm');
	}
	$pt_arr =& $ptf->getTasks(true);
	if ($ptf->isError()) {
		exit_error('Error: ' . $ptf->getErrorMessage(), 'pm');
	}
}

$_assigned_to=$ptf->assigned_to;
$_status=$ptf->status;
$_order=$ptf->order;
$_category_id=$ptf->category;
$_view=$ptf->view_type;

pm_header(array('title'=>_('Browse tasks'),'group_project_id'=>$group_project_id));

//	Code to get subprojects of this project
$group_id = getIntFromRequest('group_id');
if (!$group_id) {
	exit_no_group();
}

$g =& group_get_object($group_id);
if (!$g || !is_object($g)) {
	exit_no_group();
} elseif ($g->isError()) {
	exit_error('Error: ' . $g->getErrorMessage(), 'pm');
}

$pgf = new ProjectGroupFactory($g);
if (!$pgf || !is_object($pgf)) {
	exit_error('Error: ' . 'Could Not Get Factory', 'pm');
} elseif ($pgf->isError()) {
	exit_error('Error: ' . $pgf->getErrorMessage(), 'pm');
}

$pg_arr =& $pgf->getProjectGroups();
if ($pg_arr && $pgf->isError()) {
	exit_error('Error: ' . $pgf->getErrorMessage(), 'pm');
}

$subprojects_box = '<select name="select_subprojects" onchange="moveToSubproject(this.value)">';
for ($j = 0; $j < count($pg_arr); $j++) {

	if (!is_object($pg_arr[$j])) {
		//just skip it
	} elseif ($pg_arr[$j]->isError()) {
		echo $pg_arr[$j]->getErrorMessage();
	} else {
		$subprojects_box .= '<option ';
		if($group_project_id == $pg_arr[$j]->getID())
			$subprojects_box .= ' selected="selected" ';
		$subprojects_box .= 'value="'. util_make_url ('/pm/task.php?group_project_id='. $pg_arr[$j]->getID().'&amp;group_id='.$group_id.'&amp;func=browse')  .'"  >'. $pg_arr[$j]->getName();

		$subprojects_box .= '</option>';
	}
}
$subprojects_box .= '<option>All</option></select>';

/*
		creating a custom technician box which includes "any" and "unassigned"
*/
$engine = RBACEngine::getInstance () ;
$techs = $engine->getUsersByAllowedAction ('pm', $pg->getID(), 'tech') ;

$tech_id_arr = array () ;
$tech_name_arr = array () ;

foreach ($techs as $tech) {
	$tech_id_arr[] = $tech->getID() ;
	$tech_name_arr[] = $tech->getRealName() ;
}
$tech_id_arr[]='0';
$tech_name_arr[]=_('Any');

$tech_box=html_build_select_box_from_arrays ($tech_id_arr,$tech_name_arr,'_assigned_to',$_assigned_to,true,_('Unassigned'));

/*
		creating a custom category box which includes "any" and "none"
*/
$res_cat=$pg->getCategories();
$cat_id_arr=util_result_column_to_array($res_cat,0);
$cat_id_arr[]='0';  //this will be the 'any' row
$cat_name_arr=util_result_column_to_array($res_cat,1);
$cat_name_arr[]=_('Any');
$cat_box=html_build_select_box_from_arrays ($cat_id_arr,$cat_name_arr,'_category_id',$_category_id,true,'none');


/*
	Creating a custom sort box
*/
$order_title_arr=array();
$order_title_arr[]=_('Task Id');
$order_title_arr[]=_('Task Summary');
$order_title_arr[]=_('Start Date');
$order_title_arr[]=_('End Date');
$order_title_arr[]=_('Percent Complete');
$order_title_arr[]=_('Priority');

$order_col_arr=array();
$order_col_arr[]='project_task_id';
$order_col_arr[]='summary';
$order_col_arr[]='start_date';
$order_col_arr[]='end_date';
$order_col_arr[]='percent_complete';
$order_col_arr[]='priority';
$order_box=html_build_select_box_from_arrays ($order_col_arr,$order_title_arr,'_order',$_order,false);

/*
	Creating View array
*/
$view_arr=array();
$view_arr[]=_('Summary');
$view_arr[]=_('Detailed');
$order_col_arr=array();
$view_col_arr[]='summary';
$view_col_arr[]='detail';
$view_box=html_build_select_box_from_arrays ($view_col_arr,$view_arr,'_view',$_view,false);

/*
 * Get parameters for selecting customized columns in showing tasks details.
 */
$summary = pm_ispref("summary");
$start_date = pm_ispref("start_date");
$end_date = pm_ispref("end_date");
$percentComplete = pm_ispref("perComplet");
$category = pm_ispref("category");
$assignedTo = pm_ispref("assignedTo");
$priority = pm_ispref("priority");
$status = pm_ispref("status");
//$projectTaskId = pm_ispref("projectTaskId");
$creatorname = pm_ispref("creator");
$duration = pm_ispref("duration");
$parentId = pm_ispref("parentId");
$details = pm_ispref("details");
$hours = pm_ispref("hours");
$hworked = pm_ispref("hworked");
$statusId = pm_ispref("statusId");
$categoryId = pm_ispref("categoryId");
$lastModifiedDate = pm_ispref("lastModDat");
$externalId = pm_ispref("externalId");
$subProjectName = pm_ispref("subprjname");

echo '
<script type="text/javascript"><!--//--><![CDATA[//><!--
	function gup(name) {
		name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
		var regexS = "[\\?&]"+name+"=([^&#]*)";
		var regex = new RegExp( regexS );
		var results = regex.exec( window.location.href );
		if( results == null )
			return "";
		else
			return results[1];
	}

	function moveToSubproject(url) {
		if(url != "All") {
			top.location.href = url;
		} else
			top.location.href = "/pm/task.php?group_id='.$group_id.'&group_project_id='.$group_project_id.'&func=browse&showAll=showAllSubProjects";
	}
//--><!]]></script>';

/*
	Show the new pop-up boxes to select assigned to and/or status
*/
echo '	<form name="headerForm" action="'. getStringFromServer('PHP_SELF') .'?group_id='.$group_id.'&amp;group_project_id='.$group_project_id.'" method="post">
	<input type="hidden" name="set" value="custom" />
	<input type="hidden" name="_col_status" value="" />
	<input type="hidden" name="showAll" value="'. $showAll  .'" />
	<table width="10%" border="0">
	<tr>
		<td>'._('Subprojects').'<br />'.$subprojects_box.'</td>
		<td>'._('Assignee').'<br />'. $tech_box .'</td>
		<td>'._('Status').'<br />'. $pg->statusBox('_status',$_status,true, _('Any')) .'</td>
		<td>'._('Category').'<br />'. $cat_box .'</td>
		<td>'._('Sort On').'<br />'. $order_box .'</td>
		<td>'._('Detail View').'<br />'. $view_box .'</td>
        <td>&nbsp;<br /><div class="hidePartForPrinting"><input type="submit" name="submit" value="'._('Search').'" /></div></td>
	<td>&nbsp;</td>
	</tr>
	<tr>
	<td colspan="8">
		' . _("Customise display:") . '
		<input type="submit" name="selectcolumns" value="'._('Add/Remove Columns').'" />
	</td>
	</tr>
	</table>
	</form>
	<p />

	<script type="text/javascript"><!--//--><![CDATA[//><!--

	function submitHeaderForm() {
		document.headerForm.submit.click();
	}

	document.headerForm._assigned_to.onchange = submitHeaderForm;
	document.headerForm._status.onchange = submitHeaderForm;
	document.headerForm._category_id.onchange = submitHeaderForm;
	document.headerForm._order.onchange = submitHeaderForm;
	document.headerForm._view.onchange = submitHeaderForm;

if (gup("showAll") == "showAllSubProjects" || document.headerForm.showAll.value == "showAllSubProjects") {
	document.headerForm.select_subprojects.selectedIndex= document.headerForm.select_subprojects.length-1;

	var allLinks = document.links;
	for (var i=0; i<allLinks.length; i++) {
		var currentLinkPath = allLinks[i].pathname;
		if (currentLinkPath == "/pm/task.php" || currentLinkPath == "/pm/reporting/" || currentLinkPath == "/pm/admin/" ) {
			allLinks[i].onclick = function(){return false;}	//disable link
			allLinks[i].style.color="darkgray";		//make it greyed out
		}
	}
}

//--><!]]></script>

';

$rows=count($pt_arr);
if ($rows < 1) {

	echo '
		<div class="feedback">'._('No Matching Tasks found').'</div>
		<p />
		<div class="warning">'._('Add tasks using the link above').'</div>';
	echo db_error();
} else {
	printf('<p>' . _('Displaying %d results.') . "</p>\n", $rows);

	//create a new $set string to be used for next/prev button
	if ($set=='custom') {
		$set .= '&amp;_assigned_to='.$_assigned_to.'&amp;_status='.$_status;
	}

	/*
		Now display the tasks in a table with priority colors
	*/
	$IS_ADMIN = forge_check_perm ('pm', $pg->getID(), 'manager') ;

	if ($IS_ADMIN) {
		echo '
		<form name="taskList" action="'. getStringFromServer('PHP_SELF') .'?group_id='.$group_id.'&amp;group_project_id='.$pg->getID().'" method="post">
		<input type="hidden" name="func" value="massupdate" />
		<input type="hidden" name="showAll" value="'. $showAll  .'" />
	';
	}

//this array can be customized to display whichever columns you want
//it could be built by querying a table on a per-user basis as well
	$display_col=array(
		'summary'            => ($summary ? true : false),
		'start_date'         => (($start_date|| getStringFromRequest('start_date')) ? true : false),
		'end_date'           => (($end_date || getStringFromRequest('end_date')) ? true : false),
		'percent_complete'   => (($percentComplete || getStringFromRequest('percentComplete')) ? true : false),
		'category'           => ($category ? true : false),
		'assigned_to'        => ($assignedTo ? true : false),
		'priority'           => (($priority || getStringFromRequest('priority')) ? true : false),
		'status'             => ($status ? true : false),
		'project_task_id'    => false,
		'creator_name'       => ($creatorname ? true : false),
		'duration'           => ($duration ? true : false),
		'parent_id'          => ($parentId ? true : false),
		'details'            => ($details ? true : false),
		'hours'              => ($hours ? true : false),
		'hworked'            => ($hworked ? true : false),
		'status_id'          => ($statusId ? true : false),
		'category_id'        => ($categoryId ? true : false),
		'last_modified_date' => ($lastModifiedDate ? true : false),
		'external_id'        => ($externalId ? true : false),
		'sub_project_name'   => ($subProjectName ? true : false)
	);

	$title_arr=array();
	$title_arr[] = "";
	$title_arr[]=_('Task Id');
	if ($display_col['summary'])
		$title_arr[]=_('Task Summary');
	if ($display_col['start_date'])
		$title_arr[]=_('Start Date');
	if ($display_col['end_date']) {
		$title_arr[] = '';
		$title_arr[] = _('End Date');
	}
	if ($display_col['percent_complete'])
		$title_arr[]=_('Percent Complete');
	if ($display_col['category'])
		$title_arr[]=_('Category');
	if ($display_col['assigned_to'])
		$title_arr[]=_('Assigned to');
	if ($display_col['priority'])
		$title_arr[]=_('Priority');
	if ($display_col['status'])
		$title_arr[]=_('Status');
	if ($display_col['project_task_id'])
		$title_arr[]=_('Project Task Id');
	if ($display_col['creator_name'])
		$title_arr[]=_('Creator');
	if ($display_col['duration'])
		$title_arr[]=_('Duration');
	if ($display_col['parent_id'])
		$title_arr[]=_('Parent Id');
	if ($display_col['details'])
		$title_arr[]=_('Details');
	if ($display_col['hours'])
		$title_arr[]=_('Estimated Hours');
	if ($display_col['hworked'])
		$title_arr[]=_('Hours worked');
	if ($display_col['status_id'])
		$title_arr[]=_('Status Id');
	if ($display_col['category_id'])
		$title_arr[]=_('Category Id');
	if ($display_col['last_modified_date'])
		$title_arr[]=_('Last Modified Date');
	if ($display_col['external_id'])
		$title_arr[]=_('External Id');
	if ($display_col['sub_project_name'])
		$title_arr[]=_('Sub-Project');

	if($showAll == 'showAllSubProjects') {
		echo $GLOBALS['HTML']->listTableTopSortable ($title_arr);
	} else {
		echo $GLOBALS['HTML']->listTableTopSortable ($title_arr);
	}

	$now=time();

	for ($i=0; $i < $rows; $i++) {
		$task_group_project_id = $pt_arr[$i]->getProjectGroup()->getID();
		$url = getStringFromServer('PHP_SELF')."?func=detailtask&amp;project_task_id=".$pt_arr[$i]->getID()."&amp;group_id=".$group_id."&amp;group_project_id=".$task_group_project_id;
		//If All is selected for subprojects then Categorize tasks into subprojects in the table view
		if (($i == 0) || $task_group_project_id != $pt_arr[($i-1)]->getProjectGroup()->getID()) {
			if ($showAll == 'showAllSubProjects') {
				echo '<tr>
					<td>&nbsp;</td></tr><tr>
					<td colspan='. count($title_arr) .'><b><i>'.strtoupper($pt_arr[$i]->getProjectGroup()->getName()).'</i></b></td>

				</tr>';
			}
		}

		$st = $pt_arr[$i]->getStatusId();
		echo '
			<tr class=" '. (($st == 7) ?  ' copyAndClosed ' : ' priority'.$pt_arr[$i]->getPriority()).'"><td style="width:16px; background-color:#FFFFFF"><div class="hidePartForPrinting">' .
			util_make_link("/export/rssAboTask.php?tid=" .
			    $pt_arr[$i]->getID(), html_image('ic/rss.png',
			    16, 16, array('border' => '0'))
			) . "</div></td>\n" .
			'<td>'.
			($IS_ADMIN ? '<input type="checkbox"'. (($st == 7) ? ' disabled="disabled" onclick="return false;"':'') .' name="project_group_id_list[]" style="display:none;"  value="'. $task_group_project_id .'" /> ':'').
			($IS_ADMIN ? '<input type="checkbox"'. (($st == 7) ? ' disabled="disabled" onclick="return false;"':'') .' name="project_task_id_list[]" value="'. $pt_arr[$i]->getID() .'" /> ':'').
			html_e('span', array(
				'class' => ($st == 2 || $st == 3 ? 's' : false),
			    ), $pt_arr[$i]->getID()) . "</td>\n";
		if ($display_col['summary'])
			echo '<td class="separateRowsForPrinting"><a href="'.$url.'">'. util_html_secure($pt_arr[$i]->getSummary()) .'</a></td>';
		if ($display_col['start_date']) {
			$t = $pt_arr[$i]->getStartDate();
			echo html_e('td', array(
				'class' => array(
					'separateRowsForPrinting',
				    ),
				'content' => $t,
			    ), datepick_format($t, true));
		}
		if ($display_col['end_date']) {
			$t = $pt_arr[$i]->getEndDate();
			$as = html_ap();
			echo html_ao('td', array(
				'class' => array(
					'separateRowsForPrinting',
				    ),
			    ));
			if (($now > $t) &&
			    ($pt_arr[$i]->getStatusID() != 2)) {
				echo html_ao('strong') . '*';
			}
			echo html_ac($as);
			echo html_ao('td', array(
				'class' => array(
					'separateRowsForPrinting',
				    ),
				'content' => $t,
			    ));
			if (($now > $t) &&
			    ($pt_arr[$i]->getStatusID() != 2)) {
				echo html_ao('strong');
			}
			echo datepick_format($t, true);
			echo html_ac($as);
		}
		if ($display_col['percent_complete'])
			echo '<td class="separateRowsForPrinting">'. $pt_arr[$i]->getPercentComplete() .'%</td>';
		if ($display_col['category'])
			echo '<td class="separateRowsForPrinting">'. util_html_secure($pt_arr[$i]->getCategoryName()) .'</td>';
		if ($display_col['assigned_to']) {
			$xt = $pt_arr[$i]->getAssignedTo();
			echo html_e('td', array(
				'class' => array(
					'separateRowsForPrinting',
				    ),
				'content' => implode(':',
				    array_map(function($v) {
					return $v->getUnixName();
				    }, user_get_objects($xt))),
			    ), $pg->renderAssigneeList($xt));
		}
		if ($display_col['priority'])
			echo '<td class="separateRowsForPrinting">'. util_html_secure($pt_arr[$i]->getPriority()) .'</td>';
		if ($display_col['status'])
			echo '<td class="separateRowsForPrinting">'. util_html_secure($pt_arr[$i]->getStatusName()) .'</td>';
		if ($display_col['project_task_id'])
			echo '<td class="separateRowsForPrinting">'. $pt_arr[$i]->getID() .'</td>';
		if ($display_col['creator_name']) {
			if (($xu = user_get_object_by_name($pt_arr[$i]->getSubmittedUnixName())) &&
			    $xu->getID() != 100) {
				$xt = util_display_user($xu->getUnixName(),
				    $xu->getID(), $xu->getUnixName(),
				    'xs', false);
			} else {
				$xt = util_html_secure($pt_arr[$i]->getSubmittedUnixName());
			}
			echo html_e('td', array(
				'class' => array(
					'separateRowsForPrinting',
				    ),
				'style' => 'white-space:nowrap;',
				'title' => $pt_arr[$i]->getSubmittedRealName(),
				'content' => $pt_arr[$i]->getSubmittedUnixName(),
			    ), $xt);
		}
		if ($display_col['duration'])
			echo '<td class="separateRowsForPrinting">'. util_html_secure($pt_arr[$i]->getDuration()) .'</td>';
		if ($display_col['parent_id'])
			echo '<td class="separateRowsForPrinting">'. util_html_secure($pt_arr[$i]->getParentID()) .'</td>';
		if ($display_col['details'])
			echo '<td class="separateRowsForPrinting">'. util_html_secure($pt_arr[$i]->getDetails()) .'</td>';
		if ($display_col['hours'])
			echo '<td class="separateRowsForPrinting">'. util_html_secure($pt_arr[$i]->getHours()) .'</td>';
		if ($display_col['hworked']) {
			$hw = $pt_arr[$i]->getHoursWorked();
			$fw = $hw - (int)$hw;
			echo '<td class="separateRowsForPrinting">' .
			    sprintf(($fw == 0) ? "%d" :
			    (($fw == 0.5) ? "%.1f" : "%.2f"), $hw) . '</td>';
		}
		if ($display_col['status_id'])
			echo '<td class="separateRowsForPrinting">'. util_html_secure($pt_arr[$i]->getStatusID()) .'</td>';
		if ($display_col['category_id'])
			echo '<td class="separateRowsForPrinting">'. util_html_secure($pt_arr[$i]->getCategoryID()) .'</td>';
		if ($display_col['last_modified_date']) {
			$t = $pt_arr[$i]->getLastModifiedDate();
			echo html_e('td', array(
				'class' => array(
					'separateRowsForPrinting',
				    ),
				'content' => $t,
			    ), datepick_format($t, true));
		}
		if ($display_col['external_id'])
			echo '<td class="separateRowsForPrinting">'. util_html_secure($pt_arr[$i]->getExternalID()) .'</td>';
		if ($display_col['sub_project_name'])
			echo '<td class="separateRowsForPrinting">'. util_html_secure($pt_arr[$i]->getProjectGroup()->getName()) .'</td>';

		echo '
			</tr>';

		if ($_view=="detail") {
			echo '
			<tr class="priority'.$pt_arr[$i]->getPriority() .'">
				<td>&nbsp;</td><td colspan="'.(count($title_arr)-1).'">'. nl2br( $pt_arr[$i]->getDetails() ) .'</td>
			</tr>';

		}
	}

	echo $GLOBALS['HTML']->listTableBottom();

	if ($IS_ADMIN) {
		echo $HTML->boxTop(_('Mass Update'), 'Mass_Update', true, 'pm_browse_massupdate');
		/*
			creating a custom technician box which includes "No Change" and "Nobody"
		*/

		$engine = RBACEngine::getInstance () ;
		$techs = $engine->getUsersByAllowedAction ('pm', $pg->getID(), 'tech') ;

		$tech_id_arr = array () ;
		$tech_name_arr = array () ;

		foreach ($techs as $tech) {
			$tech_id_arr[] = $tech->getID() ;
			$tech_name_arr[] = $tech->getRealName() ;
		}
		$tech_id_arr[]='100.1';
		$tech_name_arr[]=_('Unassigned');

		$tech_box=html_build_select_box_from_arrays ($tech_id_arr,$tech_name_arr,'assigned_to',
		'100',true,_('No Change'));

		echo '
<script type="text/javascript"><!--//--><![CDATA[//><!--
	function checkAll(val) {
		al=document.taskList;
		len = al.elements.length;
		var i=0;
		for( i=0 ; i<len ; i++) {
			if (al.elements[i].name==\'project_task_id_list[]\' && !al.elements[i].disabled) {
				al.elements[i].checked=val;
			}
		}
	}
//--><!]]></script>

<a href="javascript:checkAll(1)">'._('Check all').'</a>
-
   <a href="javascript:checkAll(0)">'._('Clear all').'</a>

<div class="warning">' . _('<strong>Admin:</strong> If you wish to apply changes to all items selected above, use these controls to change their properties and click once on "Mass Update".') . '</div>
<table border="0">
			<tr>
			<td><strong>'._('Category').
				'</strong><br />'. $pg->categoryBox ('category_id','xzxz',true,
				_('No Change')) .'</td>
			<td><strong>'._('Priority').
				'</strong><br />';
			echo html_build_priority_select_box ('priority', '100', true);
			echo '</td>
			</tr>

			<tr>
			<td><strong>'._('Assigned to').
				'</strong><br />'. $tech_box .'</td>
			<td><strong>'._('State').
				'</strong><br />'. $pg->statusBox ('status_id','xzxz',true,_('No Change')) .'</td>
			</tr>

			<tr><td><strong>'._('Subproject').'</strong><br />
			'.$pg->groupProjectBox('new_group_project_id',$group_project_id,false).'</td>
			<td><input type="submit" name="submit" onclick="beforeSubmit();" value="'.
			_('Mass update').'" /></td></tr>

			</table>
' . $HTML->boxBottom() . '
		</form>

<script type="text/javascript"><!--//--><![CDATA[//><!--
		if (gup("showAll") == "showAllSubProjects" || document.taskList.showAll.value == "showAllSubProjects") {
			document.taskList.new_group_project_id.selectedIndex= 0;
		}

		function beforeSubmit() {
			//If project_task_id_list[n] is checked then check the corresponding element in the array project_group_id_list[0]

			var project_group_id_array = document.taskList.elements["project_group_id_list[]"];
			var project_task_id_array = document.taskList.elements["project_task_id_list[]"];
			//alert(project_task_id_array.selectedIndex);

			//If one or more checkboxes are selected for mass update
			if (atLeastOneCheckBoxIsSelected(project_task_id_array)) {
				//alert(\'At least one checkbox is selected\');
				for (c=0; c < project_task_id_array.length; c++) {
					project_group_id_array[c].checked = project_task_id_array[c].checked;
				}


			} else {
				//alert(\'No checkbox is selected\');
				//Single checkbox is selected or no checkbox is selected
				//return false;
			}
		}

		function atLeastOneCheckBoxIsSelected(chkBoxArray) {
			var returnValue = false;
			for(i=0; i<chkBoxArray.length; i++) {
				if (chkBoxArray[i].checked == true) {
					returnValue = true;
					break;
				}

			}
			return returnValue;
		}
//--><!]]></script>';
	}

	echo '<div class="hidePartForPrinting">';
	echo '<p>'._('* Denotes overdue tasks')."</p>\n";
	show_priority_colors_key();
	echo '</div>';

}

pm_footer(array());

// Local Variables:
// mode: php
// c-file-style: "bsd"
// End:

?>
