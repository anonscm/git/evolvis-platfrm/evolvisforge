<?php
/*-
 * Configurable columns for FusionForge PM/Tasks
 *
 * Copyright © 2010, 2011
 *	Thorsten Glaser <t.glaser@tarent.de>
 * Copyright © 2011
 *	Mike Esser <m.esser@tarent.de>
 * All rights reserved.
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *-
 * Helper functions
 */

$pm_columns_list = array(
	/*     ID     Default   Label  */
	array("summary", 1, _('Summary')),
	array("start_date", 1, _('Start Date')),
	array("end_date", 1, _('End Date')),
	array("perComplet", 1, _('Percent Complete')),
	array("category", 0, _('Category')),
	array("assignedTo", 1, _('Assigned To')),
	array("priority", 1, _('Priority')),
	array("status", 1, _('Status')),
//	array("projectTaskId", 0, _('Project Task Id')),
	array("creator", 0, _('Creator')),	/* uid and realname */
	array("duration", 0, _('Duration')),
	array("parentId", 0, _('Parent Id')),
	array("details", 0, _('Details')),
	array("hours", 0, _('Estimated Hours')),
	array("hworked", 0, _('Hours worked')),
	array("statusId", 0, _('Status Id')),
	array("categoryId", 0, _('Category Id')),
	array("lastModDat", 0, _('Last Modified Date')),
	array("externalId", 0, _('External Id')),
	array("subprjname", 0, _('Sub-Project')),
);

function pm_ispref($name) {
	global $u;

	return $u->getPreference("pmdisplay:" . $name);
}

function pm_prefset($name, $on) {
	global $u;

	if ($on)
		return $u->setPreference("pmdisplay:" . $name, "1");
	else
		return $u->deletePreference("pmdisplay:" . $name);
}

function pm_columns_initprefs() {
	global $pm_columns_list;

	if (pm_ispref("seen"))
		/* already chosen */
		return;

	/* set everything to default values */
	foreach ($pm_columns_list as $v)
		pm_prefset($v[0], $v[1]);

	/* don't need to repeat */
	pm_prefset("seen", 1);
}

$subidname = 'group_project_id';
$pm_columns_title = _('Select PM Display Columns');
$pm_columns_retf = "/pm/task.php?func=post_select_columns&amp;";

function pm_columns_header() {
	global $group_project_id;

	pm_header(array('title' => _('Select Columns'),
	    'group_project_id' => $group_project_id));
}

function pm_columns_footer() {
	pm_footer(array());
}
