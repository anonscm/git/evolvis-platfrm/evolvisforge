<?php
/**
 * Project Management Facility
 *
 * Copyright 1999/2000, Sourceforge.net Tim Perdue
 * Copyright 2002 GForge, LLC, Tim Perdue
 * Copyright 2010, FusionForge Team
 * Copyright (C) 2011 Alain Peyrat - Alcatel-Lucent
 * http://fusionforge.org
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

require_once $gfcommon.'pm/ProjectGroup.class.php';

function pm_header($params) {
	// XXX ogi: What to do with these?
	global $group_id,$is_pm_page,$words,$group_project_id,$HTML,$order,$pg;

	if (!forge_get_config('use_pm')) {
		exit_disabled();
	}

	//required by site_project_header
	$params['group']=$group_id;
	$params['toptab']='pm';

	//only projects can use the bug tracker, and only if they have it turned on
	$project = group_get_object($group_id);
	if (!$project || !is_object($project)) {
		exit_no_group();
	}

	if (!$project->usesPm()) {
		exit_disabled('home');
	}

	$labels = array();
	$links = array();
	$tips = array();

	$labels[] = _('View Subprojects');
	$links[]  = '/pm/?group_id='.$group_id;
	$tips[]   = _('-tooltip:pm:view');

	if ($group_project_id) {
		$labels[] = (($pg) ? $pg->getName() : '');
		$links[]  = '/pm/task.php?group_id='.$group_id.'&amp;group_project_id='.$group_project_id.'&amp;func=browse';
		$tips[]   = _('Browse the current Subproject');

		if (session_loggedin()) {
			$labels[] = _('Add Task');
			$links[]  = '/pm/task.php?group_id='.$group_id.'&amp;group_project_id='.$group_project_id.'&amp;func=addtask';
			$tips[]   = _('-tooltip:pm:new');
		}
		if ($group_project_id) {
			$gantt_width = 820;
			$gantt_height = 680;
			$gantt_url = "/pm/task.php?group_id=$group_id&amp;group_project_id=$group_project_id&amp;func=ganttpage";
			$gantt_title = _('Gantt Chart');
			$gantt_winopt = 'scrollbars=yes,resizable=yes,toolbar=no,height=' . $gantt_height . ',width=' . $gantt_width;
			$labels[] = $gantt_title;
			$links[]  = $gantt_url . '" onclick="window.open(this.href, \'' . preg_replace('/\s/' , '_' , $gantt_title)
			. '\', \'' . $gantt_winopt . '\'); return false;';
			$tips[]   = _('-tooltip:pm:gantt');
		}

		//upload/download as CSV files
//		$labels[] = _('Download as CSV');
//		$links[]  = '/pm/task.php?group_id='.$group_id.'&amp;group_project_id='.$group_project_id.'&amp;func=downloadcsv';
//		$tips[]   = _('-tooltip:pm:downloadcsv');

//		$labels[] = _('Upload CSV');
//		$links[]  = '/pm/task.php?group_id='.$group_id.'&amp;group_project_id='.$group_project_id.'&amp;func=uploadcsv';
//		$tips[]   = _('-tooltip:pm:uploadcsv');

		// Import/Export using CSV files.
		$labels[] = _('Import/Export CSV');
		$links[]  = '/pm/task.php?group_id='.$group_id.'&amp;group_project_id='.$group_project_id.'&amp;func=csv';
		$tips[]   = _('-tooltip:pm:imexportcsv');
	}

	if ($pg && is_object($pg) && forge_check_perm ('pm', $pg->getID(), 'manager')) {
		$labels[] = _('Reporting');
		$links[]  = '/pm/reporting/?group_id='.$group_id;
		$tips[]   = _('-tooltip:pm:reporting');

		$labels[] = _('Administration');
		$links[]  = '/pm/admin/?group_id='.$group_id.'&amp;group_project_id='.$group_project_id.'&amp;update_pg=1';
		$tips[]   = _('-tooltip:pm:adminthis');
	} else if (forge_check_perm ('pm_admin', $group_id)) {
		$labels[] = _('Reporting');
		$links[]  = '/pm/reporting/?group_id='.$group_id;
		$labels[] = _('Administration');
		$links[]  = '/pm/admin/?group_id='.$group_id;
		$tips[]   = _('-tooltip:pm:adminpm');
	}

	$params['submenu'] = array($labels,$links,$tips);

	site_project_header($params);

	if ($pg)
		plugin_hook ("blocks", "tasks_".$pg->getName());
}

function pm_footer($params) {
	site_project_footer($params);
}

class ProjectGroupHTML extends ProjectGroup {

	function __construct(&$Group, $group_project_id=false, $arr=false) {
		if (!parent::__construct($Group,$group_project_id,$arr)) {
			return false;
		} else {
			return true;
		}
	}

	function statusBox($name='status_id',$checked='xyxy',$show_100=true,$text_100='None') {
		return html_build_select_box($this->getStatuses(),$name,$checked,$show_100,$text_100);
	}

	function categoryBox($name='category_id',$checked='xzxz',$show_100=true,$text_100='None') {
		return html_build_select_box($this->getCategories(),$name,$checked,$show_100,$text_100);
	}

	function groupProjectBox($name='group_project_id',$checked='xzxz',$show_100=true,$text_100='None') {
		$res=db_query_params ('SELECT group_project_id,project_name
			FROM project_group_list
			WHERE group_id=$1',
			array($this->Group->getID()));
		return html_build_select_box($res,$name,$checked,$show_100,$text_100);
	}

	function percentCompleteBox($name='percent_complete',$selected=0) {
		$status_select = 'status_id';
                echo '
                        <select name="'.$name.'" onchange="switch(this.selectedIndex){
                                                        case 0: '.$status_select.'.selectedIndex = 0; break;
                                                        case 20: '.$status_select.'.selectedIndex = 4; break;
                                                        default: '.$status_select.'.selectedIndex = 3;
                        }">';
                echo '
		<option value="0">'._('Not Started'). '</option>';
		for ($i=5; $i<101; $i+=5) {
			echo '
			<option value="'.$i.'"';
			if ($i==$selected) {
				echo ' selected="selected"';
			}
			echo '>'.$i.'%</option>';
		}
		echo '
		</select>';
	}

	function renderAssigneeList($assignee_ids) {
		$techs =& user_get_objects($assignee_ids);
		$return = "";
		for ($i=0; $i<count($techs); $i++) {
			if (($xu = user_get_object_by_name($techs[$i]->getUnixName())) &&
			    $xu->getID() != 100) {
				$xt = util_display_user($xu->getUnixName(),
				    $xu->getID(), $xu->getUnixName(),
				    'xs', false);
			} else {
				$xt = $techs[$i]->getUnixName();
			}
			$return .= html_e('div', array(
				'title' => $techs[$i]->getRealName(),
				'style' => 'white-space:nowrap;',
			    ), $xt) . "\n";
		}
		return $return;
	}

}

// Local Variables:
// mode: php
// c-file-style: "bsd"
// End:
