<?php
/**
 * FusionForge : Project Management Facility
 *
 * http://fusionforge.org
 *
 * Copyright (C) 2011 Patrick Apel - tarent solutions GmbH
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or postmodtaskify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more detailtasks.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/* Loads the template and includes the data from the controller and postmodtaskel into the template */

require_once $gfcommon.'include/utils.php';
require_once $gfcommon.'include/descriptive.php';
require_once $gfwww.'include/html.php';

class ProjectView {

	private $url;

	private $pageView;

	private $projectGroupHTML;

	private $projectTaskHTML; //Extends class ProjectTask

	private $projectModel;

	private $tplDir;

	private $templateFile;

	private $boolManager = false;

	private $HTML;

	private $cSort;

	private $_;

	private $relatedArtifactId;

	private $relatedArtifactSummary;

	private function setUri($uri) {
		$this->uri = $uri;
	}

	public function getUri() {
		return $this->uri;
	}

	private function setPageView($pageView) {
		$this->pageView = $pageView;
	}

	public function getPageView() {
		return $this->pageView;
	}

	private function setProjectGroupHTML($pg) {
		$this->projectGroupHTML = $pg;
	}

	public function getProjectGroupHTML() {
		return $this->projectGroupHTML;
	}

	private function setProjectTaskHTML($pt) {
		$this->projectTaskHTML = $pt;
	}

	public function getProjectTaskHTML() {
		return $this->projectTaskHTML;
	}

	public function setTemplateFile($templateFile='detailtask_template') {
		$this->templateFile = $templateFile;
	}

	private function getTemplateFile() {
		return $this->templateFile;
	}

	private function setTemplateDir($tplDir) {
		$this->tplDir = $tplDir;
	}

	private function getTemplateDir() {
		return $this->tplDir;
	}

	private function setManager($boolManager) {
		$this->boolManager = $boolManager;
	}

	public function isManager() {
		return $this->boolManager;
	}

	private function setHTML($HTML) {
		$this->HTML = $HTML;
	}

	public function getHTML() {
		return $this->HTML;
	}

	private function setProjectModel($pmModel) {
		$this->projectModel = $pmModel;
	}

	private function getProjectModel() {
		return $this->projectModel;
	}

	private function setCSort($cSort) {
		$this->cSort = $cSort;
	}

	private function getCSort() {
		return $this->cSort;
	}

	private function setRelatedArtifactId($related_artifact_id) {
		$this->relatedArtifactId = $related_artifact_id;
	}

	private function getRelatedArtifactId() {
		return $this->relatedArtifactId;
	}

	private function setRelatedArtifactSummary($relatedArtifactSummary) {
		$this->relatedArtifactSummary = $relatedArtifactSummary;
	}

	private function getRelatedArtifactSummary() {
		return $this->relatedArtifactSummary;
	}

	function __construct($uri, $pg, $pt, $pageView, $boolManager, $HTML, $pmModel, $cSort, $related_artifact_id, $related_artifact_summary) {
		global $gfwww;

		$this->setUri($uri);
		$this->setProjectGroupHTML($pg);
		$this->setProjectTaskHTML($pt);
		$this->setPageView($pageView);

		$this->setManager($boolManager);
		$this->setHTML($HTML);
		$this->setProjectModel($pmModel);
		$this->setCSort($cSort);

		$this->setRelatedArtifactId($related_artifact_id);
		$this->setRelatedArtifactSummary($related_artifact_summary);

		$this->setTemplateDir($gfwww.'pm/templates/');
	}

	public function assign($key, $value) {
		$this->_[$key] = $value;
	}

	public function isPostAddTask() {
		switch ($this->getPageView()) {
		case 'postmodtask':
		case 'copytask':
			return false;
		case 'detailtask':
			return false;
		case 'postaddtask':
			return true;
		default: return '';
		}
	}

	public function showStartForm() {
		switch ($this->getPageView()) {
		case 'postmodtask':
		case 'copytask':
			return html_build_form_start_tag(
			    $this->getUri() . '?group_id=' .
			    $this->getProjectTaskHTML()->getProjectGroup()->getGroup()->getID() .
			    '&group_project_id=' .
			    $this->getProjectTaskHTML()->getProjectGroup()->getID() .
			    '&project_task_id=' .
			    $this->getProjectTaskHTML()->getID(),
			    'modtaskform');
		case 'detailtask':
			return '';
		case 'postaddtask':
				return html_build_form_start_tag(
				$this->getUri() . '?group_id=' .
				$this->getProjectTaskHTML()->getProjectGroup()->getGroup()->getID() .
				'&group_project_id=' .
				$this->getProjectTaskHTML()->getProjectGroup()->getID(),
				'addtaskform');
		default: return '';
		}
	}

	public function showEndForm() {
		switch ($this->getPageView()) {
		case 'postmodtask':
		case 'copytask':
			return '</form>';
		case 'detailtask':
			return '';
		case 'postaddtask':
			return '</form>';
		default: return '';
		}
	}

	public function showSubmitButton() {
		switch ($this->getPageView()) {
		case 'postmodtask':
		case 'copytask':
			return html_build_submit_button('submit', _('Submit'));
		case 'detailtask':
			return '';
		case 'postaddtask':
			return html_build_submit_button('submit', _('Submit'));
		default: return '';
		}
	}

	public function showHiddenBox() {
		switch ($this->getPageView()) {
		case 'postmodtask':
		case 'copytask':
			$nameFunc = array('func' => 'postmodtask', 'project_task_id' => $this->getProjectTaskHTML()->getID());
			break;
		case 'detailtask':
			$nameFunc = null;
			break;
		case 'postaddtask':
			$nameFunc = array('func' => 'postaddtask', 'showFunc' => 'postmodtask', 'add_artifact_id[]' => $this->getRelatedArtifactId());
			break;
		default:
			$nameFunc = null;
			break;
		}

		if (is_array($nameFunc)) {
			$hiddenBoxes = '';

			while ($value = current($nameFunc)) {
				$hiddenBoxes .= html_build_hiddenBox(key($nameFunc), $value);
				next($nameFunc);
			}
			return $hiddenBoxes;
		} else {
			return '';
		}
	}

	public function showTaskId() {
		return $this->getProjectTaskHTML()->getID();
	}

	public function showSummary() {
		switch ($this->getPageView()) {
		case 'postmodtask':
		case 'copytask':
			return emit_summary_field($this->getProjectTaskHTML(),
			    'summary', 'summary', 'summary');
		case 'detailtask':
			return emit_summary($this->getProjectTaskHTML());
		case 'postaddtask':

			$id = $this->getRelatedArtifactId();
			if(!empty($id)) {
				return html_build_textbox('summary', $this->getRelatedArtifactSummary(), 'summary', 'summary');
			} else {
				return emit_summary_field(NULL,
			    'summary', 'summary', 'summary');
			}

		default:
			return '';
		}
	}

	public function showAddCommentBox() {
		switch ($this->getPageView()) {
		case 'postmodtask':
		case 'copytask':
			$formname = 'modtaskform';
			break;
		case 'postaddtask':
			$formname = 'addtaskform';
			break;
		case 'detailtask':
		default:
			/* showSubmitButton() EAVAIL, no <form/> either */
			return '';
			/* otherwise: */
			$formname = 'adddefaultform';
			break;
		}
		return emit_comment_box($formname, 'comment');
	}

	public function showDetails() {
		switch ($this->getPageView()) {
		case 'postmodtask':
		case 'copytask':
			return html_e('div', array(
				'id' => 'edit',
				'style' => 'display:none;',
			    ), html_e('span', array(
				'class' => 'taskHeading',
			    ), _('Details') . ': ') . notepad_button('document.forms.modtaskform.details',
			    "messformat") . html_e('br') .
			    emit_details_field($this->getProjectTaskHTML(),
			    'details')) . emit_details($this->getProjectTaskHTML(),
			    _('Details'), true);
		case 'detailtask':
			return html_e('span', array(
				'class' => 'taskHeading',
			    ), _('Details') . ':') . html_e('br') .
			    emit_details($this->getProjectTaskHTML());
		case 'postaddtask':
			return html_e('span', array(
				'class' => 'taskHeading',
			    ), _('Details') . ':') . html_e('br') .
			    emit_details_field(NULL, 'details');
		default:
			return '';
		}
	}

	public function renderComments() {
		switch ($this->getPageView()) {
		case 'postmodtask':
		case 'copytask':
		case 'detailtask':
			showmess(_('Comments'), util_make_url(
			    '/pm/task.php?func=detailtask&group_id=' .
			    $this->getProjectTaskHTML()->getProjectGroup()->getGroup()->getID() .
			    '&group_project_id=' .
			    $this->getProjectTaskHTML()->getProjectGroup()->getID() .
			    '&project_task_id=' .
			    $this->getProjectTaskHTML()->getID()),
			    $this->getProjectTaskHTML(),
			    _('No comments have been written.'), true);
		case 'postaddtask':
			return '';
		default: return '';
		}
	}

	public function showPercentComplete() {
		switch ($this->getPageView()) {
		case 'postmodtask':
		case 'copytask':
			return $this->getProjectGroupHTML()->percentCompleteBox('percent_complete',$this->getProjectTaskHTML()->getPercentComplete());
		case 'detailtask':
			return $this->getProjectTaskHTML()->getPercentComplete();
		case 'postaddtask':
			return $this->getProjectGroupHTML()->percentCompleteBox();
		default: return '';
		}
	}

	public function showStatus() {
		switch ($this->getPageView()) {
		case 'postmodtask':
		case 'copytask':
			return $this->getProjectGroupHTML()->statusBox('status_id', $this->getProjectTaskHTML()->getStatusID(), false);
		case 'detailtask':
			return $this->getProjectTaskHTML()->getStatusName();
		case 'postaddtask':
			return _('Open');
		default: return '';
		}
	}

	public function showPriority() {
		switch ($this->getPageView()) {
		case 'postmodtask':
		case 'copytask':
			return build_priority_select_box('priority',$this->getProjectTaskHTML()->getPriority());
		case 'detailtask':
			return $this->getProjectTaskHTML()->getPriority();
		case 'postaddtask':
			return build_priority_select_box('priority');
		default: return '';
		}
	}

	public function showAssignedTo() {
		switch ($this->getPageView()) {
		case 'postmodtask':
		case 'copytask':
			return $this->getProjectTaskHTML()->multipleAssignedBox('assigned_to[]', 'checkbox', 'depentAssignee');
		case 'detailtask':
			return ShowResultSet($this->getProjectModel()->getAssignedTo(),'',false, false);
		case 'postaddtask':
			return $this->getProjectTaskHTML()->multipleAssignedBox('assigned_to[]', 'checkbox', 'depentAssignee');
		default: return '';
		}
	}

	public function showDependentTaskBox() {
		switch ($this->getPageView()) {
		case 'postmodtask':
		case 'copytask':
			return $this->getProjectTaskHTML()->multipleDependBox('dependent_on[]', 'checkbox', 'depentTask');
		case 'detailtask':
			return $this->getProjectTaskHTML()->showDependentTasks();
		case 'postaddtask':
			return $this->getProjectTaskHTML()->multipleDependBox('dependent_on[]', 'checkbox', 'depentTask');
		default: return '';
		}
	}

	public function showRelatedTrackerItems() {
		switch ($this->getPageView()) {
		case 'postmodtask':
		case 'copytask':
			return $this->getProjectTaskHTML()->showRelatedArtifacts();
		case 'detailtask':
			return $this->getProjectTaskHTML()->showRelatedArtifacts();
		case 'postaddtask':
			return '';
		default: return '';
		}
	}

	public function showStartDate() {
		switch ($this->getPageView()) {
		case 'postmodtask':
		case 'copytask':
			return datepick_emit('start_dt',
			    datepick_format($this->getProjectTaskHTML()->getStartDate(), true),
			    true, 'default', array(), false);
		case 'detailtask':
			return datepick_format($this->getProjectTaskHTML()->getStartDate());
		case 'postaddtask':
			return datepick_emit('start_dt',
			    datepick_format(time(), true),
			    true, 'default', array(), false);
		default: return '';
		}
	}

	public function showEndDate() {
		switch ($this->getPageView()) {
		case 'postmodtask':
		case 'copytask':
			return datepick_emit('end_dt',
			    datepick_format($this->getProjectTaskHTML()->getEndDate(), true),
			    true, 'default', array(), false);
		case 'detailtask':
			return datepick_format($this->getProjectTaskHTML()->getEndDate());
		case 'postaddtask':
			return datepick_emit('end_dt',
			    datepick_format(time(), true),
			    true, 'default', array(), false);
		default: return '';
		}
	}

	public function showCalendar() {
		switch ($this->getPageView()) {
		case 'postmodtask':
		case 'copytask':
		case 'detailtask':
			return util_make_link('/pm/calendar.php?group_id='.$this->getProjectTaskHTML()->getProjectGroup()->getGroup()->getID().'&amp;group_project_id='.$this->getProjectTaskHTML()->getProjectGroup()->getID(), _('View Calendar'), array('target' => '_blank'));
		case 'postaddtask':
			return '';
		default: return '';
		}
	}

	public function showEstimatedHours() {
		switch ($this->getPageView()) {
		case 'postmodtask':
		case 'copytask':
			return html_build_textbox('hours', $this->getProjectTaskHTML()->getHours(), 'hours', 'hours');
		case 'detailtask':
			return $this->getProjectTaskHTML()->getHours();
		case 'postaddtask':
			return html_build_textbox('hours', '1', 'hours', 'hours');;
		default:
			return '';
		}
	}

	public function showSubmittedBy() {
		$pt = $this->getProjectTaskHTML();
		if (($xu = user_get_object_by_name($pt->getSubmittedUnixName())) &&
		    $xu->getID() != 100) {
			$xt = util_display_user($xu->getUnixName(),
			    $xu->getID(), sprintf('%s (<tt>%s</tt>)',
			    $xu->getRealName(), $xu->getUnixName()),
			    'xs', false, true);
		} else {
			$xt = $pt->getSubmittedRealName() . ' (<tt>' .
			    $pt->getSubmittedUnixName() . '</tt>)';
		}
		return $xt;
	}

	public function getPermalink() {
		return util_make_url('/pm/t_follow.php/' .
		    $this->getProjectTaskHTML()->getID());
	}

	public function showTaskDetailInfo() {
		switch ($this->getPageView()) {
		case 'postmodtask':
		case 'copytask':
		case 'detailtask':
			return util_make_link('/pm/t_lookup.php?tid='.$this->getProjectTaskHTML()->getID(),'application/json') . ' / ' .
			    util_make_link('/pm/t_lookup.php?text=1&amp;tid='.$this->getProjectTaskHTML()->getID(),'text/plain');
		case 'postaddtask':
			return;
		default: return '';
		}
	}

	public function showSubproject() {
		switch ($this->getPageView()) {
		case 'postmodtask':
		case 'copytask':
			return $this->getProjectGroupHTML()->groupProjectBox('new_group_project_id', $this->getProjectTaskHTML()->getProjectGroup()->getID(), false);
		case 'detailtask':
			return $this->getProjectTaskHTML()->getProjectGroup()->getName();
		case 'postaddtask':
			return '';
		default: return '';
		}
	}

	/**
	 * Field only available for managers
	 */
	public function showCopyTaskFunc() {
		switch ($this->getPageView()) {
		case 'postmodtask':
		case 'copytask':
			return $this->getProjectGroupHTML()->groupProjectBox('copy_group_project_id', $this->getProjectTaskHTML()->getProjectGroup()->getID(), false). ' ' .
			    '<input type="submit" name="CopySubproject" value="'._('Copy').'" />';
		case 'detailtask':
			return '';
		case 'postaddtask':
			return;
		default: return '';
		}
	}

	public function showSubscribeLink() {
		switch ($this->getPageView()) {
		case 'postmodtask':
		case 'copytask':
			return util_make_link('/export/rssAboTask.php?tid='.$this->getProjectTaskHTML()->getID(), html_image('ic/rss.png',16, 16, array('border' => '0')) . '' . _('Subscribe to task'));
		case 'detailtask':
			return util_make_link('/export/rssAboTask.php?tid='.$this->getProjectTaskHTML()->getID(), html_image('ic/rss.png',16, 16, array('border' => '0')) . '' . _('Subscribe to task'));
		case 'postaddtask':
			return '';
		default: return '';
		}
	}

	public function showDeleteLink() {
		switch ($this->getPageView()) {
		case 'postmodtask':
			return util_make_link($this->getUri().'?func=deletetask&amp;group_id='.$this->getProjectTaskHTML()->getProjectGroup()->getGroup()->getID().'&amp;group_project_id='.$this->getProjectTaskHTML()->getProjectGroup()->getID().'&amp;project_task_id='.$this->getProjectTaskHTML()->getID(), html_image('ic/trash.png',16, 16, array('border' => '0')) . '' . _('Delete this task'));
		case 'detailtask':
			return util_make_link($this->getUri().'?func=deletetask&amp;group_id='.$this->getProjectTaskHTML()->getProjectGroup()->getGroup()->getID().'&amp;group_project_id='.$this->getProjectTaskHTML()->getProjectGroup()->getID().'&amp;project_task_id='.$this->getProjectTaskHTML()->getID(), html_image('ic/trash.png',16, 16, array('border' => '0')) . '' . _('Delete this task'));
		case 'postaddtask':
			return;
		default: return '';
		}
	}

	public function showCategory() {
		switch ($this->getPageView()) {
		case 'postmodtask':
			return $this->getProjectGroupHTML()->categoryBox('category_id',$this->getprojectTaskHTML()->getCategoryID());
		case 'detailtask':
			return $this->getProjectTaskHTML()->getCategoryName();
		case 'postaddtask':
			return $this->getProjectGroupHTML()->categoryBox('category_id');
		default: return '';
		}
	}

	public function showAddCategory() {
		switch ($this->getPageView()) {
		case 'postmodtask':
			return '';
		case 'detailtask':
			return '';
		case 'postaddtask':
		return util_make_link ('/pm/admin/?group_id='.$this->getProjectTaskHTML()->getProjectGroup()->getGroup()->getID().
			'&amp;add_cat=1&amp;group_project_id='.$this->getProjectTaskHTML()->getProjectGroup()->getID(),'('._('Add category').')');
		default: return '';
		}
	}

	public function showProtocol() {
		switch ($this->getPageView()) {
		case 'postmodtask':
			return $this->getProjectTaskHTML()->showHistory();
		case 'detailtask':
			return $this->getProjectTaskHTML()->showHistory();
		case 'postaddtask':
			return '';
		default: return '';
		}
	}

	public function loadTemplate() {
		$file = $this->getTemplateDir().$this->getTemplateFile(). '.php';
		if (file_exists($file)) {
			ob_start();
			include $file;
			$output = ob_get_contents();
			ob_end_clean();
			return $output;
		} else {
			exit_error('Template is missing');
		}
	}
}
