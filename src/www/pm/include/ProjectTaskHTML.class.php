<?php
/**
 * FusionForge Project Management Facility
 *
 * Copyright 1999/2000, Tim Perdue - Sourceforge
 * Copyright 2002 GForge, LLC
 * Copyright 2010, FusionForge Team
 * http://fusionforge.org
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

require_once $gfcommon.'include/FFError.class.php';
require_once $gfcommon.'pm/ProjectTask.class.php';
require_once $gfcommon.'include/EvolvisTable.class.php';

class ProjectTaskHTML extends ProjectTask {

	function __construct(&$ProjectGroup, $project_task_id=false, $arr=false) {
		return parent::__construct($ProjectGroup,$project_task_id,$arr);
	}

	function multipleDependBox($name='dependent_on[]', $control='select', $tagId='default') {
		$result=$this->getOtherTasks();
		//get the data so we can mark items as SELECTED
		$arr2 = array_keys($this->getDependentOn());

		switch ($control) {
		case 'select':
			return html_build_multiple_select_box($result, $name, $arr2);
		case 'checkbox':
			return html_build_multiple_checkbox($result, $name, $arr2, true, 'none', $tagId);
		}
	}

	function multipleAssignedBox($name='assigned_to[]', $control='select', $tagId='default') {
		$engine = RBACEngine::getInstance();
		$techs = $engine->getUsersByAllowedAction('pm', $this->ProjectGroup->getID(), 'tech');
		sortUserList($techs, 'unixname');

		$tech_id_arr = array();
		$tech_name_arr = array();

		foreach ($techs as $tech) {
			$tech_id_arr[] = $tech->getID();
			$xt = sprintf('%s (<tt>%s</tt>)',
			    $tech->getRealName(), $tech->getUnixName());
			if ($control == 'checkbox' &&
			    $tech->getID() != 100) {
				$xt = util_display_user($tech->getUnixName(),
				    $tech->getID(), $xt, 'xs', false, true);
			}
			$tech_name_arr[] = $xt;
		}

		//get the data so we can mark items as SELECTED
		$arr2 = $this->getAssignedTo();

		switch ($control) {
		case 'select':
			return html_build_multiple_select_box_from_arrays($tech_id_arr,$tech_name_arr,$name,$arr2);
		case 'checkbox':
			return html_build_multiple_checkbox_from_arrays($tech_id_arr,$tech_name_arr,$name,$arr2,true,'none',$tagId);
		}
	}


	function showDependentTasks () {

		$result = db_query_params('SELECT pt.project_task_id, pt.summary FROM project_task pt INNER JOIN project_dependencies pd ON (pt.project_task_id = pd.is_dependent_on_task_id) WHERE pd.project_task_id=$1',
		    array($this->getID()));

		$rows=db_numrows($result);

		if ($rows > 0) {

			$title_arr=array();
			$title_arr[]=_('Task Id');
			$title_arr[]=_('Task Summary');

			echo $GLOBALS['HTML']->listTableTop ($title_arr);

			for ($i=0; $i < $rows; $i++) {
				echo '
				<tr '. $GLOBALS['HTML']->boxGetAltRowStyle($i) .'>
					<td>'
					.util_make_link ('/pm/task.php?func=detailtask&project_task_id='. db_result($result, $i, 'project_task_id'). '&group_id='. $this->ProjectGroup->Group->getID() . '&group_project_id='. $this->ProjectGroup->getID(), db_result($result, $i, 'project_task_id')).'</td>
					<td>'.db_result($result, $i, 'summary').'</td></tr>';
			}

			echo $GLOBALS['HTML']->listTableBottom();

		} else {
			echo '<h3>'._('No Tasks are Dependent on This Task').'</h3>';
			echo db_error();
		}
	}

	function showRelatedArtifacts() {
		$res = $this->getRelatedArtifacts();
		$rows = db_numrows($res);
		if ($rows < 1) {
			echo html_e('h3', array(),
			    _('No Related Tracker Items Have Been Added'));
			return;
		}
		$is_admin = forge_check_perm('pm_admin', $this->ProjectGroup->Group->getID());
		$t = new EvolvisTable(array(
			_('ID'),
			_('Summary'),
			_('Tracker'),
			_('Status'),
			_('Open Date'),
		    ));
		for ($i = 0; $i < $rows; ++$i) {
			$aid = db_result($res, $i, 'artifact_id');
			$r = $t->tr();
			$r->td()->setraw(($is_admin ? html_e('input', array(
				'type' => 'checkbox',
				'name' => 'rem_artifact_id[]',
				'value' => $aid,
			    )) : "") . html_e('tt', array(), $aid));
			$r->td()->setraw(html_e('a', array(
				'href' => util_make_url('/tracker/t_follow.php/' . $aid),
			    ), util_html_secure(db_result($res, $i, 'summary'))));
			$r->td()->set(db_result($res, $i, 'name'));
			$r->td()->set(db_result($res, $i, 'status_name'));
			$r->td()->set(date(_('Y-m-d H:i'),
			    db_result($res, $i, 'open_date')));
		}
		echo $t->emit() . html_e('p', array(),
		    _('Tick an item and submit to remove the relation.')) . "\n";
	}

	function showHistory() {
		/*
			show the project_history rows that are
			relevant to this project_task_id, excluding details
		*/
		$result=$this->getHistory();
		$rows=db_numrows($result);

		if ($rows > 0) {

			$title_arr=array();
			$title_arr[]=_('Field');
			$title_arr[]=_('Old Value');
			$title_arr[]=_('Date');
			$title_arr[]=_('By');

			echo $GLOBALS['HTML']->listTableTop ($title_arr);

			for ($i=0; $i < $rows; $i++) {
				$field=db_result($result, $i, 'field_name');

				echo '
					<tr class="mod_task_field" '. $GLOBALS['HTML']->boxGetAltRowStyle($i) .'><td>'.$field.'</td><td>';

				if ($field == 'status_id') {
//tdP - convert to actual status name
					echo util_html_secure(db_result($result, $i, 'old_value'));

				} else if ($field == 'category_id') {
//tdP convert to actual category_name
					echo util_html_secure(db_result($result, $i, 'old_value'));

				} else if ($field == 'start_date') {

					echo date('Y-m-d',db_result($result, $i, 'old_value'));

				} else if ($field == 'end_date') {

					echo date('Y-m-d',db_result($result, $i, 'old_value'));

				} else {

					echo util_html_secure(db_result($result, $i, 'old_value'));

				}
				echo '</td>
					<td>'. date(_('Y-m-d H:i'),db_result($result, $i, 'mod_date')) .'</td>
					<td>'.db_result($result, $i, 'user_name').'</td></tr>';
			}

			echo $GLOBALS['HTML']->listTableBottom();

		} else {
			echo '
			<h3>'._('No Changes Have Been Made').'</h3>';
		}
	}

}

// Local Variables:
// mode: php
// c-file-style: "bsd"
// End:

?>
