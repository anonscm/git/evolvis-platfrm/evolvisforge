<?php
/*-
 * Evolvis Privacy Policy
 *
 * Copyright © 2023
 *	mirabilos <tg@mirbsd.de>
 * Copyright © 2018
 *	mirabilos <t.glaser@tarent.de>
 * All rights reserved.
 *
 * This file is part of EvolvisForge. EvolvisForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * EvolvisForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with EvolvisForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

require_once('./env.inc.php');
require_once $gfcommon.'include/pre.php';

setup_curlang();
$fn = forge_get_config('custom_path') . '/pvp_c.php';
if (file_exists($fn)) {
	$dopvp = include($fn);
}

site_header(array(
	'title' => sprintf(_('%s Privacy Policy'), forge_get_config('web_host')),
	'h1' => sprintf(_('%s Privacy Policy'), forge_get_config('forge_name')),
    ));

echo "<div class=\"widlim\">\n\n";

if (file_exists($fn)) {
	$dopvp();
} else if ($curlang == 'de') { ?>
 <p>Hier könnten Ihre Datenschutzbestimmungen stehen. Bitte ändern Sie sie in der Datei
  <tt><?php echo forge_get_config('custom_path'); ?>/pvp_c.php</tt>!</p>
<?php } else { ?>
 <p>This could be your privacy policy. Please adjust it in the file
  <tt><?php echo forge_get_config('custom_path'); ?>/pvp_c.php</tt>!</p>
<?php }

echo "\n\n</div>\n"; // .widlim

site_footer(array());
