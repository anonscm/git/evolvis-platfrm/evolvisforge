<?php
/*-
 * Online Help for FusionForge
 *
 * Copyright 1999-2001 (c) VA Linux Systems
 * Copyright 2010 (c) FusionForge Team
 * Copyright (C) 2010 Alain Peyrat - Alcatel-Lucent
 * Copyright © 2011, 2012
 *	Thorsten Glaser <t.glaser@tarent.de>
 * All rights reserved.
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *-
 * Provides online help as HTML tooltips
 */

function html_get_tooltip_extralink($element_name) {
	if ($element_name != 'ef-Resolution' &&
	    $element_name != 'ef-Severity') {
		return "";
	}

	return " (" . html_e('a', array(
		'href' => util_make_url('/include/helppage.php?kw=real' .
		    str_replace(' ', '_', $element_name)),
		'target' => '_blank',
	    ), util_html_secure(_('Mehr Info…'))) . ")";
}

function html_get_tooltip_description($element_name) {
	$t = "";
	$r = false;
	switch ($element_name) {

/****************************************************************/

case 'assigned_to':
	$t = _('The person to which a tracker item is assigned.');
	break;

case 'status_id':
	$t = _('This drop-down box represents the current status of a tracker item.');
	break;

case 'category':
	$t = _('Tracker category');
	break;

case 'group':
	$t = _('Tracker group');
	break;

case 'sort_by':
	$t = array(
		_('The Sort By option allows you to determine how the browse results are sorted.'),
		_('You can sort by ID, Priority, Summary, Open Date, Close Date, Submitter, or Assignee.  You can also have the results sorted in Ascending or Descending order.'),
	    );
	break;

case 'new_artifact_type_id':
	$t = array(
		_('The Data Type option determines the type of tracker item this is.  Since the tracker rolls into one the bug, patch, support, etc... managers you need to be able to determine which one of these an item should belong.'),
		_('This has the added benefit of enabling an admin to turn a support request into a bug.'),
	    );
	break;

case 'priority':
	$t = array(
		_('The priority option allows a user to define a tracker item priority (ranging from 1-Lowest to 5-Highest).'),
		_('This is especially helpful for bugs and support requests where a user might find a critical problem with a project.'),
	    );
	break;

case 'resolution':
	$t = _('Resolution');
	break;

case 'summary':
	$t = _('The summary text-box represents a short tracker item summary. Useful when browsing through several tracker items.');
	break;

case 'canned_response':
	$t = array(
		_('The canned response drop-down represents a list of project admin-defined canned responses to common support or bug submission.'),
		_('If you are a project admin you can click the \'(admin)\' link to define your own canned responses'),
	    );
	break;

case 'comment':
case 'description':
	$r = $element_name == 'description' ?
	    _('Enter the complete description.') :
	    _('Anyone can add here comments to give additional information, answers and solutions. Please, be as precise as possible to avoid misunderstanding. If relevant, screenshots or documents can be added as attached files.');
	$r = html_e('div', array('style' => 'margin:0.5em 0;'),
	    htmlspecialchars($r)) .
	    html_eo('div', array('align' => 'left')) .
	    html_e('b', array(), htmlspecialchars(_('Editing tips:')));
	foreach (array(
		array(
			_('http, https or ftp'),
			_('Hyperlinks'),
		    ),
		array(
			_('[#NNN]'),
			_('Tracker ID NNN'),
		    ),
		array(
			_('[TNNN]'),
			_('Task ID NNN'),
		    ),
		array(
			_('[wiki:<pagename>]'),
			_('Wiki page'),
		    ),
		array(
			_('[forum:<msg_id>]'),
			_('Forum post'),
		    ),
	    ) as $x) {
		$r .= html_e('br') .
		    html_e('b', array(), htmlspecialchars($x[0])) .
		    " " . htmlspecialchars($x[1]);
	}
	$r .= '</div>' . html_e('div', array('style' => 'margin:0.5em 0;'),
	    htmlspecialchars(_('Detailed formatting help is available in the notepad form.')));
	break;

case 'attach_file':
	$t = _('When you wish to attach a file to a tracker item you must check this checkbox before submitting changes.');
	break;

case 'monitor':
	$r = sprintf('<div style="margin:0.5em 0;">%s</div><strong>%s</strong> %s',
	    htmlspecialchars(_('You can monitor or un-monitor this item by clicking the "Monitor" button.')),
	    htmlspecialchars(_('Note!')),
	    htmlspecialchars(_('This will send you additional eMail. If you add comments to this item, or submitted, or are assigned this item, you will also get emails for those reasons as well!')));
	break;

case 'vote':
	$t = _('You can cast your vote for a Tracker Item to aid Project Management to decide which features to prioritise, and retract votes at any time. Please use this functionality sparingly, as it loses its meaning if you vote on *every* item.');
	break;

case 'votes':
	$t = _('This metric displays the number of people who can *currently* vote for features in this tracker, and how many of them did so. (This means historic votes of people no longer allowed to vote, while not lost, do not play into the numbers displayed.)');
	break;

case 'ef-Browser':
	$t = _('Browser, in dem der Fehler aufgetaucht ist (mit Version).');
	break;

case 'ef-Component':
	$t = _('Einzelne Komponenten des Projekts eintragen.');
	break;

case 'ef-Hardware':
	$t = _('Hardware, mit der getestet wurde, als der Bug auftrat.');
	break;

case 'ef-Operating System':
	$t = _('Betriebssystem, in dem der Fehler aufgetreten ist.');
	break;

case 'ef-Product':
	$t = _('Projektname');
	break;

case 'ef-URL':
	$t = _('Bei Browseranwendungen die URL, auf der der Fehler auftrat.');
	break;

case 'ef-Version Fixed':
	$t = _('Versionen des Projekts (wann ist der Bug behoben)');
	break;

case 'ef-Version Found':
	$t = _('Version des Projekts, in der der Bug aufgetreten ist.');
	break;

case 'ef-Resolution':
	$t = _('Meldungsstatus eines Bugeintrags; siehe „Mehr Info…“ für Prozeßspezifikation.');
	break;

case 'realef-Resolution':
	$r = '<h1>Meldungsstatus eines Bugeintrags</h1>' . "\n" .
	'<h2>Lebenszyklus einer Meldung</h2>' . "\n" .
	'<div style="margin:0.5em 0;">Während des Testlaufs kümmert sich der projektbeteiligte Prüfer um ALLE Bugs in seinem Projekt (auch, wenn sie von einem anderen Prüfer erstellt wurden). Das heißt, daß ein Bugeintrag nicht autoren-, sondern projektbezogen ist und somit gewährleistet wird, daß jeder Bugeintrag weiterbearbeitet wird. Im Lebenszyklus eines Bugs wird sichergestellt, daß die Bugeinträge auch vom entsprechenden Entwickler angenommen oder gegebenenfalls weitergeleitet werden. Spätestens eine Woche nach Einstellen des Bugeintrags wird dieser zur Bearbeitung angenommen. Ist dies nicht der Fall, wird zunächst die Resolution auf "Awaiting Response" gesetzt. Erfolgt noch immer keine Reaktion, wird zeitnah persönlich bei der zugeteilten Person nachgehakt. Darüber hinaus wird der Verlauf des Bugeintrags im Feld "Resolution" wie in der unten stehenden Tabelle beschrieben mitverfolgt.</div>' . "\n" .
	'<div style="margin:0.5em 0;">Konkrete Zeitläufe können abweichend im einzelnen Projekt festgelegt sein. In diesem Falle ist dies im Projekthandbuch oder, falls nicht in diesem integriert, im QM Handbuch vermerkt.</div>' . "\n" .
	'<hr />' . "\n" .
	'<h2>Meldungsstatus (Resolution)</h2>' . "\n" .
	'<div style="margin:0.5em 0;">Bei jeder Änderung zu einem Bugeintrag wird sichergestellt, daß die Angaben korrekt sind. Bevor z.B. ein Bugeintrag wiedereröffnet oder verifiziert wird, muß sichergestellt sein, daß beim Test der gleiche Pfad eingeschlagen wurde oder die gleichen Eingaben getätigt wurden.</div>' . "\n" .
	'<table cellspacing="0" cellpadding="6" border="1">' . "\n" .
	'<tr>' . "\n" .
	'	<th>aktuelle Resolution</th>' . "\n" .
	'	<th>aktueller Status</th>' . "\n" .
	'	<th>Ausgangssituation /<br />handelnde Ressource</th>' . "\n" .
	'	<th>Handlung / Ergebnis</th>' . "\n" .
	'	<th>verantwortlich</th>' . "\n" .
	'	<th>neue Resolution</th>' . "\n" .
	'	<th>neuer Status</th>' . "\n" .
	'</tr>' . "\n" .
	'<tr>' . "\n" .
	'	<td>Accepted As Bug</td>' . "\n" .
	'	<td>Open</td>' . "\n" .
	'	<td>Bug war reproduzierbar und wurde akzeptiert (SWE, PL)</td>' . "\n" .
	'	<td>Bugfixing → Bug wurde gefixt</td>' . "\n" .
	'	<td>SWE</td>' . "\n" .
	'	<td>Fixed</td>' . "\n" .
	'	<td>Open</td>' . "\n" .
	'</tr>' . "\n" .
	'<tr>' . "\n" .
	'	<td>Accepted As Bug</td>' . "\n" .
	'	<td>Open</td>' . "\n" .
	'	<td>Bug war reproduzierbar und wurde akzeptiert (SWE, PL)</td>' . "\n" .
	'	<td>Bugfixing → Bug wird nicht gefixt / kann nicht gefixt werden</td>' . "\n" .
	'	<td>PL, SWE</td>' . "\n" .
	'	<td>Won’t Fix</td>' . "\n" .
	'	<td>Open</td>' . "\n" .
	'</tr>' . "\n" .
	'<tr>' . "\n" .
	'	<td>Awaiting Response</td>' . "\n" .
	'	<td>Open</td>' . "\n" .
	'	<td>Eine Rückfrage an den Zugewiesenen wurde gestellt</td>' . "\n" .
	'	<td>Beantworten der Rückfrage; ggfls. Zuweisung an den Fragenden</td>' . "\n" .
	'	<td>Zugewiesener</td>' . "\n" .
	'	<td>Awaiting Response</td>' . "\n" .
	'	<td>Open</td>' . "\n" .
	'</tr>' . "\n" .
	'<tr>' . "\n" .
	'	<td>Duplicate</td>' . "\n" .
	'	<td>Open</td>' . "\n" .
	'	<td>Bugeintrag wurde als Duplikat zu einem anderen Bugeintrag erkannt (SWE, PL, QS)</td>' . "\n" .
	'	<td>Verweis auf redundanten Bugeintrag vorhanden? → Bugeintrag schließen</td>' . "\n" .
	'	<td>QS</td>' . "\n" .
	'	<td>Duplicate</td>' . "\n" .
	'	<td>Closed <a href="#fn1">(*)</a></td>' . "\n" .
	'</tr>' . "\n" .
	'<tr>' . "\n" .
	'	<td>Duplicate</td>' . "\n" .
	'	<td>Open</td>' . "\n" .
	'	<td>Bugeintrag wurde als Duplikat zu einem anderen Bugeintrag erkannt (SWE, PL, QS)</td>' . "\n" .
	'	<td>Verweis auf redundanten Bugeintrag nicht vorhanden? → redundanten Bugeintrag suchen → Duplikat gefunden? → Verweis eintragen, Bugeintrag schließen</td>' . "\n" .
	'	<td>QS</td>' . "\n" .
	'	<td>Duplicate</td>' . "\n" .
	'	<td>Closed <a href="#fn1">(*)</a></td>' . "\n" .
	'</tr>' . "\n" .
	'<tr>' . "\n" .
	'	<td>Duplicate</td>' . "\n" .
	'	<td>Open</td>' . "\n" .
	'	<td>Bugeintrag wurde als Duplikat zu einem anderen Bugeintrag erkannt (SWE, PL, QS)</td>' . "\n" .
	'	<td>Verweis auf redundanten Bugeintrag nicht vorhanden? → redundanten Bugeintrag suchen → Kein Duplikat gefunden? → Rückfrage an entspr. Person</td>' . "\n" .
	'	<td>QS</td>' . "\n" .
	'	<td>Awaiting Response</td>' . "\n" .
	'	<td>Open</td>' . "\n" .
	'</tr>' . "\n" .
	'<tr>' . "\n" .
	'	<td>Fixed</td>' . "\n" .
	'	<td>Open</td>' . "\n" .
	'	<td>Bug wurde gefixt (SWE)</td>' . "\n" .
	'	<td>Nach-Test des Bugs → Bug tritt nicht (mehr) auf. → Bugeintrag schließen</td>' . "\n" .
	'	<td>QS</td>' . "\n" .
	'	<td>Verified</td>' . "\n" .
	'	<td>Closed <a href="#fn1">(*)</a></td>' . "\n" .
	'</tr>' . "\n" .
	'<tr>' . "\n" .
	'	<td>Fixed</td>' . "\n" .
	'	<td>Open</td>' . "\n" .
	'	<td>Bug wurde gefixt (SWE)</td>' . "\n" .
	'	<td>Nach-Test des Bugs → Bug tritt nicht (mehr) auf. <a href="#fn2">(**)</a></td>' . "\n" .
	'	<td>SWE</td>' . "\n" .
	'	<td>Verified</td>' . "\n" .
	'	<td>Open</td>' . "\n" .
	'</tr>' . "\n" .
	'<tr>' . "\n" .
	'	<td>Fixed</td>' . "\n" .
	'	<td>Open</td>' . "\n" .
	'	<td>Bug wurde gefixt (SWE)</td>' . "\n" .
	'	<td>Nach-Test des Bugs → Bug tritt weiterhin auf</td>' . "\n" .
	'	<td>QS, SWE, PL</td>' . "\n" .
	'	<td>None / New</td>' . "\n" .
	'	<td>Open</td>' . "\n" .
	'</tr>' . "\n" .
	'<tr>' . "\n" .
	'	<td>Invalid</td>' . "\n" .
	'	<td>Open</td>' . "\n" .
	'	<td>Bugeintrag ist ungültig (PL, SWE)</td>' . "\n" .
	'	<td>Abgleich mit Prüfspezifikation → Eintrag wirklich ungültig. → Bugeintrag schließen</td>' . "\n" .
	'	<td>QS</td>' . "\n" .
	'	<td>Invalid</td>' . "\n" .
	'	<td>Closed <a href="#fn1">(*)</a></td>' . "\n" .
	'</tr>' . "\n" .
	'<tr>' . "\n" .
	'	<td>Invalid</td>' . "\n" .
	'	<td>Open</td>' . "\n" .
	'	<td>Bugeintrag ist ungültig (PL, SWE)</td>' . "\n" .
	'	<td>Abgleich mit Prüfspezifikation → Eintrag nicht ungültig → Rückfrage an entspr. Person</td>' . "\n" .
	'	<td>QS</td>' . "\n" .
	'	<td>Awaiting Response</td>' . "\n" .
	'	<td>Open</td>' . "\n" .
	'</tr>' . "\n" .
	'<tr>' . "\n" .
	'	<td>None / New</td>' . "\n" .
	'	<td>Open</td>' . "\n" .
	'	<td>Bugeintrag wurde neu angelegt oder muss neu bewertet werden</td>' . "\n" .
	'	<td>Bewerten des Bugeintrags durch SWE oder PL → Bug reproduzierbar und akzeptiert</td>' . "\n" .
	'	<td>SWE</td>' . "\n" .
	'	<td>Accepted As Bug</td>' . "\n" .
	'	<td>Open</td>' . "\n" .
	'</tr>' . "\n" .
	'<tr>' . "\n" .
	'	<td>None / New</td>' . "\n" .
	'	<td>Open</td>' . "\n" .
	'	<td>Bugeintrag wurde neu angelegt oder muss neu bewertet werden</td>' . "\n" .
	'	<td>Bewerten des Bugeintrags. → Bug nicht reproduzierbar</td>' . "\n" .
	'	<td>SWE, PL</td>' . "\n" .
	'	<td>Works For Me</td>' . "\n" .
	'	<td>Open</td>' . "\n" .
	'</tr>' . "\n" .
	'<tr>' . "\n" .
	'	<td>None / New</td>' . "\n" .
	'	<td>Open</td>' . "\n" .
	'	<td>Bugeintrag wurde neu angelegt oder muss neu bewertet werden</td>' . "\n" .
	'	<td>Bewerten des Bugeintrags. → Bug wurde bereits in anderem Eintrag beschrieben. (Verweis auf redundanten Eintrag einfügen.)</td>' . "\n" .
	'	<td>SWE, PL, QS</td>' . "\n" .
	'	<td>Duplicate</td>' . "\n" .
	'	<td>Open</td>' . "\n" .
	'</tr>' . "\n" .
	'<tr>' . "\n" .
	'	<td>None / New</td>' . "\n" .
	'	<td>Open</td>' . "\n" .
	'	<td>Bugeintrag wurde neu angelegt oder muss neu bewertet werden</td>' . "\n" .
	'	<td>Bewerten des Bugeintrags. → Das beschriebene Fehlverhalten ist kein Bug, sondern das spezifizierte SOLL-Verhalten ist nicht aktuell</td>' . "\n" .
	'	<td>PL, SWE</td>' . "\n" .
	'	<td>Spec Out Of Date</td>' . "\n" .
	'	<td>Open</td>' . "\n" .
	'</tr>' . "\n" .
	'<tr>' . "\n" .
	'	<td>None / New</td>' . "\n" .
	'	<td>Open</td>' . "\n" .
	'	<td>Bugeintrag wurde neu angelegt oder muss neu bewertet werden</td>' . "\n" .
	'	<td>Bewerten des Bugeintrags. → Das beschriebene Fehlverhalten ist kein Bug, sondern entspricht dem spezifizierten Verhalten. Bugeintrag ist ungültig</td>' . "\n" .
	'	<td>PL, SWE</td>' . "\n" .
	'	<td>Invalid</td>' . "\n" .
	'	<td>Open</td>' . "\n" .
	'</tr>' . "\n" .
	'<tr>' . "\n" .
	'	<td>None / New</td>' . "\n" .
	'	<td>Open</td>' . "\n" .
	'	<td>Bugeintrag wurde neu angelegt oder muss neu bewertet werden</td>' . "\n" .
	'	<td>Bewerten des Bugeintrags. → Zu dem Bugeintrag gibt es Rückfragen</td>' . "\n" .
	'	<td>SWE, PL, QS</td>' . "\n" .
	'	<td>Awaiting Response</td>' . "\n" .
	'	<td>Open</td>' . "\n" .
	'</tr>' . "\n" .
	'<tr>' . "\n" .
	'	<td>Spec Out Of Date</td>' . "\n" .
	'	<td>Open</td>' . "\n" .
	'	<td>Das beschriebene Fehlverhalten ist kein Bug, sondern das spezifizierte SOLL-Verhalten in der Prüfspezifikation ist nicht aktuell. Änderungen wurden noch nicht vorgenommen</td>' . "\n" .
	'	<td>Bugeintrag ggfls. an den Prüfspezifikations-Verantwortlichen zuweisen</td>' . "\n" .
	'	<td>QS</td>' . "\n" .
	'	<td>Spec Out Of Date</td>' . "\n" .
	'	<td>Open</td>' . "\n" .
	'</tr>' . "\n" .
	'<tr>' . "\n" .
	'	<td>Spec Out Of Date</td>' . "\n" .
	'	<td>Open</td>' . "\n" .
	'	<td>Das beschriebene Fehlverhalten ist kein Bug, sondern das spezifizierte SOLL-Verhalten in der Spezifikation oder Funktionsreferenz ist nicht aktuell. Änderungen wurden noch nicht vorgenommen</td>' . "\n" .
	'	<td>Bugeintrag ggfls. an den fachlichen Projektleiter zuweisen</td>' . "\n" .
	'	<td>QS</td>' . "\n" .
	'	<td>Spec Out Of Date</td>' . "\n" .
	'	<td>Open</td>' . "\n" .
	'</tr>' . "\n" .
	'<tr>' . "\n" .
	'	<td>Spec Out Of Date</td>' . "\n" .
	'	<td>Open</td>' . "\n" .
	'	<td>Das beschriebene Fehlverhalten ist kein Bug, sondern das spezifizierte SOLL-Verhalten ist nicht aktuell. Anpassungen oder Korrekturen wurden vorgenommen</td>' . "\n" .
	'	<td>Änderungen wurden korrekt vorgenommen? → Bugeintrag schließen</td>' . "\n" .
	'	<td>QS</td>' . "\n" .
	'	<td>Spec Out Of Date</td>' . "\n" .
	'	<td>Closed <a href="#fn1">(*)</a></td>' . "\n" .
	'</tr>' . "\n" .
	'<tr>' . "\n" .
	'	<td>Verified</td>' . "\n" .
	'	<td>Open</td>' . "\n" .
	'	<td>Bugfix wurde verifiziert (SWE, PL) (betrifft i.d.R. technische Bugs und Kundenbugs)</td>' . "\n" .
	'	<td>Bugeintrag schließen</td>' . "\n" .
	'	<td>QS</td>' . "\n" .
	'	<td>Verified</td>' . "\n" .
	'	<td>Closed <a href="#fn1">(*)</a></td>' . "\n" .
	'</tr>' . "\n" .
	'<tr>' . "\n" .
	'	<td>Verified</td>' . "\n" .
	'	<td>Closed</td>' . "\n" .
	'	<td>Bug trat nicht mehr auf, Bugeintrag wurde geschlossen</td>' . "\n" .
	'	<td>Bug taucht erneut auf. → Kommentar erstellen und Bug wieder-öffnen</td>' . "\n" .
	'	<td>QS</td>' . "\n" .
	'	<td>Reopened / None / New</td>' . "\n" .
	'	<td>Open</td>' . "\n" .
	'</tr>' . "\n" .
	'<tr>' . "\n" .
	'	<td>Won’t Fix</td>' . "\n" .
	'	<td>Open</td>' . "\n" .
	'	<td>Bug wird nicht gefixt (Entscheidung durch PL)</td>' . "\n" .
	'	<td>Bugeintrag schließen</td>' . "\n" .
	'	<td>QS</td>' . "\n" .
	'	<td>Won’t Fix</td>' . "\n" .
	'	<td>Closed <a href="#fn1">(*)</a></td>' . "\n" .
	'</tr>' . "\n" .
	'<tr>' . "\n" .
	'	<td>Won’t Fix</td>' . "\n" .
	'	<td>Open</td>' . "\n" .
	'	<td>Bug kann nicht gefixt werden (SWE)</td>' . "\n" .
	'	<td>Rückfrage an PL, ob Bugeintrag wirklich nicht gefixt wird</td>' . "\n" .
	'	<td>QS, SWE</td>' . "\n" .
	'	<td>Awaiting Response</td>' . "\n" .
	'	<td>Open</td>' . "\n" .
	'</tr>' . "\n" .
	'<tr>' . "\n" .
	'	<td>Works For Me</td>' . "\n" .
	'	<td>Open</td>' . "\n" .
	'	<td>Bug kann nicht reproduziert werden (SWE)</td>' . "\n" .
	'	<td>Nach-Test des Bugs → Bug tritt nicht (mehr) auf</td>' . "\n" .
	'	<td>QS</td>' . "\n" .
	'	<td>Verified</td>' . "\n" .
	'	<td>Closed <a href="#fn1">(*)</a></td>' . "\n" .
	'</tr>' . "\n" .
	'<tr>' . "\n" .
	'	<td>Works For Me</td>' . "\n" .
	'	<td>Open</td>' . "\n" .
	'	<td>Bug kann nicht reproduziert werden (SWE)</td>' . "\n" .
	'	<td>Nach-Test des Bugs → Bug tritt weiterhin auf</td>' . "\n" .
	'	<td>QS</td>' . "\n" .
	'	<td>None / New</td>' . "\n" .
	'	<td>Open</td>' . "\n" .
	'</tr>' . "\n" .
	'</table>' . "\n" .
	'<div style="margin:0.5em 0;" id="fn1">(*)<b>WICHTIG:</b> Closed darf derzeit <b>NUR</b> von der QS gesetzt werden!</div>' . "\n" .
	'<div style="margin:0.5em 0;" id="fn2">(**) Falls ein Bugeintrag von einem Entwickler angelegt wurde und von der QS nicht genau nachvollzogen werden kann (technische Bugs), kann der Bug auch von einem Entwickler verifiziert werden. Die QS schließt anschließend den Bug.</div>';
	break;

case 'ef-Severity':
	$t = _('Schweregrad eines Bugeintrags; siehe „Mehr Info…“ für Prozeßspezifikation.');
	break;

case 'realef-Severity':
	$r = '<div style="margin:0.5em 0;">Der Schweregrad gibt Auskunft darüber, wie dramatisch der Bug ist. (Auch ein Schreibfehler kann P1/5 sein, obwohl Schweregrad = trivial). Den Schweregrad eines Bugs abzuschätzen ist mehr oder weniger Erfahrungssache.</div>' . "\n" .
	'<h3>Severity</h3>' . "\n" .
	'<table cellspacing="0" cellpadding="6" border="1">' . "\n" .
	'<tr>' . "\n" .
	'	<th>Bezeichnung</th>' . "\n" .
	'	<th align="left">Beschreibung</th>' . "\n" .
	'</tr>' . "\n" .
	'<tr>' . "\n" .
	'	<td>blocker</td>' . "\n" .
	'	<td>Stürzt die gesamte Anwendung ab, friert die Anwendung ein oder wird das Fortführen eines Prozesses auf andere Art und Weise blockiert, ist der Bug als <b>blocker</b> einzustufen.</td>' . "\n" .
	'</tr>' . "\n" .
	'<tr>' . "\n" .
	'	<td>critical</td>' . "\n" .
	'	<td>Sind wesentliche Funktionen fehlerhaft, Anforderungen NICHT beachtet oder FALSCH umgesetzt worden oder ist das Testobjekt nur mit großen Einschränkungen einsetzbar, so sollte der Bug als <b>critical</b> eingestuft werden.</td>' . "\n" .
	'</tr>' . "\n" .
	'<tr>' . "\n" .
	'	<td>major</td>' . "\n" .
	'	<td>Wenn es sich um funktionale Abweichungen/Einschränkungen handelt oder eine Anforderung nur teilweise umgesetzt wurde, das System aber an sich nutzbar ist, wird der Bug als <b>major</b> eingestuft. Beispiele:<ul>' . "\n" .
	'		<li>Pflichtfelder werden nicht als solche berücksichtigt</li>' . "\n" .
	'		<li>Buttons können nach Betätigen unmittelbar noch mal betätigt werden</li>' . "\n" .
	'	</ul></td>' . "\n" .
	'</tr>' . "\n" .
	'<tr>' . "\n" .
	'	<td>normal</td>' . "\n" .
	'	<td>Werden z.B. Umlaute und andere Zeichen nicht korrekt dargestellt oder sind die Ladezeiten zu lang, ist der Status <b>normal</b>.</td>' . "\n" .
	'</tr>' . "\n" .
	'<tr>' . "\n" .
	'	<td>minor</td>' . "\n" .
	'	<td>Bei geringfügigen Abweichungen, wenn das System ohne Einschränkungen nutzbar ist, ist der Status als <b>minor</b> zu bewerten. Beispiele:<ul>' . "\n" .
	'		<li>Ausgegraute Felder sind zu unleserlich in der Farbgebung</li>' . "\n" .
	'		<li>Deaktivierte Felder sind zwar befüllbar, aber nicht speicherbar</li>' . "\n" .
	'	</ul></td>' . "\n" .
	'</tr>' . "\n" .
	'<tr>' . "\n" .
	'	<td>trivial</td>' . "\n" .
	'	<td>Wenn es sich um Schönheitsfehler handelt und das System ohne Einschränkung nutzbar ist, sollten diese Bugs als <b>trivial</b> markiert werden. Beispiele:<ul>' . "\n" .
	'		<li>Es tauchen Rechtschreibfehler auf</li>' . "\n" .
	'		<li>Die Aufteilung ist nicht sinnvoll oder Felder sind an einer suboptimalen Stelle platziert</li>' . "\n" .
	'		<li>Eine Fehlermeldung ist zu unaussagekräftig</li>' . "\n" .
	'		</ul>Änderungswünsche, die darüber hinausgehen, sind als <b>Change Requests</b> in die entsprechende Liste einzutragen.' . "\n" .
	'	</td>' . "\n" .
	'</tr>' . "\n" .
	'</table>';
	break;

default:
	return ("");
}

/****************************************************************/

	if ($r === false) {
		/* $t contains one or more paragraphs to be encoded */
		if (is_array($t)) {
			$r = "";
			foreach ($t as $p) {
				$r .= html_e('div', array('style' => 'margin:0.5em 0;'), htmlspecialchars($p));
			}
		} else {
			$r = htmlspecialchars($t);
		}
	}
	/* $r contains XHTML with encoded textual content */
	/* now encode $r to be passed to jQuery-Tipsy */
	return (htmlspecialchars($r));
}
