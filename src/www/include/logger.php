<?php
/**
 * Copyright 1999-2001 (c) VA Linux Systems
 * Copyright (C) 2011 Alain Peyrat - Alcatel-Lucent
 * http://fusionforge.org
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/*
	Determine group
*/
$group_id=getIntFromRequest('group_id');
$form_grp=getIntFromRequest('form_grp');
$log_group=0;
if (isset($group_id) && is_numeric($group_id) && $group_id) {
	$log_group=$group_id;
} else if (isset($form_grp) && is_numeric($form_grp) && $form_grp) {
	$log_group=$form_grp;
} else if (isset($group_name) && $group_name) {
	$group = group_get_object_by_name($group_name);
	if ($group) {
		$log_group=$group->getID();
	}
} else {
	//
	//
	//	This is a hack to allow the logger to have a group_id present
	//	for project summary pages
	//
	//
	$pos = strpos (getStringFromServer('REQUEST_URI'),
		       normalized_urlprefix ());
	$pathwithoutprefix = "";
	if (($pos !== false) && ($pos == 0)) {
		$pathwithoutprefix = substr (getStringFromServer('REQUEST_URI'),
					     strlen (normalized_urlprefix ()) - 1);
	}
	$expl_pathinfo = explode('/',$pathwithoutprefix);
	$expl_pathinfo = array_merge($expl_pathinfo, array("", "", "", ""));
	if ($expl_pathinfo[1] == 'projects') {
		$res_grp = db_query_params ('
			SELECT *
			FROM groups
			WHERE unix_group_name=$1
			AND status IN ($2, $3, $4)',
					    array ($expl_pathinfo[2],
						   'A', 'H', 'P'));

		// store subpage id for analyzing later
		$subpage  = isset($expl_pathinfo[3])?$expl_pathinfo[3]:'';
		$subpage2 = isset($expl_pathinfo[4]) ? $expl_pathinfo[4] : '';

		//set up the group_id
		$log_group = db_result($res_grp, 0, 'group_id');
		if ($log_group) {
			$grp = group_get_object($log_group, $res_grp);
			if ($grp) {
				//this is a project - so set up the project var properly
				$project =& $grp;
				$group_id = $log_group;
				//echo "IS PROJECT: ".$group_id;
			} else
				$log_group = 0;
		}
	}
}

/* disabled for now
$sql =	"INSERT INTO activity_log
(day,hour,group_id,browser,ver,platform,time,page,type)
VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9);";

$res_logger = db_query_params ($sql, array(date('Ymd'), date('H'),
	$log_group, browser_get_agent(), browser_get_version(), browser_get_platform(),
	time(), getStringFromServer('PHP_SELF'), '0'));

//
//	temp hack
//
$sys_db_is_dirty=false;

if (!$res_logger) {
	echo "An error occured in the logger.\n";
	echo htmlspecialchars(db_error());
	exit;
}
*/
