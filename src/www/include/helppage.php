<?php
/*-
 * Online Help Extension for FusionForge Trackers
 *
 * Copyright © 2011
 *	Thorsten Glaser <t.glaser@tarent.de>
 * All rights reserved.
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

require_once('../env.inc.php');
require_once $gfcommon.'include/pre.php';

$kw = getStringFromRequest('kw');
$dsc = html_get_tooltip_description($kw);

if (!$dsc) {
	exit_error(_('Do not call me directly!'), 'tracker');
}

html_generic_fileheader(sprintf(_('Forge Help Facility: %s'), $kw));
?>
</head><body>

<?php echo util_unconvert_htmlspecialchars($dsc); ?>

<hr />
<input type="button" value="<?php echo _('Close Window'); ?>"
 onclick="javascript:window.close();" />
</body></html>
