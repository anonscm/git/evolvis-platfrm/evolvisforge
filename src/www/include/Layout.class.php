<?php
/*-
 * Evolvis theme “three” for tarent solutions GmbH
 * (baded on Mac OS X like theme, which is based on "debian" theme)
 *
 * Copyright 1999-2001 (c) VA Linux Systems
 * Copyright (c) 2002-2003 Richard Offer. All rights reserved.
 * Copyright (c) 2007 Waldemar Brodkorb <w.brodkorb@tarent.de>
 * Copyright (c) 2007 Thorsten Glaser <t.glaser@aurisp.de>
 * Copyright 2010 - Alain Peyrat
 * Copyright 2010 - Franck Villaume - Capgemini
 * Copyright (C) 2010 Alain Peyrat - Alcatel-Lucent
 * Copyright (c) 2009, 2010, 2011, 2012, 2013, 2018
 *	Thorsten Glaser <t.glaser@tarent.de>
 * Copyright © 2022, 2024
 *	mirabilos <tg@mirbsd.de>
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * The following licence terms have been superseded by the GNU GPL
 * but are still required to be reproduced:
 *
 * Permission to use, copy, modify, distribute, and sell this software and its
 * documentation for any purpose is hereby granted without fee, provided that
 * the above copyright notice appear in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation.
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Except as contained in this notice, the name of the author shall not be
 * used in advertising or otherwise to promote the sale, use or other dealings
 * in this Software without prior written authorization from the author.
 */

require_once $gfcommon.'include/constants.php';
require_once $gfcommon.'include/FusionForge.class.php';
require_once $gfcommon.'include/Navigation.class.php';
require_once(dirname(__FILE__) . "/Layout.style.php");

class Layout extends FFError {

	/**
	 * Which doctype to use. Can be configured in the
	 * constructor. If set to 'strict', headerHTMLDeclaration will
	 * create a doctype definition that uses the strict doctype,
	 * otherwise it will use the transitional doctype.
	 * @var string $doctype
	 */
	var $doctype = 'transitional';

	/**
	 * The navigation object that provides the basic links. Should
	 * not be modified.
	 */
	var $navigation;

	/**
	 * The color bars in pm reporting
	 */
	var $COLOR_LTBACK1 = '#C0C0C0';

	var $js = array();
	var $js_min = array();
	var $javascriptlets = array();
	var $css = array();
	var $css_min = array();
	var $stylesheets = array();
	var $footer_hooks = array();
	var $extra_js = array();

	/**
	 * Layout() - Constructor
	 */
	function __construct() {
		global $sysdebug_enable;

		// parent constructor
		parent::__construct();

		$this->navigation = new Navigation();
		$this->ev_style = new EvStyle();

		$this->project = false;
		$this->emit_forgehead = true;
		$this->emit_forgefoot = true;
		$this->collapsible_counter = 0;
		$this->collapsible_ids = array();

		if (!$sysdebug_enable) {
			/* auto-minify ECMAscript */
			$this->js_min['/scripts/jquery/jquery.js'] =
			    '/scripts/jquery/jquery.min.js';
			$this->js_min['/scripts/jquery-ui/jquery-ui.js'] =
			    '/scripts/jquery-ui/jquery-ui.min.js';
		}

		$this->addStylesheet('/css/fusionforge.css');
		$this->addJavascriptEx("/scripts/protoaculous/protoaculous.js", false);
		$this->addJavascriptEx('/js/codendi.js', false);
		$this->addJavascriptEx('/js/common.js', false);
	}

	function addJavascriptEx($js, $isinline) {
		if ($isinline) {
			$this->javascriptlets[] = array($js, true);
			return;
		}
		if (isset($this->js_min[$js])) {
			$js = $this->js_min[$js];
		}
		if ($js && !isset($this->js[$js])) {
			$this->js[$js] = true;
			$filename = $GLOBALS['fusionforge_basedir'].'/www'.$js;
			if (file_exists($filename)) {
				$js .= '?'.date ("U", filemtime($filename));
			} else {
				$filename = str_replace('/scripts/', $GLOBALS['fusionforge_basedir'].'/lib/vendor/', $js);
				if (file_exists($filename)) {
					$js .= '?'.date ("U", filemtime($filename));
				}
			}
			$this->javascriptlets[] = array($js, false);
		}
	}

	function addStylesheet($css, $media='') {
		if (isset($this->css_min[$css])) {
			$css = $this->css_min[$css];
		}
		if (!isset($this->css[$css])) {
			$this->css[$css] = true;
			$filename = $GLOBALS['fusionforge_basedir'].'/www'.$css;
			if (file_exists($filename)) {
				$css .= '?'.date ("U", filemtime($filename));
			} else {
				$filename = str_replace('/scripts/', $GLOBALS['fusionforge_basedir'].'/lib/vendor/', $css);
				if (file_exists($filename)) {
					$css .= '?'.date ("U", filemtime($filename));
				}
			}
			$this->stylesheets[] = array('css' => $css, 'media' => $media);
		}
	}

	function getJavascripts() {
		$code = '';
		foreach ($this->javascriptlets as $jsa) {
			$js = trim($jsa[0]);
			if ($jsa[1]) {
				if (!$js) {
					continue;
				}
				$code .= "\t" .
				    '<script type="text/javascript"><!--//--><![CDATA[//><!--';
				foreach (explode("\n", $js) as $jsline) {
					$code .= "\n\t\t" . $jsline;
				}
				$code .= "\n\t//--><!]]></script>\n";
			} else {
				$code .= "\t" . html_e('script', array(
					'language' => 'JavaScript',
					'type' => 'text/javascript',
					'src' => $js,
				    ), "", false) . "\n";
			}
		}
		return $code;
	}

	function getStylesheets() {
		$code = '';
		foreach ($this->stylesheets as $c) {
			$code .= "\t";
			if ($c['media']) {
				$code .= '<link rel="stylesheet" type="text/css" href="'.$c['css'].'" media="'.$c['media'].'" />'."\n";
			} else {
				$code .= '<link rel="stylesheet" type="text/css" href="'.$c['css'].'"/>'."\n";
			}
		}
		return $code;
	}

	/**
	 * header() - generates the complete header of page by calling
	 * headerStart() and bodyHeader().
	 */
	function header($params) {
		$this->emit_html_head = true;
		return $this->internal_header($params);
	}

	/**
	 * headerStart() - generates the header code up to the closing </head>.
	 * Override any of the methods headerHTMLDeclaration(), headerTitle(),
	 * headerFavIcon(), headerRSS(), headerSearch(), headerCSS(), or
	 * headerJS() to adapt.
	 *
	 * @param	array	Header parameters array
	 */
	function headerStart($params) {
		$this->headerHTMLDeclaration();
		?>
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<?php
		$this->headerTitle($params);
		$this->headerFavIcon();
		$this->headerRSS();
		$this->headerSearch();
		$this->headerCSS();
		$this->headerJS();
		$this->headerForgepluckerMeta();
		?>
			</head>
		<?php
	}

	/**
	 * headerHTMLDeclaration() - generates the HTML declaration, i.e. the
	 * XML declaration, the doctype definition, and the opening <html>.
	 *
	 */
	function headerHTMLDeclaration() {
		global $sysDTDs, $sysXMLNSs;

		if (!util_ifsetor($this->doctype) || !util_ifsetor($sysDTDs[$this->doctype])) {
			$this->doctype = 'transitional';
		}
		echo $sysDTDs[$this->doctype]['doctype'] . "\n";
		echo '<html xml:lang="' . _('en') . '" lang="' . _('en') .
		    '" ' . $sysXMLNSs . ">\n";
	}

	/**
	 * headerTitle() - creates the <title> header
	 *
	 * @param	array	Header parameters array
	 */
	function headerTitle($params) {
		echo $this->navigation->getTitle($params);
	}


	/**
	 * headerFavIcon() - creates the favicon <link> headers.
	 *
	 */
	function headerFavIcon() {
		echo $this->navigation->getFavIcon();
	}

	/**
	 * headerRSS() - creates the RSS <link> headers.
	 *
	 */
	function headerRSS() {
		echo $this->navigation->getRSS();
	}

	/**
	 * headerSearch() - creates the search <link> header.
	 *
	 */
	function headerSearch() {
		echo '<link rel="search" title="'
			. forge_get_config ('forge_name').'" href="'
			. util_make_url ('/export/search_plugin.php')
			. '" type="application/opensearchdescription+xml"/>'."\n";
	}

	/**
	 * Create the CSS headers for all cssfiles in $cssfiles and
	 * calls the plugin cssfile hook.
	 */
	function headerCSS() {
		plugin_hook ('cssfile',$this);
		echo $this->getStylesheets();
	}

	/**
	 * headerJS() - creates the JS headers and calls the plugin javascript hook
	 * @todo generalize this
	 */
	function headerJS() {
?>
	<script type="text/javascript"><!--//--><![CDATA[//><!--
<?php
		/* provide util_make_url equivalent for ECMAscript */
		echo "\t\tvar sys_url_base = " .
		    minijson_encode(util_make_url("/"), false) . ";\n";
		/* emit extra initial ECMAscript, if any */
		foreach ($this->extra_js as $line) {
			echo "\t\t" . $line . "\n";
		}
		/* this hook comes before the files (why?) */
		plugin_hook ("javascript",false);
?>
	//--><!]]></script>
<?php
		/* this hook comes before the main files (why?) */
		plugin_hook ("javascript_file",false);

		/* now emit all those from addJavascript() et al. */
		echo $this->getJavascripts();
	}

	function headerForgepluckerMeta() {
		/* figure out our possible hostnames */
		$wh = forge_get_config('web_host');
		if (!$wh)
			throw new RuntimeException("empty web_host");
		$ourhosts = array();
		$ourhosts[forge_get_config('download_host')] = true;
		$ourhosts[forge_get_config('forum_return_domain')] = true;
		$ourhosts[forge_get_config('lists_host')] = true;
		$ourhosts[forge_get_config('scm_host')] = true;
		$ourhosts[forge_get_config('shell_host')] = true;
		$ourhosts[forge_get_config('users_host')] = true;
		$ourhosts[$wh] = true;
		unset($ourhosts[$wh]);
		$ourhosts = array_keys($ourhosts);
		sort($ourhosts, SORT_STRING);
		/* show mircvs://www/files/vextlnk.png with all other links */
		?>
	<style type="text/css"><!--/*--><![CDATA[/*><!--*/

		a[href^="ftp://"]:after,
		a[href^="http://"]:after,
		a[href^="https://"]:after,
		a[href^="irc://"]:after,
		a[href^="mailto:"]:after,
		a[href^="news:"]:after,
		a[href^="nntp://"]:after {
			content:url(<?php echo util_make_url('/vextlnk.png'); ?>);
			margin:0 0 0 3px;
			white-space:nowrap;
			display:inline-block;
			width:10px;
			height:10px;
		}

<?php
		echo "\t\t" . 'a[href^="https://' . $wh . '/"]:after';
		foreach ($ourhosts as $h) {
			if (!$h)
				continue;
			echo ",\n\t\t" . 'a[href^="https://' . $h . '/"]:after';
		} ?> {
			content:"";
			margin:0px;
			display:none;
		}

		a.vextlink:after {
			content:url(<?php echo util_make_url('/vextlnk.png'); ?>) !important;
			margin:0 0 0 3px !important;
			white-space:nowrap;
			display:inline-block !important;
			width:10px;
			height:10px;
		}

	/*]]>*/--></style><?php
		echo "\n";

		/*-
		 * Forge-Identification Meta Header, Version 1.0
		 * cf. http://home.gna.org/forgeplucker/forge-identification.html
		 */
		$ff = new FusionForge();
		echo "\t";
		printf('<meta name="Forge-Identification" content="%s:%s" />',
		    $ff->software_type, $ff->software_version);
		echo "\n";
	}

	function bodyHeader($params) {
		$this->emit_html_head = false;
		return $this->internal_header($params);
	}

	function internal_header($params) {
		global $_SERVER, $group_id, $sysDTDs, $sysXMLNSs;

		$has_group = false;
		if (array_key_exists('group', $params)) {
			$this->project = group_get_object($params['group']);
			if (isset($this->project) && is_object($this->project) &&
			    !$this->project->isError() && $this->project->isProject()) {
				$group_id = $this->project->getID();
				if ($group_id) {
					$has_group = true;
				}
			}
		}

		$params['title'] = isset($params['title']) && $params['title'] ?
		    forge_get_config('forge_name') . ': ' . $params['title'] :
		    forge_get_config('forge_name');

		if ($this->emit_html_head) {
			$this->ev_style->emit_head($this, array(
				'doctype' => $sysDTDs['transitional']['doctype'],
				'xmlns' => $sysXMLNSs,
				'lang' => _('en'),
				'title' => $params['title'],
			    ), 'cb_head');
		}
		if ($this->emit_forgehead) {
			$p = array();
			$p['globalmenu'] = array();
			if (session_loggedin()) {
				$p['globalmenu'][] = array(
					_('Log out of the session with the Forge system'),
					"/account/logout.php?return_to=" .
					    urlencode(getStringFromServer('REQUEST_URI')),
					_('Log Out'),
				    );
				$p['globalmenu'][] = array(
					_('Manage projects, assigned tasks, bugs, etc.'),
					"/my/",
					_('My Page'),
				    );
				$globalmenu_mypage = count($p['globalmenu']) - 1;
				$p['globalmenu'][] = array(
					_('Configure Account settings, Password & SSH keys'),
					"/account/",
					_('My Account'),
				    );
				$globalmenu_myaccount = count($p['globalmenu']) - 1;
			} else {
				$p['globalmenu'][] = array(
					_('Log into a new session with the Forge system'),
					"/account/login.php?return_to=" .
					    urlencode(getStringFromServer('REQUEST_URI')),
					_('Log In'),
				    );
				if (!forge_get_config('user_registration_restricted')) {
					$p['globalmenu'][] = array(
						_('Register a user account with the Forge system'),
						"/account/register.php",
						_('New Account'),
					    );
				}
			}
			$psm = array();
			if (util_ifsetor($params['toptab'])) {
				$psm['toptab'] = $params['toptab'];
			}
			$sitemenu = $this->navigation->getSiteMenu($psm);
			$request_uri = $sitemenu['request_uri'];
			if (strstr($request_uri, util_make_uri('/my/')) ||
			    strstr($request_uri, util_make_uri('/register/'))) {
				if (session_loggedin()) {
					$p['globalmenu'][$globalmenu_mypage][3] = 's';
				}
			} else if (strstr($request_uri, util_make_uri('/account/'))) {
				if (session_loggedin()) {
					$p['globalmenu'][$globalmenu_myaccount][3] = 's';
				}
			}
			$p['outertabs'] = $this->tabGenerator($sitemenu['urls'],
			    $sitemenu['titles'], false, $sitemenu['selected'],
			    "", "", util_ifsetor($sitemenu['tooltips'], array()),
			    'b');
			$p['searchbox'] = $this->navigation->getSearchBox();
			if ($has_group) {
				$p['group_id'] = $group_id;
				$p['group_label'] = sprintf(_('Project: %s'),
				    $this->project->getPublicName(true));
				$p['group_link'] = util_make_uri('/projects/' .
				    $this->project->getUnixName() . '/');
				$p['projecttabs'] = $this->projectTabs(util_ifsetor($params['toptab']), $group_id);
			} else {
				$p['group_id'] = 0;
				$p['group_label'] = sprintf(_('Forge: %s'),
				    forge_get_config('forge_name'));
				$p['group_link'] = util_make_uri('/');
				$p['projecttabs'] = $this->project0Tabs();
			}

			if (!isset($params['submenu'])) {
				$params['submenu'] = "";
			}
			if (is_array($params['submenu'])) {
				$params['submenu'] = $this->beginSubMenu() .
				    $this->printSubMenu($params['submenu'][0],
				    $params['submenu'][1],
				    $params['submenu'][2]) .
				    $this->endSubMenu();
			}
			/* coerce into string */
			$p['submenu'] = "" . $params['submenu'];

			$this->ev_style->emit_bodyheader($this, $p);
		}
		echo "<div id=\"ev_content\">\n\n";
		/* argh! */
		if (util_ifsetor($params['h1'])) {
			echo "<h1>" . util_html_secure($params['h1']) .
			    "</h1>\n\n";
		}
	}

	function cb_head($T) {
		$T->headerCSS();
		$T->headerJS();
		$T->headerForgepluckerMeta();
	}

	function cb_footer_collapsible($params,$T) {
		echo '<div id="evcollapsibleicon0min" style="display:none;">' .
		    $T->getPicto($T->_getToggleMinusForWidgets(), _('Toggle'),
		    'Toggle', '16', '16') . "</div>\n";
		echo '<div id="evcollapsibleicon0max" style="display:none;">' .
		    $T->getPicto($T->_getTogglePlusForWidgets(), _('Toggle'),
		    'Toggle', '16', '16') . "</div>\n";
?><script type="text/javascript"><!--//--><![CDATA[//><!--
	/* get real (generated) URIs of the pictographs */
	var i = document.getElementById("evcollapsibleicon0max").getElementsByTagName("img")[0];
	var icon_maximise = i.getAttributeNode("src").nodeValue;
	var i = document.getElementById("evcollapsibleicon0min").getElementsByTagName("img")[0];
	var icon_minimise = i.getAttributeNode("src").nodeValue;
	/* initialise all collapsibles with them */
	var evcollapsibleids = [];
<?php
	foreach ($T->collapsible_ids as $nr => $cid) {
		echo "\tevcollapsibleids[" . $nr . "] = '" . $cid . "';\n";
	}
?>
	for (var i = 1; i <= <?php echo $T->collapsible_counter; ?>; ++i) {
		evcollapse('evcollapsibleicon' + i,
		    'evcollapsibleelem' + i, 1, evcollapsibleids[i]);
	}
//--><!]]></script>
<?php
	}

	function footer($params) {
		/* close #ev_content div */
		echo "\n    </div>\n";

		/* add ECMAscript to footer hooks if needed */
		if ($this->collapsible_counter > 0) {
			$this->footer_hooks[] = array($this,
			    'cb_footer_collapsible');
		}

		/* display HTML footer */
		$params['showfooter'] = $this->emit_forgefoot;
		$this->ev_style->emit_bodyfooter($this, $params);

		/* end output */
		echo "\n  </body>\n</html>\n";
	}

	function footerEnd($params) { ?>

		<!-- PLEASE LEAVE "Powered By FusionForge" on your site -->
			<div align="right">
			<?php echo $this->navigation->getPoweredBy(); ?>
			</div>

			<?php echo $this->navigation->getShowSource(); ?>
<?php
		foreach ($this->footer_hooks as $h) {
			call_user_func($h, $params);
		}
?>

			</body>
			</html>
			<?php

	}

	/**
	 * boxGetAltRowStyle() - Get an alternating row style for tables
	 *
	 * @param	int	Row number
	 */
	function boxGetAltRowStyle($i) {
		switch ($i % 3 ) {
		case 0:
			return 'bgcolor="#F4F4F4"';
		case 1:
			return 'bgcolor="#EAEAEA"';
		case 2:
			return 'bgcolor="#E0E0E0"';
		}
	}

	function boxApplyAltRowStyle($i, &$attrs) {
		switch ($i % 3 ) {
		case 0:
			$attrs['bgcolor'] = "#F4F4F4";
			break;
		case 1:
			$attrs['bgcolor'] = "#EAEAEA";
			break;
		case 2:
			$attrs['bgcolor'] = "#E0E0E0";
			break;
		}
	}

	/* internal */
	function boxTopCollapsible($title, $id, $hidep, $cookie_id) {
		$nr = ++$this->collapsible_counter;
		$iid = "evcollapsibleicon" . $nr;
		$eid = "evcollapsibleelem" . $nr;
		if ($cookie_id) {
			$this->collapsible_ids[$nr] = $cookie_id;
		}
		$h = $hidep ? ' hidePartForPrinting' : '';

		return '<tr class="boxtitlebar' . $h . '"' .
		    ($id ? ' id="' . $this->toSlug($id) . '-title"' : "") . '>
			<td class="ff" align="left"' .
		    ($id ? ' id="' . $this->toSlug($id) . '-title-content"' : "") . '>
				' . $title . '
				<div class="widget_titlebar_minimize hidePartForPrinting"
				 style="display:none;"
				 onclick="evcollapse(' . "'" . $iid .
				    "', '" . $eid . "', 0, '" . $cookie_id .
				    "'" . ');"
				 id="' . $iid . '">' .
				    $this->getPicto($this->_getToggleMinusForWidgets(),
				    _('Toggle'), 'Toggle', '16', '16') . '
				</div>
			</td>
		</tr><tr class="boxcontent' . $h . '" id="' . $eid . '" align="left">
			<td' .
		    ($id ? ' id="' . $this->toSlug($id) . '-content"' : "") .
		    '>';
	}

	/**
	 * boxTop() - Top HTML box
	 *
	 * @param	string	Box title
	 * @param	string	ID attribute (optional)
	 * @param	bool	hide from printing? (optional)
	 * @param	string	Collapsible Cookie ID (optional)
	 */
	function boxTop($title, $id="", $hidep=false, $ccid="") {
		return '<!-- boxTop[ -->
		<table' . ($id ? ' id="' . $this->toSlug($id) . '"' : "") .
		    ' width="100%" border="0" class="ff" cellspacing="0">
		' . $this->boxTopCollapsible($title, $id, $hidep, $ccid) .
		    '<!-- ]boxTop -->';
	}

	/**
	 * boxMiddle() - Middle HTML box
	 *
	 * @param	string	Box title
	 * @param	string	ID attribute (optional)
	 * @param	bool	hide from printing? (optional)
	 * @param	string	Collapsible Cookie ID (optional)
	 */
	function boxMiddle($title, $id="", $hidep=false, $ccid="") {
		return '<!-- boxMiddle[ -->
			</td>
		</tr><tr bgcolor="#FFFFFF"
		 class="hidePartForPrinting"><td height="1"></td>
		</tr>' . $this->boxTopCollapsible($title, $id, $hidep, $ccid) .
		    '<!-- ]boxMiddle -->';
	}

	/**
	 * boxBottom() - Bottom HTML box
	 */
	function boxBottom() {
		return '<!-- boxBottom[ -->
			</td>
		</tr><tr bgcolor="#FFFFFF"
		 class="hidePartForPrinting"><td height="1"></td>
		</tr></table><!-- ]boxBottom -->'."\n";
	}

	/**
	 * listTableTop() - Takes an array of titles and builds the first row of a new table.
	 *
	 * @param	array	The array of titles
	 * @param	array	The array of title links
	 */
	function listTableTop($title_arr,$links_arr=false) {
		$params = array();
		$params['text'] = &$title_arr;
		if ($links_arr) {
			$params['link'] = &$links_arr;
		}
		$params['sortable'] = false;
		return $this->listTableStart($params);
	}

	/**
	 * listTableTopSortable() - Takes an array of titles and builds the first row of a new table and table is sortable.
	 *
	 * @param	array	The array of titles
	 * @param	array	The array of title links
	 */
	function listTableTopSortable($title_arr,$links_arr=false) {
		$params = array();
		$params['text'] = &$title_arr;
		if ($links_arr) {
			$params['link'] = &$links_arr;
		}
		$params['sortable'] = true;
		return $this->listTableStart($params);
	}

	function listTableStart($params) {
		/*XXX do an html_ap and pass to listTableBottom */

		$rv = "<!-- listTableStart[ -->\n";
		$rv .= html_eo('table', array(
			'cellspacing' => 0,
			'cellpadding' => 1,
			'width' => util_ifsetor($params['narrow']) ? false :
			    "100%",
			'bgcolor' => '#FFFFFF',
			'border' => "0",
		    ));
		$rv .= html_eo('tr', array('class' => 'ff'));
		$rv .= html_eo('td', array('class' => 'ff'));
		$tclass = array();
		if (util_ifsetor($params['sortable'])) {
			$tclass[] = 'sortable';
		}
		$rv .= html_eo('table', array(
			'class' => $tclass,
			'width' => "100%",
			'border' => 0,
			'cellspacing' => 2,
			'cellpadding' => 0,
		    ));

		if (($ct = util_ifsetor($params['topless'], 0)) == 0) {
			$spos = html_ap();
			$rv .= html_ao('thead');

			$ct = count($params['text']);
			$cl = is_array(util_ifsetor($params['link'])) ?
			    count($params['link']) : 0;

			if ($ct && $cl && ($ct != $cl)) {
				$rv .= html_e('tr', array(), html_e('td', array(
					'align' => 'center',
					'style' => 'color:red;',
					'colspan' => $ct,
				    ), 'Inequal magnitutes of $params["text"] and ' .
				    '$params["link"]:' . html_e('pre', array(),
				    "\n" . htmlentities(debug_string_backtrace()))));
			}

			$rv .= html_ao('tr', array('class' => 'ff'));
			if (!$ct) {
				$rv .= html_e('td', array(
					'align' => 'center',
					'style' => 'color:red;',
				    ), 'No $params["text"]:' . html_e('pre', array(),
				    "\n" . htmlentities(debug_string_backtrace())));
			} else if ($ct == $cl) {
				for ($i = 0; $i < $ct; ++$i) {
					$celltext = $params['text'][$i];
					if (is_array($celltext)) {
						$cellattr = $celltext[1];
						$celltext = $celltext[0];
					} else {
						$cellattr = array();
					}
					$cellattr['align'] =
					    util_ifsetor($cellattr['align']) ?
					    $cellattr['align'] : 'left';
					if (!util_ifsetor($cellattr['class'])) {
						$cellattr['class'] = 'ff';
					} else if (is_array($cellattr['class'])) {
						array_unshift($cellattr['class'], 'ff');
					} else {
						$cellattr['class'] = 'ff ' .
						    $cellattr['class'];
					}
					$rv .= html_e('th', $cellattr,
					    html_e('a', array(
						'class' => 'titlebar',
						'href' => $params['link'][$i],
					    ), $celltext));
				}
			} else {
				foreach ($params['text'] as $celltext) {
					if (is_array($celltext)) {
						$cellattr = $celltext[1];
						$celltext = $celltext[0];
					} else {
						$cellattr = array();
					}
					$cellattr['align'] =
					    util_ifsetor($cellattr['align']) ?
					    $cellattr['align'] : 'left';
					if (!util_ifsetor($cellattr['class'])) {
						$cellattr['class'] = 'list_table_top';
					} else if (is_array($cellattr['class'])) {
						array_unshift($cellattr['class'], 'list_table_top');
					} else {
						$cellattr['class'] = 'list_table_top ' .
						    $cellattr['class'];
					}
					$rv .= html_e('th', $cellattr, $celltext);
				}
			}
			$rv .= html_ac($spos);
		}

		$rv .= html_e('tfoot', array(), html_e('tr', array(
			'class' => 'ff',
			'align' => 'left',
			'bgcolor' => '#FFFFFF',
		    ), html_e('td', array(
			'class' => 'ff',
			'colspan' => $ct,
			'height' => 1,
		    ), html_e('img', array(
			'src' => '/images/clear.png',
			'height' => 1,
			'width' => 1,
			'alt' => "",
		    )))));
		$rv .= html_eo('tbody');
		$rv .= "\n<!-- ]listTableStart -->";
		return $rv;
	}

	function listTableBottom() {
		return "<!-- listTableBottom[ --></tbody></table></td></tr></table><!-- ]listTableBottom -->\n";
	}

	/* @deprecated */
	function outerTabs($params,$write=1) {
		$rv = "<div>bogus call <pre>{".debug_string_backtrace()."}</pre> & ERROR</div>\n";
		if ($write) {
			echo $rv;
		}
		return $rv;
	}

	/**
	 * Prints out the quicknav menu, contained here in case we
	 * want to allow it to be overridden.
	 */
	function quickNav() {
		if (!session_loggedin()) {
			return;
		} else {
			// get all projects that the user belongs to
			$groups = session_get_user()->getGroups();

			if (count($groups) < 1) {
				return;
			} else {
				sortProjectList($groups);

				echo '
					<form id="quicknavform" name="quicknavform" action=""><div>
					<select name="quicknav" id="quicknav" onchange="location.href=document.quicknavform.quicknav.value">
					<option value="">'._('Quick Jump To...').'</option>';

				foreach ($groups as $g) {
					$group_id = $g->getID();
					$menu = $this->navigation->getProjectMenu($group_id);

					echo '
						<option value="' . $menu['starturl'] . '">'
						. $menu['name'] .'</option>';

					for ($j = 0; $j < count($menu['urls']); $j++) {
						echo '
							<option value="' . $menu['urls'][$j] .'">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							. $menu['titles'][$j] . '</option>';
						if (@$menu['adminurls'][$j]) {
							echo '
								<option value="' . $menu['adminurls'][$j]
								. '">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
								. _('Admin') . '</option>';
						}
					}
				}
				echo '
					</select>
					</div></form>';
			}
		}
	}

	/**
	 *	projectTabs() - Prints out the project tabs, contained here in case
	 *		we want to allow it to be overriden
	 *
	 *	@param	string	Is the tab currently selected
	 *	@param	string	Is the group we should look up get title info
	 */
	function projectTabs($toptab,$group) {
		$menu = $this->navigation->getProjectMenu($group, $toptab);

		return $this->tabGenerator($menu['urls'], $menu['titles'], true,
		    $menu['selected'], 'white', '100%',
		    util_ifsetor($menu['tooltips'], array()), 'w');
	}

	/**
	 * project0Tabs() - Print forge-global "project" tabs
	 */
	function project0Tabs() {
		$links = array();
		$texts = array();
		$ttips = array();

		if (!forge_get_config('project_registration_restricted') ||
		    forge_check_global_perm('approve_projects', '')) {
			$links[] = util_make_url("/register/");
			$texts[] = _('Register Project');
			$ttips[] = _('Register a new Project on the forge');
		}

		$links[] = util_make_url("/softwaremap/full_list.php");
		$texts[] = _('Project List');
		$ttips[] = _('Display an alphabetically sorted list of all public projects');

		return $this->tabGenerator($links, $texts, true, -1, "", "",
		    $ttips, 'w');
	}

	function tabGenerator($TABS_DIRS,$TABS_TITLES,$nested=false,$selected=false,$sel_tab_bgcolor='BLACK',$total_width='100%',$TABS_TOOLTIPS=array(),$colourkey=' ') {
		$rv = "";
		for ($i = 0; $i < count($TABS_DIRS); $i++) {
			$rv .= $this->ev_style->make_headlink(
			    ($selected == $i) ? 's' : $colourkey,
			    util_ifsetor($TABS_TOOLTIPS[$i], ""),
			    $TABS_DIRS[$i], $TABS_TITLES[$i]);
		}
		return $rv;
	}

	function searchBox() {
		echo $this->navigation->getSearchBox();
	}

	/**
	 * beginSubMenu() - Opening a submenu.
	 *
	 * @return	string	HTML to start a submenu
	 */
	function beginSubMenu() {
		return '
<div class="forgesubmenu">';
	}

	/**
	 * endSubMenu() - Closing a submenu.
	 *
	 * @return	string	HTML to start a submenu
	 */
	function endSubMenu() {
		return "\n<!-- ]forgesubmenu --></div>\n";
	}

	/**
	 * printSubMenu() - Takes two array of titles and links and builds the contents of a menu.
	 *
	 * @param	array	The array of titles
	 * @param	array	The array of title links
	 * @param	array	The array of tooltips (optional)
	 * @return	string	HTML to build a submenu
	 */
	function printSubMenu($title_arr,$links_arr,$tooltip_arr=array()) {
		$rv = "";

		for ($i = 0; $i < count($links_arr); ++$i) {
			/*
			 * workaround for nerville’s… unique way of
			 * handling tooltips
			 */
			$tooltip = util_ifsetor($tooltip_arr[$i]);
			if (is_array($tooltip)) {
				$tooltip = util_ifsetor($tooltip['title']);
			}
			if (!$tooltip || !is_string($tooltip)) {
				$tooltip = "";
			}

			$rv .= $this->ev_style->make_headlink('b',
			    $tooltip, $links_arr[$i],
			    util_ifsetor($title_arr[$i], "Link ".$i));
		}
		return $rv;
	}

	/* deprecated, pass $params['submenu'] as array instead */
	function subMenu($title_arr,$links_arr,$tooltip_arr=array()) {
		ffDebug('error', 'deprecated Layout.subMenu call ignored:\n',
		    debug_string_backtrace());
		return "{ERROR}";
	}

	/**
	 * multiTableRow() - create a mutlilevel row in a table
	 *
	 * @param	string	the row attributes
	 * @param	array	the array of cell data, each element is an array,
	 *			the first item being the text,
	 *			the subsequent items are attributes
	 * @param	boolean is this row part of the title ?
	 *
	 */
	function multiTableRow($row_attr, $cell_data, $istitle) {
		$rv = '
		<!-- multiTableRow -->
		<tr class="multi_table_row" '.$row_attr;
		if ( $istitle ) {
			$rv .=' align="center" ';
		}
		$rv .= '>';
		for ($c = 0; $c < count($cell_data); $c++) {
			$rv .='<td class="ff" ';
			for ($a=1; $a < count($cell_data[$c]); $a++) {
				$rv .= $cell_data[$c][$a].' ';
			}
			$rv .= '>';
			if ($istitle) {
				$rv .='<span class="titlebar">';
			}
			$rv .= $cell_data[$c][0];
			if ($istitle) {
				$rv .='</span>';
			}
			$rv .= '</td>';

		}
		$rv .= '</tr>
		<!-- end multiTableRow -->
		';

		return $rv;
	}

	/**
	 * feedback() - returns the htmlized feedback string when an action is performed.
	 *
	 * @param string feedback string
	 * @return string htmlized feedback
	 */
	function feedback($feedback) {
		if (!$feedback) {
			return '';
		} else {
			return '
			<h3 style="color:red">'.strip_tags($feedback, '<br>').'</h3>';
		}
	}

	/**
	 * warning_msg() - returns the htmlized warning string when an action is performed.
	 *
	 * @param string msg string
	 * @return string htmlized warning
	 */
	function warning_msg($msg) {
		if (!$msg) {
			return '';
		} else {
			return '
				<div class="warning_msg">'.strip_tags($msg, '<br>').'</div>';
		}
	}

	/**
	 * error_msg() - returns the htmlized error string when an action is performed.
	 *
	 * @param string msg string
	 * @return string htmlized error
	 */
	function error_msg($msg) {
		if (!$msg) {
			return '';
		} else {
			return '
				<div class="error">' .
			    util_pwrap(strip_tags($msg, '<br>')) .
			    "</div>\n";
		}
	}

	function confirmBox($msg, $params, $buttons, $image='*none*') {
		if ($image == '*none*') {
			$image = html_image('stop.png','48','48',array());
		}

		foreach ($params as $b => $v) {
			$prms[] = '<input type="hidden" name="'.$b.'" value="'.$v.'" />'."\n";
		}
		$prm = join('	 	', $prms);

		foreach ($buttons as $b => $v) {
			$btns[] = '<input type="submit" name="'.$b.'" value="'.$v.'" />'."\n";
		}
		$btn = join('	 	&nbsp;&nbsp;&nbsp;'."\n	 	", $btns);

		return '
			<div id="infobox" style="margin-top: 15%; margin-left: 15%; margin-right: 15%; text-align: center;">
			<table align="center">
			<tr>
			<td>'.$image.'</td>
			<td>'.$msg.'<br/></td>
			</tr>
			<tr>
			<td colspan="2" align="center">
			<br />
			<form action="' . getStringFromServer('PHP_SELF') . '" method="get" >
			'.$prm.'
			'.$btn.'
			</form>
			</td>
			</tr>
			</table>
			</div>
			';
	}

	function html_input($name, $id = '', $label = '', $type = 'text', $value = '', $extra_params = '') {
		if (!$id) {
			$id = $name;
		}
		$return = '<div class="field-holder">
			';
		if ($label) {
			$return .= '<label for="' . $id . '">' . $label . '</label>
				';
		}
		$return .= '<input id="' . $id . '" type="' . $type . '"';
		//if input is a submit then name is not present
		if ($name) {
			$return .= ' name="' . $name . '"';
		}
		if ($value) {
			$return .= ' value="' . $value . '"';
		}
		if (is_array($extra_params)) {
			foreach ($extra_params as $key => $extra_params_value) {
				$return .= $key . '="' . $extra_params_value . '" ';
			}
		}
		$return .= '/>
			</div>';
		return $return;
	}

	function html_checkbox($name, $value, $id = '', $label = '', $checked = '', $extra_params = '') {
		if (!$id) {
			$id = $name;
		}
		$return = '<div class="field-holder">
			';
		$return .= '<input name="' . $name . '" id="' . $id . '" type="checkbox" value="' . $value . '" ';
		if ($checked) {
			$return .= 'checked="checked" ';
		}
		if (is_array($extra_params)) {
			foreach ($extra_params as $key => $extra_params_value) {
				$return .= $key . '="' . $extra_params_value . '" ';
			}
		}
		$return .= '/>';
		if ($label) {
			$return .= '<label for="' . $id . '">' . $label . '</label>
				';
		}
		$return .= '</div>';
		return $return;
	}

	function html_text_input_img_submit($name, $img_src, $id = '', $label = '', $value = '', $img_title = '', $img_alt = '', $extra_params = '', $img_extra_params = '') {
		if (!$id) {
			$id = $name;
		}
		if (!$img_title) {
			$img_title = $name;
		}
		if (!$img_alt) {
			$img_alt = $img_title;
		}
		$return = '<div class="field-holder">
			';
		if ($label) {
			$return .= '<label for="' . $id . '">' . $label . '</label>
				';
		}
		$return .= '<input id="' . $id . '" type="text" name="' . $name . '"';
		if ($value) {
			$return .= ' value="' . $value . '"';
		}
		if (is_array($extra_params)) {
			foreach ($extra_params as $key => $extra_params_value) {
				$return .= $key . '="' . $extra_params_value . '" ';
			}
		}
		$return .= '/>
			<input type="image" id="' . $id . '_submit" src="/images/' . $img_src . '" alt="' . util_html_secure($img_alt) . '" title="' . util_html_secure($img_title) . '"';
		if (is_array($img_extra_params)) {
			foreach ($img_extra_params as $key => $img_extra_params_value) {
				$return .= $key . '="' . $img_extra_params_value . '" ';
			}
		}
		$return .= '/>
			</div>';
		return $return;
	}

	function html_select($vals, $name, $label = '', $id = '', $checked_val = '', $text_is_value = false, $extra_params = '') {
		if (!$id) {
			$id = $name;
		}
		$return = '<div class="field-holder">
			';
		if ($label) {
			$return .= '<label for="' . $id . '">' . $label . '</label>
				';
		}
		$return .= '<select name="' . $name . '" id="' . $id . '" ';
		if (is_array($extra_params)) {
			foreach ($extra_params as $key => $extra_params_value) {
				$return .= $key . '="' . $extra_params_value . '" ';
			}
		}
		$return .= '>';
		$rows = count($vals);
		for ($i = 0; $i < $rows; $i++) {
			if ( $text_is_value ) {
				$return .= '
					<option value="' . $vals[$i] . '"';
				if ($vals[$i] == $checked_val) {
					$return .= ' selected="selected"';
				}
			} else {
				$return .= '
					<option value="' . $i . '"';
				if ($i == $checked_val) {
					$return .= ' selected="selected"';
				}
			}
			$return .= '>' . htmlspecialchars($vals[$i]) . '</option>';
		}
		$return .= '
			</select>
			</div>';
		return $return;
	}

	function html_textarea($name, $id = '', $label = '', $value = '', $extra_params = '') {
		if (!$id) {
			$id = $name;
		}
		$return = '<div class="field-holder">
			';
		if ($label) {
			$return .= '<label for="' . $id . '">' . $label . '</label>
				';
		}
		$return .= '<textarea id="' . $id . '" name="' . $name . '" ';
		if (is_array($extra_params)) {
			foreach ($extra_params as $key => $extra_params_value) {
				$return .= $key . '="' . $extra_params_value . '" ';
			}
		}
		$return .= '>';
		if ($value) {
			$return .= $value;
		}
		$return .= '</textarea>
			</div>';
		return $return;
	}

	/**
	 * @todo use listTableTop and make this function deprecated ?
	 */
	function html_table_top($cols, $summary = '', $class = '', $extra_params = '') {
		$return = '<table summary="' . $summary . '" ';
		if ($class) {
			$return .= 'class="' . $class . '" ';
		}
		if (is_array($extra_params)) {
			foreach ($extra_params as $key => $extra_params_value) {
				$return .= $key . '="' . $extra_params_value . '" ';
			}
		}
		$return .= '>';
		$return .= '<thead><tr>';
		$nbCols = count($cols);
		for ($i = 0; $i < $nbCols; $i++) {
			$return .= '<th scope="col">' . $cols[$i] . '</th>';
		}
		$return .= '</tr></thead>';
		return $return;
	}

	function getMonitorOPic($on, $title='', $alt='') {
		return $this->getPicto($on ? 'ic/mail16w.png' :
		    'ic/xmail16w.png', $title, $alt);
	}

	function getReleaseNotesPic($title = '', $alt = '') {
		return $this->getPicto('ic/manual16c.png', $title, $alt, '15', '15');
	}

	/* no picto for download */
	function getDownloadPic($title = '', $alt = '') {
		return $this->getPicto('ic/save.png', $title, $alt, '24', '24');
	}

	function getHomePic($title = '', $alt = '') {
		return $this->getPicto('ic/home16b.png', $title, $alt);
	}

	function getFollowPic($title = '', $alt = '') {
		return $this->getPicto('ic/tracker20g.png', $title, $alt);
	}

	function getForumPic($title = '', $alt = '') {
		return $this->getPicto('ic/forum20g.png', $title, $alt);;
	}

	function getDocmanPic($title = '', $alt = '') {
		return $this->getPicto('ic/docman16b.png', $title, $alt);
	}

	function getMailPic($title = '', $alt = '') {
		return $this->getPicto('ic/mail16b.png', $title, $alt);
	}

	function getPmPic($title = '', $alt = '') {
		return $this->getPicto('ic/taskman20g.png', $title, $alt);
	}

	function getSurveyPic($title = '', $alt = '') {
		return $this->getPicto('ic/survey16b.png', $title, $alt);
	}

	function getScmPic($title = '', $alt = '') {
		return $this->getPicto('ic/cvs16b.png', $title, $alt);
	}

	function getFtpPic($title = '', $alt = '') {
		return $this->getPicto('ic/ftp16b.png', $title, $alt);
	}

	function getPicto($url,$title,$alt,$width='20',$height='20') {
		if (!$alt) {
			$alt = $title;
		}
		return html_image($url, $width, $height, array(
			'title' => $title,
			'alt' => $alt,
			'border' => 0,
		    ));
	}

	/**
	 * toSlug() - protect a string to be used as a link or an anchor
	 *
	 * @param   string $string  the string used as a link or an anchor
	 * @param   string $space   the caracter used as a replacement for a space
	 * @return  a protected string with only alphanumeric caracters
	 */
	function toSlug($string, $space = "-") {
		if (function_exists('iconv')) {
			$string = @iconv('UTF-8', 'ASCII//TRANSLIT', $string);
		}
		$string = preg_replace("/[^a-zA-Z0-9_:. -]/", "-", $string);
		$string = strtolower($string);
		$string = str_replace(" ", $space, $string);
		if (!preg_match("/^[a-zA-Z:_]/", $string)) {
			/* some chars aren't allowed at the begin */
			$string = "_" . $string;
		}
		return $string;
	}

	function widget(&$widget, $layout_id, $readonly, $column_id, $is_minimized, $display_preferences, $owner_id, $owner_type) {
		$element_id = 'widget_'. $widget->id .'-'. $widget->getInstanceId();
		echo '<div class="widget" id="' . $element_id . "\">\n";
		echo '<div class="widget_titlebar ' . ($readonly ? '' : 'widget_titlebar_handle') . "\">\n";
		echo '<div class="widget_titlebar_title">' . $widget->getTitle() . "</div>\n";
		if (!$readonly) {
			echo '<div class="widget_titlebar_close"><a href="/widgets/updatelayout.php?owner='. $owner_type.$owner_id .'&amp;action=widget&amp;name['. $widget->id .'][remove]='. $widget->getInstanceId() .'&amp;column_id='. $column_id .'&amp;layout_id='. $layout_id .'">'. $this->getPicto('ic/close.png', _('Remove'), 'Remove', '16', '16') . "</a></div>\n";
			if ($is_minimized) {
				echo '<div class="widget_titlebar_maximize"><a href="/widgets/updatelayout.php?owner='. $owner_type.$owner_id .'&amp;action=maximize&amp;name['. $widget->id .']='. $widget->getInstanceId() .'&amp;column_id='. $column_id .'&amp;layout_id='. $layout_id .'">'. $this->getPicto($this->_getTogglePlusForWidgets(), _('Maximise'), 'Maximise', '16', '16') . "</a></div>\n";
			} else {
				echo '<div class="widget_titlebar_minimize"><a href="/widgets/updatelayout.php?owner='. $owner_type.$owner_id .'&amp;action=minimize&amp;name['. $widget->id .']='. $widget->getInstanceId() .'&amp;column_id='. $column_id .'&amp;layout_id='. $layout_id .'">'. $this->getPicto($this->_getToggleMinusForWidgets(), _('Minimise'), 'Minimise', '16', '16') . "</a></div>\n";
			}
			if (strlen($widget->hasPreferences())) {
				echo '<div class="widget_titlebar_prefs"><a href="/widgets/updatelayout.php?owner='. $owner_type.$owner_id .'&amp;action=preferences&amp;name['. $widget->id .']='. $widget->getInstanceId() .'&amp;layout_id='. $layout_id .'">'. _('Preferences') . "</a></div>\n";
			}
		}
		if ($widget->hasRss()) {
			echo '<div class="widget_titlebar_rss"><a href="'.$widget->getRssUrl($owner_id, $owner_type) . "\">rss</a></div>\n";
		}
		echo "</div>\n";
		$style = '';
		if ($is_minimized) {
			$style = 'display:none;';
		}
		echo '<div class="widget_content" style="'. $style . "\">\n";
		if (!$readonly && $display_preferences) {
			echo '<div class="widget_preferences">'. $widget->getPreferencesForm($layout_id, $owner_id, $owner_type) . "</div>\n";
		}
		if ($widget->isAjax()) {
			echo '<div id="'. $element_id .'-ajax">';
			echo '<noscript><iframe sandbox="allow-same-origin" width="99%" frameborder="0" src="'. $widget->getIframeUrl($owner_id, $owner_type) .'"></iframe></noscript>';
			echo "</div>\n";
		} else {
			echo $widget->getContent();
		}
		echo "</div>\n";
		if ($widget->isAjax()) {
?>
			<script type="text/javascript"><!--//--><![CDATA[//><!--
			document.observe('dom:loaded', function () {
				$('<?php echo $element_id; ?>-ajax').update('<div style="text-align:center"><?php
			echo $this->getPicto('ic/spinner.gif', 'spinner', 'spinner', '10', '10');
				?></div>');
				new Ajax.Updater('<?php echo $element_id; ?>-ajax', '<?php
			echo $widget->getAjaxUrl($owner_id, $owner_type);
				?>');
			});
			//--><!]]></script>
<?php
		}
		echo "</div>\n";
	}

	function _getTogglePlusForWidgets() {
		return 'ic/toggle_plus.png';
	}

	function _getToggleMinusForWidgets() {
		return 'ic/toggle_minus.png';
	}

	/* Get the navigation links for the software map pages (trove,
	 * tag cloud, full project list) according to what's enabled
	 */
	function getSoftwareMapLinks() {
		$subMenuTitle = array();
		$subMenuUrl = array();
		$subMenuTip = array();

		if (forge_get_config('use_project_tags')) {
			$subMenuTitle[] = _('Tag cloud');
			$subMenuUrl[] = '/softwaremap/tag_cloud.php';
			$subMenuTip[] = _('Display all public projects by their tags');
		}

		if (forge_get_config('use_trove')) {
			$subMenuTitle[] = _('Project Tree');
			$subMenuUrl[] = '/softwaremap/trove_list.php';
			$subMenuTip[] = _('Display all public projects as trove tree');
		}

		if (forge_get_config('use_project_full_list')) {
			$subMenuTitle[] = _('Project List');
			$subMenuUrl[] = '/softwaremap/full_list.php';
			$subMenuTip[] = _('Display an alphabetically sorted list of all public projects');
		}

		return array($subMenuTitle, $subMenuUrl, $subMenuTip);
	}

	function disableForgebar($keephead=false, $keepfoot=false) {
		$this->emit_forgehead = $keephead;
		$this->emit_forgefoot = $keepfoot;
	}

	/* display the submenu, if it was passed */
	function handleSubmenu($params) {
		if (!isset($params['submenu'])) {
			return;
		}
		if (is_array($params['submenu'])) {
			$params['submenu'] = $this->beginSubMenu() .
			    $this->printSubMenu($params['submenu'][0],
			    $params['submenu'][1], $params['submenu'][2]) .
			    $this->endSubMenu();
		}
		if ($params['submenu']) {
			echo $params['submenu'];
		}
	}

	function displayStylesheetElements() {
		/* Codendi/Tuleap compatibility */
	}

}
