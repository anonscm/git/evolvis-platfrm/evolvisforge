<?php
/*-
 * edit with:
 *	jupp -tab 2 -indentc 32 -istep 2 -spaces Layout.style.php
 */

/*-
 * Evolvis Theme – style part
 *
 * Copyright © 2011, 2012, 2013, 2018, 2019
 *	mirabilos <t.glaser@tarent.de>
 * Copyright © 2022, 2023
 *	mirabilos <tg@mirbsd.de>
 * All rights reserved.
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *-
 * Small PHP class wrapper around mostly XHTML code, with callbacks into
 * the Theme class for more logic.
 */

class EvStyle {

  /* constructor */
  function __construct() {
    /* nothing to do at the moment */
  }


  /**
   * create HTML for one headlink
   *
   * @param   char    $colourkey
   *    (one-character string) b=black w=white s=red
   * @param   string  $tooltip
   *    string to display as tooltip; ignored if it begins with a dash
   * @param   string  $target
   *    link target URI, already HTML-encoded
   * @param   string  $text
   *    text to display as link
   * @return  string
   *    HTML code containing the headlink
   */
  function make_headlink($colourkey,$tooltip,$target,$text) {
    $csscls = 'headlink' . $colourkey[0];
    if (substr($target, 0, 29) === '/plugins/extratabs/iframe.php')
      $csscls .= ' vextlink';
    if (substr($target, 0, 4) != 'http') {
      $target = util_make_url($target);
    }
    $tooltip = util_html_secure(util_ifsetor($tooltip,""));
    if ($tooltip && $tooltip[0] != '-') {
      $tooltip = ' title="' . $tooltip . '"';
    } else {
      $tooltip = "";
    }
    return '            <span class="headblock"' . $tooltip . '>
              <span class="headlinks">//</span>
              <a href="' . $target . '" class="' .
      $csscls . '">' . util_html_secure($text) . '</a>
            </span><span class="zerowidth">&#8203;</span>' . "\n";
  }


  /**
   * emit HTML for xml, doctype, head until end, body tag
   *
   * @param   Layout  $T
   *    global Theme instance to use
   * @param   array   $p
   *    associative; parameters for this function:
   *    - string  doctype
   *    - string  xmlns
   *    - string  lang
   *    - string  title
   * @param   string  $cb
   *    callback function (name of method of $T),
   *    called as $T->$cb($T)
   * @return  void
   */
  function emit_head($T,$p,$cb) {
    global $_SERVER;

    echo $p['doctype'] .
     "\n<html " . $p['xmlns'] . ' xml:lang="en" lang="' .
     $p['lang'] . "\">\n<head>";
?>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?php echo util_html_secure($p['title']); ?></title>
<?php
    call_user_func(array($T, $cb), $T);
    echo "</head><body>\n";
  }


  /**
   * emit HTML for the first div inside of body tag
   *
   * @param   Layout  $T
   *    global Theme instance to use
   * @param   array   $p
   *    associative; parameters for this function:
   *    - array   globalmenu
   *      indexed; subtype: indexed array of (tooltip;link;text;colourkey=b)
   *    - string  outertabs
   *    - string  searchbox
   *    - int     group_id (0 if not in a group)
   *    - string  group_label
   *    - string  group_link
   *    - string  projecttabs
   *    - string  submenu
   * @return  void
   */
  function emit_bodyheader($T,$p) {

    /* construct globalmenu string */
    $gm = '';
    foreach ($p['globalmenu'] as $v) {
      $gm .= $this->make_headlink(util_ifsetor($v[3], 'b'), $v[0], $v[1], $v[2]);
    }

    /* construct submenu string */
    $sm = rtrim($p['submenu']);

    /* emit HTML code */
?>
    <div id="ev_header" class="hidePartForPrinting">
      <div id="ev_pagetop">
        <table id="ev_pagetop0" width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr id="ev_pagetop1r">
            <td id="ev_pagetop1c" colspan="2" align="right" valign="top">
              <div id="ev_toplogo"><a href="<?php echo util_make_url("/"); ?>"><img
               src="<?php echo util_make_url("/images/logo_internal_repo.png"); ?>"
               alt="Evolvis Repository" border="0" /></a></div>
              <div id="ev_english"><a href="<?php
    echo util_make_url("/account/presetlang.php"); ?>"><img
               src="<?php echo util_make_url("/images/deutsch.png"); ?>"
               title="Schalte auf deutsche Texte um" style="margin-right:1px;"
               alt="German Language flag" border="0" height="18" width="27" /><img
               src="<?php echo util_make_url("/images/english.png"); ?>"
               title="Switch the site to English language texts"
               alt="English Language flag" border="0" height="18" width="27" /></a></div>
              <span id="ev_globalmenu">
<?php
    echo rtrim($gm);
?>
              </span><?php /* id="ev_globalmenu" */ ?>
            </td>
          </tr><?php /* id="ev_pagetop1r" */ ?>
          <tr id="ev_pagetop2r">
            <td id="ev_outertabs" align="left" valign="top">
<?php
    echo rtrim($p['outertabs']);
?>
            </td>
            <td id="ev_globalswitch" align="right" valign="bottom">
              <form action="<?php echo util_make_url("/pm/t_follow.php"); ?>"
               method="get">
                <input type="text" size="8" name="tid" />
                <input type="submit" name="j" value="<?php echo _("Jump to Task"); ?>" />
              </form>
            </td>
          </tr><?php /* id="ev_pagetop2r" */ ?>
          <tr id="ev_pagetop3r">
            <td id="ev_pagetop3c" colspan="2" align="right" valign="top">
              <?php echo html_e('a', array(
                "href" => $p['group_link'],
                "id" => "ev_projectname",
               ), util_html_secure($p['group_label'])); ?>
              <div id="ev_globalsearch">
<?php
    echo rtrim($p['searchbox']);
?>
              </div>
            </td>
          </tr><?php /* id="ev_pagetop3r" */ ?>
          <tr id="ev_pagetop4r">
            <td id="ev_projecttabs" colspan="2" align="left" valign="top">
<?php
    echo rtrim($p['projecttabs']);
?>
            </td>
          </tr><?php /* id="ev_pagetop4r" */ ?>
        </table><?php /* id="ev_pagetop0" */ ?>
      </div><?php /* id="ev_pagetop" */ ?>
<?php
    /* display submenu div only if it has actual content */
    if ($sm) {
?>
      <div id="ev_submenu">
<?php
      echo $sm;
?>
      </div><?php /* id="ev_submenu" */ ?>
<?php
    }
    /* this is moste annoying */
    foreach (array(
      'error_msg' => 'ev_gvmsg_error',
      'warning_msg' => 'ev_gvmsg_warning',
      'feedback' => 'ev_gvmsg_feedback',
    ) as $gv => $style) {
      $v = trim(util_ifsetor($GLOBALS[$gv], ""));
      if ($v) {
        /* ugly: $v can have embedded <p> and <br /> tags… */
        echo '      <div id="' . $style . '">' . nl2br($v) . "</div>\n";
      }
    }
?>
      <div class="ev_redbar"></div>
    </div><?php /* id="ev_header" */ ?>
<?php
  }


  /**
   * emit HTML for the last div inside of body tag; run footer_hooks
   *
   * @param   Layout  $T
   *    global Theme instance to use
   * @param   array   $p
   *    associative; parameters for this function:
   *    - bool    showfooter
   * @return  void
   */
  function emit_bodyfooter($T,$p) {
    if ($p['showfooter']) {
?>
    <div id="ev_footer" class="hidePartForPrinting">
      <span style="float:left"><a
        href="<?php echo util_make_url("/pvp.php"); ?>">Datenschutzerklärung / privacy policy</a>
      </span>
      <span style="float:right"><a
        href="<?php echo util_make_url("/impress/"); ?>">Impressum / imprint</a>
      </span>
<?php
    }
    foreach ($T->footer_hooks as $h) {
      call_user_func($h, $p, $T);
    }
    if ($p['showfooter']) {
?>
      <br clear="all" />
    </div>
<?php
    }
  }


}
