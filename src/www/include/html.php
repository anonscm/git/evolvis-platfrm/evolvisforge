<?php
/**
 * Misc HTML functions
 *
 * Copyright 1999-2001 (c) VA Linux Systems
 * Copyright 2010 (c) FusionForge Team
 * Copyright (C) 2010 Alain Peyrat - Alcatel-Lucent
 * Copyright © 2011, 2012
 *	Thorsten “mirabilos” Glaser <t.glaser@tarent.de>
 * Copyright © 2023
 *	mirabilos <tg@mirbsd.de>
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

require_once $gfwww.'include/help.php';

/**
 * html_generic_fileheader() - Output <html><head> and <meta/> inside.
 *
 * @param	string	$title
 *			Mandatory content of <title> attribute,
 *			will be HTML-secured
 */
function html_generic_fileheader($title) {
	global $HTML, $sysDTDs, $sysXMLNSs;

	if (!$title) {
		throw new Exception('A title is mandatory in XHTML!');
	}

	echo $sysDTDs['transitional']['doctype'] . "\n<html " . $sysXMLNSs .
	    ' xml:lang="en"' . ">\n<head>";
	echo '<meta http-equiv="Content-Type" ' .
	    'content="text/html; charset=utf-8" />' . "\n";
	echo '<script type="text/javascript"><!--//--><![CDATA[//><!--' .
	    "\n\tvar sys_url_base = " . minijson_encode(util_make_url("/"),
	    false) . ";\n" .
	    "//--><!]]></script>\n";
	$HTML->headerForgepluckerMeta();
	echo html_e('title', array(), util_html_secure($title)) . "\n";
}


/**
 * html_feedback_top() - Show the feedback output at the top of the page.
 *
 * @param		string	The feedback.
 */
function html_feedback_top($feedback) {
	global $HTML;
	echo $HTML->feedback($feedback);
}

/**
 * html_warning_top() - Show the warning output at the top of the page.
 *
 * @param		string	The warning message.
 */
function html_warning_top($msg) {
	global $HTML;
	echo $HTML->warning_msg($msg);
}

/**
 * html_error_top() - Show the error output at the top of the page.
 *
 * @param		string	The error message.
 */
function html_error_top($msg) {
	global $HTML;
	echo $HTML->error_msg($msg);
}

/**
 * make_user_link() - Make a username reference into a link to that users User page on SF.
 *
 * @param		string	The username of the user to link.
 */
function make_user_link($username,$displayname='') {
    if (empty($displayname))
        $displayname = $username;

	if (!strcasecmp($username,'Nobody') || !strcasecmp($username,'None')) {
		return $username;
	} else {
		return '<a href="/users/'.$username.'">'.$displayname.'</a>' ;
	}
}

/**
 * html_feedback_bottom() - Show the feedback output at the bottom of the page.
 *
 * @param		string	The feedback.
 */
function html_feedback_bottom($feedback) {
	global $HTML;
	echo $HTML->feedback($feedback);
}

/**
 * html_blankimage() - Show the blank spacer image.
 *
 * @param		int		The height of the image
 * @param		int		The width of the image
 */
function html_blankimage($height,$width) {
	return html_e('img', array(
		'src' => '/images/blank.png',
		'width' => $width,
		'height' => $height,
		'alt' => "",
	    ));
}

/**
 * html_abs_image() - Show an image given an absolute URL.
 *
 * @param		string	URL
 * @param		int	width of the image
 * @param		int 	height of the image
 * @param		array	Any <img> tag parameters (i.e. 'border', 'alt', etc...)
 */
function html_abs_image($url, $width, $height, $args) {
	$args['src'] = $url;
	// Add image dimensions (if given)
	if ($width) {
		$args['width'] = $width;
	}
	if ($height) {
		$args['height'] = $height;
	}
	$args['alt'] = "" . util_ifsetor($args['alt'], "");

	return html_e('img', $args);
}

/**
 * html_image() - Build an image tag of an image contained in $src
 *
 * @param		string	The source location of the image
 * @param		int		The width of the image
 * @param		int		The height of the image
 * @param		array	Any IMG tag parameters associated with this image (i.e. 'border', 'alt', etc...)
 * @param		bool	DEPRECATED
 */
function html_image($src,$width='',$height='',$args=array(),$display=1) {
	global $HTML;

	if (method_exists($HTML, 'html_image')) {
		$HTML->html_image($src, $width, $height, $args);
	}
	$s = ((session_issecure()) ? forge_get_config('images_secure_url') : forge_get_config('images_url') );
	return html_abs_image($s . '/images/' . $src, $width, $height, $args);
}

/**
 * html_get_ccode_popup() - Pop up box of supported country_codes.
 *
 * @param		string	The title of the popup box.
 * @param		string	Which element of the box is to be selected.
 * @return	string	The html select box.
 */
function html_get_ccode_popup ($title='ccode',$selected='xzxz') {
	$res=db_query_params ('SELECT ccode,country_name FROM country_code ORDER BY country_name',
			array());
	return html_build_select_box ($res,$title,$selected,false);
}

/**
 * html_get_timezone_popup() - Pop up box of supported Timezones.
 * Assumes you have included Timezones array file.
 *
 * @param		string	The title of the popup box.
 * @param		string	Which element of the box is to be selected.
 * @return	string	The html select box.
 */
function html_get_timezone_popup ($title='timezone',$selected='xzxz') {
	global $TZs;
	if ($selected == 'xzxzxzx') {
	  $r = file ('/etc/timezone');
	  $selected = str_replace ("\n", '', $r[0]);
	}
	return html_build_select_box_from_arrays ($TZs,$TZs,$title,$selected,false);
}


/**
 * html_build_select_box_from_assoc() - Takes one assoc array and returns a pop-up box.
 *
 * @param	array	An array of items to use.
 * @param	string	The name you want assigned to this form element.
 * @param	string	The value of the item that should be checked.
 * @param	boolean	Whether we should swap the keys / names.
 * @param	bool	Whether or not to show the '100 row'.
 * @param	string	What to call the '100 row' defaults to none.
 */
function html_build_select_box_from_assoc ($arr,$select_name,$checked_val='xzxz',$swap=false,$show_100=false,$text_100='None') {
	if ($swap) {
		$keys=array_values($arr);
		$vals=array_keys($arr);
	} else {
		$vals=array_values($arr);
		$keys=array_keys($arr);
	}
	return html_build_select_box_from_arrays ($keys,$vals,$select_name,$checked_val,$show_100,$text_100);
}

/**
 * html_build_select_box_from_array() - Takes one array, with the first array being the "id"
 * or value and the array being the text you want displayed.
 *
 * @param	array	An array of items to use.
 * @param	string	The name you want assigned to this form element.
 * @param	string	The value of the item that should be checked.
 */
function html_build_select_box_from_array ($vals,$select_name,$checked_val='xzxz',$samevals = 0) {
	$return = '
		<select name="'.$select_name.'">';

	$rows=count($vals);

	for ($i=0; $i<$rows; $i++) {
		if ( $samevals ) {
			$return .= "\n\t\t<option value=\"" . $vals[$i] . "\"";
			if ($vals[$i] == $checked_val) {
				$return .= ' selected="selected"';
			}
		} else {
			$return .= "\n\t\t<option value=\"" . $i .'"';
			if ($i == $checked_val) {
				$return .= ' selected="selected"';
			}
		}
		$return .= '>'.htmlspecialchars($vals[$i]).'</option>';
	}
	$return .= '
		</select>';

	return $return;
}

/**
 * html_build_radio_buttons_from_arrays() - Takes two arrays, with the first array being the "id" or value and the other
 * array being the text you want displayed.
 *
 * The infamous '100 row' has to do with the SQL Table joins done throughout all this code.
 * There must be a related row in users, categories, et	, and by default that
 * row is 100, so almost every pop-up box has 100 as the default
 * Most tables in the database should therefore have a row with an id of 100 in it so that joins are successful
 *
 * @param		array	The ID or value
 * @param		array	Text to be displayed
 * @param		string	Name to assign to this form element
 * @param		string	The item that should be checked
 * @param		bool	Whether or not to show the '100 row'
 * @param		string	What to call the '100 row' defaults to none
 * @param		bool	Whether or not to show the 'Any row'
 * @param		string	What to call the 'Any row' defaults to any
 */
function html_build_radio_buttons_from_arrays ($vals,$texts,$select_name,$checked_val='xzxz',$show_100=true,$text_100='none',$show_any=false,$text_any='any') {
	if ($text_100=='none'){
		$text_100=_('None');
	}
	$return = '';

	$rows=count($vals);
	if (count($texts) != $rows) {
		$return .= 'ERROR - uneven row counts';
	}

	//we don't always want the default Any row shown
	if ($show_any) {
		$return .= '
		<input type="radio" name="'.$select_name.'" value=""'.(($checked_val=='') ? ' checked="checked"' : '').' />&nbsp;'. $text_any .'<br />';
	}
	//we don't always want the default 100 row shown
	if ($show_100) {
		$return .= '
		<input type="radio" name="'.$select_name.'" value="100"'.(($checked_val==100) ? ' checked="checked"' : '').' />&nbsp;'. $text_100 .'<br />';
	}

	$checked_found=false;

	for ($i=0; $i<$rows; $i++) {
		//  uggh - sorry - don't show the 100 row
		//  if it was shown above, otherwise do show it
		if (($vals[$i] != '100') || ($vals[$i] == '100' && !$show_100)) {
			$return .= '
				<input type="radio" name="'.$select_name.'" value="'.$vals[$i].'"';
			if ((string)$vals[$i] == (string)$checked_val) {
				$checked_found=true;
				$return .= ' checked="checked"';
			}
			$return .= ' />&nbsp;'.htmlspecialchars($texts[$i]).'<br />';
		}
	}
	//
	//	If the passed in "checked value" was never "SELECTED"
	//	we want to preserve that value UNLESS that value was 'xzxz', the default value
	//
	if (!$checked_found && $checked_val != 'xzxz' && $checked_val && $checked_val != 100) {
		$return .= '
		<input type="radio" value="'.$checked_val.'" checked="checked" />&nbsp;'._('No Change').'<br />';
	}

	return $return;
}

function html_use_jquery() {
	use_javascript('/scripts/jquery/jquery.js');
	use_javascript_snippet('
		/*
		 * switch jQuery into no conflict mode, i.e. do not let
		 * it possess the dollar sign function; according to the
		 * docs, this must come after both jQuery and Prototype
		 * have been loaded, not before (why?) but apparently it
		 * should come immediately after jQuery…
		 */

		jQuery.noConflict();
	    ');
}

function html_use_tooltips($defaults=false) {
	global $gfcommon, $HTML;

	if ($defaults === false) {
		$defaults = array(
			'html' => true,
			'delayIn' => 1000,
			'delayOut' => 500,
			'fade' => true,
		    );
	}
	$HTML->extra_js[] = 'var tooltip_defaults = ' .
	    minijson_encode($defaults, false) . ';';

	html_use_jquery();
	use_javascript('/js/tooltips.js');
}

function html_use_jqueryui() {
	html_use_jquery();
	use_javascript('/scripts/jquery-ui/jquery-ui.js');
	use_stylesheet('/scripts/jquery-ui/css/smoothness/jquery-ui.css');
}

function html_use_lightbox() {
	/* Prototype is always loaded */
	/* script.aculo.us is always loaded completely */
	use_javascript('/js/lightbox.js');
}

/**
 * html_build_select_box_from_arrays() - Takes two arrays, with the first array being the "id" or value and the other
 * array being the text you want displayed.
 *
 * The infamous '100 row' has to do with the SQL Table joins done throughout all this code.
 * There must be a related row in users, categories, et	, and by default that
 * row is 100, so almost every pop-up box has 100 as the default
 * Most tables in the database should therefore have a row with an id of 100 in it so that joins are successful
 *
 * @param		array	The ID or value
 * @param		array	Text to be displayed
 * @param		string	Name to assign to this form element
 * @param		string	The item that should be checked
 * @param		bool	Whether or not to show the '100 row'
 * @param		string	What to call the '100 row' defaults to none
 * @param		bool	Whether or not to show the 'Any row'
 * @param		string	What to call the 'Any row' defaults to any
 * @param		array	Array of all allowed values from the full list.
 * @param		string	Override id attribute (default: false)
 * @param		string	Override title attribute (default: false)
 */
function html_build_select_box_from_arrays ($vals,$texts,$select_name,$checked_val='xzxz',$show_100=true,$text_100='none',$show_any=false,$text_any='any',$allowed=false,$override_id=false,$override_title=false) {
	$have_a_subelement = false;

	if ($text_100=='none'){
		$text_100=_('None');
	}
	$return = '';

	$rows=count($vals);
	if (count($texts) != $rows) {
		$return .= 'ERROR - uneven row counts';
	}

	$title = html_get_tooltip_description($select_name);
	$id = '';
	if ($title) {
		$id = ' id="tracker-'.$select_name.'"';
		if (preg_match('/\[\]/', $id)) {
			$id = '';
		}
	}

	if ($override_id !== false) {
		$id = ' id="' . $override_id . '"';
	}
	if ($override_title !== false) {
		$title = $override_title;
	}

	$return .= '
		<select'.$id.' name="'.$select_name.'" title="'.util_html_secure($title).'">';

	//we don't always want the default Any row shown
	if ($show_any) {
		$return .= '
		<option value=""'.(($checked_val=='') ? ' selected="selected"' : '').'>'. util_html_secure($text_any) .'</option>';
		$have_a_subelement = true;
	}
	//we don't always want the default 100 row shown
	if ($show_100) {
		$return .= '
		<option value="100"'.(($checked_val==100) ? ' selected="selected"' : '').'>'. util_html_secure($text_100) .'</option>';
		$have_a_subelement = true;
	}

	$checked_found=false;

	for ($i=0; $i<$rows; $i++) {
		//  uggh - sorry - don't show the 100 row
		//  if it was shown above, otherwise do show it
		if (($vals[$i] != '100') || ($vals[$i] == '100' && !$show_100)) {
			$return .= '
				<option value="'.util_html_secure($vals[$i]).'"';
			if ((string)$vals[$i] == (string)$checked_val) {
				$checked_found=true;
				$return .= ' selected="selected"';
			}
			if (is_array($allowed) && !in_array($vals[$i], $allowed)) {
				$return .= ' disabled="disabled" class="option_disabled"';
			}
			$return .= '>'.util_html_secure($texts[$i]).'</option>';
			$have_a_subelement = true;
		}
	}
	//
	//	If the passed in "checked value" was never "SELECTED"
	//	we want to preserve that value UNLESS that value was 'xzxz', the default value
	//
	if (!$checked_found && $checked_val != 'xzxz' && $checked_val && $checked_val != 100) {
		$return .= '
		<option value="'.util_html_secure($checked_val).'" selected="selected">'._('No Change').'</option>';
		$have_a_subelement = true;
	}

	if (!$have_a_subelement) {
		/* <select></select> without <option/> in between is invalid */
		return '<!-- select without options -->';
	}

	$return .= '
		</select>';
	return $return;
}

/**
 * html_build_select_box() - Takes a result set, with the first column being the "id" or value and
 * the second column being the text you want displayed.
 *
 * @param		int		The result set
 * @param		string	Text to be displayed
 * @param		string	The item that should be checked
 * @param		bool	Whether or not to show the '100 row'
 * @param		string	What to call the '100 row'.  Defaults to none.
 */
function html_build_select_box ($result, $name, $checked_val="xzxz",$show_100=true,$text_100='none',$show_any=false,$text_any='Select One') {
	if ($text_100=='none'){
		$text_100=_('None');
	}
	return html_build_select_box_from_arrays (util_result_column_to_array($result,0),util_result_column_to_array($result,1),$name,$checked_val,$show_100,$text_100, $show_any, $text_any);
}

/**
 * html_build_select_box_sorted() - Takes a result set, with the first column being the "id" or value and
 * the second column being the text you want displayed.
 *
 * @param		int		The result set
 * @param		string	Text to be displayed
 * @param		string	The item that should be checked
 * @param		bool	Whether or not to show the '100 row'
 * @param		string	What to call the '100 row'.  Defaults to none.
 */
function html_build_select_box_sorted ($result, $name, $checked_val="xzxz",$show_100=true,$text_100='none') {
	if ($text_100=='none'){
		$text_100=_('None');
	}
	$vals = util_result_column_to_array($result, 0);
	$texts = util_result_column_to_array($result, 1);
	array_multisort($texts, SORT_ASC, SORT_STRING,
	                $vals);
	return html_build_select_box_from_arrays ($vals, $texts, $name, $checked_val, $show_100, $text_100);
}

/**
 * html_build_multiple_select_box() - Takes a result set, with the first column being the "id" or value
 * and the second column being the text you want displayed.
 *
 * @param		int	The result set
 * @param		string	Text to be displayed
 * @param		string	The item that should be checked
 * @param		int		The size of this box
 * @param		bool	Whether or not to show the '100 row'
 */
function html_build_multiple_select_box ($result,$name,$checked_array,$size='8',$show_100=true) {
	$checked_count=count($checked_array);
	$return = '
		<select name="'.$name.'" multiple="multiple" size="'.$size.'">';
	if ($show_100) {
		/*
			Put in the default NONE box
		*/
		$return .= '
		<option value="100"';
		for ($j=0; $j<$checked_count; $j++) {
			if ($checked_array[$j] == '100') {
				$return .= ' selected="selected"';
			}
		}
		$return .= '>'._('None').'</option>';
	}

	$rows=db_numrows($result);
	for ($i=0; $i<$rows; $i++) {
		if ((db_result($result,$i,0) != '100') || (db_result($result,$i,0) == '100' && !$show_100)) {
			$return .= '
				<option value="'.db_result($result,$i,0).'"';
			/*
				Determine if it's checked
			*/
			$val=db_result($result,$i,0);
			for ($j=0; $j<$checked_count; $j++) {
				if ($val == $checked_array[$j]) {
					$return .= ' selected="selected"';
				}
			}
			$return .= '>'. substr(db_result($result,$i,1),0,35). '</option>';
		}
	}
	$return .= '
		</select>';
	return $return;
}

/**
 * html_build_multiple_select_box_from_arrays() - Takes two arrays and builds a multi-select box
 *
 * @param		array	id of the field
 * @param		array	Text to be displayed
 * @param		string	id of the items selected
 * @param		string	The item that should be checked
 * @param		int		The size of this box
 * @param		bool	Whether or not to show the '100 row'
 */
function html_build_multiple_select_box_from_arrays($ids,$texts,$name,$checked_array,$size='8',$show_100=true,$text_100='none') {
	$checked_count=count($checked_array);
	$return ='
		<select name="'.$name.'" multiple="multiple" size="'.$size.'">';
	if ($show_100) {
		if ($text_100=='none') {
			$text_100=_('None');
		}
		/*
			Put in the default NONE box
		*/
		$return .= '
		<option value="100"';
		for ($j=0; $j<$checked_count; $j++) {
			if ($checked_array[$j] == '100') {
				$return .= ' selected="selected"';
			}
		}
		$return .= '>'.$text_100.'</option>';
	}

	$rows=count($ids);
	for ($i=0; $i<$rows; $i++) {
		if (( $ids[$i] != '100') || ($ids[$i] == '100' && !$show_100)) {
			$return .='
				<option value="'.$ids[$i].'"';
			/*
				Determine if it's checked
			*/
			$val=$ids[$i];
			for ($j=0; $j<$checked_count; $j++) {
				if ($val == $checked_array[$j]) {
					$return .= ' selected="selected"';
				}
			}
			$return .= '>'.$texts[$i].' </option>';
		}
	}
	$return .= '
		</select>';
	return $return;
}

function html_build_multiple_checkbox($result, $name, $arr2, $show_100=true, $text_100='none', $tagId='default') {
	if ($text_100 == 'none') {
		$text_100 = _('None');
	}
	return html_build_multiple_checkbox_from_arrays(util_result_column_to_array($result,0), util_result_column_to_array($result,1), $name, $arr2, $show_100, $text_100, $tagId);
}

/**
 * html_build_multiple_checkbox_from_arrays() - Takes two arrays and builds
 * checkboxes with the same name
 *
 * @param	array	$values of the checkboxes
 * @param	array	$texts of the checkboxes
 * @param	string	$name of the checkbox
 * @param	array	$arrCheckedValues of all checked checkboxes
 * @param	bool	$show_100 true if values 100
 * @param	string	$text_100
 */
function html_build_multiple_checkbox_from_arrays($values, $texts, $name, $checked_array, $show_100=true, $text_100='none', $tagId='default') {
	$checkboxes = '';
	if ($show_100) {
		$ischecked = false;
		for ($i = 0; $i < count($values); $i++) {
			if (in_array($values[$i], $checked_array)) {
				$ischecked = true;
				break;
			}
		}
		$checkboxes .= html_e('div', array(
			'class' => array(
				'checkBoxInnerLabel',
			    ),
		    ), html_e('label', array(
			'for' => $tagId . "-0",
		    ), html_e('input', array(
			'id' => $tagId . "-0",
			'type' => 'checkbox',
			'name' => $name,
			'value' => 100,
			'onclick' => 'checkboxHandle100(' .
			    minijson_encode($name, false) . ', 0)',
			'checked' => $ischecked ? false : 'checked',
		    )) . $text_100)) . "\n";
	}

	for ($i = 0; $i < count($values); $i++) {
		$checkboxes .= html_e('div', array(
			'class' => array(
				'checkBoxInnerLabel',
			    ),
		    ), html_e('label', array(
			'for' => $tagId . "-" . ($i + 1),
		    ), html_e('input', array(
			'id' => $tagId . "-" . ($i + 1),
			'type' => 'checkbox',
			'name' => $name,
			'value' => $values[$i],
			'onclick' => 'checkboxHandle100(' .
			    minijson_encode($name, false) . ', ' .
			    ($i + 1) . ')',
			'checked' => (in_array($values[$i], $checked_array)) ?
			    'checked' : false,
		    )) . $texts[$i])) . "\n";
	}

	return $checkboxes;
}

/**
 *	html_build_checkbox() - Render checkbox control
 *
 *	@param name - name of control
 *	@param value - value of control
 *	@param checked - true if control should be checked
 *	@return html code for checkbox control
 */
function html_build_checkbox($name, $value, $checked) {
	return html_e('input', array(
		'type' => 'checkbox',
		'name' => $name,
		'value' => $value,
		'checked' => ($checked ? 'checked' : false),
	    ));
}

/**
 * build_priority_select_box() - Wrapper for html_build_priority_select_box()
 *
 * @see html_build_priority_select_box()
 */
function build_priority_select_box ($name='priority', $checked_val='3', $nochange=false) {
	echo html_build_priority_select_box ($name, $checked_val, $nochange);
}

/**
 * html_build_priority_select_box() - Return a select box of standard priorities.
 * The name of this select box is optional and so is the default checked value.
 *
 * @param		string	Name of the select box
 * @param		string	The value to be checked
 * @param		bool	Whether to make 'No Change' selected.
 */
function html_build_priority_select_box ($name='priority', $checked_val='3', $nochange=false) {
	$rv = '<select id="tracker-' . $name . '" name="' . $name . '" title="' .
	    util_html_secure(html_get_tooltip_description($name)) . '">' . "\n";
	if ($nochange) {
		$rv .= '<option value="100"';
		/*XXX this comparison is redundant; sure it was meant? */
		if ($nochange) {
			$rv .= ' selected="selected"';
		}
		/*XXX should it be selected at all, anyway? -mirabilos */
		$rv .= '>' . _('No Change') . "</option>\n";
	}
	$vals = array(
		'1' => '1 - ' . _('Lowest'),
		'2' => '2',
		'3' => '3',
		'4' => '4',
		'5' => '5 - ' . _('Highest'),
	    );
	foreach ($vals as $k => $v) {
		$rv .= '<option value="' . $k . '"';
		if ($checked_val == $k) {
			$rv .= ' selected="selected"';
		}
		$rv .= '>' . $v . "</option>\n";
	}
	$rv .= "</select>\n";
	return $rv;
}

/**
 * html_buildcheckboxarray() - Build an HTML checkbox array.
 *
 * @param		array	Options array
 * @param		name	Checkbox name
 * @param		array	Array of boxes to be pre-checked
 */
function html_buildcheckboxarray($options,$name,$checked_array) {
	$option_count=count($options);
	$checked_count=count($checked_array);

	for ($i=1; $i<=$option_count; $i++) {
		echo '
			<br /><input type="checkbox" name="'.$name.'" value="'.$i.'"';
		for ($j=0; $j<$checked_count; $j++) {
			if ($i == $checked_array[$j]) {
				echo ' checked="checked"';
			}
		}
		echo ' /> '.$options[$i];
	}
}

/**
 *	site_header() - everything required to handle security and
 *	add navigation for user pages like /my/ and /account/
 *
 *	@param		array	Must contain $user_id
 */
function site_header($params) {
	GLOBAL $HTML;
	/*
		Check to see if active user
		Check to see if logged in
	*/
	echo $HTML->header($params);
}

/**
 * site_footer() - Show the HTML site footer.
 *
 * @param		array	Footer params array
 */
function site_footer($params) {
	global $HTML;
	$HTML->footer($params);
}

/**
 *	site_project_header() - everything required to handle
 *	security and state checks for a project web page
 *
 *	@param params array() must contain $toptab and $group
 */
function site_project_header($params) {
	global $HTML;

	/*
		Check to see if active
		Check to see if project rather than foundry
		Check to see if private (if private check if user_ismember)
	*/

	$group_id=$params['group'];

	//get the project object
	$project = group_get_object($group_id);
	if ($project && !is_object($project)) {
		$project = false;
	}
	if (!$project || $project->isError()) {
		/* prevent information leak */
		if (!session_get_user()) {
			$next = '/account/login.php?error_msg=' .
			    urlencode('You must log in to access a project.');
			if (getStringFromServer('REQUEST_METHOD') != 'POST') {
				$next .= '&return_to=' .
				    urlencode(getStringFromServer('REQUEST_URI'));
			}
			session_redirect($next);
			/* NOTREACHED */
		}
		if (!$project) {
			exit_error(sprintf(_('Could not access the project #%d'),
			    $group_id), 'home');
			/* NOTREACHED */
		}
		exit_error(sprintf(_('Error accessing the project #%d: %s'),
		    $group_id, $project->getErrorMessage()), 'home');
	}

	// Check permissions in case of restricted access
	session_require_perm ('project_read', $group_id);

	//for dead projects must be member of admin project
	if (!$project->isActive()) {
		session_require_global_perm ('forge_admin');
	}

	if (isset($params['title'])){
		$h1=$params['title'];
		$params['title']=$project->getPublicName().': '.$params['title'];
	} else {
		$h1=$project->getPublicName();
		$params['title']=$project->getPublicName();
	}
	if (!isset($params['h1'])){
		$params['h1'] = $h1;
	}

	site_header($params);
}

/**
 *	site_project_footer() - currently a simple shim
 *	that should be on every project page,  rather than
 *	a direct call to site_footer() or theme_footer()
 *
 *	@param params array() empty
 */
function site_project_footer($params) {
	site_footer($params);
}

/**
 *	site_user_header() - everything required to handle security and
 *	add navigation for user pages like /my/ and /account/
 *
 *	@param params array() must contain $user_id
 */
function site_user_header($params) {
	GLOBAL $HTML;

	/*
		Check to see if active user
		Check to see if logged in
	*/
	$arr_t = array() ;
	$arr_l = array() ;
	$arr_p = array() ;

	$arr_t[] = _('My Personal Page') ;
	$arr_l[] = '/my/' ;
	$arr_p[] = _('Manage projects, assigned tasks, bugs, etc.') ;

	if (($layout_id = util_ifsetor($params['layout_id']))) {
		$baseurl = '/widgets/widgets.php?owner=u' .
		    user_getid() . '&amp;layout_id=' . $layout_id;

		$arr_t[] = _("Add widgets");
		$arr_l[] = $baseurl;
		$arr_p[] = _('Select widgets to add to the User Summary page from a list of available widgets');

		$arr_t[] = _("Customise Layout");
		$arr_l[] = $baseurl . '&amp;update=layout';
		$arr_p[] = _('Customise number and sizes of columns to use for widgets');
	}

	$arr_t[] = _('Trackers dashboard') ;
	$arr_l[] = '/my/dashboard.php' ;
	$arr_p[] = _('Have an overview about all tracker items related to you') ;

	if (forge_get_config('use_diary')) {
		$arr_t[] = _('Diary &amp; Notes') ;
		$arr_l[] = '/my/diary.php' ;
		$arr_p[] = _('-tooltip:diary-and-notes') ;
	}

	$arr_t[] = _('Account Maintenance') ;
	$arr_l[] = '/account/' ;
	$arr_p[] = _('Change the password, SSH keys; configure account settings') ;

	$smp = array(
		'title_arr' => $arr_t,
		'links_arr' => $arr_l,
		'tooltip_arr' => $arr_p,
	    );
	plugin_hook("usermenuEx", $smp);

	$params['submenu'] = array($smp['title_arr'],
	    $smp['links_arr'], $smp['tooltip_arr']);
	site_header($params);
}

/**
 *	site_user_footer() - currently a simple shim that should be on every user page,
 *	rather than a direct call to site_footer() or theme_footer()
 *
 *	@param params array() empty
 */
function site_user_footer($params) {
	site_footer($params);
}

/**
 *	html_clean_hash_string() - Remove noise characters from hex hash string
 *
 *	Thruout SourceForge, URLs with hexadecimal hash string parameters
 *	are being sent via email to request confirmation of user actions.
 *	It was found that some mail clients distort this hash, so we take
 *	special steps to encode it in the way which help to preserve its
 *	recognition. This routine
 *
 *	@param hashstr required hash parameter as received from browser
 *	@return pure hex string
 */
function html_clean_hash_string($hashstr) {

	if (substr($hashstr,0,1)=="_") {
		$hashstr = substr($hashstr, 1);
	}

	if (substr($hashstr, strlen($hashstr)-1, 1)==">") {
		$hashstr = substr($hashstr, 0, strlen($hashstr)-1);
	}

	return $hashstr;
}


/* TODO: think about beautifying output */

/**
 * html_eo() - Return proper element XHTML start tag
 *
 * @param	string	$name
 *			element name
 * @param	array	$attrs
 *		(optional) associative array of element attributes
 *			values: arrays are space-imploded;
 *			    false values and empty arrays ignored
 * @return	string
 *		XHTML string suitable for echo'ing
 */
function html_eo($name, $attrs=array()) {
	$rv = '<' . $name;
	foreach ($attrs as $key => $value) {
		if (is_array($value)) {
			$value = count($value) ? implode(" ", $value) : false;
		}
		if ($value === false) {
			continue;
		}
		$rv .= ' ' . $key . '="' . htmlspecialchars($value) . '"';
	}
	$rv .= '>';
	return $rv;
}

$html__empty_allowed = array(
	'area' => true,
	'base' => true,
	'basefont' => true,
	'br' => true,
	'col' => true,
	'hr' => true,
	'img' => true,
	'input' => true,
	'isindex' => true,
	'link' => true,
	'meta' => true,
	'orgid' => true,
	'param' => true,
	'pronounce' => true,
	'series' => true,
);

/**
 * html_e() - Return proper element XHTML start/end sequence
 *
 * @param	string	$name
 *			element name
 * @param	array	$attrs
 *		(optional) associative array of element attributes
 *			values: arrays are space-imploded;
 *			    false values and empty arrays ignored
 * @param	string	$content
 *		(optional) XHTML to be placed inside
 * @param	bool	$shortform
 *		(optional) allow short open-close form
 *		(default: true)
 * @return	string
 *		XHTML string suitable for echo'ing
 */
function html_e($name, $attrs=array(), $content="", $shortform=true) {
	global $html__empty_allowed;

	$rv = '<' . $name;
	foreach ($attrs as $key => $value) {
		if (is_array($value)) {
			$value = count($value) ? implode(" ", $value) : false;
		}
		if ($value === false) {
			continue;
		}
		$rv .= ' ' . $key . '="' . htmlspecialchars($value) . '"';
	}
	if ($content === "" && $shortform &&
	    array_key_exists($name, $html__empty_allowed)) {
		$rv .= ' />';
	} else {
		$rv .= '>' . $content . '</' . $name . '>';
	}
	return $rv;
}

$html_autoclose_stack = array();
$html_autoclose_pos = 0;

/**
 * html_ap() - Return XHTML element autoclose stack position
 *
 * @return	integer
 */
function html_ap() {
	global $html_autoclose_pos;

	return $html_autoclose_pos;
}

/**
 * html_ao() - Return proper element XHTML start tag, with autoclose
 *
 * @param	string	$name
 *			element name
 * @param	array	$attrs
 *		(optional) associative array of element attributes
 *			values: arrays are space-imploded;
 *			    false values and empty arrays ignored
 * @return	string
 *		XHTML string suitable for echo'ing
 */
function html_ao($name, $attrs=array()) {
	global $html_autoclose_pos, $html_autoclose_stack;

	$html_autoclose_stack[$html_autoclose_pos++] = array(
		'name' => $name,
		'attr' => $attrs,
	    );
	return html_eo($name, $attrs);
}

/**
 * html_aonce() - Return once proper element XHTML start tag, with autoclose
 *
 * @param	ref	&$sptr
			initialise this to false; will be modified
 * @param	string	$name
 *			element name
 * @param	array	$attrs
 *		(optional) associative array of element attributes
 *			values: arrays are space-imploded;
 *			    false values and empty arrays ignored
 * @return	string
 *		XHTML string suitable for echo'ing
 */
function html_aonce(&$sptr, $name, $attrs=array()) {
	if ($sptr !== false) {
		/* already run */
		return "";
	}
	$sptr = html_ap();
	return html_ao($name, $attrs);
}

/**
 * html_ac() - Return proper element XHTML end tags, autoclosing
 *
 * @param	integer	$spos
 *			stack position to return to
 *			(nothing is done if === false)
 * @return	string
 *		XHTML string suitable for echo'ing
 */
function html_ac($spos) {
	global $html_autoclose_pos, $html_autoclose_stack;

	if ($spos === false) {
		/* support for html_aonce() */
		return "";
	}

	if ($html_autoclose_pos < $spos) {
		$e = "html_autoclose stack underflow; closing down to " .
		    $spos . " but we're down to " . $html_autoclose_pos .
		    " already!";
		throw new Exception($e);
	}

	$rv = "";
	while ($html_autoclose_pos > $spos) {
		--$html_autoclose_pos;
		$rv .= '</' . $html_autoclose_stack[$html_autoclose_pos]['name'] . '>';
		unset($html_autoclose_stack[$html_autoclose_pos]);
	}
	return $rv;
}

/**
 * html_a_copy() - Return a copy of part of the autoclose stack
 *
 * @param	integer	$spos
 *			stack position caller will return to
 * @return	opaque
 *		argument suitable for html_a_apply()
 */
function html_a_copy($spos) {
	global $html_autoclose_pos, $html_autoclose_stack;

	if ($spos === false) {
		return array();
	}

	if ($spos > $html_autoclose_pos) {
		$e = "html_autoclose stack underflow; closing down to " .
		    $spos . " but we're down to " . $html_autoclose_pos .
		    " already!";
		throw new Exception($e);
	}

	$rv = array();
	while ($spos < $html_autoclose_pos) {
		$rv[] = $html_autoclose_stack[$spos++];
	}
	return $rv;
}

/**
 * html_a_apply() - Reopen tags based on an autoclose stack copy
 *
 * @param	opaque	$scopy
 *			return value from html_a_copy()
 * @return	string
 *		XHTML string suitable for echo'ing
 */
function html_a_apply($scopy) {
	/* array_reduce() would be useful here... IF IT WORKED, FFS! */
	$rv = "";
	foreach ($scopy as $value) {
		$rv .= html_ao($value['name'], $value['attr']);
	}
	return $rv;
}


/**
 *	html_build_textbox() - Render textbox control
 *
 *	@param name - name of control
 *	@param value - value of control
 *	@return html code for textbox control
 */
function html_build_textbox($name, $value, $id=null, $class=null) {
	return html_e('input', array(
		'type' => 'text',
		'id' => ($id ? $id : false),
		'name' => ($name ? $name : false),
		'value' => ($value ? $value : false),
		'class' => ($class ? $class : false),
	    )) . "\n";
}

/**
 *	html_build_hiddenbox() - Render hiddenbox control
 *
 *	@param name - name of control
 *	@param value - value of control
 *	@return html code for hiddenbox control
 */
function html_build_hiddenBox($name, $value) {
	return html_e('input', array(
		'type' => 'hidden',
		'name' => $name,
		'value' => $value,
	  )) . "\n";
}

function html_build_submit_button($name, $value) {
	return html_e('input', array(
		'type' => 'submit',
		'name' => $name,
		'value' => $value,
	  )) . "\n";
}

function html_build_textarea($name, $rows, $cols, $txt='') {
	return html_e('textarea', array(
		'name' => $name,
		'rows' => $rows,
		'cols' => $cols,
	    ), util_html_secure($txt), false) . "\n";
}

/**
 *	html_build_form_start_tag() - Render form start tag control. The
 *	$variables $encrypt and acceptCharset have already the default
 *	$value that will be used if the attributes are not added
 *	for the form. The attributes will be added by default if not overwritten.
 *
 *	@param action - uri to the webpage or file that handels the request
 *	@param id - id of the form
 *	@param name - name of form
 *	@param accept - accepted data of the form
 *	@param method - method of the form
 *	@param enctype - enctype of the form. Default type already included
 *	@param acceptCharset - accepted charset of the form. Default type already included
 *	@return html code for start tag control
 */
function html_build_form_start_tag($action, $id=null, $name=null, $accept=null, $method='post', $enctype='application/x-www-form-urlencoded', $acceptCharset='UNKNOWN') {
	return html_ao('form', array(
		'id' => ($id ? $id : false),
		'name' => ($name ? $name : false),
		'accept' => ($accept ? $accept : false),
		'action' => $action,
		'method' => $method,
		'enctype' => $enctype,
		'accept-charset' => $acceptCharset,
	    )) . "\n";
}


function notepad_button($form, $kind="") {
	return html_e('a', array(
		'href' => 'javascript:notepad_window(' .
		    minijson_encode($form, false) . ', ' .
		    minijson_encode($kind, false) . ');',
		'title' => _('FusionForge Notepad'),
	    ), html_image('ic/msg.png', '12', '14', array(
		'alt' => 'Notepad',
		'border' => '0',
	    )));
}


// Local Variables:
// mode: php
// c-file-style: "bsd"
// End:
