<?php
/**
 * Tracker UUID implementation for FusionForge
 *
 * Copyright © 2010, 2011
 *	Thorsten “mirabilos” Glaser <t.glaser@tarent.de>
 * All rights reserved.
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *-
 * Follow up to the task information page by UUID (project_task_id)
 * via a redirection.
 */

require_once('../env.inc.php');
require_once $gfwww.'include/pre.php';
require_once $gfcommon.'pm/ProjectTaskSqlQueries.php';

$aid = getIntFromRequest('aid');
if (!$aid)
	$aid = getIntFromRequest('id');
if (!$aid)
	$aid = util_path_info_last_numeric_component();
if (!$aid) {
	sysdebug_off("HTTP/1.0 404 Not Found", true, 404);
	echo "You forgot to pass the aid.\n";
	exit;
}

$ainfo = tasktracker_getinfo($aid, false, 1);
if ($ainfo && $ainfo['is_a'] == 'tid') {
	$dsturl = util_make_url("/pm/task.php?func=detailtask&project_task_id=" .
	    $ainfo['project_task_id'] . "&group_id=" . $ainfo['group_id'] .
	    "&group_project_id=" . $ainfo['group_project_id']);
	sysdebug_off("HTTP/1.0 302 Found", true, 302);
	header("Location: " . $dsturl);
	echo "The result is at:\n" . $dsturl . "\n";
	exit;
}

if (!$ainfo) {
	sysdebug_off("HTTP/1.0 404 Not Found", true, 404);
	echo "There is no tracker item with id ".$aid."!\n";
	exit;
}

$dsturl = util_make_url("/tracker/index.php?func=detail&aid=" .
    $ainfo['aid'] . "&group_id=" . $ainfo['group_id'] .
    "&atid=" . $ainfo['atid']);

foreach (array(
	'error_msg',
	'warning_msg',
	'feedback',
    ) as $k) {
	$v = getStringFromRequest($k);
	if ($v) {
		$dsturl .= '&' . $k . '=' . urlencode($v);
	}
}

sysdebug_off("HTTP/1.0 302 Found", true, 302);
header("Location: " . $dsturl);
echo "The result is at:\n" . $dsturl . "\n";
exit;
