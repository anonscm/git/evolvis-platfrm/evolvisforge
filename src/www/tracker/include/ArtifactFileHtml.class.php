<?php
/**
  *
  * SourceForge Generic Tracker facility
  *
  * SourceForge: Breaking Down the Barriers to Open Source Development
  * Copyright 1999-2001 (c) VA Linux Systems
  * http://sourceforge.net
  *
  */


require_once $gfcommon.'tracker/ArtifactFile.class.php';

class ArtifactFileHtml extends ArtifactFile {

	/**
	 *  ArtifactFileHtml() - constructor
	 *
	 *  Use this constructor if you are modifying an existing artifact
	 *
	 *	@param $Artifact object
	 *  @param $data associative array (all fields from artifact_file_user_vw) OR id from database
	 *  @return true/false
	 */
	function __construct(&$Artifact, $data=false) {
		return parent::__construct($Artifact,$data);
	}

	function upload($input_file,$input_file_name,$input_file_type,$description) {
		if (!util_check_fileupload($input_file)) {
			$this->setError('ArtifactFile: Invalid filename');
			return false;
		}
		$size = @filesize($input_file);
		if (!$size) {
			/* fread() bails out if $size is 0 */
			$this->setError(_('Cannot upload zero-length file'));
			return false;
		}
		$input_data = fread(fopen($input_file, 'r'), $size);
		return $this->create($input_file_name,$input_file_type,$size,$input_data,$description);
	}

}

/*
 * Checks whether $f is a valid uploaded file,
 * and whether the upload will succeed.
 *
 * @param $f the file
 * @param $n the attachment number
 * @return false if to be skipped, true if valid, string if error
 */
function afh_uploadcheck($f, $n) {
	if (!$f)
		return false;
	if (isset($f['error']) && $f['error'] > 0) {
		if ($f['error'] === 1 || $f['error'] === 2)
			return sprintf(_('ERROR: Skipping attachment %d: file is too large.'), $n);
		elseif ($f['error'] === 3)
			return sprintf(_('ERROR: Skipping attachment %d: transfer interrupted.'), $n);
		return false;
	}
	$input_file = $f['tmp_name'];
	if (!is_uploaded_file($input_file))
		return false;
	$filename = $f['name'];
	$filetype = $f['type'];

	if (!util_check_fileupload($input_file))
		return sprintf(_('ERROR: Skipping attachment %d: invalid filename: %s'), $n, $input_file);
	$filesize = @filesize($input_file);
	if (!$filesize)
		return sprintf(_('ERROR: Skipping attachment %d: cannot upload zero-length file'), $n);
	$bin_data = fread(fopen($input_file, 'r'), $filesize);

	/* this is… ouch – try uploading a file with only a 0 in it… */
	if (!$filename || !$filesize || !$bin_data)
		return sprintf(_('ERROR: Skipping attachment %d: file name, type, size, and data are required'), $n);
	return true;
}
