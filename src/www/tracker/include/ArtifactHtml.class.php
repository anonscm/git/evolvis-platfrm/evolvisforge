<?php
/*
 *
 * SourceForge Generic Tracker facility
 *
 * SourceForge: Breaking Down the Barriers to Open Source Development
 * Copyright 1999-2001 (c) VA Linux Systems
 * Copyright (C) 2011 Alain Peyrat - Alcatel-Lucent
 * http://sourceforge.net
 *
 */

require_once $gfcommon.'tracker/Artifact.class.php';
require_once $gfcommon.'include/EvolvisTable.class.php';

class ArtifactHtml extends Artifact {

	/**
	 *  ArtifactHtml() - constructor
	 *
	 *  Use this constructor if you are modifying an existing artifact
	 *
	 *  @param $ArtifactType object
	 *  @param $artifact_id integer (primary key from database)
	 *  @return true/false
	 */
	function __construct(&$ArtifactType,$artifact_id=false) {
		return parent::__construct($ArtifactType,$artifact_id);
	}

	function showHistory() {
		global $artifact_cat_arr,$artifact_grp_arr,$artifact_res_arr;
		$result = $this->getHistory();
		$rows = db_numrows($result);

		if ($rows < 1) {
			echo html_e('p', array(),
			    _('No Changes Have Been Made to This Item'));
			return;
		}

		$aT =& $this->getArtifactType();
		$t = new EvolvisTable(array(
			_('Field'),
			_('Old Value'),
			_('New Value'),
			_('Date'),
			_('By'),
		    ));
		for ($i = 0; $i < $rows; ++$i) {
			$r =& $t->tr();
			$field = db_result($result, $i, 'field_name');
			$r->td()->set($field);
			foreach (array(
				db_result($result, $i, 'old_value'),
				db_result($result, $i, 'new_value'),
			    ) as $ov) {
				if ($field == 'status_id' && $ov > 0) {
					$r->td()->set($aT->getStatusName($ov));
				} else if ($field == 'assigned_to' && $ov) {
					$r->td()->set(user_getname($ov));
				} else if ($field == 'close_date') {
					if ($ov) {
						$r->td()->set(datepick_format($ov, true));
					} else {
						$r->td()->setraw(html_e('i', array(),
						    _('None')));
					}
				} else {
					$r->td()->set($ov);
				}
			}
			$r->td()->set(datepick_format(db_result($result, $i, 'entrydate'), true));
			$r->td()->set(db_result($result, $i, 'user_name'));
		}
		echo $t->emit();
	}

	function showRelations() {
		$aid = $this->getID();

		// Search for all relations pointing to this record.

		$res = db_query_params ('SELECT *
		FROM artifact_extra_field_list, artifact_extra_field_data, artifact_group_list, artifact, groups
		WHERE field_type=9
		AND artifact_extra_field_list.extra_field_id=artifact_extra_field_data.extra_field_id
		AND artifact_group_list.group_artifact_id = artifact_extra_field_list.group_artifact_id
		AND artifact.artifact_id = artifact_extra_field_data.artifact_id
		AND groups.group_id = artifact_group_list.group_id
		AND (field_data = $1 OR field_data LIKE $2 OR field_data LIKE $3 OR field_data LIKE $4)
		ORDER BY artifact_group_list.group_id ASC, name ASC, field_name ASC, artifact.artifact_id ASC',
					array($aid,
					      "$aid %",
					      "% $aid %",
					      "% $aid"));
		if (db_numrows($res) < 1) {
			return;
		}

		echo '<h3>' . _('Backward Relations') . ":</h3>\n";

		$last_gn = false;
		$last_tn = false;
		$last_fn = false;
		$t = new EvolvisTable(array(false));
		while ($arr = db_fetch_array($res)) {
			if ($arr['group_name'] !== $last_gn ||
			    $arr['name'] !== $last_tn ||
			    $arr['field_name'] !== $last_fn) {
				$t->tr()->th()->setraw(sprintf(
				    _('%1$s: %2$s <%4$s>(Relation: %3$s)</%5$s>'),
				    util_html_secure($arr['group_name']),
				    util_html_secure($arr['name']),
				    util_html_secure($arr['field_name']),
				    'i style="font-weight:normal;"', 'i'));
				$last_gn = $arr['group_name'];
				$last_tn = $arr['name'];
				$last_fn = $arr['field_name'];
			}
			$aid = (int)$arr['artifact_id'];
			$atext = sprintf('[#%d]', $aid);
			$alink = util_make_url('/tracker/t_follow.php/' . $aid);
			$abody = util_html_secure($arr['summary']);
			$t->tr()->td()->setraw(html_e('tt', array(), $atext) .
			    " " . html_e('a', array('href' => $alink), $abody));
		}
		echo $t->emit();
	}

}
