<?php
/**
 * FusionForge Generic Tracker facility
 *
 * Copyright 1999-2001 (c) VA Linux Systems - Sourceforge
 * Copyright 2010 (c) Fusionforge Team
 * Copyright (C) 2011 Alain Peyrat - Alcatel-Lucent
 * Copyright © 2014 Thorsten “mirabilos” Glaser <t.glaser@tarent.de>
 * http://fusionforge.org
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

require_once $gfcommon.'tracker/ArtifactType.class.php';
require_once $gfcommon.'tracker/ArtifactExtraField.class.php';
require_once $gfcommon.'tracker/ArtifactExtraFieldElement.class.php';
require_once $gfcommon.'tracker/ArtifactWorkflow.class.php';
require_once $gfcommon.'include/EvolvisTable.class.php';

class ArtifactTypeHtml extends ArtifactType {

	/**
	 *  ArtifactType() - constructor
	 *
	 *  @param $group object
	 *  @param $artifact_type_id - the id # assigned to this artifact type in the db
	 */
	function __construct(&$group,$artifact_type_id=false, $arr=false) {
		return parent::__construct($group,$artifact_type_id,$arr);
	}

	function header($params) {
		$this->header_internal(false, $params);
	}

	function footer($params) {
		site_project_footer($params);
	}

	function adminHeader($params) {
		$this->header_internal(true, $params);
	}

	function adminFooter($params) {
		echo $this->footer($params);
	}

	function renderSubmitInstructions() {
		$msg = $this->getSubmitInstructions();
		return str_replace("\n","<br />", $msg);
	}

	function renderBrowseInstructions() {
		$msg = $this->getBrowseInstructions();
		return str_replace("\n","<br />", $msg);
	}

	function renderExtraFields($selected=array(),$show_100=false,$text_100='none',$show_any=false,$text_any='Any',$types=array(),$status_show_100=false,$mode='',$intable=true,$skips=array()) {
		$efarr = $this->getExtraFields($types);
		$found = false;

		foreach ($skips as $whichone) {
			if ($whichone === 'Status') {
				foreach ($efarr as $k => $v) {
					if ($v['field_type'] == ARTIFACT_EXTRAFIELDTYPE_STATUS) {
						$found = $k;
						break;
					}
				}
			} else {
				foreach ($efarr as $k => $v) {
					if ($v['field_name'] == $whichone) {
						$found = $k;
						break;
					}
				}
			}
			if ($found !== false) {
				unset($efarr[$found]);
			}
		}

		$template = $intable ?
		    $this->generateRenderHTML($types, $mode) :
		    $this->generateEfRendering($efarr, $mode);
		$this->_substExtraFields($mode, $efarr, $selected, $show_100, $text_100, $show_any, $text_any, $status_show_100, $template);
	}

	function renderExtraField($selected,$show_100,$text_100,$show_any,$text_any,$types,$status_show_100,$mode,$whichone,$extracallback=false) {
		$ofarr = $this->getExtraFields($types);
		$found = false;

		if ($whichone === 'Status') {
			foreach ($ofarr as $k => $v) {
				if ($v['field_type'] == ARTIFACT_EXTRAFIELDTYPE_STATUS) {
					$found = $k;
					break;
				}
			}
		} else {
			foreach ($ofarr as $k => $v) {
				if ($v['field_name'] == $whichone) {
					$found = $k;
					break;
				}
			}
		}
		if ($found === false) {
			return false;
		}

		$ef_found = $ofarr[$found];
		$efarr = array($found => $ef_found);
		$template = $this->generateEfRender($ef_found, $mode,
		    $extracallback ?
		    call_user_func($extracallback, $ef_found) : "");
		$this->_substExtraFields($mode, $efarr, $selected, $show_100, $text_100, $show_any, $text_any, $status_show_100, $template);
		return $ef_found;
	}

	function renderRelatedTasks($group, $ah) {
		if (!$group->usesPM()) {
			return;
		}

		$is_admin = forge_check_perm('tracker_admin', $this->Group->getID());
		$taskcount = db_numrows($ah->getRelatedTasks());
		$totalPercentage = 0;

		if ($taskcount < 1) {
			echo html_e('p', array(), _('No related tasks.'));
			return;
		}

		$title_arr = array();
		$title_arr[] = _('Task Summary');
		$title_arr[] = _('Start Date');
		$title_arr[] = _('End Date');
		$title_arr[] = _('Status');
		if ($is_admin) {
			$title_arr[] = _('Remove Relation');
		}
		$t = new EvolvisTable($title_arr);

		for ($i = 0; $i < $taskcount; ++$i) {
			$taskinfo = db_fetch_array($ah->relatedtasks, $i);
			$totalPercentage += $taskinfo['percent_complete'];
			$taskid = $taskinfo['project_task_id'];
			$projectid = $taskinfo['group_project_id'];
			$groupid = $taskinfo['group_id'];
			$summary = util_html_secure($taskinfo['summary']);
			$startdate = date(_('Y-m-d H:i'), $taskinfo['start_date']);
			$enddate = date(_('Y-m-d H:i'), $taskinfo['end_date']);
			$status = $taskinfo['status_name'];

			$r =& $t->tr();
			$r->td()->setraw(html_e('a', array(
				'href' => '/pm/task.php?func=detailtask' .
				    '&project_task_id=' . $taskid .
				    '&group_id=' . $groupid .
				    '&group_project_id=' . $projectid,
			    ), '[T' . $taskid . '] ' . $summary));
			$r->td()->set($startdate);
			$r->td()->set($enddate);
			$r->td()->set($status . ' (' .
			    $taskinfo['percent_complete'] . '%)');
			if ($is_admin) {
				$r->td()->setraw(html_e('input', array(
					'type' => 'checkbox',
					'name' => 'remlink[]',
					'value' => $taskid,
				    )));
			}
		}
		echo $t->emit();

		echo "\n<hr /><p style=\"text-align:right;\">";
		printf(_('Average completion rate: %d%%'), (int)($totalPercentage/$taskcount));
		echo "</p>\n";
	}

	function renderFiles($group_id, $ah) {
		$file_list =& $ah->getFiles();
		if (!count($file_list)) {
			return;
		}

		echo '<h3>' . _('Attachments') . ":</h3>\n";

		$t = new EvolvisTable(array(
			html_image('ic/trash.png', '16', '16'),
			_('Name'),
			_('Date'),
			_('By'),
			_('Size'),
		    ));
		foreach ($file_list as $file) {
			$fln = $file->getName();
			$fla = array();
			$fla['href'] = '/tracker/download.php/' . $group_id .
			    '/' . $this->getID() . '/' . $ah->getID() .
			    '/' . $file->getID() . '/' . $fln;
			if (strncasecmp($file->getType(), "image/", 6) === 0) {
				$fla['rel'] = "lightbox";
			}

			$r =& $t->tr();
			$r->td()->setraw(html_e('input', array(
				'type' => 'checkbox',
				'name' => 'delete_file[]',
				'value' => $file->getID(),
			    )));
			$r->td()->setraw(html_e('a', $fla,
			    htmlspecialchars($fln)));
			$r->td()->set(date(_('Y-m-d H:i'), $file->getDate()));
			$r->td(array(
				'title' => $file->getSubmittedRealName(),
			    ), -1, $file->getSubmittedUnixName());
			$r->td()->set(human_readable_bytes($file->getSize()));
		}
		echo $t->emit();
		echo html_e('p', array(),
		    _('Tick an attachment and submit to delete it.'));
	}

	/**
	 *	generateRenderHTML
	 *
	 *	@return	string	HTML template (bunch of <tr>s).
	 */
	function generateRenderHTML($types=array(), $mode) {
		$efarr = $this->getExtraFields($types);
		//each two columns, we'll reset this and start a new row

		$return = '
			<!-- Start Extra Fields Rendering -->
			<tr>';
		$col_count=0;

		$keys=array_keys($efarr);
		$count=count($keys);
		if ($count == 0) return '';

		for ($k=0; $k<$count; $k++) {
			$i=$keys[$k];

			// Do not show the required star in query mode (creating/updating a query).
			$is_required = ($mode == 'QUERY' || $mode == 'DISPLAY') ?	0 : $efarr[$i]['is_required'];
			$name = $efarr[$i]['field_name'].($is_required ? utils_requiredField() : '').': ';
			$name = '<strong>'.$name.'</strong>';

			if ($efarr[$i]['field_type'] == ARTIFACT_EXTRAFIELDTYPE_SELECT) {

				$return .= '
					<td width="50%" valign="top">'.$name.'<br />{$'.$efarr[$i]['field_name'].'}</td>';

			} elseif ($efarr[$i]['field_type'] == ARTIFACT_EXTRAFIELDTYPE_CHECKBOX) {

				$return .= '
					<td width="50%" valign="top">'.$name.'<br />{$'.$efarr[$i]['field_name'].'}</td>';

			} elseif ($efarr[$i]['field_type'] == ARTIFACT_EXTRAFIELDTYPE_RADIO) {

				$return .= '
					<td width="50%" valign="top">'.$name.'<br />{$'.$efarr[$i]['field_name'].'}</td>';

			} elseif ($efarr[$i]['field_type'] == ARTIFACT_EXTRAFIELDTYPE_DATETIME) {

				$return .= '
					'.$name.'{$PostName:'.$efarr[$i]['field_name'].'}<br />{$'.$efarr[$i]['field_name'].'}';

			} elseif ($efarr[$i]['field_type'] == ARTIFACT_EXTRAFIELDTYPE_TEXT ||
				$efarr[$i]['field_type'] == ARTIFACT_EXTRAFIELDTYPE_INTEGER) {

				//text fields might be really wide, so need a row to themselves.
				if (($col_count == 1) && ($efarr[$i]['attribute1'] > 30)) {
					$colspan=2;
					$return .= '
					<td> </td>
			</tr>
			<tr>';
				} else {
					$colspan=1;
				}
				$return .= '
					<td width="'.(50*$colspan).'%" colspan="'.$colspan.'" valign="top">'.$name.'{$PostName:'.$efarr[$i]['field_name'].'}<br />{$'.$efarr[$i]['field_name'].'}</td>';

			} elseif ($efarr[$i]['field_type'] == ARTIFACT_EXTRAFIELDTYPE_TEXTAREA) {

				//text areas might be really wide, so need a row to themselves.
				if (($col_count == 1) && ($efarr[$i]['attribute2'] > 30)) {
					$colspan=2;
					$return .= '
					<td> </td>
			</tr>
			<tr>';
				} else {
					$colspan=1;
				}
				$return .= '
					<td width="'.(50*$colspan).'%" colspan="'.$colspan.'" valign="top">'.$name.'{$PostName:'.$efarr[$i]['field_name'].'}<br />{$'.$efarr[$i]['field_name'].'}</td>';

			} elseif ($efarr[$i]['field_type'] == ARTIFACT_EXTRAFIELDTYPE_MULTISELECT) {

				$return .= '
					<td width="50%" valign="top">'.$name.'<br />{$'.$efarr[$i]['field_name'].'}</td>';

			} elseif ($efarr[$i]['field_type'] == ARTIFACT_EXTRAFIELDTYPE_STATUS) {

				$return .= '
					<td width="50%" valign="top">'.$name.'<br />{$'.$efarr[$i]['field_name'].'}</td>';

			} elseif ($efarr[$i]['field_type'] == ARTIFACT_EXTRAFIELDTYPE_RELATION) {

				//text fields might be really wide, so need a row to themselves.
				if (($col_count == 1) && ($efarr[$i]['attribute1'] > 30)) {
					$colspan=2;
					$return .= '
					<td> </td>
			</tr>
			<tr>';
				} else {
					$colspan=1;
				}
				$return .= '
					<td width="'.(50*$colspan).'%" colspan="'.$colspan.'" valign="top">'.$name.'{$PostName:'.$efarr[$i]['field_name'].'}<br />{$'.$efarr[$i]['field_name'].'}</td>';

			}
			$col_count++;
			//we've done two columns - if there are more to do, start a new row
			if (($col_count == 2) && ($k != $count-1)) {
				$col_count = 0;
				$return .= '
			</tr>
			<tr>';
			}
		}
		if ($col_count == 1) {
			$return .= '
					<td> </td>';
		}
		$return .= '
			</tr>
			<!-- End Extra Fields Rendering -->';
		return $return;
	}

	/**
	 *	generateEfRendering
	 *
	 *	@return	string	XHTML template (bunch of <div>s).
	 */
	function generateEfRendering($efarr, $mode) {
		if (!count($efarr)) {
			return "";
		}

		$rv = "\n<!-- Start Extra Fields Rendering -->";
		$order_val = $this->getRenderOrder();
		foreach ($order_val[1] as $k) {
			if (!isset($efarr[$k])) {
				/* skip, not in filter set */
				continue;
			}
			$rv .= $this->generateEfRender($efarr[$k], $mode);
		}
		$rv .= "\n<!-- End Extra Fields Rendering -->\n";
		return ($rv);
	}

	function geteftipid($v) {
		return ('tracker-' . str_replace(' ', '_',
		    $this->geteftipname($v)));
	}

	function geteftipname($v) {
		return ($v['field_type'] == ARTIFACT_EXTRAFIELDTYPE_STATUS ?
		    'status_id' : ('ef-' . $v['alias']));
	}

	function generateEfRender($v, $mode, $extrahtml="") {
		$name = $v['field_name'];
		/*
		 * Do not show the required star in query
		 * mode (creating/updating a query).
		 */
		if ($mode != "QUERY" && $mode != "DISPLAY" &&
		    $v['is_required']) {
			$name .= utils_requiredField();
		}

		return "\n\t<div><strong>" . $name . ":</strong>" .
		    html_get_tooltip_extralink($this->geteftipname($v)) .
		    "\n\t\t" . '{$PostName:' . $v['field_name'] . '}' .
		    "<br />\n\t\t" . '{$' . $v['field_name'] . '}' .
		    "\n\t" . $extrahtml . "</div>";
	}

	/**
	 *	renderSelect - this function builds pop up
	 *	box with choices.
	 *
	 *	@param		int 	The ID of this field.
	 *	@param 		string	The item that should be checked
	 *	@param		string	Whether to show the '100 row'
	 *	@param		string	What to call the '100 row'
	 *	@param		?
	 *	@param		?
	 *	@param		?
	 *	@param		array	extra field, for tooltip
	 *	@return		box and choices
	 */
	function renderSelect($extra_field_id,$checked='xzxz',$show_100=false,$text_100='none',$show_any=false,$text_any='Any',$allowed=false,$efv=false) {
		if ($text_100 == 'none') {
			$text_100 = _('None');
		}
		$arr = $this->getExtraFieldElements($extra_field_id);
		$keys = array();
		$vals = array();
		for ($i=0; $i<count($arr); $i++) {
			$keys[$i]=$arr[$i]['element_id'];
			$vals[$i]=$arr[$i]['element_name'];
		}
		if ($efv !== false) {
			$oid = $this->geteftipid($efv);
			$otitle = html_get_tooltip_description($this->geteftipname($efv));
		} else {
			$oid = false;
			$otitle = false;
		}
		return html_build_select_box_from_arrays($keys,$vals,'extra_fields['.$extra_field_id.']',htmlspecialchars($checked),$show_100,$text_100,$show_any,$text_any,$allowed,$oid,$otitle);
	}

	/**
	 *	renderRadio - this function builds radio buttons.
	 *
	 *	@param		int 	The ID of this field.
	 *	@param 		string	The item that should be checked
	 *	@param		string	Whether to show the '100 row'
	 *	@param		string	What to call the '100 row'
	 *	@param		?
	 *	@param		?
	 *	@param		array	extra field, for tooltip
	 *	@return		radio buttons
	 */
	function renderRadio($extra_field_id,$checked='xzxz',$show_100=false,$text_100='none',$show_any=false,$text_any='Any',$efv=false) {
		$arr = $this->getExtraFieldElements($extra_field_id);
		$keys = array();
		$vals = array();
		for ($i=0; $i<count($arr); $i++) {
			$keys[$i]=$arr[$i]['element_id'];
			$vals[$i]=$arr[$i]['element_name'];
		}
		$r = html_build_radio_buttons_from_arrays($keys,$vals,'extra_fields['.$extra_field_id.']',htmlspecialchars($checked),$show_100,$text_100,$show_any,$text_any);
		if ($efv !== false) {
			$r = html_e('span', array(
				'id' => $this->geteftipid($efv),
				'title' => util_unconvert_htmlspecialchars(html_get_tooltip_description($this->geteftipname($efv))),
			    ), $r);
		}
		return $r;
	}

	/**
	 *	renderCheckbox - this function builds checkboxes.
	 *
	 *	@param		int 	The ID of this field.
	 *	@param 		array	The items that should be checked
	 *	@param		string	Whether to show the '100 row'
	 *	@param		string	What to call the '100 row'
	 *	@param		array	extra field, for tooltip
	 *	@return		radio buttons
	 */
	function renderCheckbox($extra_field_id,$checked=array(),$show_100=false,$text_100='none',$efv=false) {
		if ($text_100 == 'none'){
			$text_100 = _('None');
		}
		if (!$checked || !is_array($checked)) {
			$checked = array();
		}
		$arr = $this->getExtraFieldElements($extra_field_id);
		$return = '';
		if ($efv !== false) {
			$return .= html_eo('span', array(
				'id' => $this->geteftipid($efv),
				'title' => util_unconvert_htmlspecialchars(html_get_tooltip_description($this->geteftipname($efv))),
			    ));
		}
		if ($show_100) {
			$return .= '
				<input type="checkbox" name="extra_fields['.$extra_field_id.'][]" value="100" '.
			((in_array(100,$checked)) ? 'checked="checked"' : '').'/> '.$text_100.'<br />';
		}
		for ($i=0; $i<count($arr); $i++) {
			$return .= '
				<input type="checkbox" name="extra_fields['.$extra_field_id.'][]" value="'.$arr[$i]['element_id'].'" '.
			((in_array($arr[$i]['element_id'],$checked)) ? 'checked="checked"' : '').'/> '.$arr[$i]['element_name'].'<br />';
		}
		if ($efv !== false) {
			$return .= "</span>\n";
		}
		return $return;
	}

	/**
	 *	renderMultiSelectBox - this function builds checkboxes.
	 *
	 *	@param		int 	The ID of this field.
	 *	@param 		array	The items that should be checked
	 *	@param		string	Whether to show the '100 row'
	 *	@param		string	What to call the '100 row'
	 *	@param		array	extra field, for tooltip
	 *	@return		radio multiselectbox
	 */
	function renderMultiSelectBox($extra_field_id,$checked=array(),$show_100=false,$text_100='none',$efv=false) {
		$arr =$this->getExtraFieldElements($extra_field_id);
		if (!$checked) {
			$checked=array();
		}
		if (!is_array($checked)) {
			$checked = explode(',',$checked);
		}
		$keys=array();
		$vals=array();
		$arr = $this->getExtraFieldElements($extra_field_id);
		for ($i=0; $i<count($arr); $i++) {
			$keys[]=$arr[$i]['element_id'];
			$vals[]=$arr[$i]['element_name'];
		}
		$size = min( count($arr)+1, 15);
		$r = html_build_multiple_select_box_from_arrays($keys,$vals,"extra_fields[$extra_field_id][]",$checked,$size,$show_100,$text_100);
		if ($efv !== false) {
			$t = '<select id="' . $this->geteftipid($efv) .
			    '" title="' .
			    html_get_tooltip_description($this->geteftipname($efv)) .
			    '"';
			$r = str_replace('<select', $t, $r);
		}
		return $r;
	}

	/**
	 *	renderTextField - this function builds a text field.
	 *
	 *	@param		int 	The ID of this field.
	 *	@param 		string	The data for this field.
	 *	@param		?
	 *	@param		?
	 *	@param		array	extra field, for tooltip
	 *	@return		text area and data.
	 */
	function renderTextField($extra_field_id,$contents,$size,$maxlength,$efv=false) {
		$p = array(
			'type' => 'text',
			'name' => 'extra_fields[' . $extra_field_id . ']',
			'value' => $contents,
			'size' => $size,
			'maxlength' => $maxlength,
		    );
		if ($efv !== false) {
			$p['id'] = $this->geteftipid($efv);
			$p['title'] = util_unconvert_htmlspecialchars(html_get_tooltip_description($this->geteftipname($efv)));
		}
		return html_e('input', $p);
	}

	/**
	 *	renderRelationField - this function builds a relation field.
	 *
	 *	@param		int 	The ID of this field.
	 *	@param 		string	The data for this field.
	 *	@param		?
	 *	@param		?
	 *	@param		array	extra field, for tooltip
	 *	@return		text area and data.
	 */
	function renderRelationField($extra_field_id,$contents,$size,$maxlength,$efv=false) {
		$arr = $this->getExtraFieldElements($extra_field_id);
		for ($i=0; $i<count($arr); $i++) {
			$keys[$i]=$arr[$i]['element_id'];
			$vals[$i]=$arr[$i]['element_name'];
		}
		// Convert artifact id to links.
		$html_contents = "";
		foreach (preg_split("/\D+/", $contents) as $v) {
			$v = (int)$v;
			if (!$v) {
				continue;
			}
			$res = db_query_params('SELECT summary, status_id
				FROM artifact
				WHERE artifact_id=$1',
			    array(
				$v,
			    ));
			if (db_numrows($res) != 1) {
				$html_contents .= sprintf(
				    _('[#%d] (unknown)'), $v) . "<br />\n";
				continue;
			}
			$row = db_fetch_array($res);
			$ts = array();
			if ($row['status_id'] == 2) {
				$ts['class'][] = 'artifact_closed';
			}
			$html_contents .= html_e('tt', $ts, sprintf('[#%d]',
			    $v)) . " " . html_e('a', array(
				'href' => util_make_url('/tracker/t_follow.php/' . $v),
			    ), util_html_secure($row['summary'])) . html_e('br');
		}
		$edit_contents = $this->renderTextField($extra_field_id,$contents,$size,$maxlength,$efv);
		$edit_tips = '<br/><span class="tips">'._('Tip: Enter a space-separated list of artifact ids ([#NNN] also accepted)').'</span>';
		return '
			<div id="edit'.$extra_field_id.'" style="display: none;">'.$edit_contents.$edit_tips.'</div>
			<div id="show'.$extra_field_id.'" style="display: block;">'.$html_contents.'</div>';
	}

	/**
	 *	renderTextArea - this function builds a text area.
	 *
	 *	@param		int 	The ID of this field.
	 *	@param 		string	The data for this field.
	 *	@param		?
	 *	@param		?
	 *	@param		array	extra field, for tooltip
	 *	@return		text area and data.
	 */
	function renderTextArea($extra_field_id,$contents,$rows,$cols,$efv=false) {
		$p = array(
			'name' => 'extra_fields[' . $extra_field_id . ']',
			'rows' => $rows,
			'cols' => $cols,
		    );
		if ($efv !== false) {
			$p['id'] = $this->geteftipid($efv);
			$p['title'] = util_unconvert_htmlspecialchars(html_get_tooltip_description($this->geteftipname($efv)));
		}
		return html_e('textarea', $p, util_html_encode($contents), false);
	}

	function technicianBox($name='assigned_to[]', $checked='xzxz',
	    $show_100=true, $text_100='none', $extra_id='-1', $extra_name='',
	    $multiple=false, $legend=false) {
		if ($text_100=='none'){
			$text_100=_('Nobody');
		}

		$engine = RBACEngine::getInstance () ;
		$techs = $engine->getUsersByAllowedAction ('tracker', $this->getID(), 'tech') ;
		sortUserList($techs, 'unixname');

		$ids = array();
		$names = array();
		$users = array();

		foreach ($techs as $tech) {
			$ids[] = $tech->getID() ;
			$names[] = $tech->getRealName() . ' (' .
			    $tech->getUnixName() . ')';
			$users[] = $tech;
		}

		if ($extra_id != '-1') {
			$ids[]=$extra_id;
			$names[]=$extra_name;
			$users[] = false;
		}

		if ($multiple) {
			if (!is_array($checked)) {
				$checked = explode(',',$checked);
			}
			$size = min( count($ids)+1, 15);
			return html_build_multiple_select_box_from_arrays ($ids,$names,$name,$checked,$size,$show_100,$text_100);
		}

		/* old-style drop-down box */
		//return html_build_select_box_from_arrays($ids,$names,$name,$checked,$show_100,$text_100);
		/* experimental radio button box */

		if (($title = html_get_tooltip_description($name))) {
			$id = 'tracker-' . $name;
			if (preg_match('/\[\]/', $id)) {
				$id = false;
			}
		} else {
			$id = false;
		}

		$has_checked = false;
		$c = 0;

		$rv = html_eo('fieldset', array(
			'id' => $id,
			'title' => $title,
		    ));
		if ($legend) {
			$rv .= html_e('legend', array(),
			    util_html_secure($legend));
		}
		if ($show_100) {
			$p = array(
				'id' => $id . '-' . $c,
				'type' => 'radio',
				'name' => $name,
				'value' => 100,
			    );
			if ($checked == 100 && !$has_checked) {
				$p['checked'] = 'checked';
				$has_checked = true;
			}
			$rv .= html_e('div', array(), html_e('label', array(
				'for' => $id . '-' . $c,
			    ), html_e('input', $p) .
			    util_html_secure($text_100)));
			$c++;
		}

		foreach ($users as $i => $tech) {
			if ($ids[$i] == '100' && $show_100) {
				/* already shown */
				continue;
			}
			$p = array(
				'id' => $id . '-' . $c,
				'type' => 'radio',
				'name' => $name,
				'value' => $ids[$i],
			    );
			if (!$has_checked &&
			    (string)$ids[$i] == (string)$checked) {
				$p['checked'] = 'checked';
				$has_checked = true;
			}
			$xt = util_html_secure($names[$i]);
			if ($tech && $tech->getID() != 100) {
				$xt = util_display_user($tech->getUnixName(),
				    $tech->getID(), $xt, 'xs', false, true);
			}
			$rv .= html_e('div', array(), html_e('label', array(
				'for' => $id . '-' . $c,
			    ), html_e('input', $p) . $xt));
			$c++;
		}

		/*
		 * preserve passed-in "checked value" if never selected
		 * UNLESS that value was 'xzxz', the default value
		 */
		if (!$has_checked && $checked && $checked != 'xzxz') {
			$rv .= html_e('div', array(), html_e('label', array(
				'for' => $id . '-' . $c,
			    ), html_e('input', array(
				'id' => $id . '-' . $c,
				'type' => 'radio',
				'name' => $name,
				'value' => $checked,
				'checked' => 'checked',
			    )) . util_html_secure(_('No Change'))));
		}

		return $rv . "</fieldset>\n";
	}

        function creatorBox ($name='submitted_by[]',$checked='xzxz',$show_100=true,$text_100='none',$extra_id='-1',$extra_name='',$multiple=false) {
		if ($text_100=='none'){
			$text_100=_('Nobody');
		}
		$result = $this->getArtifactCreators();
		//	this was a bad hack to allow you to mass-update to unassigned, which is ID=100, which
		//	conflicted with the "No Change" ID of 100.
		$ids =& util_result_column_to_array($result,0);
		$names =& util_result_column_to_array($result,1);
		if ($extra_id != '-1') {
			$ids[]=$extra_id;
			$names[]=$extra_name;
		}

		if ($multiple) {
			if (!is_array($checked)) {
				$checked = explode(',',$checked);
			}
			return html_build_multiple_select_box_from_arrays ($ids,$names,$name,$checked,15,$show_100,$text_100);
		} else {
			return html_build_select_box_from_arrays ($ids,$names,$name,$checked,$show_100,$text_100);
		}
	}

	function cannedResponseBox ($name='canned_response',$checked='xzxz') {
		return html_build_select_box ($this->getCannedResponses(),$name,$checked);
	}

	/**
	 *	statusBox - show the statuses - automatically shows the "custom statuses" if they exist
	 *
	 *
	 */
	function statusBox ($name='status_id',$checked='xzxz',$show_100=false,$text_100='none') {
		if ($text_100=='none'){
			$text_100=_('None');
		}
		return html_build_select_box($this->getStatuses(),$name,$checked,$show_100,$text_100);
	}

	function header_internal($isadmin, $params) {
		global $HTML;

		if (!forge_get_config('use_tracker')) {
			exit_disabled();
		}

		$atid = $this->getID();
		$group_id = $this->Group->getID();
		$params['group'] = $group_id;
		$params['toptab'] = 'tracker';
		$params['tabtext'] = $this->getName();

		$text = array();
		$link = array();
		$ttip = array();

		if ($isadmin) {
			$text[] = _('New Tracker');
			$link[] = '/tracker/admin/?group_id=' . $group_id;
			$ttip[] = _('Create a new Tracker');
		}

		if (!$isadmin) {
			$text[] = _("View Trackers");
			$link[] = '/tracker/?group_id=' . $group_id;
			$ttip[] = _('-tooltip:tracker:viewtrackers');
		}

		$text[] = $this->getName();
		$link[] = '/tracker/?func=browse&amp;group_id=' . $group_id .
		    '&amp;atid=' . $atid;
		$ttip[] = _('Browse all items in the current Tracker');

		if (!$isadmin) {
			$text[] = _('Download .csv');
			$link[] = '/tracker/?func=downloadcsv&amp;group_id=' .
			   $group_id . '&amp;atid=' . $atid;
			$ttip[] = _('Download an overview over the items in the current Tracker as CSV file');

			if ($this->allowsAnon() || session_loggedin()) {
				$text[] = _('Submit New');
				$link[] = '/tracker/?func=add&amp;group_id=' .
				    $group_id . '&amp;atid=' . $atid;
				$ttip[] = _('Enter a new item into the current Tracker');
			}

			if (session_loggedin()) {
				$text[] = _('Reporting');
				$link[] = '/tracker/reporting/?group_id=' .
				    $group_id . '&amp;atid=' . $atid;
				$ttip[] = _('-tooltip:tracker:reporting');

				if ($this->isMonitoring()) {
					$text[] = _('Stop Monitor');
					$link[] = '/tracker/?group_id=' .
					    $group_id . '&amp;atid=' . $atid .
					    '&amp;func=monitor&amp;stop=1';
					$ttip[] = _('-tooltip:tracker:stopmonitor');
				}
			}

			if (!session_loggedin() || !$this->isMonitoring()) {
				$text[] = _('Monitor');
				$link[] = '/tracker/?group_id=' .$group_id .
				    '&amp;atid=' . $atid .
				    '&amp;func=monitor&amp;start=1';
				$ttip[] = _('-tooltip:tracker:startmonitor');
			}
		}

		if ($isadmin || (session_loggedin() &&
		    forge_check_perm('tracker', $atid, 'manager'))) {
			$text[] = _('Administration');
			$link[] = '/tracker/admin/?group_id=' . $group_id .
			    '&amp;atid=' . $atid;
			if ($isadmin) {
				$ttip[] = _('Administrating that tracker; click here to return to the Tracker Admin overview page');
			} else {
				$ttip[] = _('Administrate the current tracker');
			}
		}

		if ($isadmin) {
			$text[] = _('Update Settings');
			$link[] = '/tracker/admin/?group_id=' .$group_id .
			    '&amp;atid=' . $atid . '&amp;update_type=1';
			$ttip[] = _('Set up preferences like expiration times, email addresses.');

			$text[] = _('Manage Custom Fields');
			$link[] = '/tracker/admin/?group_id=' .$group_id .
			    '&amp;atid=' . $atid . '&amp;add_extrafield=1';
			$ttip[] = _('Add new boxes like Phases, Quality Metrics, Components, etc.  Once added they can be used with other selection boxes (for example, Categories or Groups) to describe and browse bugs or other artifact types.');

			$text[] = _('Reorder Custom Fields');
			$link[] = '/tracker/admin/?group_id=' .$group_id .
			    '&amp;atid=' . $atid . '&amp;efreorder=1';
			$ttip[] = _('Change the order in which Extra Fields are displayed in the middle column of the Tracker Item detail view.');

			$text[] = _('Manage Workflow');
			$link[] = '/tracker/admin/?group_id=' .$group_id .
			    '&amp;atid=' . $atid . '&amp;workflow=1';
			$ttip[] = _('Assign Role permissions to state transitions.');

			$text[] = _('Customise List');
			$link[] = '/tracker/admin/?group_id=' .$group_id .
			    '&amp;atid=' . $atid . '&amp;customize_list=1';
			$ttip[] = _('Customise order in which the columns in the tracker browse view are shown.');

			$text[] = _('Add/Update Canned Responses');
			$link[] = '/tracker/admin/?group_id=' .$group_id .
			    '&amp;atid=' . $atid . '&amp;add_canned=1';
			$ttip[] = _('Create/change generic response messages for the tracker.');

			$text[] = _('Clone Tracker');
			$link[] = '/tracker/admin/?group_id=' .$group_id .
			    '&amp;atid=' . $atid . '&amp;clone_tracker=1';
			$ttip[] = _('Duplicate parameters and fields from a template trackers in this one.');

			$text[] = _('Really delete the entire tracker');
			$link[] = '/tracker/admin/?group_id=' .$group_id .
			    '&amp;atid=' . $atid . '&amp;delete=1';
			$ttip[] = _('Irrevocably delete this entire Tracker and all its contents.');
		}

		$params['submenu'] = array($text, $link, $ttip);
		site_project_header($params);

		/*XXX if ($this) ← WTF? */
			plugin_hook("blocks", "tracker_" . $this->getName());
	}

	function _substExtraFields($mode, $efarr, $selected, $show_100, $text_100, $show_any, $text_any, $status_show_100, $template) {
		if ($mode == 'QUERY') {
			$keys = array_keys($efarr);
			for ($k = 0; $k < count($keys); $k++) {
				$i = $keys[$k];
				if ($efarr[$i]['field_type'] == ARTIFACT_EXTRAFIELDTYPE_SELECT ||
				    $efarr[$i]['field_type'] == ARTIFACT_EXTRAFIELDTYPE_CHECKBOX ||
				    $efarr[$i]['field_type'] == ARTIFACT_EXTRAFIELDTYPE_RADIO ||
				    $efarr[$i]['field_type'] == ARTIFACT_EXTRAFIELDTYPE_STATUS ||
				    $efarr[$i]['field_type'] == ARTIFACT_EXTRAFIELDTYPE_MULTISELECT) {
					$efarr[$i]['field_type'] = ARTIFACT_EXTRAFIELDTYPE_MULTISELECT;
				} else if ($efarr[$i]['field_type'] != ARTIFACT_EXTRAFIELDTYPE_TEXT) {
					$efarr[$i]['field_type'] = ARTIFACT_EXTRAFIELDTYPE_TEXT;
					$efarr[$i]['attribute1'] = 60;
					$efarr[$i]['attribute2'] = 999;
				}
			}
		}

		// 'DISPLAY' mode is for renderding in 'read-only' mode (for detail view).
		if ($mode === 'DISPLAY') {
			$keys = array_keys($efarr);
			for ($k = 0; $k < count($keys); $k++) {
				$i = $keys[$k];

				$value = util_ifsetor($selected[$efarr[$i]['extra_field_id']]);
				if (!is_array($value)) {
					$value = htmlspecialchars_decode($value);
				}

				if ($efarr[$i]['field_type'] == ARTIFACT_EXTRAFIELDTYPE_SELECT ||
				    $efarr[$i]['field_type'] == ARTIFACT_EXTRAFIELDTYPE_CHECKBOX ||
				    $efarr[$i]['field_type'] == ARTIFACT_EXTRAFIELDTYPE_RADIO ||
				    $efarr[$i]['field_type'] == ARTIFACT_EXTRAFIELDTYPE_STATUS ||
				    $efarr[$i]['field_type'] == ARTIFACT_EXTRAFIELDTYPE_MULTISELECT) {
					if ($value == 100) {
						$value = 'None';
					} else {
						$arr = $this->getExtraFieldElements($efarr[$i]['extra_field_id']);

						// Convert the values (ids) to names in the ids order.
						$new = array();
						for ($j = 0; $j < count($arr); $j++) {
							if (is_array($value)) {
								if (in_array($arr[$j]['element_id'],$value))
									$new[] = $arr[$j]['element_name'];
							} elseif ($arr[$j]['element_id'] === $value) {
									$new[] = $arr[$j]['element_name'];
							}
						}
						$value = join('<br />', $new);
					}
				} else if ($efarr[$i]['field_type'] == ARTIFACT_EXTRAFIELDTYPE_TEXT ||
				    $efarr[$i]['field_type'] == ARTIFACT_EXTRAFIELDTYPE_TEXTAREA) {
					$value = preg_replace('/((http|https|ftp):\/\/\S+)/',
					    "<a href=\"\\1\" target=\"_blank\">\\1</a>", $value);
				} else if ($efarr[$i]['field_type'] == ARTIFACT_EXTRAFIELDTYPE_DATETIME) {
					$value = datepick_format($value, true);
				} else if ($efarr[$i]['field_type'] == ARTIFACT_EXTRAFIELDTYPE_RELATION) {
					// Convert artifact id to links.
					$r = "";
					foreach (preg_split("/\D+/", $value) as $v) {
						$v = (int)$v;
						if (!$v) {
							continue;
						}
						$r .= html_e('a', array(
							'href' => util_make_url('/tracker/t_follow.php/' . $v),
						    ), sprintf('[#%d]', $v)) . "\n";
					}
					$value = $r;
				}
				$value = html_e('span', array(
					'style' => trim($value) ? false : 'color:#AAAAAA;',
					'id' => $this->geteftipid($efarr[$i]),
					'title' => util_unconvert_htmlspecialchars(html_get_tooltip_description($this->geteftipname($efarr[$i]))),
				    ), trim($value) ? $value : '-');

				$template = str_replace('{$PostName:'.$efarr[$i]['field_name'].'}',"",$template);
				$template = str_replace('{$'.$efarr[$i]['field_name'].'}',$value,$template);
			}
			echo $template;
			return;
		}

		$keys = array_keys($efarr);
		for ($k = 0; $k < count($keys); $k++) {
			$i = $keys[$k];
			$post_name = '';

			$value = util_ifsetor($selected[$efarr[$i]['extra_field_id']]);
			if (!is_array($value)) {
				$value = htmlspecialchars_decode($value);
			}

			if ($efarr[$i]['field_type'] == ARTIFACT_EXTRAFIELDTYPE_SELECT) {
				$str = $this->renderSelect($efarr[$i]['extra_field_id'],$value,$show_100,$text_100,$show_any,$text_any,false,$efarr[$i]);
			} elseif ($efarr[$i]['field_type'] == ARTIFACT_EXTRAFIELDTYPE_CHECKBOX) {
				$str = $this->renderCheckbox($efarr[$i]['extra_field_id'],$value,$show_100,$text_100,$efarr[$i]);
			} elseif ($efarr[$i]['field_type'] == ARTIFACT_EXTRAFIELDTYPE_RADIO) {
				$str = $this->renderRadio($efarr[$i]['extra_field_id'],$value,$show_100,$text_100,$show_any,$text_any,$efarr[$i]);
			} elseif ($efarr[$i]['field_type'] == ARTIFACT_EXTRAFIELDTYPE_DATETIME) {
				$p = array(
					'class' => 'form-control',
				    );
				if ($efarr[$i] !== false) {
					$p['id'] = $this->geteftipid($efarr[$i]);
					$p['title'] = util_unconvert_htmlspecialchars(html_get_tooltip_description($this->geteftipname($efarr[$i])));
				}
				$str = datepick_emit('extra_fields[' .
				    $efarr[$i]['extra_field_id'] . ']',
				    datepick_format($value, true), true,
				    'default', $p, false);
			} elseif ($efarr[$i]['field_type'] == ARTIFACT_EXTRAFIELDTYPE_TEXT ||
			    $efarr[$i]['field_type'] == ARTIFACT_EXTRAFIELDTYPE_INTEGER) {
				$str = $this->renderTextField($efarr[$i]['extra_field_id'],$value,$efarr[$i]['attribute1'],$efarr[$i]['attribute2'],$efarr[$i]);
				if ($mode == 'QUERY') {
					$post_name =  ' <i>'._('(%% for wildcards)').'</i>   ';
				}
			} elseif ($efarr[$i]['field_type'] == ARTIFACT_EXTRAFIELDTYPE_TEXTAREA) {
				$str = $this->renderTextArea($efarr[$i]['extra_field_id'],$value,$efarr[$i]['attribute1'],$efarr[$i]['attribute2'],$efarr[$i]);
				if ($mode == 'QUERY') {
					$post_name =  ' <i>'._('(%% for wildcards)').'</i>   ';
				}
			} elseif ($efarr[$i]['field_type'] == ARTIFACT_EXTRAFIELDTYPE_MULTISELECT) {
				$str = $this->renderMultiSelectBox($efarr[$i]['extra_field_id'],$value,$show_100,$text_100,$efarr[$i]);
			} elseif ($efarr[$i]['field_type'] == ARTIFACT_EXTRAFIELDTYPE_STATUS) {
				// Get the allowed values from the workflow.
				$atw = new ArtifactWorkflow($this, $efarr[$i]['extra_field_id']);

				// Special treatement for the initial step (Submit).
				// In this case, the initial value is the first value.
				if ($selected === true) {
					$selected_node = 100;
				} elseif ($value) {
					$selected_node = $value;
				} else {
					$selected_node = 100;
				}

				$allowed = $atw->getNextNodes($selected_node);
				$allowed[] = $selected_node;
				$str = $this->renderSelect($efarr[$i]['extra_field_id'],$selected_node,$status_show_100,$text_100,$show_any,$text_any,$allowed,$efarr[$i]);
			} elseif ($efarr[$i]['field_type'] == ARTIFACT_EXTRAFIELDTYPE_RELATION) {
				$str = $this->renderRelationField($efarr[$i]['extra_field_id'],$value,$efarr[$i]['attribute1'],$efarr[$i]['attribute2'],$efarr[$i]);
				if ($mode == 'UPDATE') {
					$post_name = html_image('ic/forum_edit.gif','37','15',array('title'=>"Click to edit", 'alt'=>"Click to edit", 'onclick'=>"switch2edit(this, 'show$i', 'edit$i')"));
				}
			}
			$template = str_replace('{$PostName:'.$efarr[$i]['field_name'].'}',$post_name,$template);
			$template = str_replace('{$'.$efarr[$i]['field_name'].'}',$str,$template);
		}
		if ($template) {
			echo $template;
		}
	}
}

// Local Variables:
// mode: php
// c-file-style: "bsd"
// End:
