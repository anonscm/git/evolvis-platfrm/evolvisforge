<?php
/**
 * JSON information on Tracker Items for FusionForge
 *
 * Copyright © 2010, 2012, 2018
 *	Thorsten “mirabilos” Glaser <t.glaser@tarent.de>
 * All rights reserved.
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

require_once('../env.inc.php');
require_once $gfcommon.'include/pre.php';
require_once $gfcommon.'pm/ProjectTaskSqlQueries.php';
require_once $gfwww.'tracker/include/ArtifactHtml.class.php';

/*-
 * We have things like protected properties. We have abstract methods.
 * We have all this stuff that your computer science teacher told you
 * you should be using. I don't care about this crap at all.
 * -- Rasmus Lerdorf
 */

$aid = getIntFromRequest('aid');
if (!$aid)
	$aid = util_path_info_last_numeric_component();
if (!$aid) {
	exit_missing_param('', array(_('Tracker Item ID')), 'tracker');
}

$tinfo = getArtefactTrackerIdGroupId($aid);

if (!$tinfo) {
	exit_error(_('No Tracker Item with ID: ') . $aid, 'tracker');
}

$asuser = getStringFromRequest('asuser');

if (getIntFromRequest('text')) {
	$asformat = "text/plain; charset=\"UTF-8\"";
	$jsonindent = "";
} else {
	$asformat = "application/json; charset=\"UTF-8\"";
	$jsonindent = false;
}

$islogin = session_loggedin();
$isadmin = forge_check_global_perm('forge_admin');
$ishttps = session_issecure();
$ispublic = isArtefactInfoPublic($aid, $tinfo['atid']);

if (!$ishttps) {
	$islogin = false;
	$isadmin = false;
}

if ($ispublic) {
	$showall = true;
} else if ($islogin) {
	if (!$isadmin || !$asuser) {
		/* operate as ourselves */
		$asuser = session_get_user()->getUnixName();
	}

	if (forge_check_perm_for_user(user_get_object_by_name($asuser),
	    'tracker', $tinfo['atid'], 'read')) {
		$showall = true;
	} else {
		$showall = false;
	}
} else {
	$showall = false;
}

$xinfo = array(
	'_permalink' => util_make_url('/tracker/t_follow.php/' . $aid),
	'artifact_id' => $tinfo['aid'],
	'forge_base' => forge_get_config('web_host'),
	'forge_name' => forge_get_config('forge_name'),
	'group_artifact_id' => $tinfo['atid'],
	'group_id' => $tinfo['group_id'],
	'is_a' => $tinfo['is_a'],
	'public' => $ispublic,
    );

if ($showall) {
	$ah =& artifact_get_object($aid);
	if (!$ah || !is_object($ah) || $ah->isError()) {
		echo "error item $aidx\n";
		db_rollback();
		die;
	}

	foreach ($ah->data_array as $k => $v) {
		/* skip numeric fields */
		if (!preg_match('/^[a-z]/', $k)) {
			continue;
		}

		/* distinguish actions for specific fields */
		switch ($k) {
		case 'summary':
		case 'details':
			/* fix mistake of how stuff is stored in the DB */
			$v = util_unconvert_htmlspecialchars($v);
			/* fix issue with how stuff may be stored in the DB */
			$v = util_sanitise_multiline_submission($v);
			/* but export using logical newlines */
			$v = str_replace("\r\n", "\n", $v);
			/* now we’ve got something we can use */
			$xinfo[$k] = $v;
			break;

		case 'artifact_id':
		case 'assigned_to':
		case 'close_date':
		case 'group_artifact_id':
		case 'group_id':
		case 'last_modified_date':
		case 'open_date':
		case 'priority':
		case 'status_id':
		case 'submitted_by':
			$xinfo[$k] = (int)$v;
			break;

		default:
			$xinfo[$k] = $v;
			break;
		}
	}

	$xinfo['group_artifact_realname'] = $ah->getArtifactType()->getName();
	$xinfo['group_artifact_unixname'] = $ah->getArtifactType()->getUnixName();
	$xinfo['group_realname'] = $ah->getArtifactType()->getGroup()->getPublicName(true);
	$xinfo['group_unixname'] = $ah->getArtifactType()->getGroup()->getUnixName();
	$xinfo['_votes'] = array_combine(array(
		'votes',
		'voters',
		'votage_percent',
	    ), $ah->getVotes());
	$xinfo['_related_tasks'] = array();
	if ($ah->getArtifactType()->getGroup()->usesPM()) {
		$taskcount = db_numrows($ah->getRelatedTasks());
		if ($taskcount >= 1) for ($i = 0; $i < $taskcount; ++$i) {
			$taskinfo = db_fetch_array($ah->relatedtasks, $i);
			$xinfo['_related_tasks'][] =
			    (int)$taskinfo['project_task_id'];
		}
		sort($xinfo['_related_tasks'], SORT_NUMERIC);
	}

	if (util_ifsetor($xinfo['assigned_unixname']))
		unset($xinfo['assigned_email']);
	if (util_ifsetor($xinfo['submitted_unixname']))
		unset($xinfo['submitted_email']);
}

sysdebug_off("Content-type: " . $asformat);
echo minijson_encode($xinfo, $jsonindent) . "\n";
exit;
