<?php
/*-
 * FusionForge form for applying a template tracker ("cloning")
 *
 * Copyright © 2012, 2014
 *	Thorsten Glaser <t.glaser@tarent.de>
 * All rights reserved.
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

$tlist = group_get_template_projects();
sortProjectList($tlist, 'id', true);

$ids = array();
$names = array();
$has_error = false;

function atsortbyname($a, $b) {
	$av = $a->getName();
	$bv = $b->getName();

	return strcoll($av, $bv);
}

foreach ($tlist as $tp) {
	if (!$tp || !is_object($tp) || $tp->isError()) {
		/* skip it */
		$has_error = true;
		continue;
	}
	$atf = new ArtifactTypeFactory($tp);
	if (!$atf || !is_object($atf) || $atf->isError()) {
		$has_error = true;
		continue;
	}
	$ats = $atf->getArtifactTypes(true);
	$ata = array();
	foreach ($ats as $at) {
		if (!$at || !is_object($at) || $at->isError()) {
			$has_error = true;
			continue;
		}
		$ata[$at->getID()] = $at;
	}
	uasort($ata, 'atsortbyname');
	foreach ($ata as $aid => $at) {
		$ids[] = $aid;
		$names[] = sprintf('%s::%s (%d::%d)',
		    $tp->getPublicName(), $at->getName(),
		    $tp->getID(), $aid);
	}
}

if (!$ids) {
	exit_error($has_error ? _('Error looking up template projects') :
	    _('No template project found'), 'tracker');
}

if ($has_error) {
	$warning_msg = _('Some errors occured during template project lookup');
}

$ath->adminHeader(array('title' => _('Apply Template Tracker')));

echo "<p>" . _('Choose the template tracker to clone.') . "</p>\n";

echo '<form action="' . getStringFromServer('PHP_SELF') .
    '?group_id=' . $group_id . '&amp;atid=' . $ath->getID() .
    '" method="post">
	<input type="hidden" name="clone_tracker" value="y" />
	<div class="warning">' .
    _('WARNING!!! Cloning this tracker will duplicate all extra fields and their elements into this tracker. There is nothing to prevent you from cloning multiple times or making a huge mess. You have been warned!') .
    '</div>
	<p>' . html_build_select_box_from_arrays($ids, $names,
    'clone_id', '', false) . '</p>
	<input type="submit" name="post_changes" value="' .
    _('Submit') . '" />
</form>';

$ath->footer(array());
