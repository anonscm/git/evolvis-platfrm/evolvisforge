<?php
/*-
 * FusionForge tracker form to define a workflow
 *
 * Copyright 2010, FusionForge Team
 * Copyright © 2012
 *	Thorsten Glaser <t.glaser@tarent.de>
 * All rights reserved.
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

require_once $gfcommon.'include/EvolvisTable.class.php';
require_once $gfcommon.'tracker/ArtifactWorkflow.class.php';

$has_error = false;
$efarr = $ath->getExtraFields(array(ARTIFACT_EXTRAFIELDTYPE_STATUS));
if (count($efarr) === 0) {
	$has_error = true;
	$error_msg .= _('To create a workflow, you need first to create a custom field of type \'Status\'.');
} elseif (count($efarr) !== 1) {
	$has_error = true;
	$error_msg .= _('Internal error: Illegal number of status fields (only one allowed).');
}

$ath->adminHeader(array(
	'title' => _('Configure workflow'),
	'pagename' => 'tracker_admin_customize_liste',
	'titlevals' => array($ath->getName()),
    ));

if ($has_error) {
	echo html_e('p', array(),
	    util_html_encode(_('Cannot continue due to an error.')));
	$ath->footer(array());
	exit(0);
}

$keys = array_keys($efarr);
$field_id = $keys[0];
$field_name = $efarr[$field_id]['field_name'];

$atw = new ArtifactWorkflow($ath, $field_id);

$elearray = $ath->getExtraFieldElements($field_id);
$states = $elearray;

echo html_e('h2', array(),
    util_html_encode(sprintf(_('Allowed initial values for the %1$s field'),
    $field_name))) . html_eo('form', array(
	'action' => util_gethref(false, array(
		'group_id' => $group_id,
		'atid' => $ath->getID(),
	    ), false),
	'method' => 'post',
    )) . html_e('input', array(
	'type' => 'hidden',
	'name' => 'field_id',
	'value' => $field_id,
    )) . html_e('input', array(
	'type' => 'hidden',
	'name' => 'workflow',
	'value' => 1,
    ));

/* cache gettext lookups */
$from = _('From').' ';
$to = _('To').' ';

$titles = array('');
foreach ($elearray as $status) {
	$titles[] = $status['element_name'];
}
$e = new EvolvisTable($titles);

$r = $e->tr(array(
	'id' => 'initval',
	'style' => 'text-align:left;',
    ));
$r->th()->set(_('Initial values'));
$next = $atw->getNextNodes('100');
foreach ($states as $s) {
	$r->td()->setraw(html_e('input', array(
		'type' => 'checkbox',
		'name' => 'wk[100][' . $s['element_id'] . ']',
		'checked' => (in_array($s['element_id'], $next) ? 'checked' :
		    false),
	    )) . ' ' . html_image('spacer.gif', 20, 20));
}

echo $e->emit() . html_e('h2', array(),
    util_html_encode(sprintf(_('Allowed transitions for the %1$s field'),
    $field_name)));

$count = count($titles);
for ($i = 1; $i < $count; ++$i) {
	$titles[$i] = $to . $titles[$i];
}
$e = new EvolvisTable($titles);

$i = 1;
foreach ($elearray as $status) {
	$r = $e->tr(array(
		'id' => ('configuring-' . $i++),
		'style' => 'text-align:left;',
	    ));
	$r->th()->set($from . $status['element_name']);
	$next = $atw->getNextNodes($status['element_id']);
	foreach ($states as $s) {
		$r->td()->setraw($status['element_id'] === $s['element_id'] ?
		    html_e('input', array(
			'type' => 'checkbox',
			'checked' => 'checked',
			'disabled' => 'disabled',
		    )) : (html_e('input', array(
				'type' => 'checkbox',
				'name' => 'wk[' . $status['element_id'] .
				    '][' . $s['element_id'] . ']',
				'checked' => (in_array($s['element_id'], $next) ?
				    'checked' : false),
			    )) . ' ' . (in_array($s['element_id'], $next) ?
			    html_e('a', array(
				'href' => util_gethref(false, array(
					'group_id' => $group_id,
					'atid' => $ath->getID(),
					'workflow_roles' => 1,
					'from' => $status['element_id'],
					'next' => $s['element_id'],
				    ), false),
				'title' => _('Edit roles'),
			    ), html_image('ic/acl_roles20.png', 20, 20, array(
				'alt' => _('Edit roles'),
			    ))) : html_image('spacer.gif', 20, 20))));
	}
}

echo $e->emit() . html_e('div', array(
	'class' => 'tips',
    ), sprintf(_('Tip: Click on %s to configure allowed roles for a transition (all by default).'),
    html_image('ic/acl_roles20.png', 20, 20, array(
	'alt' => _('Edit roles'),
    )))) . html_e('p', array(), html_e('input', array(
	'type' => 'submit',
	'name' => 'post_changes',
	'value' => _('Submit'),
    ))) . '</form>';

$ath->footer(array());
