<?php
/*-
 * EvolvisForge Tracker Facility
 *
 * Copyright © 2012
 *	Thorsten “mirabilos” Glaser <t.glaser@tarent.de>
 * Copyright © 2010
 *	FusionForge Team
 * All rights reserved.
 *
 * This file is part of EvolvisForge. EvolvisForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * EvolvisForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with EvolvisForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *-
 * Visual aid to adjust the display order of extra fields in the
 * tracker item detail view.
 */

require_once $gfcommon.'include/EvolvisTable.class.php';

$new_order = false;
if (($eforder = getStringFromRequest('eforder'))) {
	$new_order = explode('-', $eforder);
} elseif (getStringFromRequest('efresetorder')) {
	$new_order = array(0);
} elseif (getStringFromRequest('efresort')) {
	$order = $ath->getRenderOrder();
	$o_sortable = $order[1];
	if (count($o_sortable) < 1) {
		exit_error(_('You have not defined any extra fields'),
		    'tracker');
	}
	$toskip = $order[0];
	while ($toskip--) {
		array_shift($o_sortable);
	}
	$i = 0;
	$j = count($o_sortable);
	$pass_order = array_flip(getArrayFromRequest('order'));
	$new_order = array();
	while ($i < $j) {
		if (($v = util_ifsetor($pass_order[$i + 1])) &&
		    util_nat0($v) &&
		    ($k = array_search($v, $o_sortable)) !== false) {
			unset($o_sortable[$k]);
			$new_order[] = $v;
		} else {
			/* put in placeholders */
			$new_order[] = NULL;
		}
		$i++;
	}
	$i = 0;
	while ($i < $j) {
		if ($new_order[$i] === NULL) {
			$new_order[$i] = array_shift($o_sortable);
		}
		$i++;
	}
}

$eftypes = ArtifactExtraField::getAvailableTypes();
$efarr = $ath->getExtraFields();
$order = $ath->getRenderOrder($new_order);

if (count($order[1]) < 1) {
	exit_error(_('You have not defined any extra fields'), 'tracker');
}

$t = new EvolvisTable(array(
	_('Up/Down positions'),
	_('Current/New positions'),
	'' /* mandatory? */,
	_('Field Name'),
	'' /* [edit] */,
	_('Type'),
    ));

$o_const = array();
$o_sortable = $order[1];
$toskip = $order[0];
while ($toskip--) {
	$o_const[] = array_shift($o_sortable);
}

$toskip = $order[0];
$i = 0;
foreach ($order[1] as $k) {
	$r = $t->tr();
	if ($toskip) {
		$toskip--;
		$r->td(array(
			'colspan' => 2,
			'align' => 'center',
		    ))->set(_('Static position in left column'));
	} else {
		$o_sortable_up = $o_sortable;
		if ($i == 0) {
			$j = array_shift($o_sortable_up);
			$o_sortable_up[] = $j;
		} else {
			array_splice($o_sortable_up, $i - 1, 2,
			    array($o_sortable[$i], $o_sortable[$i - 1]));
		}
		$o_sortable_down = $o_sortable;
		if ($i < count($o_sortable) - 1) {
			array_splice($o_sortable_down, $i, 2,
			    array($o_sortable[$i + 1], $o_sortable[$i]));
		} else {
			$j = array_pop($o_sortable_down);
			array_unshift($o_sortable_down, $j);
		}
		$r->td(array(
			'align' => 'center',
		    ))->setRaw('   ' . html_e('a', array(
			'href' => util_gethref(false, array(
				'group_id' => $group_id,
				'atid' => $ath->getID(),
				'efreorder' => 1,
				'eforder' => implode('-', $o_sortable_up),
			    ), false),
		    ), html_image('ic/btn_up.png', '19', '18', array(
			'alt' => "Up",
		    ))) . '  ' . html_e('a', array(
			'href' => util_gethref(false, array(
				'group_id' => $group_id,
				'atid' => $ath->getID(),
				'efreorder' => 1,
				'eforder' => implode('-', $o_sortable_down),
			    ), false),
		    ), html_image('ic/btn_down.png', '19', '18', array(
			'alt' => "Down",
		    ))));
		$r->td(array(
			'align' => 'right',
		    ))->setRaw(util_html_encode(++$i . ' --> ') .
		    html_e('input', array(
			'type' => 'text',
			'name' => 'order[' . $k . ']',
			'value' => '',
			'size' => 3,
			'maxlength' => 3,
		    )));
	}
	$r->td(array(
		'style' => 'width:1em;',
	    ))->setRaw($efarr[$k]['is_required'] ? utils_requiredField() : '');
	$r->td()->set($efarr[$k]['field_name']);
	$r->td(array(
		'align' => 'center',
	    ))->setRaw(html_e('a', array(
		'href' => util_gethref(false, array(
			'update_box' => 1,
			'id' => $k,
			'group_id' => $group_id,
			'atid' => $ath->getID(),
		    ), false),
	    ), html_image('ic/forum_edit.gif', '37', '15', array(
		'alt' => "Edit",
	    ))));
	$r->td()->set($eftypes[$efarr[$k]['field_type']]);
}

$r = $t->tr();
$r->td(array(
	'colspan' => 2,
	'align' => 'right',
    ))->setRaw(html_e('input', array(
	'type' => 'submit',
	'name' => 'efresort',
	'value' => _('Reorder'),
    )));
$r->td(array(
	'colspan' => 4,
	'align' => 'center',
    ))->setRaw(html_e('input', array(
	'type' => 'submit',
	'name' => 'efresetorder',
	'value' => _('Reset to Default'),
    )));

/* now, present the page */

$ath->adminHeader(array(
	'title' => sprintf(_('Reorder Custom Fields for %s'),
	    $ath->getName()),
    ));

echo html_e('form', array(
	'action' => util_gethref(false, array(
		'group_id' => $group_id,
		'atid' => $ath->getID(),
		'efreorder' => 1,
	    ), false),
	'method' => 'post',
    ), $t->emit());

$ath->footer(array());
