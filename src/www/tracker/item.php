<?php
/*-
 * Tracker Item detail view / modification interface for FusionForge
 *
 * Copyright © 2011, 2012, 2013, 2022
 *	Thorsten “mirabilos” Glaser <t.glaser@tarent.de>
 * Copyright © 2011
 *	Katja Hapke <k.hapke@tarent.de>
 * Copyright © 2011
 *	Alain Peyrat – Alcatel-Lucent
 * Copyright © 2010
 *	Franck Villaume – Capgemini
 * Copyright © 2005
 *	GForge, LLC
 * Copyright © 1999-2001
 *	VA Linux Systems
 * All rights reserved.
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *-
 * Calling chain for this file:
 * www/tracker/index.php
 *	includes necessary classes
 *	provides $group_id, $atid (≠ 0), $aid
 *	sets and checks $group
 * www/tracker/tracker.php (func='detail' or 'add')
 *	sets and checks $ath ($group, $atid)
 *	calls form submission actions
 *	sets and checks $ah ($ath, $aid) for 'detail'
 *	define $is_add = false for 'detail' or true for 'add'
 *
 * That means we can use the following variables already:
 *	$group_id = $group->getID()
 *	$atid = $ath->getID()
 *	$aid = $ah->getID() for 'detail'
 */

define('TRK_VIEW', 0);
define('TRK_TECH', 1);
define('TRK_FULL', 2);

html_use_tooltips(array(
	/* part of defaults */
	'html' => true,
	/* use form fields */
	'trigger' => 'autofocus',
    ));

require_once $gfcommon.'include/descriptive.php';

if (forge_check_perm('tracker', $atid, 'manager')) {
	$accesslevel = TRK_FULL;
} else if (forge_check_perm('tracker', $atid, 'tech')) {
	$accesslevel = TRK_TECH;
} else {
	$accesslevel = TRK_VIEW;
}

if ($accesslevel >= TRK_FULL && $sysdebug_enable) {
	/* debugging function: look at this page the way a user would */
	$reduce_accesslevel = getIntFromRequest('reduce_accesslevel');
	if ($reduce_accesslevel == 1) {
		$accesslevel = TRK_TECH;
	} else if ($reduce_accesslevel == 2) {
		$accesslevel = TRK_VIEW;
	}
}

if ($ath->usesCustomStatuses() &&
    ($res = db_query_params('SELECT aefe.element_id, a_s.status_name
	FROM artifact_extra_field_elements aefe, artifact_status a_s
	WHERE aefe.status_id=a_s.id
	AND aefe.extra_field_id=$1',
    array($ath->getCustomStatusField()))) &&
    db_numrows($res) >= 1) {
	$smap = array();
	while ($row = db_fetch_array($res)) {
		$smap[$row['element_id']] = $row['status_name'];
	}
	$HTML->extra_js[] = 'var status_map = ' .
	    minijson_encode($smap, false) . ';';
	$needstatusjs = true;
} else {
	$needstatusjs = false;
}

if (!$ath->remapStatus(1, false)) {
	exit_error(_('Custom status field with no elements, please add some!'),
	    'tracker');
}

if ($is_add) {
	$ath->header(array(
		'title' => sprintf(_('%1$s %2$s: Submit New'),
		    $group->getPublicName(), $ath->getName()),
	    ));
	echo $ath->renderSubmitInstructions();
} else {
	html_use_lightbox();
	$ath->header(array(
		'title' => '[#' . $aid . '] ' . $ah->getSummary(),
		'atid' => $atid,
	    ));
}

function getselfhref($p=array(),$return_encoded=true) {
	global $group_id, $atid, $aid, $is_add;

	$p['group_id'] = $group_id;
	$p['atid'] = $atid;
	if (!$is_add) {
		/* grml… */
		$p['aid'] = $aid;
		$p['artifact_id'] = $aid;
	}
	return util_gethref(false, $p, $return_encoded);
}

function gettipspan($idpart, $content) {
	$id = 'tracker-' . str_replace(' ', '_', $idpart);
	return '<span id="' . $id . '" title="' .
	    html_get_tooltip_description($idpart) . '">' .
	    $content . '</span>';
}

$data_extrafields = $is_add ? array() : $ah->getExtraFieldData();
$mode_extrafields = $is_add || ($accesslevel >= TRK_TECH) ? 'UPDATE' : 'DISPLAY';
$skip_extrafields = array();

?>
<form id="trackeritemform" action="<?php echo getselfhref(); ?>"
 enctype="multipart/form-data" method="post">
<input type="hidden" name="form_key" value="<?php echo form_generate_key(); ?>" />
<input type="hidden" name="func" value="<?php echo ($is_add ? "postadd" : "postmod"); ?>" />
<input type="hidden" name="MAX_FILE_SIZE" value="10000000" />

<?php
echo $HTML->boxTop(_('Details'), 'Details', false, 'tracker_item_details');
?>
<table width="100%" cellspacing="12" cellpadding="3" style="table-layout:fixed">
<tr align="left" valign="top">
<?php
echo "<td " . $HTML->boxGetAltRowStyle(0) . " width=\"30%\">\n";
$i = false;
if ($ath->usesCustomStatuses()) {
	function esm_callback($i) {
		global $data_extrafields;

		$esm = util_ifsetor($data_extrafields[$i['extra_field_id']]);
		$res = $esm === false ? false :
		    db_query_params('SELECT a_s.status_name
			FROM artifact_extra_field_elements aefe,
			    artifact_status a_s
			WHERE aefe.status_id=a_s.id
			AND aefe.element_id=$1',
		    array($esm));
		if (!$res || db_numrows($res) < 1) {
			$esm = ' ';
		} else {
			$esm = '(' . db_result($res, 0,
			    'status_name') . ')';
		}
		return ' <span id="efstatusmap">' . $esm . '</span>';
	}
	$i = $ath->renderExtraField($data_extrafields, true, 'none',
	    false, 'Any', array(), false, $mode_extrafields, 'Status',
	    'esm_callback');
	$skip_extrafields[] = 'Status';
} elseif (!$is_add) {
	echo "\t<div><strong>" . _('Status') . ":</strong><br />\n";
	if ($accesslevel >= TRK_TECH) {
		echo $ath->statusBox('status_id', $ah->getStatusID());
	} else {
		echo gettipspan('status_id', $ah->getStatusName());
	}
	echo "</div>\n";
}
if (!$i || $i['field_name'] != 'Resolution') {
	$ath->renderExtraField($data_extrafields, true, 'none', false,
	    'Any', array(), false, $mode_extrafields, 'Resolution');
}
$skip_extrafields[] = 'Resolution';
?>
	<div><strong><?php echo _('Priority'); ?>:</strong><br /><?php
if ($accesslevel >= TRK_TECH) {
	echo html_build_priority_select_box('priority',
	    $is_add ? 3 : $ah->getPriority());
} else {
	echo gettipspan('priority', $is_add ? 3 : $ah->getPriority());
}
?></div>
<?php
if (!$i || $i['field_name'] != 'Severity') {
	$ath->renderExtraField($data_extrafields, true, 'none', false,
	    'Any', array(), false, $mode_extrafields, 'Severity');
}
$skip_extrafields[] = 'Severity';
if (!$is_add || $accesslevel >= TRK_FULL) {
	echo '<div>';
	if ($accesslevel >= TRK_FULL) {
		echo $ath->technicianBox('assigned_to',
		    $is_add ? 'xzxz' : $ah->getAssignedTo(),
		    true, 'none', '-1', '', false, _('Assigned to'));
	} else {
		echo html_e('strong', array(), _('Assigned to') . ':') .
		    html_e('br') . gettipspan('assigned_to',
		    $ah->getAssignedRealName() . ' (<tt>' .
		    $ah->getAssignedUnixName() . '</tt>)');
	}
	echo "</div>\n";
}
if (!$is_add) {
	$votes = $ah->getVotes();
	echo html_e('p', array(), gettipspan('votes',
	    html_e('strong', array(), _('Votes') . ':') . ' ' .
	    sprintf('%1$d/%2$d (%3$d%%)', $votes[0], $votes[1], $votes[2])));
}
?>
</td>
<td <?php echo $HTML->boxGetAltRowStyle(0); ?> width="40%"><?php
$ath->renderExtraFields($data_extrafields, true, 'none', false,
    'Any', array(), false, $mode_extrafields, false, $skip_extrafields);
?></td>
<td <?php echo $HTML->boxGetAltRowStyle(0); ?> width="30%">
<?php
if ($is_add) {
	echo html_e('div', array(), html_e('strong', array(),
	    _('New submission')));
} else {
?>
	<div><strong><?php echo _('Date Submitted'); ?>:</strong><br /><?php
	echo date(_('Y-m-d H:i'), $ah->getOpenDate());
	$close_date = $ah->getCloseDate();
	/*XXX hardcoded status ID */
	if ($ah->getStatusID() == 2 && $close_date > 1) {
		echo "\n<br /><strong>" . _('Date Closed') . ':</strong><br />' .
		    datepick_format($close_date, true);
	}
?></div>
	<p><strong><?php echo _('Submitted by'); ?>:</strong><br /><?php
	if (($xu = user_get_object($ah->getSubmittedBy())) &&
	    $xu->getID() != 100) {
		echo util_display_user($xu->getUnixName(), $xu->getID(),
		    sprintf('%s (<tt>%s</tt>)', $xu->getRealName(),
		    $xu->getUnixName()), 'xs', true, true);
	} elseif ($ah->getSubmittedBy() != 100) {
		echo $ah->getSubmittedRealName();
		echo ' (<tt>' . util_make_link_u($ah->getSubmittedUnixName(),
		    $ah->getSubmittedBy(), $ah->getSubmittedUnixName()) . '</tt>)';
	} else {
		echo $ah->getSubmittedRealName();
	}
?></p>
<?php } ?>
<?php if (!$is_add && $accesslevel >= TRK_FULL) { ?>
	<p><strong><?php echo _('Data Type'); ?>:</strong><br /><?php
	$atf = new ArtifactTypeFactory($group);
	$tids = array();
	foreach ($atf->getArtifactTypes() as $at) {
		if (forge_check_perm('tracker', $at->getID(), 'manager')) {
			$tids[] = $at->getID();
		}
	}
	$res = db_query_params('SELECT group_artifact_id, name
	    FROM artifact_group_list WHERE group_artifact_id = ANY ($1)',
	    array(db_int_array_to_any_clause($tids)));
	echo html_build_select_box($res, 'new_artifact_type_id', $atid, false);
?></p>
<?php } ?>
<?php if (!$is_add) { ?>
	<p><strong><?php
	$permalink = util_make_url('/tracker/t_follow.php/' . $aid);
	echo '<a href="' . $permalink . '">' . _('Permalink') . '</a>';
	echo "</strong><br />\n";
?>
		<span class="smaller gentium"><?php echo $permalink; ?></span>
	</p>
	<p><strong><?php echo _('Item Detail Information'); ?>:</strong><br /><?php
	echo util_make_link('/tracker/t_lookup.php?aid=' . $aid,
	    'application/json') . ' / ' .
	    util_make_link('/tracker/t_lookup.php?text=1&amp;aid=' . $aid,
	    'text/plain');
?></p>
<?php } ?>
<?php if (!$is_add && session_loggedin()) { ?>
	<p><?php
	if (($dostop = $ah->isMonitoring())) {
		$key = "monitorstop";
		$txt = _('Stop monitor');
	} else {
		$key = "monitor";
		$txt = _('Monitor');
	}
	echo gettipspan('monitor', '<a href="' .
	    getselfhref(array('func' => 'monitor')) . '"><strong>' .
	    '<span style="margin-right:10px;">' .
	    $HTML->getMonitorOPic(!$dostop) . '</span>' . $txt .
	    '</strong></a>');
?></p>
<?php
	if ($ath->canVote()) {
		if ($ah->hasVote()) {
			$key = 'thumb_down';
			$txt = _('Retract Vote');
		} else {
			$key = 'thumb_up';
			$txt = _('Cast Vote');
		}
		echo '<p>' . gettipspan('vote', '<a href="' .
		    getselfhref(array('func' => $key)) . '"><strong>' .
		    '<span style="margin-right:14px;">' . html_image('ic/' .
		    $key . '16.png', '16', '16', array('border' => '0')) .
		    '</span>' . $txt . '</strong></a>') . "</p>\n";
	}
	if ($accesslevel >= TRK_FULL) {
?>
	<p><a href="<?php
		echo getselfhref(array('func' => 'deleteartifact'));
?>"><strong>
		<span style="margin-right:14px;"><?php
		echo html_image('ic/trash.png', '16', '16',
		    array('border' => '0')) . "</span>" .
		    _('Delete'); ?></strong></a></p>
<?php
	}
}
?>
</td>
</tr>
<?php if (session_loggedin()) { ?>
<tr align="left" valign="top"><td colspan="3" align="right">
	<input type="submit" name="submit"
	 value="<?php echo _('Save Changes'); ?>" />
</td></tr>
<?php } ?>
</table>

<?php
if (!session_loggedin()) {
	echo $HTML->boxMiddle(_('Please login'));
?>
<div style="width:80%; position:relative; left:10%; margin-top:12px;">
	<div class="warning_msg"><?php
	echo html_e('a', array(
		'href' => util_make_url('/account/login.php?return_to=' .
		    urlencode(getStringFromServer('REQUEST_URI'))),
	    ), _('Please log in.'));
	?></div>
</div>
<?php
}
if (!$is_add && $accesslevel >= TRK_FULL && $sysdebug_enable) {
	/* debugging function: look at this page the way a user would */
	echo $HTML->boxMiddle(_('Debugging Functions'), 'Debugging_Functions',
	    true, 'tracker_item_debug');
?>
<div style="width:80%; position:relative; left:10%;">
	You have <tt>TRK_FULL</tt> access, and this is a debugging system.
	If you want, you can look at this page with reduced access levels,
	e.g. if you’re a Forge developer. – <a href="<?php
	echo getselfhref(array('func' => 'detail',
	    'reduce_accesslevel' => 1)); ?>">TRK_TECH</a> – <a href="<?php
	echo getselfhref(array('func' => 'detail',
	    'reduce_accesslevel' => 2)); ?>">TRK_VIEW</a> – To get back,
	just access the Permalink of this entry, or remove the HTTP GET
	parameter <tt>reduce_accesslevel</tt> and reload.
</div>
<?php
}

echo $HTML->boxMiddle(_('Item Description'), 'Item_Description', false,
    'tracker_item_description');
?>
<table width="100%" cellspacing="12" style="table-layout:fixed;">
<tr align="left" valign="top">
<td <?php echo $HTML->boxGetAltRowStyle(0); ?> width="50%">
	<div style="margin-bottom:1em;"><strong><?php echo _('Summary') .
	    utils_requiredField(); ?>:</strong><br /><?php
if ($is_add || $accesslevel >= TRK_FULL) {
	echo emit_summary_field($is_add ? NULL : $ah, 'summary',
	    'tracker-summary', false,
	    util_unconvert_htmlspecialchars(html_get_tooltip_description('summary')));
} else {
	echo gettipspan('summary', emit_summary($ah));
}
echo "</div>\n";

if ($is_add || $accesslevel >= TRK_FULL) {
	$fieldname = $is_add ? "details" : "description";
	echo '<div';
	if (!$is_add) {
		echo ' id="edit" style="display:none;"';
	}
	echo '><strong>' . _('Detailed description') .
	    utils_requiredField() . ': ' .
	    notepad_button('document.forms.trackeritemform.' . $fieldname,
	    "messformat") . '</strong><br />';
	echo emit_details_field($is_add ? NULL : $ah,
	    $fieldname, 'tracker-description',
	    util_unconvert_htmlspecialchars(html_get_tooltip_description('description')));
	echo "</div>\n";
}

if (!$is_add) {
	echo emit_details($ah, _('Detailed description'),
	    ($accesslevel >= TRK_FULL));
}
?>
</td>
<td <?php echo $HTML->boxGetAltRowStyle(0); ?> width="50%">
<?php
if (!$is_add) {
	showmess(_('Followups'), getselfhref(array('func' => 'detail'), false),
	    $ah, _('No Followups Have Been Posted'),
	    ($accesslevel >= TRK_TECH || $ath->allowsAnon() ||
	    session_loggedin()));
}
?>
</td>
</tr>
<tr align="left" valign="top">
<td <?php echo $HTML->boxGetAltRowStyle(0); ?> width="50%">
<?php
if (!$is_add) {
	$ath->renderFiles($group_id, $ah);
}
if ($is_add) {
	echo '<h3>' . _('Attach files to this submission') . ":</h3>\n";
	echo '<input type="file" name="input_file0" size="42" />' . "\n";
	echo '<input type="file" name="input_file1" size="42" />' . "\n";
	echo '<input type="file" name="input_file2" size="42" />' . "\n";
	echo '<input type="file" name="input_file3" size="42" />' . "\n";
	echo '<input type="file" name="input_file4" size="42" />' . "\n";
} else if ($accesslevel >= TRK_TECH ||
    (session_loggedin() && ($ah->getSubmittedBy() == user_getid()))) {
	echo '<h3>' . _('Create an attachment') . ":</h3>\n";
	echo '<input type="file" name="input_file0" style="width:98%;" />' . "\n";
}
?>
</td>
<td <?php echo $HTML->boxGetAltRowStyle(0); ?> width="50%">
<?php
if (!$is_add) {
	if ($accesslevel >= TRK_TECH || $ath->allowsAnon() ||
	    session_loggedin()) {
		echo emit_comment_box('trackeritemform', 'details',
		    'tracker-comment',
		    util_unconvert_htmlspecialchars(html_get_tooltip_description('comment')));
	}
	if ($accesslevel >= TRK_FULL) {
		echo '<h3>' . _('OR use a canned response') . ":</h3>\n";
		echo "<div>\n";
		echo $ath->cannedResponseBox('canned_response') . ' ' .
		    util_make_link('/tracker/admin/?group_id=' . $group_id .
		    '&amp;atid=' . $atid . '&amp;add_canned=1',
		    '(' . _('Admin') . ')');
		echo "</div>\n";
	}
}
?>
</td>
</tr>
<tr align="left" valign="top">
	<td>
		<?php echo utils_requiredField() . ' ' .
		    _('indicates required fields.'); ?>
	</td><td colspan="2" align="right">
<?php if (session_loggedin()) { ?>
		<input type="submit" name="submit"
		 value="<?php echo _('Save Changes'); ?>" />
<?php } ?>
	</td>
</tr>
</table>

<?php
if (!$is_add) {
	echo $HTML->boxMiddle(_('Item Relationships'), 'Item_Relationship',
	    false, 'tracker_item_relationship');
?>

<table width="100%" cellspacing="12">
<tr align="left" valign="top">
<td <?php echo $HTML->boxGetAltRowStyle(0); ?> width="50%">
<?php
	if ($accesslevel < TRK_TECH) {
		echo _('You do not have sufficient privileges to view Task relations.');
	} else if (!$group->usesPM()) {
		echo _('This project does not use Tasks.');
	} else {
		echo html_e('span', array('style' => 'float:right;'),
		    html_e('a', array('href' =>
			getselfhref(array('func' => 'taskmgr'), false)
		    ), html_image('ic/taskman20w.png', '20', '20',
		    array('border' => '0')) .
		    html_e('strong', array(), _('Build Task Relation')))) . "\n";
		echo '<h3>' . _('Related Tasks') . ":</h3>\n";
		$ath->renderRelatedTasks($group, $ah);
	}
?>
</td>
<td <?php echo $HTML->boxGetAltRowStyle(0); ?> width="50%">
<?php
	$ah->showRelations();

	/* old-style svn-only extra detail */
	$linked_commits = showmess_getcommits($aid);
	if ($linked_commits) {
		echo '<h3>' . _('Related Commits') . ":</h3>\n";
	}
?>
	<table border="0" width="100%">
		<tr><td colspan="2"><!--
			dummy in case the hook is empty
		--></td></tr>
<?php
	/* old-style svn-only extra detail */
	if ($linked_commits) {
		echo html_e('tr', array(), html_e('th', array(),
		    _('Group name')) . html_e('th', array(),
		    _('Revision')));
	}
	foreach ($linked_commits as $i) {
		echo html_e('tr', array(), html_e('td', array(),
		    $i['group_name']) . html_e('td', array(), html_e('a', array(
			'href' => $i['href'],
		    ), $i['revision'])));
	}
?>
<?php plugin_hook("artifact_extra_detail", array('artifact_id' => $aid)); ?>
	</table>
</td>
</tr>
</table>

<?php
	echo $HTML->boxMiddle(_('History'), 'Item_History', false,
	    'tracker_history');
	$ah->showHistory();
} else {
	echo $HTML->boxMiddle(_('Instructions'));
	echo html_e('p', array('style' => 'text-align:center;'),
	    html_e('span', array('class' => array('veryimportant')),
	    _('DO NOT enter passwords or confidential information in your message!')));
}
echo $HTML->boxBottom();
?>
</form>
<?php

if ($needstatusjs) {
	?>
	<script type="text/javascript"><!--//--><![CDATA[//><!--
		$('tracker-status_id').onchange = update_tracker_status;
		update_tracker_status();
	//--><!]]></script>
	<?php
}

$ath->footer(array());
exit;
