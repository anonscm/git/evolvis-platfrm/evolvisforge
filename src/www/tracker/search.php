<?php
/*-
 * Advanced Search
 *
 * Copyright © 2011
 *	Mike Esser <m.esser@tarent.de>
 * All rights reserved.
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

require_once('../env.inc.php');
require_once $gfcommon.'advanced_search/FusionForgeDFIProvider.class.php';
require_once $gfcommon.'advanced_search/ASInputAnalyzer.class.php';

sysdebug_off("Content-type: text/plain; charset=\"UTF-8\"");

//Get the required paramters.
$params = array();
$params['tracker_id'] = $_GET['tracker_id'];

$input = $_GET['cur_input'];

$dfiProvider   = new FusionForgeDFIProvider();
$inputAnalyzer = new ASInputAnalyzer();

$inputAnalyzer->setDFIProvider($dfiProvider);
ASTokenizer::$DFIProvider = $dfiProvider;

$autoComplete = $inputAnalyzer->analyzeInput($input, $params);

$words = explode(' ', $input);

$reducedAutoComplete = array();

if (strlen(end($words)) > 1) {
	// Remove all elements that aren't matching the current input
	foreach ($autoComplete as $curWord) {
		if (strpos($curWord, end($words)) !== FALSE) {
			$reducedAutoComplete[] = $curWord;
		}
	}
	echo minijson_encode($reducedAutoComplete);
} else {
	echo minijson_encode($autoComplete);
}
