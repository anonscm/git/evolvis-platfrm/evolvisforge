<?php
/**
 * FusionForge Tracker
 *
 * Copyright 1999-2001 (c) VA Linux Systems
 * Copyright 2010 Roland Mas
 * Copyright (C) 2011 Alain Peyrat - Alcatel-Lucent
 * Copyright © 2012
 *	Thorsten Glaser <t.glaser@tarent.de>
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

require_once $gfcommon.'tracker/ArtifactFactory.class.php';
require_once $gfcommon.'tracker/ArtifactQuery.class.php';
require_once $gfcommon.'advanced_search/FusionForgeDFIProvider.class.php';
require_once $gfcommon.'advanced_search/ASFusionForgeParser.class.php';
require_once $gfcommon.'advanced_search/AdvancedSearch.class.php';

html_use_tooltips(array(
	/* stock defaults */
	'html' => true,
	'delayIn' => 1000,
	'delayOut' => 500,
	'fade' => true,
	/* we want them on the top, all of them */
	'gravity' => 's',
    ));
use_javascript('/js/sortable.js');

//
//  make sure this person has permission to view artifacts
//
session_require_perm ('tracker', $ath->getID(), 'read') ;

$query_id = getIntFromRequest('query_id');
$usedefault = getIntFromRequest('usedefault');

//
//	The browse page can be powered by a pre-saved query
//	or by select boxes chosen by the user
//
//	If there is a $query_id coming from the request OR the pref
//	was already saved, use the artifact factory that way.
//
//	If the query_id = -1, unset the pref and use regular browse boxes
//
if (session_loggedin()) {
	$u =& session_get_user();

	if($query_id) {
		if ($query_id == '-1') {
			$u->setPreference('art_query'.$ath->getID(),'');
		} else {
			$aq = new ArtifactQuery($ath,$query_id);
			if (!$aq || !is_object($aq)) {
				exit_error($aq->getErrorMessage(),'tracker');
			}
			$aq->makeDefault();
		}
	} else {
		$query_id=$u->getPreference('art_query'.$ath->getID(),'');
	}

	if ($usedefault) {
		$u->deletePreference('art_cust'.$ath->getID());
		$u->deletePreference('art_query'.$ath->getID());
		$query_id = 0;
	}
} elseif ($usedefault) {
	setcookie("GFTrackerQuery", "");
	$query_id = 0;
} elseif ($query_id) {
	// If user is not logged, then use a cookie to store the current query.
	if (isset($_COOKIE["GFTrackerQuery"])) {
		$gf_tracker = unserialize($_COOKIE["GFTrackerQuery"]);
	} else {
		$gf_tracker = array();
	}
	$gf_tracker[$ath->getID()] = $query_id;
	// Send the query_id as a cookie to save it.
	setcookie("GFTrackerQuery", serialize($gf_tracker));
	$_COOKIE["GFTrackerQuery"] = serialize($gf_tracker);
} elseif (isset($_COOKIE["GFTrackerQuery"])) {
	$gf_tracker = unserialize($_COOKIE["GFTrackerQuery"]);
	$query_id = (int)$gf_tracker[$ath->getID()];
}

$af = new ArtifactFactory($ath);

if (!$af || !is_object($af)) {
	exit_error(_('Could Not Get Factory'),'tracker');
} elseif ($af->isError()) {
	exit_error($af->getErrorMessage(),'tracker');
}

if (!isset($_sort_col)) {
	/* default sort order: highest priority first */
	$_sort_col = 'priority';
	$_sort_ord = 'DESC';
}
$_sort_col = getStringFromRequest('_sort_col',$_sort_col);
$_adv_sort_col = getStringFromRequest('_sort_col',$_sort_col);
$_sort_ord = getStringFromRequest('_sort_ord',$_sort_ord);
$_adv_sort_ord = getStringFromRequest('_sort_ord',$_sort_ord);
$set = $usedefault ? 'default' : getStringFromRequest('set');
$_assigned_to = getIntFromRequest('_assigned_to');
$_submitted_by = getIntFromRequest('_submitted_by');
$_status = getIntFromRequest('_status');
$_extra_fields = array() ;
$aux_extra_fields = array() ;

if ($set == 'custom') {
	/* may be past in next/prev url */
	$i = $ath->getCustomStatusField();
	$tmp_extra_fields = getArrayFromRequest('extra_fields');
	if (isset($tmp_extra_fields[$i])) {
		$_extra_fields[$i] = $tmp_extra_fields[$i];
	}
}

if (is_array($_extra_fields)){
	$keys=array_keys($_extra_fields);
	foreach ($keys as $key) {
		if ($_extra_fields[$key] != 'Array') {
			$aux_extra_fields[$key] = $_extra_fields[$key];
		}
	}
} else {
	if (isset($_extra_fields)){
		$aux_extra_fields = $_extra_fields;
	} else {
		$aux_extra_fields = '';
	}
}

$af->setup(0,$_sort_col,$_sort_ord,0,$set,$_assigned_to,$_status,$aux_extra_fields, $_submitted_by);
//
//	These vals are sanitized and/or retrieved from ArtifactFactory stored settings
//
$_sort_col=$af->order_col;
$_sort_ord=$af->sort;
$_status=$af->status;
$_assigned_to=$af->assigned_to;
$_extra_fields=$af->extra_fields;
$_submitted_by=$af->submitted_by;

$art_arr = $af->getArtifacts();

//build page title to make bookmarking easier
//if a user was selected, add the user_name to the title
//same for status
use_javascript('/tabber/tabber.js');
html_use_jqueryui();

/**
 * New advaced search begins here!
 */
$curSearch = getStringFromRequest('advanced_search');
$queryName = _('Query Name');
$oldArt    = $art_arr;

if (getIntFromRequest('load_request')) {
	if (getStringFromRequest('load_query') === _('Load')) {
		$sql = "SELECT query, name FROM AdvancedQuery WHERE id = $1;";
		$result = db_query_params($sql, array(getIntFromRequest('saved_advanced_queries')));

		$curSearchArr = db_fetch_array($result);

		$curSearch = $curSearchArr['query'];
		$queryName = $curSearchArr['name'];
	} else if (getStringFromRequest('load_query') === _('Delete')) {
		if (AdvancedSearch::deleteUserQuery(user_getid(),
		    getIntFromRequest('saved_advanced_queries'),
		    getIntFromRequest('atid'))) {
			$feedback .= _('Deleted query successful.');
		}
	}
}

if (getStringFromRequest('search') == 'true') {
	$parser = new ASFusionForgeParserTracker();

	AStokenizer::$DFIProvider = new FusionForgeDFIProvider();

	$parser->setParam('tracker_id', getIntFromRequest('atid'));
	$parser->setParam('_sort_col', $_adv_sort_col);
	$parser->setParam('_sort_ord', $_adv_sort_ord);

	$art_arr = $parser->parse($curSearch, $ath);

	if ($art_arr === false) {
		$art_arr = $oldArt;
	} else if ($art_arr === "Invalid Syntax") {
		$art_arr = $oldArt;
		$error_msg = "Invalid Syntax!<br />Syntax: [Field Name] [Operator] [Data] [and/or]<br />Example: \"Submitted by like Schmitz%\"";
	}

	if ((getStringFromRequest('save_advanced_query') === 'true') &&
	    user_getid() &&
	    ($rq_save = getStringFromRequest('save_as')) &&
	    ($rq_atid = getIntFromRequest('atid'))) {
		// Check if a query for this user and this tracker allready exists. If yes just update the query.
		$sql = "SELECT * FROM AdvancedQuery, user_has_query WHERE id = query_id AND name = $1 AND group_artifact_id = $2";

		$result = db_query_params($sql, array($rq_save, $rq_atid));

		$num_rows = db_numrows($result);

		if ($num_rows == 0) {
			//No query existing. Save it
			$sql = 'INSERT INTO AdvancedQuery(name, query) VALUES($1, $2);';

			$result = db_query_params($sql, array($rq_save,
			    $curSearch));

			$sql = 'INSERT INTO user_has_query VALUES($1, (SELECT CURRVAL(\'advancedquery_id_seq\')), $2);';

			db_query_params($sql, array(user_getid(), $rq_atid));

			$feedback .= sprintf(_('Saved query with name "%s".'),
			    $rq_save);
		} else {
			$arr = db_fetch_array($result);

			$sql = 'UPDATE AdvancedQuery SET query = $1 WHERE id = $2';

			db_query_params($sql, array($curSearch,
			    $arr['id']));
		}
	}
}
if (!$art_arr && $af->isError()) {
	exit_error($af->getErrorMessage(), 'tracker');
}

if ($art_arr && ($art_cnt = count($art_arr)) > 0) {
	; /* deleted */
} else {
	$art_cnt = 0;
}

$ath->header(array('atid'=>$ath->getID(), 'title'=>$ath->getName()));

/**
 *
 *	Build the powerful browsing options pop-up boxes
 *
 */

//
//	creating a custom technician box which includes "any" and "unassigned"
//
$engine = RBACEngine::getInstance () ;
$techs = $engine->getUsersByAllowedAction ('tracker', $ath->getID(), 'tech') ;
sortUserList($techs, 'unixname');

$tech_id_arr = array () ;
$tech_name_arr = array () ;

foreach ($techs as $tech) {
	$tech_id_arr[] = $tech->getID() ;
	$tech_name_arr[] = $tech->getRealName() . ' (' .
	    $tech->getUnixName() . ')';
}
$tech_id_arr[]='0';  //this will be the 'any' row
$tech_name_arr[]=_('Any');

if (is_array($_assigned_to)) {
	$_assigned_to='';
}
$tech_box=html_build_select_box_from_arrays ($tech_id_arr,$tech_name_arr,'_assigned_to',$_assigned_to,true,_('Unassigned'));


//
//	custom order by arrays to build a pop-up box
//
$order_name_arr=array();
$order_name_arr[]=_('ID');
$order_name_arr[]=_('Priority');
$order_name_arr[]=_('Summary');
$order_name_arr[]=_('Open Date');
$order_name_arr[]=_('Close Date');
$order_name_arr[]=_('Submitter');
$order_name_arr[]=_('Assignee');


$order_arr=array();
$order_arr[]='artifact_id';
$order_arr[]='priority';
$order_arr[]='summary';
$order_arr[]='open_date';
$order_arr[]='close_date';
$order_arr[]='submitted_unixname';
$order_arr[]='assigned_unixname';

//
//	custom sort arrays to build pop-up box
//
$sort_name_arr=array();
$sort_name_arr[]=_('Ascending');
$sort_name_arr[]=_('Descending');

$sort_arr=array();
$sort_arr[]='ASC';
$sort_arr[]='DESC';

//
//	custom changed arrays to build pop-up box
//
$changed_name_arr=array();
$changed_name_arr[]=_('Any changes');
$changed_name_arr[]=_('Last 24H');
$changed_name_arr[]=_('Last 7days');
$changed_name_arr[]=_('Last 2weeks');
$changed_name_arr[]=_('Last 1month');

$changed_arr=array();
$changed_arr[]= 0x7fffffff;	 // Any
$changed_arr[]= 3600 * 24;	 // 24 hour
$changed_arr[]= 3600 * 24 * 7; // 1 week
$changed_arr[]= 3600 * 24 * 14;// 2 week
$changed_arr[]= 3600 * 24 * 30;// 1 month

/**
 *
 *	Show the free-form text submitted by the project admin
 */
echo $ath->renderBrowseInstructions();

//
//	statuses can be custom in GForge 4.5+
//
if ($ath->usesCustomStatuses()) {
	$aux_extra_fields = array();
	if (is_array($_extra_fields)){
		$keys=array_keys($_extra_fields);
		foreach ($keys as $key) {
			if (!is_array($_extra_fields[$key])) {
				$aux_extra_fields[$key] = $_extra_fields[$key];
			}
		}
	} else {
		$aux_extra_fields = $_extra_fields;
	}
	$status_box=$ath->renderSelect ($ath->getCustomStatusField(),util_ifsetor($aux_extra_fields[$ath->getCustomStatusField()], 'xzxz'),false,'',true,_('Any'));
	/*XXX what a fuckup, this… */
	$status_box_name = 'extra_fields['.$ath->getCustomStatusField().']';
} else {
	if (is_array($_status)) {
		$_status='';
	}
	$status_box = $ath->statusBox('_status',$_status,true,_('Any'));
	$status_box_name = '_status';
}

// start of RDFa
$proj_name = $group->getUnixName();
$proj_url = util_make_url_g($group->getUnixName(),$group_id);
// the tracker's URIs are constructed in order to support addition of an OSLC-CM REST server
// inside /tracker/cm/. There each tracker has a URL in the form .../project/PROJ_NAME/atid/ATID
$tracker_stdzd_uri = util_make_url('/tracker/cm/project/'. $proj_name .'/atid/'. $ath->getID());
print '<div about="'. $tracker_stdzd_uri
	.'" typeof="sioc:Container">'."\n";
print '<span rel="http://www.w3.org/2002/07/owl#sameAs" resource="" />'."\n";
print '<span rev="doap:bug-database sioc:space_of" resource="'. $proj_url .'" />'."\n";
print "</div>\n"; // end of about

echo '<div id="tabber" class="tabber">';
////////////////////////////
// OLD POWERQUERIES BEGIN //
////////////////////////////
echo '

	<div class="tabbertab" title="'._('Power Query [OLD]').'">';

if (session_loggedin()) {
	$res = db_query_params ('SELECT artifact_query_id,query_name, CASE WHEN query_type>0 THEN 1 ELSE 0 END as type
	FROM artifact_query
	WHERE group_artifact_id=$1 AND (user_id=$2 OR query_type>0)
	ORDER BY type ASC, query_name ASC',
				array ($ath->getID(),
				       user_getid()));
} else {
	$res = db_query_params ('SELECT artifact_query_id,query_name, CASE WHEN query_type>0 THEN 1 ELSE 0 END as type
	FROM artifact_query
	WHERE group_artifact_id=$1 AND query_type>0
	ORDER BY type ASC, query_name ASC',
				array ($ath->getID()));
}


if (db_numrows($res)>0) {
	echo '<form action="'. getStringFromServer('PHP_SELF') .'" method="get">';
	echo '<input type="hidden" name="group_id" value="'.$group_id.'" />';
	echo '<input type="hidden" name="atid" value="'.$ath->getID().'" />';
	echo '<input type="hidden" name="power_query" value="1" />';
	echo '	<table width="100%" cellspacing="0">
	<tr>
	<td>
	';
	$optgroup['key'] = 'type';
	$optgroup['values'][0] = 'Private queries';
	$optgroup['values'][1] = 'Project queries';
	echo '<span style="font-size:smaller">';
	echo '<select name="query_id">';
	echo '<option value="100">Select One</option>';
	$current = '';
	$selected = $af->getDefaultQuery();
	while ($row = db_fetch_array($res)) {
		if ($current != $row['type']) {
			if ($current !== '')
				echo '</optgroup>';
			$label = $row['type'] ? 'Project' : 'Private';
			echo '<optgroup label="'.$label.'">';
			$current = $row['type'];
		}
		echo '<option value="'.$row['artifact_query_id'].'"';
		if ($row['artifact_query_id'] == $selected)
			echo ' selected="selected"';
		echo '>'. $row['query_name'] .'</option>'."\n";
	}
	if ($current !== '')
		echo '</optgroup>';
	echo '</select>';
	echo '</span>
	<input type="submit" name="run" value="'._('Power Query').'" />
	&nbsp;&nbsp;<a href="/tracker/?atid='. $ath->getID().'&amp;group_id='.$group_id.'&amp;func=query">'.
	_('Build Query').'</a>
	</td></tr></table>
	</form>';
} else {
	echo '<strong>
	<a href="/tracker/?atid='. $ath->getID().'&amp;group_id='.$group_id.'&amp;func=query">'._('Build Query').'</a></strong>';
}
echo '</div>';
//////////////////////////
// OLD POWERQUERIES END //
//////////////////////////
echo '<div class="tabbertab'.($set == 'advanced' ? ' tabbertabdefault' : '').'" title="'._('Advanced queries').'">';

if (session_loggedin()) {
	$res = db_query_params ('SELECT artifact_query_id,query_name, CASE WHEN query_type>0 THEN 1 ELSE 0 END as type
	FROM artifact_query
	WHERE group_artifact_id=$1 AND (user_id=$2 OR user_id=100 OR query_type>0)
	ORDER BY type ASC, query_name ASC',
				array ($ath->getID(),
				       user_getid()));
} else {
	$res = db_query_params ('SELECT artifact_query_id,query_name, CASE WHEN query_type>0 THEN 1 ELSE 0 END as type
	FROM artifact_query
	WHERE group_artifact_id=$1 AND query_type>0
	ORDER BY type ASC, query_name ASC',
				array ($ath->getID()));
}

?>

<form id="load_query_form" name="load_query_form" method="post" action="<?php echo getStringFromServer('PHP_SELF').'?group_id='.$group_id.'&amp;atid='.$ath->getID().'&amp;search=true'; ?>">
<?php echo _('Select Query'); ?>:
<select name="saved_advanced_queries">
	<option value="100"><?php echo _('Select One'); ?></option>
<?php
	// Get all global queries. This includes System and Tracker queries.
	$sql = "SELECT aq.id, aq.name FROM AdvancedQuery as aq, artifact_group_has_query as aghq
			WHERE (aghq.group_artifact_id = $1 AND aq.id = aghq.query_id)";

	$result = db_query_params($sql, array(getIntFromRequest('atid')));

	$sptr = false;
	while ($arr = db_fetch_array($result)) {
		echo html_aonce($sptr, 'optgroup', array(
			'label' => _('Tracker Queries'),
		    )) . html_e('option', array(
			'value' => $arr['id'],
		    ), util_html_secure($arr['name']));
	}
	echo html_ac($sptr);

if (session_loggedin()) {
	//Get all global queries. This includes System and Tracker queries. //global? --mirabilos
	$sql = "SELECT aq.id, aq.name FROM AdvancedQuery as aq, user_has_query as uhq
			WHERE (uhq.user_id = $1 AND aq.id = uhq.query_id AND uhq.group_artifact_id = $2)";

	$result = db_query_params($sql, array(user_getid(),
	    getIntFromRequest('atid')));

	$sptr = false;
	while ($arr = db_fetch_array($result)) {
		echo html_aonce($sptr, 'optgroup', array(
			'label' => _('Private Queries'),
		    )) . html_e('option', array(
			'value' => $arr['id'],
		    ), util_html_secure($arr['name']));
	}
	echo html_ac($sptr);
}
?>
</select>
<input type="hidden" name="load_request" id="load_request" value="1" />
<input type="hidden" name="set" value="advanced" />
<input type="submit" name="load_query" id="load_query" value="<?php echo _('Load'); ?>" />
<input type="submit" name="load_query" id="delete_query" value="<?php echo _('Delete'); ?>" />
</form>
<?php

AdvancedSearch::renderSearchBar(getStringFromServer('PHP_SELF').'?group_id='.$group_id.'&amp;atid='.$ath->getID().'&amp;search=true',
    "search.php?tracker_id=" . getIntFromRequest('atid') . "&cur_input=",
    $curSearch, $queryName);

/**
 * New advaced search ends here!
 */

echo '
	</div>
	<div class="tabbertab'.($af->query_type == 'custom' ? ' tabbertabdefault' : '').'" title="'._('Simple Filtering and Sorting').'">
	<form name="headerForm" action="'. getStringFromServer('PHP_SELF') .'?group_id='.$group_id.'&amp;atid='.$ath->getID().'" method="post">
	<input type="hidden" name="query_id" value="-1" />
	<input type="hidden" name="set" value="custom" />
	<table width="100%" cellspacing="0">
	<tr>
	<td>
	'._('Assignee').':&nbsp;'. $tech_box .'
	</td>
	<td align="center">
	'._('State').':&nbsp;'. $status_box .'
	</td>
	<td align="right">';

// Compute the list of fields which can be sorted.
// Currently, only text & integer are taken (for simplicity only).
	$efarr = $ath->getExtraFields(array(ARTIFACT_EXTRAFIELDTYPE_TEXT,
					    ARTIFACT_EXTRAFIELDTYPE_TEXTAREA,
					    ARTIFACT_EXTRAFIELDTYPE_INTEGER,
					    ARTIFACT_EXTRAFIELDTYPE_SELECT,
					    ARTIFACT_EXTRAFIELDTYPE_RADIO,
					    ARTIFACT_EXTRAFIELDTYPE_DATETIME,
					    ARTIFACT_EXTRAFIELDTYPE_STATUS));
$keys=array_keys($efarr);
for ($k=0; $k<count($keys); $k++) {
	$i=$keys[$k];
	$order_name_arr[] = $efarr[$i]['field_name'];
	$order_arr[] = $efarr[$i]['extra_field_id'];
}

echo _('Order by').
	html_build_select_box_from_arrays($order_arr,$order_name_arr,'_sort_col',$_sort_col,false) .
	html_build_select_box_from_arrays($sort_arr,$sort_name_arr,'_sort_ord',$_sort_ord,false) .
	'<input type="submit" name="submit" value="'._('Quick Browse').'" />';

echo '
	</td>
	</tr>
	</table>
	</form>

	<script type="text/javascript"><!--//--><![CDATA[//><!--

	function submitHeaderForm() {
		document.headerForm.submit.click();
	}

	document.headerForm._assigned_to.onchange = submitHeaderForm;
	document.headerForm.elements["'.$status_box_name.'"].onchange = submitHeaderForm;
	document.headerForm._sort_col.onchange = submitHeaderForm;
	document.headerForm._sort_ord.onchange = submitHeaderForm;

	//--><!]]></script>

	</div>';
if ($af->query_type == 'default') {
	echo '<div class="tabbertab tabbertabdefault" title="'._('Default').'">';
	echo '<strong>'._('Viewing only opened records by default, use \'Advanced queries\' or \'Simple Filtering and Sorting\' to change.').'</strong>';
} else {
	echo '<div class="tabbertab" title="'._('Default').'">';
	echo '<form name="headerFormDflt" action="'. getStringFromServer('PHP_SELF') .'?group_id='.$group_id.'&amp;atid='.$ath->getID().'" method="post">
	<input type="hidden" name="usedefault" value="1" />
	<input type="submit" name="submit" value="'._('View only opened records (default)').'" />
</form>';
}
echo "</div>\n";

/* end of tabbertab followed by results # */
echo "\n</div>\n<div style=\"border:1px solid #CCCCCC; padding:6px; margin:12px 0px;\">\n";
printf(_('Displaying %d results.'), $art_cnt);
echo "\n</div>\n";

if ($art_cnt > 0) {

	if ($query_id) {
		$aq = new ArtifactQuery($ath,$query_id);
		$has_bargraph = (in_array('bargraph', $aq->getQueryOptions()));
	} else {
		$has_bargraph = false;
	}

	if ($has_bargraph) {
		// Display the roadmap block based on the values of the Status field.
		$colors = array('#a71d16', '#ffa0a0', '#f5f5b5', '#bae0ba', '#16a716');
		$count = array();
		$percent = array();
		foreach($art_arr as $art) {
			if ($ath->usesCustomStatuses()) {
				$custom_id = $ath->getCustomStatusField();
				$extra_data = $art->getExtraFieldDataText();
				$count[ $extra_data[$custom_id]['value'] ]++;
			} else {
				$count[ $art->getStatusName()]++;
			}
		}
		foreach($count as $n => $c) {
			$percent[$n] = round(100*$c/count($art_arr));
		}

		$i=0;
		$efarr = $ath->getExtraFields(array(ARTIFACT_EXTRAFIELDTYPE_STATUS));
		$keys=array_keys($efarr);
		$field_id = $keys[0];
		$states = $ath->getExtraFieldElements($field_id);
		$graph = '';
		$legend = '';
		if (is_array($states)) {
			foreach($states as $state) {
				$name = $state['element_name'];
				if ($count[$name]) {
					$graph  .= '<td style="background: '.$colors[$i].'; width: '.$percent[$name].'%;">&nbsp;</td>';
					$legend .= '<td style="white-space: nowrap; width: '.$percent[$name].'%;">'."<i>$name: $count[$name] ($percent[$name]%)</i></td>";
				}
				$i++;
			}
		}

		if ($graph) {
		?>
		<table class="progress">
      	<tbody>
      		<tr><?php echo $graph; ?></tr>
      	</tbody>
      	</table>
      	<table class="progress_legend">
      		<tr><?php echo $legend ?></tr>
      	</table>
	<?php
		}
	}

	if ($set=='custom') {
		$set .= '&amp;_assigned_to='.$_assigned_to.'&amp;_status='.$_status.'&amp;_sort_col='.$_sort_col.'&amp;_sort_ord='.$_sort_ord;
		if (array_key_exists($ath->getCustomStatusField(),$_extra_fields)) {
			$set .= '&amp;extra_fields['.$ath->getCustomStatusField().']='.$_extra_fields[$ath->getCustomStatusField()];
		}
	}


	$IS_ADMIN = forge_check_perm ('tracker', $ath->getID(), 'manager') ;

	if ($IS_ADMIN) {
		echo '
		<form name="artifactList" action="'. getStringFromServer('PHP_SELF') .'?group_id='.$group_id.'&amp;atid='.$ath->getID().'" method="post">
		<input type="hidden" name="form_key" value="'.form_generate_key().'" />
		<input type="hidden" name="func" value="massupdate" />';
	}

	$browse_fields = explode(',', "id,".$ath->getBrowseList());
	$title_arr = array();
	$links_arr = array();
	foreach ($browse_fields as $f) {
		$sortf = $f;
		if (intval($f) > 0) {
			$title = $ath->getExtraFieldName($f);
			if (!$title) {
				$title = sprintf(_('unknown #%d'), $f);
			}
		} else switch ($f) {
		case 'id':
			$title = _('ID');
			$sortf = 'artifact_id';
			break;
		case 'summary':
			$title = _('Summary');
			break;
		case 'details':
			$title = _('Description');
			$sortf = false;
			break;
		case 'open_date':
			$links_arr[] = '';
			$title_arr[] = '';
			$title = _('Open Date');
			break;
		case 'close_date':
			$title = _('Close Date');
			break;
		case 'status_id':
			$title = _('State');
			break;
		case 'priority':
			$title = _('Priority');
			break;
		case 'assigned_to':
			$title = _('Assigned to');
			$sortf = 'assigned_unixname';
			break;
		case 'submitted_by':
			$title = _('Submitted by');
			$sortf = 'submitted_unixname';
			break;
		case 'related_tasks':
			$title = _('Related tasks');
			$sortf = false;
			break;
		case '_votes':
			$title = _('# Votes');
			$sortf = false;
			break;
		case '_voters':
			$title = _('# Voters');
			$sortf = false;
			break;
		case '_votage':
			$title = _('% Voted');
			$sortf = false;
			break;
		default:
			$title = sprintf(_('unknown ("%s")'), $f);
			break;
		}
		$links_arr[] = $sortf ? util_gethref(false, array(
			'group_id' => $group_id,
			'atid' => $ath->getID(),
			'_sort_col' => $sortf,
			'_sort_ord' => ($_sort_col == $sortf &&
			    $_sort_ord == 'ASC' ? 'DESC' : 'ASC'),
			'set' => 'advanced',
			'search' => 'true',
			'advanced_search' => $curSearch,
		    ), false) : false;
		$title_arr[] = $title;
	}

	if ($art_cnt) {
		echo $GLOBALS['HTML']->listTableTopSortable($title_arr);
	}

	$then=(time()-$ath->getDuePeriod());
	$voters = count($ath->getVoters());

	$extra_checks = array();
	/* check fields needing special handling */
	foreach ($ath->getExtraFields() as $f => $v) {
		switch ($v['field_type']) {
		case ARTIFACT_EXTRAFIELDTYPE_DATETIME:
			/* check for End Date flag */
			if ($v['attribute2'] & 1) {
				$extra_checks[] = $f;
			}
			break;
		}
	}

	for ($i = 0; $i < $art_cnt; $i++) {
 		$extra_data = $art_arr[$i]->getExtraFieldDataText();
		$rowstyle = $HTML->boxGetAltRowStyle($i);
		foreach ($extra_checks as $f) {
			switch ($extra_data[$f]['field']['field_type']) {
			case ARTIFACT_EXTRAFIELDTYPE_DATETIME:
				if ($extra_data[$f]['field']['attribute2'] & 1) {
					/* End Date semantics */
					if (time() > $extra_data[$f]['value']) {
						/* past due date */
						$rowstyle = 'bgcolor="#F2DEDE"';
					}
				}
				break;
			}
		}
		echo '
		<tr '. $rowstyle . '>';
 		foreach ($browse_fields as $f) {
			if ($f == 'id' || $f == 'summary') {
				$c = array(
					'href' => util_gethref(false, array(
						'func' => 'detail',
						'aid' => $art_arr[$i]->getID(),
						'group_id' => $group_id,
						'atid' => $ath->getID(),
					    ), false),
					'class' => array(),
				    );
			}
			if ($f == 'id') {
				if ($art_arr[$i]->getStatusID() != 1) {
					$c['class'][] = 's';
				}
				echo html_e('td', array(
					'style' => 'white-space:nowrap;',
					'content' => $art_arr[$i]->getID(),
				    ), ($IS_ADMIN ? html_e('input', array(
					'type' => 'checkbox',
					'name' => 'artifact_id_list[]',
					'value' => $art_arr[$i]->getID(),
				    )) : '') .
				    html_e('a', $c, $art_arr[$i]->getID())) .
				    "\n";
			} else if ($f == 'summary') {
				echo html_e('td', array(
					'content' => $art_arr[$i]->getSummary(),
				    ), html_e('a', $c,
				    $art_arr[$i]->getSummary())) . "\n";
			} else if ($f == 'open_date') {
				$t = $art_arr[$i]->getOpenDate();
				$as = html_ap();
				echo html_ao('td');
				if ($set != 'closed' && $t < $then) {
					echo html_ao('strong') . '*';
				}
				echo html_ac($as) . html_e('td', array(
					'content' => $t,
				    ), datepick_format($t, true));
			} else if ($f == 'status_id') {
				echo '<td>'. $art_arr[$i]->getStatusName() .'</td>';
			} else if ($f == 'priority') {
				echo '<td class="priority'.$art_arr[$i]->getPriority()  .'">'. $art_arr[$i]->getPriority() .'</td>';
			} else if ($f == 'assigned_to') {
				if (($xu = user_get_object($art_arr[$i]->getAssignedTo())) &&
				    $xu->getID() != 100) {
					$xt = util_display_user($xu->getUnixName(),
					    $xu->getID(), $xu->getUnixName(),
					    'xs', false);
				} else {
					$xt = $art_arr[$i]->getAssignedUnixName();
				}
				echo html_e('td', array(
					'title' => $art_arr[$i]->getAssignedRealName(),
					'content' => $art_arr[$i]->getAssignedUnixName(),
				    ), $xt);
			} else if ($f == 'submitted_by') {
				if (($xu = user_get_object($art_arr[$i]->getSubmittedBy())) &&
				    $xu->getID() != 100) {
					$xt = util_display_user($xu->getUnixName(),
					    $xu->getID(), $xu->getUnixName(),
					    'xs', false);
				} else {
					$xt = $art_arr[$i]->getSubmittedUnixName();
				}
				echo html_e('td', array(
					'title' => $art_arr[$i]->getSubmittedRealName(),
					'content' => $art_arr[$i]->getSubmittedUnixName(),
				    ), $xt);
			} else if ($f == 'close_date') {
				$t = $art_arr[$i]->getCloseDate();
				echo html_e('td', array(
					'content' => $t,
				    ), datepick_format($t, true), false);
			} else if ($f == 'details') {
				echo '<td>'. $art_arr[$i]->getDetails() .'</td>';
			} else if ($f == 'related_tasks') {
				/*XXX how to sort? */
				echo '<td>';
				$tasks_res = $art_arr[$i]->getRelatedTasks();
				$s ='';
				while ($rest = db_fetch_array($tasks_res)) {
					$link = '/pm/task.php?func=detailtask&amp;project_task_id='.$rest['project_task_id'].
						'&amp;group_id='.$group_id.'&amp;group_project_id='.$rest['group_project_id'];
					$title = '[T'.$rest['project_task_id'].']';
					if ($rest['status_id'] == 2) {
						$title = '<strike>'.$title.'</strike>';
					}
					print $s.'<a href="'.$link.'" title="'.util_html_secure($rest['summary']).'">'.$title.'</a>';
					$s = ' ';
				}
				echo '</td>';
			} else if (intval($f) > 0) {
				// Now display extra-fields (fields are numbers).
				$value = $extra_data[$f]['value'];
				$p = array();
				if ($extra_data[$f]['type'] == ARTIFACT_EXTRAFIELDTYPE_RELATION) {
					/*XXX how to sort? */
					$r = "";
					foreach (preg_split("/\D+/", $value) as $v) {
						$v = (int)$v;
						if (!$v) {
							continue;
						}
						$r .= html_e('a', array(
							'href' => util_make_url('/tracker/t_follow.php/' . $v),
						    ), sprintf('[#%d]', $v)) . "\n";
					}
					$value = $r;
				} elseif ($extra_data[$f]['type'] == ARTIFACT_EXTRAFIELDTYPE_STATUS) {
					$p['content'] = $value;
					if ($art_arr[$i]->getStatusID() == 2) {
						$value = '<strike>'.$value.'</strike>';
					}

				} elseif ($extra_data[$f]['type'] == ARTIFACT_EXTRAFIELDTYPE_DATETIME) {
					$p['content'] = $value;
					$value = datepick_format($value, true);
				}
				echo html_e('td', $p, $value, false);
			} else if ($f == '_votes') {
				$v = $art_arr[$i]->getVotes();
				echo html_e('td', array(), $v[0], false);
			} else if ($f == '_voters') {
				echo html_e('td', array(), $voters, false);
			} else if ($f == '_votage') {
				$v = $art_arr[$i]->getVotes();
				echo html_e('td', array(), $v[2], false);
			} else {
				// Display ? for unknown values.
				echo '<td>?</td>';
			}
 		}
		echo '</tr>';
	}

	if ($art_cnt) {
		echo $GLOBALS['HTML']->listTableBottom();
	}

	/*
		Mass Update Code
	*/
	if ($IS_ADMIN) {
		echo $HTML->boxTop(_('Mass Update'), 'MassUpdate', true, 'tracker_browse_massup');

		echo '<script type="text/javascript"><!--//--><![CDATA[//><!--
	function checkAll(val) {
		al=document.artifactList;
		len = al.elements.length;
		var i=0;
		for( i=0 ; i<len ; i++) {
			if (al.elements[i].name==\'artifact_id_list[]\') {
				al.elements[i].checked=val;
			}
		}
	}
	//--><!]]></script>

			<table width="100%" border="0" id="admin_mass_update">
			<tr><td colspan="2">

<a href="javascript:checkAll(1)">'._('Check &nbsp;all').'</a>
-
  <a href="javascript:checkAll(0)">'._('Clear &nbsp;all').'</a>

<div class="important">'._('<strong>Admin:</strong> If you wish to apply changes to all items selected above, use these controls to change their properties and click once on "Mass Update".').'</div>
			</td></tr>';

		//
		//	build custom fields
		//
	$ef = $ath->getExtraFields(array(ARTIFACT_EXTRAFIELD_FILTER_INT));
	$keys=array_keys($ef);

	$sel=array();
	for ($i=0; $i<count($keys); $i++) {
		if (($ef[$keys[$i]]['field_type']==ARTIFACT_EXTRAFIELDTYPE_CHECKBOX) || ($ef[$keys[$i]]['field_type']==ARTIFACT_EXTRAFIELDTYPE_MULTISELECT)) {
			$sel[$keys[$i]]=array('100');
		} else {
			$sel[$keys[$i]]='100';
		}
	}
	$ath->renderExtraFields($sel,true,_('No Change'),false,'',array(ARTIFACT_EXTRAFIELD_FILTER_INT),true);
?>
<tr>
	<td>
		<strong><?php echo _('Priority'); ?></strong><br />
<?php echo html_build_priority_select_box('priority', '100', true); ?>
	</td><td>
<?php echo $ath->technicianBox('assigned_to', '100.1', true, _('Nobody'),
    '100.1', _('No Change'), false, _('Assigned to')); ?>
	</td>
</tr><tr>
	<td>
		<strong><?php echo _('Canned Response'); ?></strong><br />
<?php echo $ath->cannedResponseBox('canned_response'); ?>
	</td><td>
<?php if (!$ath->usesCustomStatuses()) { ?>
		<strong><?php echo _('State'); ?></strong><br />
<?php echo $ath->statusBox('status_id','xzxz',true,_('No Change')); ?>
<?php } ?>
	</td>
</tr>
<tr>
	<td colspan="3" align="center">
		<input type="submit" name="submit"
		 value="<?php echo _('Mass update'); ?>" />
	</td>
</tr>
</table>
<?php
		echo $HTML->boxMiddle(_('Caption'), 'Legende', true, 'tracker_browse_caption');
	} else {
		echo $HTML->boxTop(_('Caption'), 'Legende', true, 'tracker_browse_caption');
	}

	printf(_('* Denotes requests > %1$s Days Old'), ($ath->getDuePeriod()/86400));

	if (in_array('priority', $browse_fields)) {
		show_priority_colors_key();
	}
	echo $HTML->boxBottom();
	if ($IS_ADMIN) {
		echo "</form>\n";
	}
} else {
	echo '<p class="warning_msg">'._('No items found').'</p>';
	echo db_error();
}

$ath->footer(array());

// Local Variables:
// mode: php
// c-file-style: "bsd"
// End:
