<?php
/*-
 * Evolvis Impressum, change as suitable
 */

return function () {
	global $curlang;

	if ($curlang == 'de') { ?>
 <p>Hier könnte Ihr Impressum stehen. Bitte ändern Sie es in der Datei
  <tt><?php echo forge_get_config('custom_path'); ?>/imprint_c.php</tt>!</p>
<?php	} else { ?>
 <p>This could be your imprint. Please adjust it in the file
  <tt><?php echo forge_get_config('custom_path'); ?>/imprint_c.php</tt>!</p>
<?php	}

// note: the <h2>About Evolvis</h2> section + ToS link follows below
// the output, and this all is inside a .widlim div
};
