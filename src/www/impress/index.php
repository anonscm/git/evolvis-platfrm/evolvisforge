<?php
/*-
 * About Evolvis & Impressum
 *
 * Copyright © 2023
 *	mirabilos <tg@mirbsd.de>
 * Copyright © 2012, 2018
 *	mirabilos <t.glaser@tarent.de>
 * All rights reserved.
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *-
 * Load the actual imprint for this forge instance.
 */

require_once('../env.inc.php');
require_once $gfcommon.'include/pre.php';

$bn = 'imprint_c.php';
$fn = forge_get_config('custom_path') . '/' . $bn;
if (!file_exists($fn)) {
	$fn = $gfwww . '/impress/' . $bn;
}
setup_curlang();
$doimprint = include($fn);

site_header(array(
	'title' => _('Imprint'),
	'h1' => 'Impressum / Imprint',
    ));

echo "<div class=\"widlim\">\n\n";

$doimprint();

//----------------------------------------------------------------

printf('<h2>' . _('About %s') . "</h2>\n", forge_get_config('forge_name'));

printf('<p>' . _('Read the <a href="%1$s">terms of use</a> for this site.') . '</p>',
    util_make_url('/terms.php'));

require_once $gfcommon.'include/FusionForge.class.php';
$ff = new FusionForge();

printf(_('<p>The %1$s forge is running the %2$s software, version <b>%3$s</b>.</p>'),
    forge_get_config('forge_name'), $ff->software_name, $ff->software_version);
echo "\n";

printf(_('<h3>About the %s software</h3>'), $ff->software_name);
echo "\n";

// keep me in
printf('<p>' . _('This software forge runs on the platform <a href="%1$s">Evolvis</a> using <a href="%2$s">FusionForge</a> technology on <a href="%3$s">Debian</a> GNU/Linux.') . "</p>\n",
    'https://evolvis.org/projects/evolvis-platfrm/',
    'https://fusionforge.org/',
    'https://www.debian.org/');

require_once $gfcommon.'pm/ProjectTaskSqlQueries.php';
printf(_('<p><small>Minimum Unique Task/Tracker Item ID: <b>%d</b></small></p>'),
    tasktracker_getminid());

echo "\n\n</div>\n"; // .widlim

site_footer(array());
