<?php
/**
 * FusionForge Documentation Manager
 *
 * Copyright 1999-2001, VA Linux Systems
 * Copyright 2000, Quentin Cregan/SourceForge
 * Copyright 2002-2004, GForge Team
 * Copyright 2010, Franck Villaume - Capgemini
 * Copyright (C) 2011 Alain Peyrat - Alcatel-Lucent
 * http://fusionforge.org
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/* please do not add require here : use www/docman/index.php to add require */
/* global variables used */
global $docman_submenu; // return value
global $d_arr; // document array
global $group_id; // id of group

/* create the submenu following role, rules and content */
$menu_text = array();
$menu_links = array();
$menu_tips = array();

if (forge_check_perm('docman', $group_id, 'submit')) {
	$menu_text[] = _('Submit new documentation');
	$menu_links[] = '/docman/?group_id='.$group_id.'&amp;view=addfile';
	$menu_tips[] = _('-tooltip:docman:submit');
}

if (session_loggedin()) {
	if (forge_check_perm('docman', $group_id, 'approve')) {
		$menu_text[] = _('Add new documentation directory');
		$menu_links[] = '/docman/?group_id='.$group_id.'&amp;view=addsubdocgroup';
		$menu_tips[] = _('-tooltip:docman:newdir');
	}
}

if ($g->useDocmanSearch()) {
	if ($d_arr || count($d_arr) > 1) {
		$menu_text[] = _('Search in documents');
		$menu_links[] = '/docman/?group_id='.$group_id.'&amp;view=search';
		$menu_tips[] = _('-tooltip:docman:searchdocs');
	}
}

if (session_loggedin()) {
	if (forge_check_perm('docman', $group_id, 'approve')) {
		$menu_text[] = _('Admin');
		$menu_links[] = '/docman/?group_id='.$group_id.'&amp;view=admin';
		$menu_tips[] = _('-tooltip:docman:admin');
	}
}

if (count($menu_text)) {
	$docman_submenu = array($menu_text, $menu_links, $menu_tips);
} else {
	$docman_submenu = false;
}
?>
