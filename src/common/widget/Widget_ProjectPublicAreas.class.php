<?php
/**
 * Copyright (c) Xerox Corporation, Codendi Team, 2001-2009.
 * Copyright © 2012
 *	Thorsten Glaser <t.glaser@tarent.de>
 * All rights reserved.
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

require_once('Widget.class.php');
require_once($gfcommon.'tracker/ArtifactTypeFactory.class.php');
require_once($gfcommon.'pm/ProjectGroupFactory.class.php');

/**
 * Widget_ProjectPublicAreas
 */
class Widget_ProjectPublicAreas extends Widget {
	function __construct() {
		parent::__construct('projectpublicareas');
	}

	function getTitle() {
		return _('Project Areas');
	}

	function getContent() {
		global $HTML;

		$request =& HTTPRequest::instance();
		$group_id = $request->get('group_id');
		$pm = ProjectManager::instance();
		$project = $pm->getProject($group_id);

		$group = group_get_object($group_id);
		if (!$group || !is_object($group)) {
			echo "<div>Could not get Group #$group_id</div>\n";
			return false;
		} elseif ($group->isError()) {
			echo "<div>Could not get Group #$group_id: " .
			    util_html_encode($group->getErrorMessage()) .
			    "</div>\n";
			return false;
		}

		// ################# Homepage Link

		echo '<div class="public-area-box" rel="doap:homepage">';
		echo util_make_link($project->getHomePage(),
		    $HTML->getHomePic(_('Home Page')) . ' ' .
		    _('Project Home Page'), false, true);
		echo "</div>\n";

		// ################## ArtifactTypes

		if ($project->usesTracker()) {
			echo '<div class="public-area-box">'."\n";
			$link_content = $HTML->getFollowPic(_('Tracker')) . ' ' . _('Tracker');
			echo util_make_link('/tracker/?group_id=' . $group_id, $link_content);

			$atf = new ArtifactTypeFactory($group);
			$emsg = _('There are no public trackers available');
			if (!$atf || !is_object($atf) || $atf->isError()) {
				$emsg = _('Could Not Get ArtifactTypeFactory');
				$at_arr = array();
			} else {
				$at_arr = $atf->getArtifactTypes();
				if (!$at_arr) {
					$at_arr = array();
				}
			}

			$at_cnt = 0;
			foreach ($at_arr as $at) {
				if (!$at || !is_object($at) || $at->isError()) {
					/* just skip it */
					continue;
				}
				if (!$at_cnt++) {
					/* first one to be displayed */
					echo "\n<ul class=\"tracker\" rel=\"doap:bug-database\">\n";
				}
				$tracker_stdzd_uri = util_make_url('/tracker/cm/project/' .
				    $project->getUnixName() . '/atid/' .
				    $at->getID());
				echo "\t<li about=\"$tracker_stdzd_uri\" typeof=\"sioc:Container\">\n" .
				    '<span rel="http://www.w3.org/2002/07/owl#sameAs">' . "\n" .
				    util_make_link('/tracker/?atid=' . $at->getID() .
				    '&amp;group_id=' . $group_id . '&amp;func=browse',
				    $at->getName()) . "</span>\n (" . sprintf(ngettext(
				    '<strong>%d</strong> open',
				    '<strong>%d</strong> open',
				    (int)$at->getOpenCount()), (int)$at->getOpenCount()) .
				    ' / ' . sprintf(ngettext(
				    '<strong>%d</strong> total',
				    '<strong>%d</strong> total',
				    (int)$at->getTotalCount()), (int)$at->getTotalCount()) .
				    ")<br /><span rel=\"sioc:has_space\" resource=\"\" ></span>\n</li>\n";
			}
			if ($at_cnt) {
				echo "</ul>\n";
			} else {
				echo "<br />\n<em>$emsg</em>\n";
			}
			echo "</div>\n";
		}

		// ################## Forums

		if ($project->usesForum()) {
			echo '<div class="public-area-box">'."\n";
			$link_content = $HTML->getForumPic('') . ' ' . _('Public Forums');
			echo util_make_link('/forum/?group_id=' . $group_id, $link_content);
			print ' (';
			$messages_count = project_get_public_forum_message_count($group_id);
			$forums_count = project_get_public_forum_count($group_id);
			printf(ngettext("<strong>%d</strong> message",
			    "<strong>%d</strong> messages", $messages_count),
			    $messages_count);
			print ' in ';
			printf(ngettext("<strong>%d</strong> forum",
			    "<strong>%d</strong> forums", $forums_count),
			    $forums_count);
			print ")\n</div>\n";
		}

		// ##################### Doc Manager

		if ($project->usesDocman()) {
			echo '<div class="public-area-box">'."\n";
			$link_content = $HTML->getDocmanPic('') .
			     ' ' . _('DocManager: Project Documentation');
			print util_make_link('/docman/?group_id=' . $group_id, $link_content);
			echo "\n</div>\n";
		}

		// ##################### Mailing lists

		if ($project->usesMail()) {
			echo '<div class="public-area-box">'."\n";
			$link_content = $HTML->getMailPic('') . ' ' . _('Mailing Lists');
			print util_make_link('/mail/?group_id=' . $group_id, $link_content);
			$n = project_get_mail_list_count($group_id);
			echo ' ';
			printf(ngettext('(<strong>%1$s</strong> public mailing list)',
			    '(<strong>%1$s</strong> public mailing lists)', $n), $n);
			echo "\n</div>\n";
		}

		// ##################### Task Manager

		if ($project->usesPm()) {
			echo '<div class="public-area-box">'."\n";
			$link_content = $HTML->getPmPic('') . ' ' . _('Tasks');
			print util_make_link('/pm/?group_id=' . $group_id, $link_content);

			$pgf = new ProjectGroupFactory($group);
			$emsg = _('There are no public subprojects available');
			if (!$pgf || !is_object($pgf) || $pgf->isError()) {
				$emsg = _('Could Not Get Factory');
				$pg_arr = array();
			} else {
				$pg_arr = $pgf->getProjectGroups();
				if (!$pg_arr || $pgf->isError()) {
					$pg_arr = array();
				}
			}

			$at_cnt = 0;
			foreach ($pg_arr as $at) {
				if (!$at || !is_object($at) || $at->isError()) {
					/* just skip it */
					continue;
				}
				if (!$at_cnt++) {
					/* first one to be displayed */
					echo "\n<ul class=\"task-manager\">\n";
				}
				echo "\n\t<li>" . util_make_link(
				    '/pm/task.php?group_project_id=' .
				    $at->getID() . '&amp;group_id=' .
				    $group_id . '&amp;func=browse',
				    $at->getName()) . " (" . sprintf(ngettext(
				    '<strong>%d</strong> open',
				    '<strong>%d</strong> open',
				    (int)$at->getOpenCount()), (int)$at->getOpenCount()) .
				    ' / ' . sprintf(ngettext(
				    '<strong>%d</strong> total',
				    '<strong>%d</strong> total',
				    (int)$at->getTotalCount()), (int)$at->getTotalCount()) .
				    ")</li>\n";
			}
			if ($at_cnt) {
				echo "</ul>\n";
			} else {
				echo "<br />\n<em>$emsg</em>\n";
			}
			echo "\n</div>\n";
		}

		// ######################### Surveys

		if ($project->usesSurvey()) {
			echo '<div class="public-area-box">'."\n";
			$link_content = $HTML->getSurveyPic('') . ' ' . _('Surveys');
			echo util_make_link( '/survey/?group_id='.$group_id, $link_content);
			echo ' (<strong>' .
			    project_get_survey_count($group_id) .
			    '</strong> ' . _('surveys') . ')';
			echo "\n</div>\n";
		}

		// ######################### SCM

		if ($project->usesSCM()) {
			echo '<div class="public-area-box">'."\n";

			$link_content = $HTML->getScmPic('') . ' ' . _('SCM Repository');
			print util_make_link('/scm/?group_id=' . $group_id, $link_content);

			$hook_params = array();
			$hook_params['group_id'] = $group_id;
			plugin_hook("scm_stats", $hook_params);
			echo "\n</div>\n";
		}

		// ######################### Plugins

		$hook_params = array();
		$hook_params['group_id'] = $group_id;
		plugin_hook("project_public_area", $hook_params);

		// ######################## AnonFTP

		if ($project->usesFTP() && $project->isActive()) {
			echo '<div class="public-area-box">'."\n";

			$link_content = $HTML->getFtpPic('') . ' ' . _('Anonymous FTP Space');
			$link_target = 'ftp://';
			$link_target .= forge_get_config('web_host') . '/pub/' .
			    $project->getUnixName();
			print util_make_link($link_target, $link_content, false, true);
			echo "\n</div>\n";
		}

		// ######################## WebCalendar

		plugin_hook("cal_link_group", $group_id);
	}

	function canBeUsedByProject(&$project) {
		return true;
	}

	function getDescription() {
		return _('List all available services for this project along with some information next to it. Click on any of this item to access a service.<br />The role of this area is pretty much equivalent to the Project Main Menu at the top of the screen except that it shows additional information about each of the service (e.g. total number of open bugs, tasks, ...)');
	}
}
