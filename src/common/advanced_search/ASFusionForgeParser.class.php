<?php
/*-
 * Advanced Search
 *
 * Copyright © 2012
 *	Thorsten Glaser <t.glaser@tarent.de>
 * Copyright © 2011
 *	Mike Esser <m.esser@tarent.de>
 * All rights reserved.
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

require_once $gfcommon."advanced_search/ASTokenizer.class.php";

class ASFusionForgeParserTracker {
	protected $tokens;
	protected $parameters;
	protected $ArtifactType;

	/**
	 * This function returns the artifact array.
	 *
	 * @param string $inputString
	 */
	public function parse($inputString, $artifactType) {
		//Parameters for db_query_params.
		$params            = array();
		$paramcount        = 1;
		$extraFieldCounter = 0;
		$curTokenPointer   = 0;
		$bDone             = false;

		$params['tracker_id'] = $this->parameters['tracker_id'];

		//First tokenize the input string.
		$this->tokens = ASTokenizer::tokenize($inputString, $params);

		if($this->tokens === false) {
			return false;
		}

		//Go trough all tokens and build the query
		$selectsql = 'SELECT DISTINCT ON (group_artifact_id, artifact_id) artifact_vw.* FROM artifact_vw';
		$wheresql  = ' WHERE group_artifact_id=$'.$paramcount++.' AND';

		$curToken = $this->tokens[0];

		//Check if the first token is an DFI. If not return an error.
		if($curToken->getType() !== EASToken::AS_DFI) {
			return "Invalid Syntax";
		}

		// Variable for error checking in language
		$lastToken = null;

		while(!$bDone) {
			$curType = $curToken->getType();

			if($curType === EASToken::AS_DFI) {

				//Check if the DFI is a Standard field. This is nowhere defined!
				$dfiName = $curToken->getData();

				if ($dfiName == 'ID'          ||
				    $dfiName == 'Summary'     ||
				    $dfiName == 'Detailed Description' ||
				    $dfiName == 'Open Date'   ||
				    $dfiName == 'Close Date'  ||
				    $dfiName == 'Assigned to' ||
				    $dfiName == 'Submitted by' ||
				    $dfiName == 'Priority' ||
				    $dfiName == 'Status') {
					//Get the next token. This has to be an operator!
					$curTokenPointer++;
					$curToken = $this->tokens[$curTokenPointer];
					$curType  = $curToken->getType();

					//We go a Standard field. Standard fields are not stored as extra fields!
					$temp = $this->_generateStdFieldWhereSQL($dfiName, $curType, $curTokenPointer, $paramcount, $params);

					if ($temp === false) {
						return "Invalid Syntax";
					}

					$wheresql .= $temp;
				} else {
					//We got an extra field. First get it's id.
					$result = db_query_params('SELECT extra_field_id FROM artifact_extra_field_list WHERE group_artifact_id=$1 AND field_name=$2;',
									          array($this->parameters['tracker_id'],
									          $curToken->getData()));

					$field_id = db_result($result, 0, 'extra_field_id');

					$selectsql .= ', artifact_extra_field_data aefd'.$extraFieldCounter;
					$wheresql .= ' (aefd'.$extraFieldCounter.'.extra_field_id=$'.$paramcount++;
					$params[] = $field_id;

					// Hack: Determine the type of the element to get the right search query.
					$res = db_query_params('SELECT field_type FROM artifact_extra_field_list WHERE extra_field_id=$1', array($field_id));
					$type = db_result($res,0,'field_type');

					//Get the next token. This has to be an operator!
					$curTokenPointer++;
					$curToken = $this->tokens[$curTokenPointer];
					$curType  = $curToken->getType();

					if ($type == 4 or $type == 6) {
						//We got a String as type. This means greater and less have to operate on string length.
						//Get the next token and check the operation.
						if($curType === EASToken::AS_OP_EQUALS) {
							//Get the next token. This has to be the data!
							$curTokenPointer++;
							$curToken = $this->tokens[$curTokenPointer];
							$curType  = $curToken->getType();

							$wheresql .= ' AND aefd'.$extraFieldCounter.'.field_data = $'.$paramcount++;
							$params[] = $curToken->getData();
						}
						else if($curType === EASToken::AS_OP_NOT) {
							//Get the next token. This has to be the data!
							$curTokenPointer++;
							$curToken = $this->tokens[$curTokenPointer];
							$curType  = $curToken->getType();

							$wheresql .= ' AND aefd'.$extraFieldCounter.'.field_data != $'.$paramcount++;
							$params[] = $curToken->getData();
						}
						else if($curType === EASToken::AS_OP_GREATER) {
							//Get the next token. This has to be the data!
							$curTokenPointer++;
							$curToken = $this->tokens[$curTokenPointer];
							$curType  = $curToken->getType();

							$wheresql .= ' AND aefd'.$extraFieldCounter.'.field_data > $'.$paramcount++;
							$params[] = $curToken->getData();
						}
						else if($curType === EASToken::AS_OP_LESS) {
							//Get the next token. This has to be the data!
							$curTokenPointer++;
							$curToken = $this->tokens[$curTokenPointer];
							$curType  = $curToken->getType();

							$wheresql .= ' AND aefd'.$extraFieldCounter.'.field_data < $'.$paramcount++;
							$params[] = $curToken->getData();
						}
						else if($curType === EASToken::AS_OP_LIKE) {
							//Get the next token. This has to be the data!
							$curTokenPointer++;
							$curToken = $this->tokens[$curTokenPointer];
							$curType  = $curToken->getType();

							$wheresql .= ' AND aefd'.$extraFieldCounter.'.field_data ILIKE $'.$paramcount++;
							$params[] = $curToken->getData();
						} else {
							return "Invalid Syntax";
						}
					} else {
						//We got any other type. Mostly this types are
						//enums. this means we have to do another query to get the corresponding
						//field data id.
						//Get the next token and check the operation.
						if($curType === EASToken::AS_OP_EQUALS) {
							//Get the next token. This has to be the data!
							$curTokenPointer++;
							$curToken = $this->tokens[$curTokenPointer];
							$curType  = $curToken->getType();

							if(strtoupper($curToken->getData()) == 'NONE') {
								$wheresql .= ' AND aefd'.$extraFieldCounter.'.field_data = $'.$paramcount++;
								$params[] = '100';
							} else {
								// Get the id
								$sql = 'SELECT element_id FROM artifact_extra_field_elements WHERE extra_field_id='.$field_id.
								    ' AND element_name=$1';

								$res = db_query_params($sql, array($curToken->getData()));
								$real_data_id = db_fetch_array($res);
								$real_data_id = $real_data_id['element_id'];

								$wheresql .= ' AND aefd'.$extraFieldCounter.
								             '.field_data = $'.$paramcount++.'';
								$params[] = $real_data_id;
							}
						}
						else if($curType === EASToken::AS_OP_NOT) {
							//Get the next token. This has to be the data!
							$curTokenPointer++;
							$curToken = $this->tokens[$curTokenPointer];
							$curType  = $curToken->getType();

							if(strtoupper($curToken->getData()) == 'NONE') {
								$wheresql .= ' AND aefd'.$extraFieldCounter.'.field_data != $'.$paramcount++;
								$params[] = '100';
							} else {
								// Get the id
								$sql = 'SELECT element_id FROM artifact_extra_field_elements WHERE extra_field_id='.$field_id.
								    ' AND element_name=$1';

								$res = db_query_params($sql, array($curToken->getData()));
								$real_data_id = db_fetch_array($res);
								$real_data_id = $real_data_id['element_id'];

								$wheresql .= ' AND aefd'.$extraFieldCounter.
								             '.field_data != $'.$paramcount++.'';
								$params[] = $real_data_id;
							}
						}
						else if($curType === EASToken::AS_OP_GREATER) {
							//Get the next token. This has to be the data!
							$curTokenPointer++;
							$curToken = $this->tokens[$curTokenPointer];
							$curType  = $curToken->getType();

							if(strtoupper($curToken->getData()) == 'NONE') {
								$wheresql .= ' AND aefd'.$extraFieldCounter.'.field_data > $'.$paramcount++;
								$params[] = '100';
							} else {
								$wheresql .= ' AND aefd'.$extraFieldCounter.
								             '.field_data = (SELECT element_id FROM artifact_extra_field_elements WHERE extra_field_id='.$field_id.
								             ' AND element_name=$'.$paramcount++.')';
								$params[] = $curToken->getData();
							}
						}
						else if($curType === EASToken::AS_OP_LESS) {
							//Get the next token. This has to be the data!
							$curTokenPointer++;
							$curToken = $this->tokens[$curTokenPointer];
							$curType  = $curToken->getType();

							if(strtoupper($curToken->getData()) == 'NONE') {
								$wheresql .= ' AND aefd'.$extraFieldCounter.'.field_data < $'.$paramcount++;
								$params[] = '100';
							} else {
								$wheresql .= ' AND aefd'.$extraFieldCounter.
								             '.field_data = (SELECT element_id FROM artifact_extra_field_elements WHERE extra_field_id='.$field_id.
								             ' AND element_name=$'.$paramcount++.')';
								$params[] = $curToken->getData();
							}
						}
						else if($curType === EASToken::AS_OP_LIKE) {
							//Get the next token. This has to be the data!
							$curTokenPointer++;
							$curToken = $this->tokens[$curTokenPointer];
							$curType  = $curToken->getType();

							if(strtoupper($curToken->getData()) == 'NONE') {
								$wheresql .= ' AND aefd'.$extraFieldCounter.'.field_data ILIKE $'.$paramcount++;
								$params[] = '100';
							} else {
								// Get the ids. Consider that we have to check more then one field.
								$sql = 'SELECT element_id FROM artifact_extra_field_elements WHERE extra_field_id='.$field_id.
								    ' AND element_name LIKE $1';

								$res = db_query_params($sql, array($curToken->getData()));
								$arr_data_id = array();

								while (($real_data_id = db_fetch_array($res))) {
									$arr_data_id[] = $real_data_id[0];
								}

								if (count($arr_data_id) > 1) {
									$wheresql .= ' AND (';

									foreach ($arr_data_id as $curId) {
										$wheresql .= 'aefd' . $extraFieldCounter .
										    '.field_data = $' . $paramcount++ . '';
										$params[] = $curId;
										$wheresql .= ' OR ';
									}

									$wheresql = rtrim($wheresql, ' OR ');
									$wheresql .= ')';
								} else {
									$real_data_id = $arr_data_id[0];
									$wheresql .= ' AND aefd'.$extraFieldCounter.
										     '.field_data = $'.$paramcount++.'';
									$params[] = $real_data_id;
								}
							}
						} else {
							//Invalid input. Return invalid syntax error.
							return "Invalid Syntax";
						}
					}
					$wheresql .= ' AND aefd'.$extraFieldCounter.'.artifact_id=artifact_vw.artifact_id)';
					$extraFieldCounter++;
				}
			} else {
				//This is an invalid input. Return invalid syntax error.
				return "Invalid Syntax";
			}

			$curTokenPointer++;
			if(!key_exists($curTokenPointer, $this->tokens)) {
				$bDone = true;
			}
			else {
				//This is an AND or OR operator. Add it to the wheresql
				if($this->tokens[$curTokenPointer]->getType() === EASToken::AS_OP_AND) {
					$wheresql .= ' AND';
				}
				else {
					$wheresql .= ' OR';
				}
				$curTokenPointer++;
				$curToken = $this->tokens[$curTokenPointer];
			}
		}

		// ORDER BY'.$this->getParam('_sort_col').' '.$this->getParam('_sort_ord').';',
		$result = db_query_params('SELECT * FROM (' . $selectsql . $wheresql . ') AS Artifacts;',
					   $params);
		$rows = db_numrows($result);

		$artifacts = array();

		if (db_error()) {
			echo db_error();
			return false;
		} else {
			while ($arr = db_fetch_array($result)) {
				$artifacts[] = new Artifact($artifactType, $arr);
			}
		}

		if ($this->getParam('_sort_col') !== null &&
		    $this->getParam('_sort_ord') !== null) {
			if ($this->getParam('_sort_col') == 'id') {
				sortArtifactList($artifacts, 'artifact_id',
				    $this->getParam('_sort_ord'));
			} else {
				sortArtifactList($artifacts,
				    $this->getParam('_sort_col'),
				    $this->getParam('_sort_ord'));
			}
		}

		return $artifacts;
	}

	public function setParam($name, $value) {
		$this->parameters[$name] = $value;
	}

	public function getParam($name) {
		if(key_exists($name, $this->parameters)) {
			return $this->parameters[$name];
		}

		return null;
	}

	protected function _generateStdFieldWhereSQL($fieldName, $curType, &$curTokenPointer, &$paramcount, &$params) {
		$wheresql    = '';
		$field_alias = '';
		$field_alias2 = '';
		$is_date     = false;
		$is_name      = false;

		//NOTE: We have to thread dates as a special type.
		//      We have to convert it to a string with an like operation
		//      and the input has to be converted if we use greater less or equals.
		//      It also seems like there is no possibility for a custom field to be a date field.

		if ($fieldName == 'ID') {
			$field_alias = 'artifact_id';
		} else if ($fieldName == 'Submitted by') {
			$is_name = true;
			$field_alias = 'submitted_realname';
			$field_alias2 = 'submitted_unixname';
		} else if ($fieldName == 'Assigned to') {
			$is_name = true;
			$field_alias = 'assigned_realname';
			$field_alias2 = 'assigned_unixname';
		} else if ($fieldName == 'Open Date') {
			$is_date = true;
			$field_alias = 'open_date';
		} else if ($fieldName == 'Close Date') {
			$is_date = true;
			$field_alias = 'close_date';
		} else if ($fieldName == 'Priority') {
			$field_alias = 'priority';
		} else if ($fieldName == 'Status') {
			$field_alias = 'status_name';
		} else if ($fieldName == 'Summary') {
			$field_alias = 'summary';
		} else if ($fieldName == 'Detailed Description') {
			$field_alias = 'details';
		}

		if ($curType === EASToken::AS_OP_EQUALS) {
			// Get the next token. This has to be the data!
			$curTokenPointer++;
			$curToken = $this->tokens[$curTokenPointer];
			$curType = $curToken->getType();

			if ($is_date) {
				$curToken->setData($curToken->getData().'%');
				$wheresql .= ' to_char((SELECT TIMESTAMP \'epoch\' + '.$field_alias.' * INTERVAL \'1 second\'), \'YYYY-MM-DD HH24:MI\') ILIKE $' . $paramcount++;
				$params[] = $curToken->getData();
			} else if ($is_name) {
				$wheresql .= '( '.$field_alias.' = $' . $paramcount++ .' OR '.$field_alias2.' = $'.$paramcount++.' )';
				$params[] = $curToken->getData();
				$params[] = $curToken->getData();
			} else {
				$wheresql .= ' '.$field_alias.' = $' . $paramcount++;
				$params[] = $curToken->getData();
			}
		} else if ($curType === EASToken::AS_OP_NOT) {
			//Get the next token. This has to be the data!
			$curTokenPointer++;
			$curToken = $this->tokens[$curTokenPointer];
			$curType = $curToken->getType();

			if ($is_date) {
				$curToken->setData($curToken->getData().'%');
				$wheresql .= ' to_char((SELECT TIMESTAMP \'epoch\' + '.$field_alias.' * INTERVAL \'1 second\'), \'YYYY-MM-DD HH24:MI\') NOT LIKE $' . $paramcount++;
				$params[] = $curToken->getData();
			} else if ($is_name) {
				$wheresql .= '( '.$field_alias.' NOT ILIKE $' . $paramcount++ .' AND '.$field_alias2.' NOT ILIKE $'.$paramcount++.' )';
				$params[] = $curToken->getData();
				$params[] = $curToken->getData();
			} else {
				$wheresql .= ' '.$field_alias.' != $' . $paramcount++;
				$params[] = $curToken->getData();
			}

		} else if ($curType === EASToken::AS_OP_GREATER) {
			//Get the next token. This has to be the data!
			$curTokenPointer++;
			$curToken = $this->tokens[$curTokenPointer];
			$curType = $curToken->getType();

			$wheresql .= ' '.$field_alias.' > $' . $paramcount++;

			if ($is_date) {
				// Check if the user included a time format
				if (preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $curToken->getData())) {
					$params[] = strtotime($curToken->getData()) + 86400;
				} else {
					$params[] = strtotime($curToken->getData());
				}
			} else {
				$params[] = $curToken->getData();
			}
		} else if ($curType === EASToken::AS_OP_LESS) {
			//Get the next token. This has to be the data!
			$curTokenPointer++;
			$curToken = $this->tokens[$curTokenPointer];
			$curType = $curToken->getType();

			$wheresql .= ' '.$field_alias.' < $' . $paramcount++;

			if($is_date) {
				$params[] = strtotime($curToken->getData());
			} else {
				$params[] = $curToken->getData();
			}
		} else if ($curType === EASToken::AS_OP_LIKE) {
			//Get the next token. This has to be the data!
			$curTokenPointer++;
			$curToken = $this->tokens[$curTokenPointer];
			$curType = $curToken->getType();

			if($is_date) {
				//Convert unix timestamp to postgres date and then to string.
				$wheresql .= ' to_char((SELECT TIMESTAMP \'epoch\' + '.$field_alias.' * INTERVAL \'1 second\'), \'YYYY-MM-DD HH24:MI\') ILIKE $' . $paramcount++;
				$params[] = $curToken->getData();
			} else if ($is_name) {
				$wheresql .= '( '.$field_alias.' ILIKE $' . $paramcount++ .' OR '.$field_alias2.' ILIKE $'.$paramcount++.' )';
				$params[] = $curToken->getData();
				$params[] = $curToken->getData();
			} else {
				$wheresql .= ' '.$field_alias.' ILIKE $' . $paramcount++;
				$params[] = $curToken->getData();
			}


		} else {
			return false;
		}

		return $wheresql;
	}
}
