<?php
/*-
 * Advanced Search
 *
 * Copyright © 2011
 *	Mike Esser <m.esser@tarent.de>
 * All rights reserved.
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

class AdvancedSearch {
	public static function deleteUserQuery($userID, $queryID, $trackerID) {
		$sql = "DELETE FROM user_has_query WHERE user_id = $1 AND query_id = $2 AND group_artifact_id = $3";
		$res = db_query_params($sql, array($userID, $queryID, $trackerID));
		if (db_affected_rows($res) === 0) {
			return false;
		}

		$sql = "DELETE FROM AdvancedQuery WHERE id = $1";
		db_query_params($sql, array($queryID));

		return true;
	}

	public static function renderSearchBar($searchActionUrl, $ajaxRequestUrl, $searchValue, $queryName) {
?>
<script type="text/javascript"><!--//--><![CDATA[//><!--
	jQuery(function() {
		function split(val) {
			return val.split(" ");
		}
		function extractLast(term) {
			return split(term).pop();
		}

		jQuery("#advanced_search")
			// don't navigate away from the field on tab when selecting an item
			.bind("keydown", function(event) {
				if (event.keyCode === jQuery.ui.keyCode.TAB &&
				    jQuery(this).data("autocomplete").menu.active) {
					event.preventDefault();
				}
			})
			.autocomplete({
				source: function(request, response) {
					jQuery.getJSON("<?php echo $ajaxRequestUrl; ?>" +
					    request.term, {
						term: extractLast(request.term)
					}, response);
				},
				search: function() {
					// custom minLength
					var term = extractLast(this.value);
					if (term.length < 0) {
						return false;
					}
				},
				focus: function() {
					// prevent value inserted on focus
					return false;
				},
				select: function(event, ui) {
					var terms = split(this.value);
					// remove the current input
					terms.pop();
					// add the selected item
					terms.push(ui.item.value);
					terms.push("");
					this.value = terms.join(" ");
					return false;
				}
			});
	});
//--><!]]></script>
<div class="ui-widget"><form id="advanced_search_form"
 method="post" action="<?php echo $searchActionUrl; ?>">
	<input type="hidden" name="set" value="advanced" />
	<label for="advanced_search"><?php echo _('Search'); ?>: </label>
	<input id="advanced_search" name="advanced_search"
	 style="width: 500px; font-size: 12px;"
	 value="<?php echo $searchValue; ?>" />
	<input id="search_submit" name="search_submit" type="submit"
	 value="<?php echo _('Search'); ?>"/>
	<input id="save_advanced_query" name="save_advanced_query"
	 type="checkbox" value="true" /><?php echo _('Save Query as '); ?>
	<input type="text" name="save_as" id="save_as" value="<?php echo $queryName; ?>" />
</form></div>
<?php
	}
}
