<?php
/*-
 * Advanced Search
 *
 * Copyright © 2011
 *	Mike Esser <m.esser@tarent.de>
 * All rights reserved.
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *-
 * The Tokenizer converts an string into the correpsonding tokens.
 * Not reckognized tokens are automatically deleted.
 */

require_once $gfcommon."advanced_search/EASToken.enum.php";
require_once $gfcommon."advanced_search/ASToken.class.php";

class ASTokenizer {
	public static $DFIProvider = null;

	public static function tokenize($inputString, $params) {
		if (!key_exists('analyze_input', $params)) {
			$params['analyze_input'] = false;
		}

		if(trim($inputString) == "") {
			return false;
		}

		$tokens       = array();
		$wordList     = explode(' ', $inputString);
		$curTerm      = '';
		$dfiList      = AStokenizer::$DFIProvider->getSimpleDFIList($params);

		foreach($wordList as $curWord) {
			//Check if it is one of the operators
			switch(strtoupper($curWord)) {
				case 'EQUALS': {
					//Check if $curTerm is not empty. If it is not, add an DFI Token.
					if($curTerm != '') {
						$tokens[] = new ASToken(EASToken::AS_DFI, $curTerm);
						$curTerm = '';
					}

					$tokens[] = new ASToken(EASToken::AS_OP_EQUALS, null);
				}break;
				case 'NOT': {
					//Check if $curTerm is not empty. If it is not, add an DFI Token.
					if($curTerm != '') {
						$tokens[] = new ASToken(EASToken::AS_DFI, $curTerm);
						$curTerm = '';
					}

					$tokens[] = new ASToken(EASToken::AS_OP_NOT, null);
				}break;
				case 'LIKE': {
					//Check if $curTerm is not empty. If it is not, add an DFI Token.
					if($curTerm != '') {
						$tokens[] = new ASToken(EASToken::AS_DFI, $curTerm);
						$curTerm = '';
					}

					$tokens[] = new ASToken(EASToken::AS_OP_LIKE, null);
				}break;
				case 'GREATER': {
					//Check if $curTerm is not empty. If it is not, add an DFI Token.
					if($curTerm != '') {
						$tokens[] = new ASToken(EASToken::AS_DFI, $curTerm);
						$curTerm = '';
					}

					$tokens[] = new ASToken(EASToken::AS_OP_GREATER, null);
				}break;
				case 'LESS': {
					//Check if $curTerm is not empty. If it is not, add an DFI Token.
					if($curTerm != '') {
						$tokens[] = new ASToken(EASToken::AS_DFI, $curTerm);
						$curTerm = '';
					}

					$tokens[] = new ASToken(EASToken::AS_OP_LESS, null);
				}break;
				case 'AND': {
					//Check if $curTerm is not empty. If it is not, add an Data Token.
					if($curTerm != '') {
						$tokens[] = new ASToken(EASToken::AS_DATA, $curTerm);
						$curTerm = '';
					}
					$tokens[] = new ASToken(EASToken::AS_OP_AND, null);
				}break;
				case 'OR': {
					//Check if $curTerm is not empty. If it is not, add an Data Token.
					if($curTerm != '') {
						$tokens[] = new ASToken(EASToken::AS_DATA, $curTerm);
						$curTerm = '';
					}

					$tokens[] = new ASToken(EASToken::AS_OP_OR, null);
				}break;
				default: {
					if($curTerm != '') {
						$curTerm .= ' '.$curWord;
					}
					else
					{
						$curTerm .= $curWord;
					}
				}break;
			}
			// Check if we found an DFI AND if we are analyzing the input.
			// NOTE: This is a hack. Maybe there is as better way to do it but i dont get it now.
			if (in_array($curTerm, $dfiList) && $params['analyze_input'] === true) {
				$tokens[] = new ASToken(EASToken::AS_DFI, $curTerm);
				$curTerm = '';
			}
		}

		//If curTerm is not empty there is still a data token left to add.
		if($curTerm != '') {
			$tokens[] = new ASToken(EASToken::AS_DATA, $curTerm);
			$curTerm = '';
		}

		return $tokens;
	}
}
