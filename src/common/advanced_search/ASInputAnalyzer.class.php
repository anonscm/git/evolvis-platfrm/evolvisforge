<?php
/*-
 * Advanced Search
 *
 * Copyright © 2011
 *	Mike Esser <m.esser@tarent.de>
 * All rights reserved.
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *-
 * The ASInputAnalyzer is able to analyze the current input and can
 * return the next possible string identifier of a token in the current
 * input. This can be used to give the user some feedback about their
 * search query entered.
 */

require_once $gfcommon."advanced_search/ASTokenizer.class.php";

class ASInputAnalyzer {
	protected $DFIProvider = null;

	public function analyzeInput($input, $params) {
		// Add the analyze_input Flag
		$params['analyze_input'] = true;

		////Use the tokenizer to generate tokens.
		$result = array();
		$tokens = ASTokenizer::tokenize($input, $params);
		$tokenCount = count($tokens);
		$forelastToken = $tokens[$tokenCount - 2];

		$dfiList = $this->DFIProvider->getDFIList($params);

		if(empty($tokens)) {
			//There are no tokens yet. Just print out the DFI List
			foreach($dfiList as $curDFI) {
				$result[] = $curDFI[0];
			}
		}
		else {
			//Token list is not empty. Check if the first token is a data token. If yes, print out DFIs again.
			if($tokens[0]->getType() == EASToken::AS_DATA) {
				foreach($dfiList as $curDFI) {
					$result[] = $curDFI[0];
				}
			}
			else {
				//All Special cases have been tested. Now check for the next possible
				//Tokens.
				//If we got a DFI token last, the next token has to be an operator
				if($forelastToken->getType() == EASToken::AS_DFI) {
					$result = array('equals', 'not', 'like', 'greater', 'less');
				}
				else if($forelastToken->getType() == EASToken::AS_OP_EQUALS ||
						$forelastToken->getType() == EASToken::AS_OP_GREATER ||
						$forelastToken->getType() == EASToken::AS_OP_LESS ||
						$forelastToken->getType() == EASToken::AS_OP_LIKE ||
						$forelastToken->getType() == EASToken::AS_OP_NOT) {
					//Next should be an data field but we cant determine when this field is finished
					//so just give a hint for the next possible operators.
					foreach($dfiList as $curDFI) {
						$result = array('and', 'or');
					}
				}
				else if($forelastToken->getType() == EASToken::AS_OP_AND ||
				    $forelastToken->getType() == EASToken::AS_OP_OR) {
						//Print the DFI List
						foreach($dfiList as $curDFI) {
							$result[] = $curDFI[0];
						}
				}
			}
		}
		return $result;
	}

	public function setDFIProvider($provider) {
		$this->DFIProvider = $provider;
	}

}
