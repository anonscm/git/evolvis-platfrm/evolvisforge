<?php
/*-
 * Advanced Search
 *
 * Copyright © 2011
 *	Mike Esser <m.esser@tarent.de>
 * All rights reserved.
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

require_once $gfcommon."advanced_search/IDFIProvider.interface.php";
require_once $gfwww.'env.inc.php';
require_once $gfwww.'include/pre.php';

class FusionForgeDFIProvider implements IDFIProvider {
	/**
	 * Nothing todo for fusionforge. FusionForgeDFIs can be identified
	 * trough a view.
	 *
	 * @param type $fileName the file in which this info is stored
	 * @param type $params extra parameters.
	 */
	public function scanAll($params) {
	}

	/**
	 * Return the available DFIs for a given tracker defined in
	 * $params.
	 *
	 * @param array $params
	 */
	public function getDFIList($params) {
		$tracker_id = $params['tracker_id'];
		//Don't forget the Standard fields.
		$result	 = array(
			array('ID', 'artifact_id'),
			array('Summary', 'summary'),
			array('Detailed Description', 'details'),
			array('Open Date', 'open_date'),
			array('Close Date', 'close_date'),
			array('Assigned to', 'assigned_to'),
			array('Submitted by', 'submitted_by'),
			array('Priority', 'priority'),
			array('Status', 'status_name'),
		    );

		$sql = "SELECT field_name, alias FROM artifact_extra_field_list WHERE group_artifact_id = $1";
		$dbRes = db_query_params($sql, array($tracker_id));

		while ($arr = db_fetch_array($dbRes)) {
			if($arr['field_name'] == 'Relation between artifacts'){
				//Ignore this
			}
			else {
				$result[] = array($arr['field_name'], $arr['alias']);
			}
		}

		return $result;
	}

	public function getSimpleDFIList($params) {
		$tracker_id = $params['tracker_id'];
		$result	 = array();
		$result	 = array('ID',
			    'Summary',
			    'Detailed Description',
			    'Open Date',
			    'Close Date',
			    'Assigned to',
			    'Submitted by');

		$sql = "SELECT field_name FROM artifact_extra_field_list WHERE group_artifact_id = $1";
		$dbRes = db_query_params($sql, array($tracker_id));

		while ($arr = db_fetch_array($dbRes)) {
			if($arr['field_name'] == 'Relation between artifacts'){
				//Ignore this
			}
			else {
				$result[] = $arr['field_name'];
			}
		}

		return $result;
	}
}
