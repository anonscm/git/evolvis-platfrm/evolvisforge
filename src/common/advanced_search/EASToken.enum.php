<?php
/*-
 * Advanced Search
 *
 * Copyright © 2011
 *	Mike Esser <m.esser@tarent.de>
 * All rights reserved.
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

final class EASToken {
	const AS_DFI        = 0;
	const AS_OP_EQUALS  = 1;
	const AS_OP_LIKE    = 2;
	const AS_OP_GREATER = 3;
	const AS_OP_LESS    = 4;
	const AS_OP_AND     = 5;
	const AS_OP_OR      = 6;
	const AS_OP_NOT     = 7;
	const AS_DATA       = 8;
}
