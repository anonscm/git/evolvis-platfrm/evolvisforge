<?php
/*-
 * Advanced Search
 *
 * Copyright © 2011
 *	Mike Esser <m.esser@tarent.de>
 * All rights reserved.
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *-
 * This interface is used to give the user of this library the possibility to
 * define his own DFIs. This interface has to be reimplement per application.
 */

interface IDFIProvider {
	/**
	 * This function scans for available DFIs. It generates
	 * a file in which the current available DFIs are stored.
	 *
	 * @param array list of parameters.
	 */
	public function scanAll($params);
	/**
	 * Returns all available DFIs.
	 */
	public function getDFIList($params);
	/**
	 * Returns a simplified DFI List. This excludes aliases!
	 */
	public function getSimpleDFIList($params);
}
