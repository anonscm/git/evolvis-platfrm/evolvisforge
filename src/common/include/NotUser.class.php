<?php
/*-
 * Fake User class for FusionForge
 *
 * Copyright © 2010
 *	Thorsten Glaser <t.glaser@tarent.de>
 * All rights reserved.
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *-
 * Class with similar interface to the User class, for use with
 * not logged in “users” where we need to have duck type level
 * compatibility, e.g. for Preferences. Slightly based on the
 * FusionForge User class by VA Linux and Roland Mas, and work
 * done by Umer Kayani.
 */

class NotUser extends FFError {

	/**
	 *	NotUser - constructor
	 *
	 */
	function __construct() {
		session_start();
	}

	/**
	 *	deletePreference - delete a preference
	 *
	 *	@param	string	unique identifier
	 *	@return	boolean	success
	 */
	function deletePreference($name) {
		global $_SESSION;

		unset($_SESSION[$name]);
		return true;
	}

	/**
	 *	setPreference - set preference
	 *
	 *	@param	string	unique identifier
	 *	@param	string	value
	 *	@return	boolean	success
	 */
	function setPreference($name, $value) {
		global $_SESSION;

		$_SESSION[$name] = $value;
		return true;
	}

	/**
	 *	getPreference - get preference
	 *
	 *	@param	string	unique identifier
	 *	@return	string	value or false on failure
	 */
	function getPreference($name) {
		global $_SESSION;

		return util_ifsetor($_SESSION[$name]);
	}

	/**
	 *	isLoggedIn - only used by session code
	 *
	 *	@return	boolean	whether the user is logged in
	 */
	function isLoggedIn() {
		return false;
	}

}

?>