<?php
/**
 * FusionForge account functions
 *
 * Copyright 1999-2001, VA Linux Systems, Inc.
 * Copyright 2010, Franck Villaume - Capgemini
 * Copyright © 2012
 *	Thorsten Glaser <t.glaser@tarent.de>
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/**
 * account_pwvalid() - Validates a password
 *
 * @param		string	The plaintext password string
 * @returns		true on success/false on failure
 *
 */
function account_pwvalid($pw) {
	if (strlen($pw) < 6) {
		$GLOBALS['register_error'] = _('Password must be at least 6 characters.');
		return 0;
	}
	return 1;
}

/**
 * account_namevalid() - Validates a login username
 *
 * @param		string	The username string
 * @param		int	(optional) maximum length (default: 15)
 * @param		int	(optional) minimum length (default: 3)
 * @returns		true on success/false on failure
 *
 */
function account_namevalid($name, $maxlen=15, $minlen=3) {


	// no spaces
	if (strrpos($name,' ') > 0) {
		$GLOBALS['register_error'] = _('There cannot be any spaces in the login name.');
		return 0;
	}

	// min and max length
	if (strlen($name) < $minlen) {
		$GLOBALS['register_error'] = sprintf(_('Name is too short. It must be at least %d characters.'), $minlen);
		return 0;
	}
	if (strlen($name) > $maxlen) {
		$GLOBALS['register_error'] = sprintf(_('Name is too long. It must be less than %d characters.'), $maxlen);
		return 0;
	}

	if (!preg_match('/^[a-z][-a-z0-9_]*\z/', $name)) {
		$GLOBALS['register_error'] = _('Illegal character in name. Only lower-case letters, digits, hyphen-minus and underscore are permitted, and a Unix name must begin with a letter.');
		return 0;
	}

	// illegal names
	$regExpReservedNames = "^(root|bin|daemon|adm|lp|sync|shutdown|halt|mail|news|"
		. "uucp|operator|games|mysql|httpd|nobody|dummy|www|cvs|shell|ftp|irc|"
		. "debian|ns|download)$";
	if( preg_match("/$regExpReservedNames/i", $name) ) {
		$GLOBALS['register_error'] = _('Name is reserved.');
		return 0;
	}
	if (forge_get_config('use_shell')) {
		if ( exec("getent passwd $name") != "" ){
			$GLOBALS['register_error'] = _('That username already exists.');
			return 0;
		}
		if ( exec("getent group $name") != "" ){
			$GLOBALS['register_error'] = _('That username already exists.');
			return 0;
		}
	}
	if (preg_match("/^(anoncvs_)/i",$name)) {
		$GLOBALS['register_error'] = _('Name is reserved for CVS.');
		return 0;
	}

	return 1;
}

/**
 * account_groupnamevalid() - Validates an account group name
 *
 * @param		string	The group name string
 * @param		int	(optional) maximum length (default: 15)
 * @param		int	(optional) minimum length (default: 3)
 * @returns		true on success/false on failure
 *
 */
function account_groupnamevalid($name, $maxlen=15, $minlen=3) {
	if (!account_namevalid($name, $maxlen, $minlen)) {
		return 0;
	}

	// invalid names
	$regExpReservedGroupNames = "^(www[0-9]?|cvs[0-9]?|shell[0-9]?|ftp[0-9]?|"
		. "irc[0-9]?|news[0-9]?|mail[0-9]?|ns[0-9]?|download[0-9]?|pub|users|"
		. "compile|lists|slayer|orbital|tokyojoe|webdev|projects|cvs|monitor|"
		. "mirrors?)$";
	if(preg_match("/$regExpReservedGroupNames/i",$name)) {
		$GLOBALS['register_error'] = _('Name is reserved for DNS purposes.');
		return 0;
	}

	if(preg_match("/_/",$name)) {
		$GLOBALS['register_error'] = _('Group name cannot contain underscore for DNS reasons.');
		return 0;
	}

	return 1;
}

/**
 * account_genstr() - Generate an encoded random string
 *
 * This is a local function used for account_genunixpw()
 *
 * @param		int	Number of bytes to spew out
 * @returns The random string, bcrypt ASCIIfied, or false if an error occured
 *
 */
function account_genstr($n) {
	$o = (int)(($n + 3) / 4);
	$b = base64_encode(util_randbytes($o * 3));
	if (strlen($b) != ($o * 4))
		return false;

	$res = substr(strtr($b, '+', '.'), 0, $n);
	if (strchr($res, '='))
		/* pad character not allowed */
		return false;
	if (strlen($res) != $n)
		return false;
	return $res;
}

/**
 * account_getcipher() - Get and check {core/unix_cipher}
 * @return	string
 *		lowercase'd {core/unix_cipher} if usable
 */
function account_getcipher($flushcache=false) {
	static $c = false;

	/* for unit testing */
	if ($flushcache) {
		$c = false;
	}

	if (!$c) {
		$c = strtolower(forge_get_config('unix_cipher'));

		/* run self-tests (yes, there are two stories to this…) */
		switch ($c) {
		case 'plain':
			return $c;
		case 'des':
			$t = 'aajVvTOjacxCY';
			break;
		case 'blowfish':
			$t = '$2a$06$aaaaaaaaaaaaaaaaaaaaaOK7n3Kyqc0rrS6xHTCdZWeqGPpVHeytO';
			break;
		default:
			$c = 'md5';
			/* FALLTHROUGH */
		case 'md5':
			$t = '$1$aaaaaaaa$hkqmY7IoZFLxaTeeAIVu10';
			break;
		case 'sha-256':
			$t = '$5$aaaaaaaaaaaaaaaa$YLL1DqD8nyAm9Pogst89RcvyGLokQXYoweQ4fGrwy04';
			break;
		case 'sha-512':
			$t = '$6$aaaaaaaaaaaaaaaa$Ybk6Im9Sw17HFVlElf6Ehd.OGcyCUcgLc91KoMhFWRYe4CHhtavFcTVs3qhM5VD9hga6sFhnub5dLeP/H14OC1';
			break;
		}

		if (crypt("8b \xd0\xc1\xd2\xcf\xcc\xd8", $t) !== $t) {
			throw new Exception('Unable to crypt with "' . $c . '"!');
			die; /* just in case… */
		}
	}
	return $c;
}

/**
 * account_chkunixpw() - Check unix password
 *
 * @param	string	$key
 *			plaintext password the user typed
 * @param	string	$salt
 *			encrypted password to check against
 * @return	enumerated-integer
 *		0 = password not valid
 *		1 = password valid but not in {core/unix_cipher} format
 *		2 = password valid and its format matches what we want
 */
define('ACCOUNT_ALLOW_PLAIN', 0);
function account_chkunixpw($key, $salt, $unittest=false) {
	$rv = 0;
	$guessed = 'unknown';

	if (!$key || !$salt || strlen($salt) < 2) {
		return 0;
	}

	while (true) {
		$lo = strtolower($salt);

		/* PLAIN directly */
		if ($salt === $key) {
			$rv = ACCOUNT_ALLOW_PLAIN;
			$guessed = 'plain';
			break;
		}

		/* PLAIN via LDAP {plain} */
		if (strlen($lo) > 7 &&
		    substr($lo, 0, 7) === '{plain}' &&
		    substr($salt, 7) === $key) {
			$rv = ACCOUNT_ALLOW_PLAIN;
			$guessed = 'not-preferred';
			break;
		}

		/* UNIX CRYPT directly */
		if (crypt($key, $salt) === $salt) {
			$rv = 1;
			$guessed = 'crypt';
			break;
		}

		/* UNIX CRYPT via LDAP {crypt} */
		if (strlen($lo) > 7 &&
		    substr($lo, 0, 7) === '{crypt}' &&
		    crypt($key, substr($salt, 7)) === substr($salt, 7)) {
			$rv = 1;
			$guessed = 'not-preferred';
			break;
		}

		/* LDAP {ssha} */
		if (strlen($lo) > 6 &&
		    substr($lo, 0, 6) === '{ssha}') {
			$chk = base64_decode(substr($salt, 6));
			if (strlen($chk) == 24) {
				$c_salt = substr($chk, 20);
				$chk = substr($chk, 0, 20);
				if (pack("H*", sha1($key . $c_salt)) === $chk) {
					$rv = 1;
					$guessed = 'not-preferred';
					break;
				}
			}
		}

		/* any crypt or LDAP or other weird things (last resort) */
		if (extension_loaded('perl')) {
			try {
				/* try Authen::Passphrase, if available */
				$chk = (($salt[0] == '{' /*}*/) ? '' :
				    '{crypt}') . $salt;
				$perl = new Perl();
				$perl->eval('use Authen::Passphrase');
				$perl->pwhash = $chk;
				$perl->pwplain = $key;
				$perl->eval('$ppr = Authen::Passphrase->from_rfc2307($pwhash);');
				if ($perl->eval('$ppr->match($pwplain)')) {
					$rv = 1;
					$guessed = 'needs-perl';
				}
			} catch (PerlException $e) {
				$rv = 0;
			}
			if ($rv) {
				break;
			}
		}

		/* do not loop */
		break;
	}
	if ($rv == 1) {
		if ($guessed == 'crypt') {
			/* try to guess better */
			switch (strlen($salt)) {
			case 13:
				$guessed = 'des';
				break;
			case 34:
				if (substr($salt, 0, 3) === '$1$') {
					$guessed = 'md5';
				}
				break;
			case 60:
				if (substr($salt, 0, 4) === '$2a$') {
					$guessed = 'blowfish';
				}
				break;
			default:
				if (strlen($salt) >= 47 &&
				    substr($salt, 0, 3) === '$5$') {
					$guessed = 'sha-256';
				} else if (strlen($salt) >= 90 &&
				    substr($salt, 0, 3) === '$6$') {
					$guessed = 'sha-512';
				}
				break;
			}
		}
		if ($guessed === account_getcipher()) {
			$rv = 2;
		}
	}
	if ($unittest) {
		return array($rv, $guessed);
	}
	return $rv;
}

/**
 * account_genunixpw() - Generate unix password
 *
 * @param		string	The plaintext password string
 * @return		The encrypted password
 *
 */
function account_genunixpw($plainpw) {
	$cmplensub = 0;
	switch (account_getcipher()) {
	case 'des':
		$prefix = '';
		$salt = account_genstr(2);
		$postfix = '';
		break;
	case 'blowfish':
		$prefix = '$2a$08$';
		$salt = account_genstr(22);
		$postfix = '';
		$cmplensub = 1;
		break;
	case 'plain':
		if (ACCOUNT_ALLOW_PLAIN) {
			return $plainpw;
		}
		/* FALLTHROUGH */
	default:
	case 'md5':
		$prefix = '$1$';
		$salt = account_genstr(8);
		$postfix = '$';
		break;
	case 'sha-256':
		$prefix = '$5$';
		$salt = account_genstr(16);
		$postfix = '$';
		break;
	case 'sha-512':
		$prefix = '$6$';
		$salt = account_genstr(16);
		$postfix = '$';
		break;
	}

	if (!$salt) {
		/* XXX better error handling */
		throw new Exception('Unable to generate random password salt!');
		die; /* just in case… */
	}
	$salt = $prefix . $salt . $postfix;
	$cryptpw = crypt($plainpw, $salt);

	/*
	 * This check is necessary because the Blowfish (bcrypt) cipher
	 * doesn’t work on Debian Lenny (but does on MirBSD).
	 */
	if (strncmp($salt, $cryptpw, strlen($salt) - $cmplensub)) {
		/* XXX better error handling */
		throw new Exception('Unable to crypt with "' .
		    account_getcipher() . '" the password!');
		die; /* just in case… */
	}
	return $cryptpw;
}

/**
 * account_shellselects() - Print out shell selects
 *
 * @param		string	The current shell
 *
 */
function account_getshells() {
	$shells = file("/etc/shells");
	array_unshift($shells, "/lib/anonsvnsh");
	array_unshift($shells, "/bin/anonsvnsh.dochroot");
	array_unshift($shells, "/bin/anonsvnsh.nochroot");
	$out_shells = array();
	foreach ($shells as $s) {
		if (substr($s, 0, 1) != '#') {
			$out_shells[] = chop($s);
		}
	}
	return $out_shells;
}
function account_shellselects($current) {
	$shells = account_getshells();
	foreach ($shells as $this_shell) {
		echo html_e('option', array(
			'selected' => (($current == $this_shell) ?
			    'selected' : false),
			'value' => $this_shell,
		    ), htmlspecialchars($this_shell)) . "\n";
	}
}

/**
 *	account_user_homedir() - Returns full path of user home directory
 *
 *  @param		string	The username
 *	@return home directory path
 */
function account_user_homedir($user) {
	//return '/home/users/'.substr($user,0,1).'/'.substr($user,0,2).'/'.$user;
	return forge_get_config('homedir_prefix').'/'.$user;
}

/**
 *	account_group_homedir() - Returns full path of group home directory
 *
 *  @param		string	The group name
 *	@return home directory path
 */
function account_group_homedir($group) {
	//return '/home/groups/'.substr($group,0,1).'/'.substr($group,0,2).'/'.$group;
	return forge_get_config('groupdir_prefix').'/'.$group;
}


/* CAPTCHA related code */

if (!forge_get_config('use_captcha')) {
	$have_captcha = false;
} else if (file_exists($gfcommon.'include/captcha.php')) {
	require_once $gfcommon.'include/captcha.php';
	$have_captcha = true;
} else {
	$have_captcha = false;
}

/**
 *	captcha_check() - Checks if captcha submission was successful
 *
 * @return true if the check succeeded, false otherwise
 *
 */
function captcha_check() {
	global $have_captcha;

	/* Site admins always pass CAPTCHA checks */
	if (forge_check_global_perm('forge_admin')) {
		return true;
	}

	/* Non-existent CAPTCHA support also passes */
	if ($have_captcha === false) {
		return true;
	}

	/* Defer checking to the CAPTCHA backend */
	if ($have_captcha === true) {
		return captcha_do_check();
	}

	/* Something weird happened */
	return false;
}

/**
 *	captcha_form() - Insert form code for captcha submission
 *
 * @param string	Text to output before the label
 * @param string	Text to output between the label and the <input/>
 * @param string	Text to output after the <input/>
 * @return XHTML code (string)
 */
function captcha_form($texta, $textb, $textc) {
	global $have_captcha;

	/* Site admins always pass CAPTCHA checks */
	if (forge_check_global_perm('forge_admin')) {
		return "";
	}

	/* Non-existent CAPTCHA support also passes */
	if ($have_captcha === false) {
		return "";
	}

	/* Defer checking to the CAPTCHA backend */
	if ($have_captcha === true) {
		return $texta . captcha_get_label() . $textb .
		    captcha_get_input() . $textc;
	}

	/* Something weird happened */
	return "";
}

// Local Variables:
// mode: php
// c-file-style: "bsd"
// End:

?>
