<?php
/*-
 * XHTML Table generation support for FusionForge
 *
 * Copyright © 2011
 *	Thorsten “mirabilos” Glaser <t.glaser@tarent.de>
 * All rights reserved.
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *-
 * Easily generate XHTML tables, with a Prototype-like API.
 * Supports colspan but not rowspan at the moment.
 */

class EvolvisTable {

	/**
	 * EvolvisTable() - Initialise a table object
	 *
	 * @param	array	$title_arr
	 *			The array of titles
	 * @param	array	$links_arr
	 *		(optional) The array of title links
	 * @param	array	$param
	 *		(optional) flags, such as:
	 *			- bool sortable
	 *			- false/string border
	 */
	function __construct($title_arr, $links_arr=false, $param=array()) {
		$ct = count($title_arr);
		$cl = is_array(util_ifsetor($links_arr)) ?
		    count($links_arr) : 0;

		if (!$ct) {
			throw new Exception('Missing $title_arr');
			return false;
		}

		if ($cl && ($ct != $cl)) {
			throw new Exception('Inequal magnitude of ' .
			    '$title_arr and $links_arr');
			return false;
		}

		$param['text'] = $title_arr;
		$param['link'] = $cl ? $links_arr : array();
		if ($title_arr[0] === false) {
			$param['topless'] = $ct;
		}
		$this->param = $param;

		$this->ncols = $ct;
		$this->rows = array();
		return true;
	}

	/**
	 * tr() - Add table row
	 *
	 * @param	array	$attrs
	 *		(optional) attributes of the table row
	 * @param	integer	$atpos
	 *		(optional) after which position to add
	 *		(default: -1 a.k.a. at the end)
	 * @return	&EvolvisTableRow object
	 */
	function &tr($attrs=array(), $atpos=-1) {
		$therow = new EvolvisTableRow($attrs);
		if ($atpos == -1) {
			$this->rows[] = $therow;
		} else {
			array_splice($this->rows, $atpos, 0, array($therow));
		}
		return $therow;
	}

	/**
	 * emit() - Return the table as XHTML
	 *
	 * @return	string
	 */
	function emit() {
		global $HTML;

		if (!count($this->rows)) {
			return "";
		}
		$rv = $HTML->listTableStart($this->param);
		$i = 0;
		foreach ($this->rows as $v) {
			$numcols = $v->getncols();
			if ($numcols != $this->ncols) {
				throw new Exception("Row " .
				    $i . " #cols " . $numcols .
				    " not expected " . $this->ncols);
			}
			/*XXX breaks Kapselung */
			$HTML->boxApplyAltRowStyle($i++, $v->attrs);
			$rv .= $v->emit();
		}
		$rv .= $HTML->listTableBottom();
		return $rv;
	}
}

class EvolvisTableRow {
	function __construct($attrs=array()) {
		$this->attrs = $attrs;
		$this->cols = array();
		return true;
	}

	/**
	 * th() - Add table column (header)
	 *
	 * @param	array	$attrs
	 *		(optional) attributes of the table field
	 * @param	integer	$atpos
	 *		(optional) after which position to add
	 *		(default: -1 a.k.a. at the end)
	 * @param	string	$txt
	 *		(optional) text to htmlencode and set
	 * @return	&EvolvisTableColumn object
	 */
	function &th($attrs=array(), $atpos=-1, $txt=false) {
		$thecol = new EvolvisTableColumn($attrs, 'th');
		if ($atpos == -1) {
			$this->cols[] = $thecol;
		} else {
			array_splice($this->cols, $atpos, 0, array($thecol));
		}
		if ($txt !== false) {
			$thecol->set($txt);
		}
		return $thecol;
	}

	/**
	 * td() - Add table column (data)
	 *
	 * @param	array	$attrs
	 *		(optional) attributes of the table field
	 * @param	integer	$atpos
	 *		(optional) after which position to add
	 *		(default: -1 a.k.a. at the end)
	 * @param	string	$txt
	 *		(optional) text to htmlencode and set
	 * @return	&EvolvisTableColumn object
	 */
	function &td($attrs=array(), $atpos=-1, $txt=false) {
		$thecol = new EvolvisTableColumn($attrs);
		if ($atpos == -1) {
			$this->cols[] = $thecol;
		} else {
			array_splice($this->cols, $atpos, 0, array($thecol));
		}
		if ($txt !== false) {
			$thecol->set($txt);
		}
		return $thecol;
	}

	function getncols() {
		$nc = 0;
		foreach ($this->cols as $v) {
			/*XXX breaks Kapselung */
			$nc += util_ifsetor($v->attrs['colspan'], 1);
		}
		return $nc;
	}

	function emit() {
		$rv = "\n\t" . html_eo('tr', $this->attrs);
		foreach ($this->cols as $v) {
			$rv .= "\n\t\t" . $v->emit();
		}
		$rv .= "\n\t</tr>";
		return $rv;
	}
}

class EvolvisTableColumn {
	function __construct($attrs=array(), $type='td') {
		$this->type = $type;
		$this->attrs = $attrs;
		$this->content = "";
		return true;
	}

	function set($to) {
		$this->setraw(util_html_secure($to));
	}

	function append($me) {
		$this->appendraw(util_html_secure($me));
	}

	function setraw($to) {
		$this->content = "" . $to;
	}

	function appendraw($me) {
		$this->content .= $me;
	}

	function emit() {
		return html_e($this->type, $this->attrs, $this->content, false);
	}
}
