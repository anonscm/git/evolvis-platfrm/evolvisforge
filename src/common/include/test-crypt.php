<?php
/*-
 * Test crypt() functions in FusionForge
 *
 * Copyright © 2012
 *	Thorsten Glaser <t.glaser@tarent.de>
 * All rights reserved.
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/* mock */

$cfg = array('use_captcha' => false);
function forge_get_config($s) {
	global $cfg;

	if (!isset($cfg[$s])) {
		echo "E: forge_get_config undef '$s'\n";
		$cfg[$s] = false;
	}
	return $cfg[$s];
}

/* aid */

require "utils.php";

/* to test */

require "account.php";

/* test batteries */

$tests_unix = array(
	'Plain' => 'fnord',
	'DES' => 'ndCam7XnSiOQU',
	'Blowfish' => '$2a$08$lnW6D7RO02ji5Z3SWt9Pee0WZpEbBN20bPZdNLRpCF9rErqY4bZnS',
	'MD5' => '$1$OEaikIe0$h6vVHVs2nzbt2PUe2lx381',
	'SHA-256' => '$5$ZP1n6RBh2Kz4neoh$3P1sF606SWhIpLkhLqAR8UrHX/vTiv/z0AI0YFtrCu6',
	'SHA-512' => '$6$YUqLlwG2hXyAILcG$.05BP55eRz21GCUHDNx83JxtxyymfncBAi0wIxyAzUp78Gae6UUs7GvNDO128SBc48S7QuF0JOHpxjRLG2qt10',
	'Foo' => '$1$YxJD7PNC$aECtZazSxk.JBs7iJWgQf0',
);

$tests_incoming = array_merge(array_values($tests_unix), array(
	'{Plain}fnord',
	'{Crypt}$1$sY3NGTnE$jJ2p3diJoDNparlT2OZKt0',
	'{sSHA}j8KfLagdk8RkTgu7zgxe7/G482Dvx+Ep',
	'{MD5}sV5ADI29ZpfyY4UhbTKkDw==',
));

foreach ($tests_unix as $method => $hash) {
	echo "\nTesting UNIX with $method\n";
	$cfg['unix_cipher'] = $method;
	try {
		echo "Recognised as: " . account_getcipher(true) . "\n";
	} catch (Exception $e) {
		echo 'getcipher, caught exception: ',  $e->getMessage(), "\n";
	}
	try {
		$res = account_chkunixpw('fnord', $hash, true);
		echo "Validated as: " . minijson_encode($res,false) . "\n";
	} catch (Exception $e) {
		echo 'chkunixpw, caught exception: ',  $e->getMessage(), "\n";
	}
	try {
		echo "Generated as: " . account_genunixpw('fnord') . "\n";
	} catch (Exception $e) {
		echo 'genunixpw, caught exception: ',  $e->getMessage(), "\n";
	}
}

$cfg['unix_cipher'] = 'PLAIN';
try {
	account_getcipher(true);
} catch (Exception $e) {
	echo "\n\nUnexpected exception: ", $e->getMessage(), "\n";
}

foreach ($tests_incoming as $hash) {
	try {
		$res = account_chkunixpw('fnord', $hash, true);
	} catch (Exception $e) {
		$res = "!" . $e->getMessage();
	}
	echo "\nIncoming result " . minijson_encode($res,false) . " from: $hash";
}

echo "\n\nAll tests run.\n";
