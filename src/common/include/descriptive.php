<?php
/*-
 * Factored-out code for handling descriptive item elements
 *
 * Copyright © 2011, 2012, 2014, 2015
 *	Thorsten “mirabilos” Glaser <t.glaser@tarent.de>
 * All rights reserved.
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *-
 * This code attempts to handle descriptive item elements (Summary,
 * Details, Comments/Followups) in a manner compatible to the old
 * GForge database encoding, but with new rules.
 *
 * In the database, everything is passed through htmlspecialchars()
 * before storing, so we must call htmlspecialchars_decode() on any
 * database content before using it. This will even decode partial
 * entities, so when handling the unencoded string we need to be
 * careful as usual. We decide to only pass around unencoded strings
 * except once formatted for display, and the accessor and database
 * functions for compatibility.
 * Except that htmlspecialchars_decode() is not enough, and we must
 * call util_unconvert_htmlspecialchars() on the contents instead…
 *
 * Details and Comments further can have formatting attributes and
 * links. The format for that is:
 * - a leading BOM disables formatting
 * - lines consisting only of "{{{" and "}}}" delimit preformatted
 *   blocks; if a comment contains none of them and no ">>>" it’s
 *   wrapped into a preformatted block if the heuristic (contains
 *   three spaces in a row or a horizontal tab) thinks it is
 * - lines consisting of four hyphen-minus characters, outside of
 *   a preformatted block, are converted to a <hr /> tag
 * - any [#123] [T123] [forum:123] [wiki:Page_Name] are parsed into
 *   links in preformatted and running blocks
 * - running blocks have "<<<kw:" ... ">>>" parsed into wrapping
 *   the in-between content into <kw>...</kw> tags, for a given
 *   set of keywords
 * - running blocks have lines wrapped into non-empty paragraphs
 */

require_once $gfcommon.'include/EvolvisTable.class.php';
require_once $gfcommon.'pm/ProjectTaskSqlQueries.php';

/**
 * showmess() - display messages
 *
 * @param	string	$label
 *			localised label for the block
 * @param	string	$loc
 *			hyperlink of “this” page, without commentsort
 * @param	object	$inst
 *			must provide the getMessages(ascending) method
 *			and the getMessagesKeys() method
 * @param	string	$nomsg
 *			localised label for when no messages
 * @param	bool	$inform
 *		optional(false) inside <form>, allowed to emit hidden fields
 */
function showmess($label, $loc, $inst, $nomsg, $inform=false) {
	$spos = html_ap();
	echo html_ao('div', array('class' => 'showmess'));
	$loc = util_unconvert_htmlspecialchars($loc);
	if (!($t = getStringFromRequest('commentsort'))) {
		$t = getStringFromRequest('commentsort_inherited');
	}
	$commentsort_chrono = $t != 'anti';
	if ($commentsort_chrono) {
		$tgt = $loc . '&commentsort=anti';
		$txt = '▼△ ' . _('Sort comments antichronologically');
	} else {
		$tgt = $loc . '&commentsort=chrono';
		$txt = '▽▲ ' . _('Sort comments chronologically');
	}
	echo html_e('h3', array(
		'style' => array(
			'width:100%;',
			'overflow:hidden;',
			'white-space:nowrap;',
		    ),
	    ), $label . ': ' . html_e('a', array(
		'class' => 'btn',
		'href' => $tgt,
	    ), $txt)) . "\n";
	if ($inform) {
		echo html_e('input', array(
			'type' => 'hidden',
			'name' => 'commentsort_inherited',
			'value' => ($commentsort_chrono ? 'chrono' : 'anti'),
		    ));
	}

	if (!($keys = $inst->getMessagesKeys()) ||
	    !($res = $inst->getMessages($commentsort_chrono)) ||
	    (($rows = db_numrows($res)) < 1)) {
		echo html_e('p', array(), $nomsg) . html_ac($spos) . "\n";
		return;
	}
	echo html_ao('div', array(
		'style' => array(
			'height:30em;',
			'overflow:scroll;',
		    ),
	    ));
	$t = new EvolvisTable(array(false));
	while ($row = db_fetch_array($res)) {
		$uid = $row[$keys['user_id']];
		$uname = $row[$keys['user_name']];
		$rname = $row[$keys['realname']];
		$mdate = $row[$keys['msgdate']];
		$mbody = $row[$keys['msgbody']];
		$mmail = $row[$keys['msgmail']];

		/* I hate FusionForge plugin hooks */
		ob_start();
		plugin_hook("user_logo", array(
			'user_id' => $uid,
			'size' => 's',
		    ));
		$userlogo = ob_get_contents();
		ob_end_clean();

		$firstlines = html_e('tt', array(), _('Date') . ': ' .
		    date(_('Y-m-d H:i'), $mdate) . "<br />\n" .
		    _('From') . ': ');
		if ($uid == 100) {
			$firstlines .= html_e('tt', array(),
			    util_html_secure($mmail));
		} else {
			$firstlines .= util_make_link_u($uname, $uid,
			    util_html_secure($rname)) . ' (' .
			    html_e('tt', array(), $uname) . ')';
		}

		if ($userlogo) {
			$firstlines = html_e('table', array(
				'border' => 0,
				'cellpadding' => 0,
				'cellspacing' => 2,
			    ), html_e('tr', array(),
			    html_e('td', array(), $userlogo) .
			    html_e('td', array(), $firstlines)));
		}

		$t->tr()->td()->setraw($firstlines .
		    showmess_fmt(util_unconvert_htmlspecialchars($mbody)));
	}
	echo $t->emit() . html_ac($spos) . "\n";
}

/**
 * emit_summary() - render item summary as text
 *
 * @param	object	$inst
 *			must provide the getSummary() method
 * @return	string
 *		XHTML encoded item summary text (no tag)
 */
function emit_summary($inst) {
	$txt = util_unconvert_htmlspecialchars($inst->getSummary());
	return util_html_encode($txt);
}

/**
 * emit_summary_field() - render item summary as input field
 *
 * @param	object	$inst (or NULL)
 *			must provide the getSummary() method
 * @param	string	$name
 * @param	string	$id
 * @param	string	$class (optional)
 * @param	string	$title (optional)
 * @return	string
 *		XHTML form field
 */
function emit_summary_field($inst, $name, $id, $class=false, $title=false) {
	$txt = $inst ? util_unconvert_htmlspecialchars($inst->getSummary()) : "";
	return html_e('input', array(
		'type' => 'text',
		'name' => $name,
		'style' => 'width:98%;',
		'id' => $id,
		'title' => $title,
		'class' => $class,
		'value' => $txt,
	    ));
}

/**
 * emit_details() - render item detailed description as text
 *
 * @param	object	$inst
 *			must provide the getDetails() method
 * @param	string	$headline
 *		(optional) headline to show
 * @param	bool	$editable
 *		(optional) editable? (default: false)
 * @return	string
 *		XHTML encoded item details text <div>
 * @todo add formatting
 */
function emit_details($inst, $headline="", $editable=false) {
	$txt = util_unconvert_htmlspecialchars($inst->getDetails());

	$ftx = html_e('div', array(
		'class' => array(
			'item_details',
		    ),
		'style' => array(
			'clear:both;',
			'overflow:scroll;',
		    ),
	    ), showmess_fmt($txt));

	if ($editable) {
		$headline = html_e('div', array(
			'style' => array(
				'width:100%;',
			    ),
		    ), html_e('div', array(
			'style' => array(
				'float:left;',
				'font-weight:bold;',
			    ),
		    ), $headline . ":") . html_e('div', array(
			'style' => array(
				'float:right',
			    ),
		    ), html_image('ic/forum_edit.gif', '37', '15', array(
			'title' => _('Click to edit'),
			'alt' => _('Click to edit'),
			'onclick' => "switch2edit(this, 'show', 'edit');",
		    ))));
	} else if (!$headline) {
		return $ftx;
	} else {
		$headline = html_e('strong', array(), $headline . ":");
	}

	return html_e('div', array(
		'id' => 'show',
		'style' => array(
			'display:block;',
		    ),
	    ), $headline . "\n" . $ftx) . "\n";
}

/**
 * emit_details_field() - render item detailed description as textarea
 *
 * @param	object	$inst (or NULL)
 *			must provide the getSummary() method
 * @param	string	$name
 * @param	string	$id (optional)
 * @param	string	$title (optional)
 * @return	string
 *		XHTML form field
 */
function emit_details_field($inst, $name, $id=false, $title=false) {
	$txt = $inst ? util_unconvert_htmlspecialchars($inst->getDetails()) : "";
	return html_e('textarea', array(
		'class' => array(
			'tt',
		    ),
		'style' => 'width:98%;',
		'rows' => 24,
		'cols' => 60,
		'name' => $name,
		'id' => $id,
		'title' => $title,
	    ), util_html_encode($txt), false);
}

/**
 * emit_comment_box() - render write a comment box
 *
 * @param	string	$formname
 *			DOM name attribute of the form we live in
 * @param	string	$fieldname
 *			DOM name attribute of the field to generate
 * @param	string	$id (optional)
 * @param	string	$title (optional)
 * @return	string
 *		XHTML code to insert into a form
 */
$emit_comment_box_done = 0;
function emit_comment_box($formname, $fieldname, $id=false, $title=false) {
	global $emit_comment_box_done;

	if ($emit_comment_box_done++)
		exit_error('emit_comment_box() called more than once!');

	return html_e('h3', array(), _('Write a comment') . ': ' .
	    notepad_button('document.forms.' . $formname . '.' . $fieldname,
	    "messformat")) . "\n" . html_e('div', array(
	    ), html_e('textarea', array(
		'class' => array(
			'tt',
		    ),
		'style' => 'width:98%;',
		'rows' => 7,
		'cols' => 60,
		'name' => $fieldname,
		'id' => $id,
		'title' => $title,
	    ), "", false) . html_e('div', array(
	    ), html_e('label', array(
		'for' => 'comment_formatting',
	    ), _('Formatting') . ':') . html_e('select', array(
		'id' => 'comment_formatting',
		'name' => 'comment_formatting',
		'size' => 1,
	    ), html_e('option', array(
		'value' => 'formatted',
		'selected' => 'selected',
	    ), _('formatted (default)')) . html_e('option', array(
		'value' => 'pot',
	    ), _('Plain Old Text'))))) . "\n";
}

/**
 * read_comment_box() - parse comment box input fields
 *
 * @param	string	$fieldname
 *			DOM/field name passed to emit_comment_box()
 * @return	string
 *		parsed comment with magic chars added if necessary
 */
function read_comment_box($fieldname) {
	$fld = getStringFromRequest($fieldname);
	$fmt = getStringFromRequest('comment_formatting');

	/* this fails $fld==='0' too, but the rest of the code uses this… */
	if (!$fld)
		return '';

	switch (strtolower($fmt)) {
	case 'pot':
		$fld = '﻿' . $fld;
		break;
	case 'formatted':
	default:
		break;
	}

	return $fld;
}

/**
 * showmess_fmt() - render message body for display
 *
 * @param	string	$mess
 *			already util_unconvert_htmlspecialchars()d text body
 * @return	string
 *		XHTML rendition of the message body
 */
define("SHOWMESS_FMT_INITIAL", 0);
define("SHOWMESS_FMT_WANTPRE", 1);
define("SHOWMESS_FMT_INPRE", 2);
define("SHOWMESS_FMT_INPARA", 3);
function showmess_fmt($mess) {
	global $group_id;

	$state = SHOWMESS_FMT_INITIAL;
	$spos = html_ap();
	$sarr = array();

	/* pcre patterns */
	$p_uri = '([a-zA-Z][a-zA-Z0-9+.-]*):([#0-9a-zA-Z;/?:@&=+$,_.!~*\'()%-]+)';
	$p_bexp = preg_quote('[') . '(?|(#|T|forum:)([1-9][0-9]*)';
	$p_bexp .= ')' . preg_quote(']');
	$p_fbeg = preg_quote('<<<') .
	    /* valid keywords */
	    '(b|i|u|tt|ins|del)' .
	    preg_quote(':');
	$p_fend = preg_quote('>>>');

	/* strip leading/trailing blank lines and UNIXify newlines */
	$mess = trim(str_replace("\r", "", $mess), "\n");
	/* get message as array of lines */
	$marr = explode("\n", $mess);

	/* unformatted? */
	if (substr($mess, 0, 3) === '﻿') {
		$s = "";
		$state = SHOWMESS_FMT_INITIAL;
		foreach ($marr as $t) {
			if ($state == SHOWMESS_FMT_INPARA) {
				if (!$t) {
					$s .= "</p>\n";
					$state = SHOWMESS_FMT_INITIAL;
					continue;
				}
				$s .= "<br />\n";
			} else {
				/* SHOWMESS_FMT_INITIAL */
				if (!$t) {
					/* collapse empty lines */
					continue;
				}
				$s .= "<p>";
				$state = SHOWMESS_FMT_INPARA;
			}
			$s .= util_html_encode($t);
		}
		if ($state == SHOWMESS_FMT_INPARA)
			$s .= "</p>\n";
		return html_e('div', array(
			'class' => array(
				'showmess_fmt',
			    ),
		    ), "\n" . $s);
	}

	/* heuristics {{{ <<< */
	if (strpos($mess, ">>>") === false &&
	    array_search("}}}", $marr) === false) {
		/* no formatting in the text */
		if (strpos($mess, "\t") !== false ||
		    strpos($mess, "   ") !== false) {
			/* but tab or three spaces in a row */
			$state = SHOWMESS_FMT_WANTPRE;
		}
	}

	$s = "";
	/* main parse loop */
	foreach ($marr as $t) {
		if ($state == SHOWMESS_FMT_WANTPRE) {
			$s .= "<pre>";
			$state = SHOWMESS_FMT_INPRE;
		} else if ($state == SHOWMESS_FMT_INPRE && $t != /*{{{*/ "}}}") {
			$s .= "\n";
		}
		if ($state == SHOWMESS_FMT_INPRE) {
			if ($t == /*{{{*/ "}}}") {
				$s .= "</pre>\n";
				$state = SHOWMESS_FMT_INITIAL;
				continue;
			}
			/* fall through to main line formatter */
		} else if ($state == SHOWMESS_FMT_INPARA) {
			if (!$t || $t == '----') {
				$sarr = html_a_copy($spos);
				$s .= html_ac($spos) . "</p>\n";
				if ($t == '----')
					$s .= "<hr />\n";
				$state = SHOWMESS_FMT_INITIAL;
				continue;
			}
			if ($t == "{{{" /*}}}*/) {
				$sarr = array();
				$s .= html_ac($spos) . "</p>\n";
				$state = SHOWMESS_FMT_WANTPRE;
				continue;
			}
			$s .= "<br />\n";
		} else {
			/* SHOWMESS_FMT_INITIAL */
			if (!$t) {
				/* collapse empty lines */
				continue;
			}
			if ($t == '----') {
				$s .= "<hr />\n";
				continue;
			}
			if ($t == "{{{" /*}}}*/) {
				$sarr = array();
				$state = SHOWMESS_FMT_WANTPRE;
				continue;
			}
			$s .= "<p>" . html_a_apply($sarr);
			$state = SHOWMESS_FMT_INPARA;
		}

		/* main text conversion loop, per-line */
		while ($t) {
			$curpos = html_ap();

			/* cobble together PCRE looking for special stuff */
			$mypcre = "\x01(?";
			/* look for bracket-expression */
			$mypcre .= "|" . $p_bexp;
			if ($state != SHOWMESS_FMT_INPRE) {
				/* look for formatting-begin */
				$mypcre .= "|" . $p_fbeg;
				if ($curpos > $spos) {
					/* also look for formatting-end */
					$mypcre .= "|" . $p_fend;
				}
			}
			$mypcre .= ")\x01";

			/* check if, and where, we have special stuff */
			$matches = array();
			$matched = preg_match($mypcre, $t, $matches,
			    PREG_OFFSET_CAPTURE);
			if ($matched === false) {
				throw new Exception("PCRE Error: " .
				    preg_last_error());
			}

			$mtype = 0;
			if ($matched) {
				$os = $matches[0][0];
				$bs = substr($t, 0, $matches[0][1]);
				switch ($os[0]) {
				case '[':
					$mtype = 1;
					break;
				case '<':
					$mtype = 2;
					break;
				case '>':
					$mtype = 3;
					break;
				default:
					throw new Exception("Invalid match: " .
					    $os);
				}
				$t = substr($t, $matches[0][1] + strlen($os));
			} else {
				$os = "";
				$bs = $t;
				$t = "";
			}

			/* handle the prefix (before) string */

			/* wrap URIs in \r for later processing */
			$bs = preg_replace("|".$p_uri."|", "\r\\0\r", $bs);
			if ($bs === NULL) {
				throw new Exception("PCRE Error: " .
				    preg_last_error());
			}

			/* HTML-encode the string */
			$bs = util_html_encode($bs);

			/* convert wrapped URIs into hyperlinks */
			$bs = preg_replace("|\r([^\r]+)\r|",
			    '<a href="$1">$1</a>', $bs);

			/* append the before-the-match part to running string */
			$s .= $bs;

			/* handle the match, if any */

			switch ($mtype) {
			/* bracketed expression */
			case 1:
				$lnk = "";
				$ttip = false;
				$doselect = true;
				switch ($matches[1][0]) {
				case "#":
					if (($r = tasktracker_gettype($matches[2][0]))) {
						/* we know what we need */
						$doselect = false;
						break;
					}
					/* prepare for a database call */
					$sname = "summary";
					$tname = "artifact";
					$iname = "artifact_id";
					$r = array("is_a" => "aid");
					break;

				case "T":
					$sname = "summary";
					$tname = "project_task";
					$iname = "project_task_id";
					$r = array("is_a" => "tid");
					break;

				case "forum:":
					$sname = "subject";
					$tname = "forum";
					$iname = "msg_id";
					$r = array("is_a" => "msgid");
					break;

				default:
					/* error: unknown type */
					$lnk = false;
				}
				if ($lnk === "" && $doselect &&
				    !($mid = (int)$matches[2][0])) {
					/* error: not numeric */
					$lnk = false;
				}
				/* query database if necessary */
				if ($lnk === "" && $doselect) {
					/*
					 * I think we cannot avoid the string
					 * concatenation here because substi-
					 * tution only affects arguments
					 */
					$res = db_query_params("SELECT " .
					    $sname . " FROM " . $tname .
					    " WHERE " . $iname . '=$1;',
					    array($mid));
					if (db_numrows($res) == 1) {
						$row = db_fetch_array($res);
						$r["summary"] = $row[$sname];
					} else {
						$lnk = false;
					}
				}
				/* convert tt/sql result into link */
				if ($lnk === "") {
					switch ($r["is_a"]) {
					case "aid":
						$linkf = "/tracker/t_follow.php/%d";
						$textf = _("%s (Task)");
						break;

					case "tid":
						$linkf = "/pm/t_follow.php/%d";
						$textf = _("%s (Bug/FR)");
						break;

					case "msgid":
						$linkf = "/forum/message.php?msg_id=%d";
						$textf = _("%s (Forum Message)");
						break;

					default:
						/* error: unknown type */
						$lnk = false;
					}
					if ($lnk === "") {
						$lnk = sprintf($linkf,
						    $matches[2][0]);
						$ttip = sprintf($textf,
						    $r["summary"]);
					}
				}
				if (!$lnk) {
					$s .= util_html_encode($os);
				} else {
					$s .= "[" . html_e("a", array(
						"href" => util_make_url($lnk),
						"title" => $ttip,
					    ), util_html_encode($matches[1][0] .
					    $matches[2][0])) . "]";
				}
				break;

			/* formatting begin */
			case 2:
				switch ($matches[1][0]) {
				case 'u':
					$s .= html_ao('span', array(
						'style' => 'text-decoration:underline;',
					    ));
					break;
				default:
					$s .= html_ao($matches[1][0]);
					break;
				}
				break;

			/* formatting end */
			case 3:
				/* bounds checked earlier */
				$s .= html_ac($curpos - 1);
				break;

			/* none */
			case 0:
			default:
				$s .= util_html_encode($os);
				break;
			}

			/* rinse, repeat */
		}
	}
	/* handle closing all elements */
	switch ($state) {
	case SHOWMESS_FMT_INITIAL:
	case SHOWMESS_FMT_WANTPRE:
		break;

	case SHOWMESS_FMT_INPRE:
		$s .= "</pre>\n";
		break;

	case SHOWMESS_FMT_INPARA:
		$s .= html_ac($spos) . "</p>\n";
		break;
	}

	/* return the worm */
	return html_e('div', array(
		'class' => array(
			'showmess_fmt',
		    ),
	    ), "\n" . $s);
}

/**
 * showmess_getcommits() - get array of linked commits
 *
 * @param	int	$id
 *			ID of the task/tracker item
 * @return	array
 *		of tasktracker_scm table rows
 * @deprecated Do not use, will be replaced.
 */
function showmess_getcommits($id) {
	$rv = array();
	$res = db_query_params('
		SELECT * FROM tasktracker_scm
		WHERE id=$1
		ORDER BY group_name, revision
	    ', array($id));
	if ($res) while (($row = db_fetch_array($res))) {
		$rv[] = $row;
	}
	return ($rv);
}
