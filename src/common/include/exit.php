<?php
/**
 * FusionForge : Exit functions
 *
 * Copyright 1999-2001 (c) VA Linux Systems
 * Copyright 2010, Franck Villaume
 * Copyright © 2012
 *	Thorsten Glaser <t.glaser@tarent.de>
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

$forge_exit_handler = false;

/**
 * exit_errorlevel – Return code for exit_error() and friends
 *
 * A script can set this to explicitly return a different
 * status code than the historic default of 0 to the caller.
 */
$exit_errorlevel = 0;

/**
 * exit_error() - Exit PHP with error
 *
 * @param	string	Error text
 * @param	string  toptab for navigation bar
 */
function exit_error($text="", $toptab='') {
	global $HTML, $group_id, $sysdebug_enable, $forge_exit_handler, $exit_errorlevel;

	if ($forge_exit_handler) {
		$forge_exit_handler($text);
		exit;
	}
	$HTML->header(array('title'=>_('Exiting with error'), 'group'=>$group_id, 'toptab'=>$toptab));
	echo $HTML->error_msg(htmlspecialchars($text));
	if ($sysdebug_enable) echo html_e('div', array(
		'id' => 'ffBacktrace',
	    ), str_replace("<br />", '</tt></p><p><tt>', /* suboptimal */
	    util_ttwrap(htmlspecialchars(debug_string_backtrace()))));
	$HTML->footer(array());
	exit($exit_errorlevel);
}

/**
 * exit_permission_denied() - Exit with permission denied error
 *
 * @param	string	$reason_descr
 * @param   string  toptab needed for navigation
 */
function exit_permission_denied($reason_descr='', $toptab='') {
	if(!session_loggedin()) {
		exit_not_logged_in();
	} else {
		if (!$reason_descr) {
			$reason_descr=_('Permission denied. This project\'s administrator will have to grant you permission to view this page.');
		}
		exit_error($reason_descr,$toptab);
	}
}

/**
 * exit_not_logged_in() - Exit with not logged in error
 */
function exit_not_logged_in() {
	global $forge_exit_handler;

	if ($forge_exit_handler) {
		$forge_exit_handler(_('You are not logged in.'));
		exit;
	}
	//instead of a simple error page, now take them to the login page
	session_redirect("/account/login.php?triggered=1&return_to=" .
	    urlencode(getStringFromServer('REQUEST_URI')), false);
}

/**
 * exit_no_group() - Exit with no group chosen error
 * @param	string toptab
 */
function exit_no_group() {
	exit_error(_('Permission denied. No project was chosen, project does not exist or you can\'t access it.'),$toptab='');
}

/**
 * exit_missing_param() - Exit with missing required parameters error
 *
 * @param   string  URL : usually $_SERVER['HTTP_REFERER'] minus forge_get_config('web_host') + forge_get_config('use_ssl')
 * @param   array   array of missing parameters
 * @param   string  toptab needed for navigation
 */
function exit_missing_param($url='',$missing_params=array(),$toptab='') {
	global $forge_exit_handler;

    if (!empty($missing_params)) {
        $error = _('Missing required parameters : ');
        foreach ($missing_params as $missing_param) {
            $error .= $missing_param.' ';
        }
    } else {
        $error = sprintf(_('Missing required parameters.'));
    }
    if (!empty($url) && !$forge_exit_handler) {
        if (strpos($url,'?')) {
            session_redirect($url.'&error_msg='.urlencode($error));
        }
        session_redirect($url.'?error_msg='.urlencode($error));
    } else {
	    exit_error($error,$toptab);
    }
}

/**
 * exit_disabled() - Exit with disabled feature error.
 *
 * @param   string  toptab needed for navigation
 */
function exit_disabled($toptab='summary') {
	exit_error(_('The Site Administrator has turned off this feature.'),$toptab);
}

/**
 * exit_form_double_submit() - Exit with double submit error.
 *
 * @param   string  toptab needed for navigation
 */
function exit_form_double_submit($toptab='') {
	exit_error(_('You Attempted To Double-submit this item. Please avoid double-clicking.'),$toptab);
}

// Local Variables:
// mode: php
// c-file-style: "bsd"
// End:
