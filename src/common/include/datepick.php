<?php
/**
 * Date operations for FusionForge
 *
 * Copyright © 2010, 2011, 2014
 *	Thorsten “mirabilos” Glaser <t.glaser@tarent.de>
 * All rights reserved.
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *-
 * API documentation:
 *
 * datepick_prepare(); // before emitting the Theme header
 * datepick_emit('fieldname'[, 'value', usetime?, 'type', attrs(), doecho?]);
 * // type: default (prefs), locdef (locale), or a specific one
 * $timet = datepick_parse(getStringFromRequest('fieldname'));
 * $value = datepick_format($timet[, usetime?, 'type']);
 * // these two convert back and forth between time_t and string
 * datepick_weekstart_monday('default') (false american, true all others)
 */

/*-
 * list of acceptable datepicker locales;
 * must be defined in gforge/www/js/datepicker.js
 * datepick_weekstart_monday() must match
 */
$datepick_locales = array(
	'locdef' => _('Locale dependent default'),
	'american' => _('American (m/d/y, English labels, 12 hour clock, week starts on Sunday)'),
	'dmy' => _('Day.Month.Year with English labels'),
	'mdy' => _('Month/Day/Year with English labels'),
	'ymd' => _('Year-Month-Day with English labels'),
	'dmy_es' => _('Day.Month.Year with Spanish labels'),
	'mdy_es' => _('Month/Day/Year with Spanish labels'),
	'ymd_es' => _('Year-Month-Day with Spanish labels'),
	'dmy_de' => _('Day.Month.Year with German labels'),
	'mdy_de' => _('Month/Day/Year with German labels'),
	'ymd_de' => _('Year-Month-Day with German labels'),
	'dmy_fr' => _('Day.Month.Year with French labels'),
	'mdy_fr' => _('Month/Day/Year with French labels'),
	'ymd_fr' => _('Year-Month-Day with French labels'),
	'dmy_it' => _('Day.Month.Year with Italian labels'),
	'mdy_it' => _('Month/Day/Year with Italian labels'),
	'ymd_it' => _('Year-Month-Day with Italian labels'),
	'dmy_nl' => _('Day.Month.Year with Dutch labels'),
	'mdy_nl' => _('Month/Day/Year with Dutch labels'),
	'ymd_nl' => _('Year-Month-Day with Dutch labels'),
	'dmy_pl' => _('Day.Month.Year with Polish labels'),
	'mdy_pl' => _('Month/Day/Year with Polish labels'),
	'ymd_pl' => _('Year-Month-Day with Polish labels'),
	'dmy_pt' => _('Day.Month.Year with Portuguese labels'),
	'mdy_pt' => _('Month/Day/Year with Portuguese labels'),
	'ymd_pt' => _('Year-Month-Day with Portuguese labels'),
	'dmy_ro' => _('Day.Month.Year with Romanian labels'),
	'mdy_ro' => _('Month/Day/Year with Romanian labels'),
	'ymd_ro' => _('Year-Month-Day with Romanian labels'),
	'dmy_bg' => _('Day.Month.Year with Bulgarian labels'),
	'mdy_bg' => _('Month/Day/Year with Bulgarian labels'),
	'ymd_bg' => _('Year-Month-Day with Bulgarian labels'),
	'dmy_ru' => _('Day.Month.Year with Russian labels'),
	'mdy_ru' => _('Month/Day/Year with Russian labels'),
	'ymd_ru' => _('Year-Month-Day with Russian labels'),
	'dmy_hu' => _('Day.Month.Year with Hungarian labels'),
	'mdy_hu' => _('Month/Day/Year with Hungarian labels'),
	'ymd_hu' => _('Year-Month-Day with Hungarian labels'),
	'dmy_no' => _('Day.Month.Year with Norwegian labels'),
	'mdy_no' => _('Month/Day/Year with Norwegian labels'),
	'ymd_no' => _('Year-Month-Day with Norwegian labels'),
    );

/* must match definitions in datepicker.js for reverse */
$datepick_locales_fmtstr = array(
	'american' => '%m/%d/%Y',
	'dmy' => '%d.%m.%Y',
	'mdy' => '%m/%d/%Y',
	'ymd' => '%Y-%m-%d',
	'dmy_es' => '%d.%m.%Y',
	'mdy_es' => '%m/%d/%Y',
	'ymd_es' => '%Y-%m-%d',
	'dmy_de' => '%d.%m.%Y',
	'mdy_de' => '%m/%d/%Y',
	'ymd_de' => '%Y-%m-%d',
	'dmy_fr' => '%d.%m.%Y',
	'mdy_fr' => '%m/%d/%Y',
	'ymd_fr' => '%Y-%m-%d',
	'dmy_it' => '%d.%m.%Y',
	'mdy_it' => '%m/%d/%Y',
	'ymd_it' => '%Y-%m-%d',
	'dmy_nl' => '%d.%m.%Y',
	'mdy_nl' => '%m/%d/%Y',
	'ymd_nl' => '%Y-%m-%d',
	'dmy_pl' => '%d.%m.%Y',
	'mdy_pl' => '%m/%d/%Y',
	'ymd_pl' => '%Y-%m-%d',
	'dmy_pt' => '%d.%m.%Y',
	'mdy_pt' => '%m/%d/%Y',
	'ymd_pt' => '%Y-%m-%d',
	'dmy_ro' => '%d.%m.%Y',
	'mdy_ro' => '%m/%d/%Y',
	'ymd_ro' => '%Y-%m-%d',
	'dmy_bg' => '%d.%m.%Y',
	'mdy_bg' => '%m/%d/%Y',
	'ymd_bg' => '%Y-%m-%d',
	'dmy_ru' => '%d.%m.%Y',
	'mdy_ru' => '%m/%d/%Y',
	'ymd_ru' => '%Y-%m-%d',
	'dmy_hu' => '%d.%m.%Y',
	'mdy_hu' => '%m/%d/%Y',
	'ymd_hu' => '%Y-%m-%d',
	'dmy_no' => '%d.%m.%Y',
	'mdy_no' => '%m/%d/%Y',
	'ymd_no' => '%Y-%m-%d',
    );


/* emit appropriate header lines for using DatePicker */
$datepick__prepared = false;
function datepick_prepare() {
	global $HTML, $datepick__prepared;

	if ($datepick__prepared)
		return;
	$datepick__prepared = true;

	$HTML->addJavascriptEx('/js/datepicker.js', false);

	$HTML->footer_hooks[] = 'datepick__footer_hook';
}
$datepick_used = array();

/* with the given type (or 'default' or 'locdef'), when does the week start? */
function datepick_weekstart_monday($type='default') {
	$type = datepick__type($type);

	if ($type == 'american') {
		return false;
	}
	return true;
}

/* emit a datepick input field */
function datepick_emit($fieldname, $fieldvalue='', $usetime=false,
    $type='default', $attrs=array(), $doecho=true) {
	/* 'default' locale: look at user prefs, fallback to 'locdef' */
	/* 'locdef' locale: look at gettext, fallback to 'american' */
	$type = datepick__type($type);

	$attrs['class'] = trim(util_ifsetor($attrs['class'], "") . ' ' .
	    ($usetime ? 'datetime_' : 'date_') . $type);

	$attrs['type'] = 'text';
	$attrs['name'] = $fieldname;
	$attrs['value'] = $fieldvalue;
	$rv = html_e('input', $attrs);

	global $datepick_used;
	$datepick_used[($usetime ? 'datetime_' : 'date_') . $type] =
	    array('type' => $type, 'usetime' => $usetime);

	if ($doecho)
		echo $rv;
	return $rv;
}

/* parse y-m-d [h:m] or d.m.y [h:m] or m/d/y [h:m] format into time_t (int) */
function datepick_parse($text) {
	$text = trim($text);
	$hmin = "";

	if (preg_match('#^[0-9/.-]{5,10} [0-9]{1,2}:[0-9]{1,2}$#', $text)) {
		$a = explode(' ', $text);
		$text = trim($a[0]);
		$hmin = " " . trim($a[1]);
	}

	if (preg_match('#^[0-9]{1,4}-[0-9]{1,2}-[0-9]{1,2}$#', $text)) {
		/* ISO 8601 */
		$a = explode('-', $text);
		$y = $a[0];
		$m = $a[1];
		$d = $a[2];
	} else if (preg_match('#^[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{1,4}$#', $text)) {
		/* German */
		$a = explode('.', $text);
		$y = $a[2];
		$m = $a[1];
		$d = $a[0];
	} else if (preg_match('#^[0-9]{1,2}/[0-9]{1,2}/[0-9]{1,4}$#', $text)) {
		/* American */
		$a = explode('/', $text);
		$y = $a[2];
		$m = $a[0];
		$d = $a[1];
	} else
		return false;
	return (strtotime($y."-".$m."-".$d.$hmin));
}

/* convert time_t into value suitable for emitting a datepicker */
function datepick_format($timet, $usetime=false, $type='default') {
	global $datepick_locales_fmtstr;

	$fmtstr = $datepick_locales_fmtstr[datepick__type($type)];
	if ($usetime)
		$fmtstr .= ' %H:%M';
	return ($timet ? strftime($fmtstr, $timet) : '');
}


/* internal function */
function datepick__footer_hook($params, $T) {
	global $datepick_used;

	if (!$datepick_used)
		return;

	echo '
<script type="text/javascript"><!--//--><![CDATA[//><!--
	function createPickers() {';

	foreach ($datepick_used as $k => $v) {
		echo '
		$(document.body).select(' . "'input." . $k . "').each(
			function(e) {
				new Control.DatePicker(e, {
					'locale': '" . $v['type'] . "'";
		if ($v['usetime']) {
			echo ",
					'timePickerAdjacent': true,
					'timePicker': true";
		}
		echo ",
					'icon': '/images/office-calendar.png'
				    });
			}
		    );";
	}
	echo '
	}
	Event.observe(window, "load", createPickers);
//--><!]]></script>';
}

/* internal function: convert $type to JS type */
function datepick__type($type='default') {
	global $datepick_locales, $G_SESSION;

	if ($type == 'default' && isset($G_SESSION) && $G_SESSION) {
		$v = $G_SESSION->getPreference('datepick_format');
		if (isset($v))
			$type = $v;
	}
	if (!isset($datepick_locales[$type])) {
		$type = 'locdef';
	}

	if ($type == 'locdef') {
		$type = _('locale-dependent-default-datepick-format');
	}
	if (!isset($datepick_locales[$type]) || $type == 'locdef') {
		$type = 'american';
	}

	return ($type);
}
