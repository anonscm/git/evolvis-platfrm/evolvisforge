<?php
/*-
 * JpGraph support code for FusionForge
 *
 * Copyright © 2012
 *	Thorsten Glaser <t.glaser@tarent.de>
 * All rights reserved.
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *-
 * Set of helper functions, wrappers and initialisation code for using
 * JpGraph within FusionForge.
 */

/* ensure exit_* functions are defined and overridable */
require_once $gfcommon.'include/exit.php';

/* ensure JpGraph is installed and loadable */
if (!file_exists(forge_get_config('jpgraph_path').'/jpgraph.php')) {
	exit_error(_('Package JPGraph not installed'));
}
$orig_er = error_reporting(1); // we are not responsible for errors here
require_once(forge_get_config('jpgraph_path').'/jpgraph.php');
error_reporting($orig_er);

/* we need the forge software name */
require_once $gfcommon.'include/FusionForge.class.php';

/* error formatter producing an image */
$forge_jpgraph_error_fmt_called = false;
function forge_jpgraph_error_fmt($text) {
	global $forge_jpgraph_error_fmt_called;

	$w = 640; $h = 480;

	if ($forge_jpgraph_error_fmt_called || headers_sent()) {
		sysdebug_off();
		$complain = $forge_jpgraph_error_fmt_called ?
		    _('Cannot display error picture: called recursively! Backtrace:') :
		    _('Cannot display error picture, headers already sent! Backtrace:');
		$complain = trim($complain . "\n" . debug_string_backtrace());
		$pfx = "";
		foreach (explode("\n", $complain) as $line) {
			error_log($pfx . $line);
			$pfx = ">>> ";
		}
		echo util_html_encode($complain) . "\n";
		exit;
	}
	$forge_jpgraph_error_fmt_called = true;

	$ff = new FusionForge();
	$i = new Image($w, $h);
	$i->Rectangle(0, 0, $w - 1, $h - 1);
	$i->SetFont(FF_FONT1, FS_BOLD);
	$i->SetColor("darkred");
	$i->StrokeText(12, 12, sprintf(_("%s Error:"), $ff->software_name),
	    0, "left");
	$i->SetFont(FF_FONT1, FS_NORMAL);
	$i->SetColor("black");
	$t = new Text(wordwrap($text, 80), 12, 24);
	$t->Align("left", "top");
	$t->Stroke($i);
	sysdebug_off();
	$i->Headers();
	$i->Stream();
	exit;
}

/* image error handler for FusionForge */
function forge_jpgraph_error($text) {
	$pfx = _('A Forge error occured; maybe you’re not logged in or lack permissions.');
	forge_jpgraph_error_fmt($pfx . "\n" . $text);
}

/* image error handler for JpGraph */
class JpGraphErrObjectForge extends JpGraphErrObject {
	function __construct() {
		parent::__construct();
	}
	function Raise($text, $isfatal=true) {
		if ($isfatal) {
			$pfx = _('A fatal JpGraph error occured; maybe your query did not yield any data points.');
		} else {
			$pfx = _('A non-fatal JpGraph error occured; we’re nevertheless halting execution.');
		}
		forge_jpgraph_error_fmt($pfx . "\n" .
		    preg_replace('/\s*\n\t\s*/', ' ', $text));
	}
}

/* install the JpGraph error handler */
error_reporting($orig_er & ~8192);
JpGraphError::Install('JpGraphErrObjectForge');
error_reporting($orig_er);


/* now install the Forge exit handler */
$forge_exit_handler = 'forge_jpgraph_error';

/* and finally, disable debugging */
sysdebug_off();
