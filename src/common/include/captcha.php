<?php
/**
 * CAPTCHA support for FusionForge
 *
 * Copyright © 2010, 2013
 *	Thorsten “mirabilos” Glaser <t.glaser@tarent.de>
 * All rights reserved.
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *-
 * This is a sample implementation of the FusionForge CAPTCHA API.
 */


/**
 *	captcha_get_label() - Retrieve input label for CAPTCHA
 *
 * @return some XHTML compliant string (translated if possible)
 *
 */
function captcha_get_label() {
	return _("Confirm that you’re a human:");
}

/**
 *	captcha_get_input() - Retrieve input form for CAPTCHA
 *
 * @return some XHTML compliant string (form field)
 *
 */
function captcha_get_input() {
	$s = _("Type the eight (8) digits, ‘+’, ‘/’ or uppercase/lowercase letters below.")."\n";

	$b = util_randbytes();
	$h = sprintf("x%08X", crc32($b) ^ crc32(forge_get_config('host_uuid')));
	$b = base64_encode($b);

	if (strlen($b) != 8) {
		return "captcha.php: " . _("Internal Error:") . " " .
		    _("Could not read from /dev/urandom");
	}

	// maybe store this server-side along the form_key?
	$s .= '<input type="hidden" name="captcha_hash" value="' . $h . '" />';

	$f = popen("toilet -f evolvis-captcha -w 100 -S " . $b, "r");
	$t = stream_get_contents($f);
	pclose($f);

	if (strlen($t) < /* guesstimate */ 50) {
		return "captcha.php: " . _("Internal Error:") . " " .
		    _("Could not call TOIlet");
	}

	$s .= "<pre>\n" . htmlspecialchars($t) . "</pre>\nCode: ";
	$s .= '<input type="text" name="captcha_text" size="10" />';

	return $s;
}

/**
 *	captcha_do_check() - Validate CAPTCHA in form submission
 *
 * @return true if passed, false otherwise
 *
 */
function captcha_do_check() {
	$ch = getStringFromRequest("captcha_hash");
	$ct = getStringFromRequest("captcha_text");

	if (strlen($ch) != 9 || strlen($ct) != 8) {
		return false;
	}

	$b = base64_decode($ct);

	if ($b === false || strlen($b) != 6) {
		return false;
	}

	$h = sprintf("x%08X", crc32($b) ^ crc32(forge_get_config('host_uuid')));
	if ($h === $ch) {
		return true;
	}
	return false;
}

?>
