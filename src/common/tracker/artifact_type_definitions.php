<?php
/**
 * FusionForge trackers: template trackers for Evolvis
 *
 * Copyright 2005, GForge, LLC
 * Copyright © 2011, 2012, 2013, 2014
 *	Thorsten Glaser <t.glaser@tarent.de>
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

//
//	Here is where you define different sets of default elements
//

$oss = array(
	'All',
	/* Desktop Microsoft */
	'Windows 3.1',
	'Windows 95',
	'Windows 98',
	'Windows ME',
	'Windows 2000',
	'Windows NT',
	'Windows XP',
	'Windows Server 2003',
	'Windows Vista / Server 2008',
	'Windows 7 / Server 2008R2',
	'Windows 8 / Server 2012',
	'Windows 8.1 / Server 2012R2',
	'Windows (other)',
	/* Unix for Windows */
	'Cygwin',
	'Interix/SFU/SUA',
	'MinGW32',
	'PW32',
	'AT&T UWIN',
	/* Free WinAPI */
	'ReactOS',

	/* Desktop Apple */
	'Mac System 7',
	'Mac System 7.5',
	'Mac System 7.6.1',
	'Mac System 8.0',
	'Mac System 8.5',
	'Mac System 8.6',
	'Mac System 9.x',
	'MacOS X',
	'NeXTstep',
	'Rhapsody',

	/* Mostly Unix */
	'Linux',
	'ÆrieBSD',
	'BSDi BSD/OS',
	'DragonFly BSD',
	'FreeBSD',
	'MidnightBSD',
	'MirBSD',
	'NetBSD',
	'OpenBSD',

	/* Old or not quite Unix */
	'AIX',
	'DG/UX',
	'HP-UX',
	'Hurd',
	'IRIX',
	'Minix ≠ 3',
	'Minix 3 (with Minix userland)',
	'Minix 3 (with NetBSD userland)',
	'SCO OpenServer',
	'SCO UnixWare',
	'Solaris',
	'SunOS',
	'Tru64 (OSF/1)',

	/* Less Unix */
	'BeOS',
	'Coherent',
	'Haiku',
	'Inferno',
	'Plan 9',
	'Syllable Desktop',
	'Xinu',

	/* Not Unix but of interest */
	'Midori (MS Research)',
	'Novell Netware/OES',
	'Singularity (MS Research)',
	'OpenVMS',
	'OS/2',

	/* Mobile */
	'Android 2.x',
	'Android 4.x',
	'Apple Newton OS',
	'EPOC32 (Psion)',
	'Maemo',
	'MeeGo',
	'Mer',
	'Symbian',
	'Tizen',
	'Windows CE',
	'Windows Mobile',
	'Windows Phone 7',
	'Windows Phone 8',
	'Apple iOS (iPhoneOS)',

	/* Embedded or Real-Time */
	'brickOS',
	'leJOS',
	'LEGO® NXT Firmware',
	'*WRT/ADK (Embedded Linux)',
	'µClinux',
	'A/ROSE',
	'eCos',
	'Contiki',
	'LynxOS',
	'MenuetOS',
	'PikeOS',
	'QNX/Neutrino',
	'RTEMS',
	'VxWorks',
	'Xbox 360 system software',

	/* DOS */
	'DR DOS / Caldera/Novell',
	'FreeDOS',
	'MS-DOS',
	'PTS-DOS',
	'DOSplus',
	'DOS with GEM',
	'DR Multiuser DOS',
	'FlexOS',
	'Concurrent PC DOS',

	/* Old Unix */
	'IBM AOS',
	'A/UX',
	'Amix (Amiga Unix)',
	'BS2000/OSD',
	'Dell UNIX (SVR4)',
	'DomainOS / Aegis',
	'Dynix',
	'Interactive Unix',
	'Acorn RISC iX',
	'SINIX',
	'System V/AT',
	'DEC ULTRIX',
	'Xenix',

	/* Obscure */
	'Cisco IOS',
	'JunOS',
	'Oberon',
	'Symbolics',
	'Acorn RISC OS',
	'AmigaOS (Classic)',
	'AmigaOS 4',
	'MorphOS',
	'Atari TOS',
	'Atari MultiTOS',
	'FreeMiNT',
	'Atari GEM',
	'DEC/HP NonStop',
	'HP MPE, MPE/XL, MPE/iX',
	'Multics',
	'TOPS-10',
	'TOPS-20',
	'TENEX',
	'CTSS',
	'OS/360',
	'OS/390',
	'z/OS',
	'OS/400',
	'Amoeba',
	'SkyOS',
	'GEOS/Geoworks Ensemble',
	'CP/M',
	'CP/M-68K',
	'CP/M-86',
	'Concurrent CP/M',
	'Concurrent CP/M-68K',
	'Concurrent CP/M-86',
	'MP/M',
	'MP/M-86',
	'TRS-DOS (Tandy)',
	'NewDos/80 (for Tandy)',
	'TSX-32',
	'L4/Genode',
	'Noux (Genode)',
	'Apple DOS',
	'Apple Pascal',
	'Atari DOS',

	'other',
);

$severities = array(
	'enhancement',
	'trivial',
	'minor',
	'normal',
	'major',
	'critical',
	'blocker',
);

$statuses = array(
	array('New', 1),
	array('Accepted', 1),
	array('Done', 1),
	array('Verified', 2),
	array('Awaiting Response', 1),	/* should be PENDING not OPEN */
	array('Duplicate', 3),
	array('Invalid', 2),
	array('Reopened', 1),
	array('Won’t Fix', 2),
);


//
//	Here is where you combine the arrays of elements into
//	field definitions, including titles, types, and attributes
//

// NAME, TYPE, ATTR1, ATTR2, REQUIRED, SOURCE_ARRAY

$os = array('Operating System', ARTIFACT_EXTRAFIELDTYPE_SELECT, 0, 0, 0, $oss);
$url = array('URL', ARTIFACT_EXTRAFIELDTYPE_TEXT, 40, 100, 0, array());
$component = array('Component', ARTIFACT_EXTRAFIELDTYPE_TEXT, 40, 127, 0, array());
$browser = array('Browser', ARTIFACT_EXTRAFIELDTYPE_TEXT, 40, 127, 0, array());
$verfound = array('Version Found', ARTIFACT_EXTRAFIELDTYPE_TEXT, 40, 64, 0, array());
$verfixed = array('Version Fixed', ARTIFACT_EXTRAFIELDTYPE_TEXT, 40, 64, 0, array());
$status = array('Status', ARTIFACT_EXTRAFIELDTYPE_STATUS, 0, 0, 1, $statuses);
$severity = array('Severity', ARTIFACT_EXTRAFIELDTYPE_SELECT, 0, 0, 0, $severities);
$relitem = array('Linked Items', ARTIFACT_EXTRAFIELDTYPE_RELATION, 40, 255, 0, array());

//
//	Here is where you define which trackers to create
//	Note that you can define as many as you want
//

/*
 * NAME, DESCRIPTION,
 * $is_public, $allow_anon, $email_all, $email_address,
 * $due_period, (unused)0, $submit_instructions, $browse_instructions,
 * $datatype=0, $fields
 */

$trackers[] = array('Bugs', 'Bug Tracking System',
    1, 0, 1, 'UNIXNAME-discuss@' . forge_get_config('lists_host'),
    30, 0, '', '', 1, array(
	$status,
	$severity,
	$os,
	$browser,
	$component,
	$url,
	$verfound,
	$verfixed,
	$relitem,
    ));

$trackers[] = array('Feature Requests', 'Feature Request Tracking System',
    1, 0, 1, 'UNIXNAME-discuss@' . forge_get_config('lists_host'),
    30, 0, '', '', 4, array(
	$status,
	$component,
	$url,
	$verfixed,
	$relitem,
    ));

$trackers[] = array('Tasks', 'Aufgabentracker',
    0, 0, 1, 'UNIXNAME-discuss@' . forge_get_config('lists_host'),
    30, 0, '', '', 6, array(
	$status,
	array('Percent complete', ARTIFACT_EXTRAFIELDTYPE_SELECT, 0, 0, 0, array(
		'5%',
		'10%',
		'15%',
		'20%',
		'25%',
		'30%',
		'35%',
		'40%',
		'45%',
		'50%',
		'55%',
		'60%',
		'65%',
		'70%',
		'75%',
		'80%',
		'85%',
		'90%',
		'95%',
		'100%',
	    )),
	array('Start Date', ARTIFACT_EXTRAFIELDTYPE_DATETIME, 1, 0, 1, array()),
	array('End Date', ARTIFACT_EXTRAFIELDTYPE_DATETIME, 1, 1, 1, array()),
	$relitem,
    ));

// Local Variables:
// mode: php
// c-file-style: "bsd"
// End:
