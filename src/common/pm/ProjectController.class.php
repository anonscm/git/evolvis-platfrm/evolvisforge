<?php
/**
 * FusionForge : Project Management Facility
 *
 * Copyright 1999/2000, Sourceforge.net Tim Perdue
 * Copyright 2002 GForge, LLC, Tim Perdue
 * Copyright 2010, FusionForge Team
 * Copyright (C) 2011 Alain Peyrat - Alcatel-Lucent
 * Copyright (C) 2011 Patrick Apel - tarent solutions GmbH
 * Copyright © 2011
 *	Thorsten Glaser <t.glaser@tarent.de>
 * http://fusionforge.org
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

require_once $gfwww.'pm/include/ProjectView.class.php';
require_once $gfcommon.'pm/ProjectModel.class.php';

if (getStringFromRequest('commentsort') == 'anti') {
	$sort_comments_chronologically = false;
} else {
	$sort_comments_chronologically = true;
}

$report = new Report();
if ($report->isError()) {
	exit_error('Error: ' . $report->getErrorMessage());
}

$week = getStringFromRequest('week');
$project_task_id = getStringFromRequest('project_task_id');

if (getStringFromRequest('submit')) {
	$time_code = getStringFromRequest('time_code');
	$hours = getStringFromRequest('hours');

	if (getStringFromRequest('add')) {
		$report_dt = datepick_parse(getStringFromRequest('report_dt'));

		if ($project_task_id && $report_dt !== false && $time_code && $hours) {
			$res = db_query_params('INSERT INTO rep_time_tracking
				(user_id,week,report_date,project_task_id,time_code,hours)
				VALUES ($1,$2,$3,$4,$5,$6)', array(
			    user_getid(), 0, $report_dt + (12 * 3600),
			    $project_task_id, $time_code, $hours));
			if (!$res || db_affected_rows($res) < 1) {
				exit_error('DB Error: ' . db_error());
			} else {
				$feedback .= '<br />' . _('Successfully Added');
			}
		} else {
			$error_msg = _('All Fields Are Required.');
		}
	}
}

/*
 * This file gets includes from pm_task.php. The pm_tasks.php constructs
 * the object ProjectController and can overgive the variables created
 * in this file ProjectController.class.php to the ProjectController
 * object
 */

class ProjectController{

	private $url;

	private $pageView;

	private $projectGroupHTML;

	private $projectTaskHTML; //extends class ProjectTask

	private $projectModel;

	private $boolManager = false;

	private $templateFile = 'detail_template';

	private $HTML;

	private $cSort;

	private function setUri($uri) {
		$this->uri = $uri;
	}

	public function getUri() {
		return $this->uri;
	}

	private function setPageView($pageView) {
		$this->pageView = $pageView;
	}

	public function getPageView() {
		return $this->pageView;
	}

	private function setProjectGroupHTML($pg) {
		$this->projectGroupHTML = $pg;
	}

	public function getProjectGroupHTML() {
		return $this->projectGroupHTML;
	}

	private function setprojectTaskHTML($pt) {
		$this->projectTaskHTML = $pt;
	}

	public function getProjectTaskHTML() {
		return $this->projectTaskHTML;
	}

	private function setProjectModel($pmModel) {
		$this->projectModel = $pmModel;
	}

	public function getProjectModel() {
		return $this->projectModel;
	}

	private function setManager($boolManager) {
		$this->boolManager = $boolManager;
	}

	public function isManager() {
		return $this->boolManager;
	}

	private function setHTML($HTML) {
		$this->HTML = $HTML;
	}

	public function getHTML() {
		return $this->HTML;
	}

	public function setCSort($cSort) {
		$this->cSort = $cSort;
	}

	public function getCSort() {
		return $this->cSort;
	}

	private function setRelatedArtifactId($related_artifact_id) {
		$this->relatedArtifactId = $related_artifact_id;
	}

	private function getRelatedArtifactId() {
		return $this->relatedArtifactId;
	}

	private function setRelatedArtifactSummary($relatedArtifactSummary) {
		$this->relatedArtifactSummary = $relatedArtifactSummary;
	}

	private function getRelatedArtifactSummary() {
		return $this->relatedArtifactSummary;
	}

	function __construct($uri, $pg, $pt, $pageView, $boolManager, $HTML, $cSort, $related_artifact_id, $related_artifact_summary) {
		$this->setUri($uri);
		$this->setProjectGroupHTML($pg);
		$this->setprojectTaskHTML($pt);
		$this->setPageView($pageView);
		$this->setManager($boolManager);
		$this->setHTML($HTML);
		$this->setCSort($cSort);

		$this->setRelatedArtifactId($related_artifact_id);
		$this->setRelatedArtifactSummary($related_artifact_summary);
	}

	function display() {

		$pmModel = new ProjectModel($this->getProjectTaskHTML());

		$pmView = new ProjectView($this->getUri(), $this->getProjectGroupHTML(),
		    $this->getProjectTaskHTML(), $this->getPageView(), $this->isManager(),
		    $this->getHTML(), $pmModel, $this->getCSort(), $this->getRelatedArtifactId(),
		    $this->getRelatedArtifactSummary());

		$arrInfo = $pmModel->getRelatedCommits();
		$pmView->assign('commits', $arrInfo);

		switch ($this->getPageView()) {
		case 'postmodtask':
			$pmView->setTemplateFile('detail_template');
			break;
		case 'detailtask':
			$pmView->setTemplateFile('detail_template');
			break;
		case 'postaddtask':
			$pmView->setTemplateFile('detail_template');
			break;
		case 'copytask':
			$pmView->setTemplateFile('detail_template');
			break;
		default:
			$pmView->setTemplateFile('detail_template');
			break;
		}

		return $pmView->loadTemplate();
	}
}
