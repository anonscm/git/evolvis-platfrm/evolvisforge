<?php
/*-
 * FusionForge function bundle for PM and Artefact related queries
 *
 * Copyright © 2010, 2011
 *	Thorsten “mirabilos” Glaser <t.glaser@tarent.de>
 *	Patrick Apel <p.apel@tarent.de>
 * All rights reserved.
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *-
 * Retrieve extra information about task items.
 */


/* -+- generic functions -+- */


/* get minimum safe task/tracker ID for which to assume uniqueness */
function tasktracker_getminid() {
	global $tasktracker_minid;

	if (isset($tasktracker_minid))
		return $tasktracker_minid;

	$res = db_query('SELECT get_tasktracker_minid()');
	if (!$res || db_numrows($res) != 1 || !($arr = db_fetch_array($res))) {
		exit_error(_('Internal Error: Cannot read out tasktracker_seq SEQUENCE'));
	}

	$tasktracker_minid = $arr[0];
	return $tasktracker_minid;
}


/* find out if id is an aid or a tid (quick, one SELECT) */
function tasktracker_gettype($id) {
	if ($id < tasktracker_getminid())
		/* neither */
		return false;

	$res = db_query_params('SELECT summary, is_a, subproject_is_public, group_is_public
	    FROM tasktracker_ids
	    WHERE id=$1', array($id));

	if (!$res || db_numrows($res) != 1 || !($arr = db_fetch_array($res)))
		return false;

	$arrResult = array(
	    'id' => (int)$id,
	    'is_a' => $arr[1]
	);
	$arrResult['summary'] = ($arr[2] == 1 && $arr[3] == 1) ?
	    $arr[0] : _('<Information not public>');

	return $arrResult;
}


/* find out if id is an aid or a tid (verbose, two SELECTs) */
/* set defaultto to 1 (tracker) or 2 (task) if you KNOW it… */
function tasktracker_getinfo($id,$getsummary=false,$defaultto=0) {
	if ($id < tasktracker_getminid()) {
		/* neither */
		if ($defaultto == 1)
			return (getArtefactTrackerIdGroupId($id, $getsummary));
		else if ($defaultto == 2)
			return (getGroupProjectIdGroupId($id, $getsummary));
		return false;
	}

	if (($res = getGroupProjectIdGroupId($id,$getsummary)) !== false)
		/* tid */
		return $res;

	if (($res = getArtefactTrackerIdGroupId($id,$getsummary)) !== false)
		/* aid */
		return $res;

	/* unknown */
	return false;
}


/* -+- Tasks -+- PM -+- */


/**
 * Retrieve group_project_id and group_id for a specific project_task_id,
 * for URI construction and similar things.
 *
 * in:	int project_task_id
 * out:	false, or an associative array with
 *	- int project_task_id (copy)
 *	- int group_project_id
 *	- int group_id
 */
function getGroupProjectIdGroupId($project_task_id,$getsummary=false) {
	$res = db_query_params('SELECT project_task.group_project_id, project_group_list.group_id, project_task.summary FROM project_task ' .
	    'INNER JOIN project_group_list ON project_task.group_project_id = project_group_list.group_project_id ' .
	    'WHERE project_task.project_task_id = $1',
	    array($project_task_id));

	if (!$res || db_numrows($res) != 1 || !($arr = db_fetch_array($res))) {
		return false;
	}

	$arrResult = array(
	    'project_task_id' => (int)$project_task_id,
	    'group_project_id' => (int)$arr[0],
	    'group_id' => (int)$arr[1],
	    'is_a' => 'tid'
	);
	if ($getsummary)
		$arrResult['summary'] =
		    (isProjectTaskInfoPublic($project_task_id) ||
		    forge_check_global_perm('forge_admin')) ?
		    $arr[2] : _('<Information not public>');

	return $arrResult;
}


/**
 * Check if the task behind project_task_id is considered public.
 *
 * in:	int project_task_id
 * out:	true, if it is; false otherwise
 */
function isProjectTaskInfoPublic($project_task_id) {
	$res = db_query_params('SELECT group_project_id FROM project_task WHERE project_task_id=$1',
			       array ($project_task_id)) ;

	if (!$res || db_numrows($res) < 1) {
		return false;
	}

	return RoleAnonymous::getInstance()->hasPermission('pm',
							   db_result ($res, 0, 'group_project_id'),
							   'read') ;
}


/**
 * Check whether the user has access to the project task by
 * means of common group membership.
 *
 * in:	int project_task_id
 *	str user_name (Unix user name)
 * out:	true, if he has; false otherwise
 */
function isUserAndTaskinSameGroup($project_task_id, $user_name) {
	$res = db_query_params('SELECT group_project_id FROM project_task WHERE project_task_id=$1',
			       array ($project_task_id)) ;

	if (!$res || db_numrows($res) < 1 || !($arr = db_fetch_array($res))) {
		return false;
	}

	return forge_check_perm_for_user(user_get_object_by_name ($user_name), 'pm', $arr['group_project_id'], 'read') ;
}


/*-
 * Query for controlling the result. It gives back all user_names and
 * project_task_ids that matches the groups above:
 *
 * SELECT users.user_name, project_task.project_task_id FROM users
 * INNER JOIN user_group ON users.user_id = user_group.user_id
 * INNER JOIN project_group_list ON user_group.group_id = project_group_list.group_id
 * INNER JOIN project_task ON project_group_list.group_project_id = project_task.group_project_id;
 *
 * Query for controlling the result. It gives back all user_names that
 * does not match a group above:
 *
 * SELECT users.user_name, project_task.project_task_id FROM users
 * LEFT JOIN user_group ON users.user_id = user_group.user_id
 * LEFT JOIN project_group_list ON user_group.group_id = project_group_list.group_id
 * LEFT JOIN project_task ON project_group_list.group_project_id = project_task.group_project_id
 * WHERE project_task_id isNull;
 */

/**
 * Retrieve extended information about a project task.
 *
 * in:	int project_task_id
 * out:	false (if an error occured) or an associative array with
 *	- int project_task_id (copy)
 *	- int group_project_id (for URI construction)
 *	- int group_id (for URI construction)
 *	- str group_unixname
 *	- str group_realname
 *	- str summary (of the task)
 *	- int priority
 *	- int submitted_by (user ID)
 *	- str submitted_unixname
 *	- str submitted_realname
 *	- int status_id
 *	- str status_name
 *	- int category_id
 *	- str category_name (of the per-group category the task is in)
 *	- str group_project_name (of the per-group subproject the task is in)
 *	- str is_a := 'tid' (task id, as opposed to 'aid', artefact id)
 */
function getAllFromProjectTask($project_task_id) {
	$res = db_query_params('SELECT ' .
	    'project_task.project_task_id, project_task.group_project_id, project_task.summary, project_task.priority, ' .
	    'project_task.created_by, project_task.status_id, project_task.category_id, ' .
	    'users.user_name, ' .
	    'project_category.category_name, ' .
	    'project_group_list.project_name, ' .
	    'groups.group_name, ' .
	    'groups.group_id, ' .
	    'project_status.status_name ' .
	    'FROM project_status ' .
	    'INNER JOIN project_task ON ' .
	    'project_task.status_id = project_status.status_id ' .
	    'INNER JOIN users ON ' .
	    'users.user_id = project_task.created_by ' .
	    'INNER JOIN project_category ON ' .
	    'project_category.category_id = project_task.category_id ' .
	    'INNER JOIN project_group_list ON ' .
	    'project_group_list.group_project_id = project_task.group_project_id ' .
	    'INNER JOIN groups ON ' .
	    'groups.group_id = project_group_list.group_id ' .
	    'WHERE project_task.project_task_id = $1',
	    array($project_task_id));

	if (!$res || db_numrows($res) != 1 || !($arr = db_fetch_array($res)))
		return false;

	$arrResult = array(
	    'project_task_id' => (int)$arr[0],
	    'group_project_id' => (int)$arr[1],
	    'group_id' => (int)$arr[11],
	    'summary' => $arr[2],
	    'priority' => (int)$arr[3],
	    'submitted_by' => (int)$arr[4],
	    'status_id' => (int)$arr[5],
	    'category_id' => (int)$arr[6],
	    'submitted_unixname' => $arr[7],
	    'category_name' => $arr[8],
	    'group_project_name' => $arr[9],
	    'group_realname' => $arr[10],
	    'status_name' => $arr[12],
	    'is_a' => 'tid'
	);

	/*XXX merge the following two into the SQL above */

	if (($u = user_get_object($arrResult['submitted_by']))) {
		$arrResult['submitted_realname'] = $u->getRealName();
	}

	if (($g = group_get_object($arrResult['group_id']))) {
		$arrResult['group_unixname'] = $g->getUnixName();
	}

	return $arrResult;
}


/* -+- Tracker -+- Artefact -+- */


/**
 * Check if the bug behind aid is considered public.
 *
 * in:	int aid
 * out:	true, if it is; false otherwise
 */
function isArtefactInfoPublic($aid, $atid=0) {
	if (!$atid) {
		$res = db_query_params('
			SELECT group_artifact_id FROM artifact
			WHERE artifact_id=$1',
		    array($aid));
		if (!$res || db_numrows($res) != 1 ||
		    !($arr = db_fetch_array($res))) {
			return false;
		}
		$atid = (int)$arr[0];
	}

	return RoleAnonymous::getInstance()->hasPermission('tracker',
	    $atid, 'read');
}


/**
 * Retrieve atid and group_id for a specific aid (Artefact ID),
 * for URI construction and similar things.
 *
 * in:	int aid
 * out:	false, or an associative array with
 *	- int aid (copy)
 *	- int atid
 *	- int group_id
 */
function getArtefactTrackerIdGroupId($aid,$getsummary=false) {
	$res = db_query_params('
		SELECT artifact_group_list.group_artifact_id, artifact_group_list.group_id, artifact.summary
		FROM artifact_group_list, artifact
		WHERE artifact_group_list.group_artifact_id=artifact.group_artifact_id
		    AND artifact.artifact_id=$1',
	    array($aid));

	if (!$res || db_numrows($res) != 1 || !($arr = db_fetch_array($res)))
		return false;

	$arrResult = array(
	    'aid' => (int)$aid,
	    'atid' => (int)$arr[0],
	    'group_id' => (int)$arr[1],
	    'is_a' => 'aid'
	);
	if ($getsummary)
		$arrResult['summary'] = (isArtefactInfoPublic($aid, $arr[0]) ||
		    forge_check_global_perm('forge_admin')) ?
		    $arr[2] : _('<Information not public>');

	return $arrResult;
}
