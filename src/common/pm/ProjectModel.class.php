<?php
/**
 * FusionForge : Project Management Facility
 *
 * Copyright 1999/2000, Sourceforge.net Tim Perdue
 * Copyright 2002 GForge, LLC, Tim Perdue
 * Copyright 2010, FusionForge Team
 * Copyright (C) 2011 Alain Peyrat - Alcatel-Lucent
 * http://fusionforge.org
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

class ProjectModel {

	var $projectTask;

	private function setProjectTask($pt) {
		$this->projectTask = $pt;
	}

	public function getProjectTask() {
		return $this->projectTask;
	}

	function __construct($pt) {
		$this->setProjectTask($pt);
	}

	public function getAssignedTo() {
		return $result=db_query_params('SELECT users.realname FROM users,project_assigned_to
			WHERE users.user_id=project_assigned_to.assigned_to_id
			    AND project_task_id=$1
			ORDER BY users.user_name',
		    array(
			$this->getProjectTask()->getID(),
		    ));
	}

	public function getRelatedCommits() {
		return showmess_getcommits($this->getProjectTask()->getID());
	}
}
