<?php
/** FusionForge Git plugin
 *
 * Copyright 2009, Roland Mas
 * Copyright 2009, Mehdi Dogguy <mehdi@debian.org>
 * Copyright © 2011, 2012, 2013, 2016
 *	Thorsten Glaser <t.glaser@tarent.de>
 *
 * This file is part of FusionForge.
 *
 * FusionForge is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License,
 * or (at your option) any later version.
 *
 * FusionForge is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

forge_define_config_item ('default_server', 'scmgit', forge_get_config ('web_host')) ;
forge_define_config_item ('repos_path', 'scmgit', '/scmrepos/git');

class GitPlugin extends SCMPlugin {
	function __construct() {
		parent::__construct();
		$this->name = 'scmgit';
		$this->text = 'Git';
		$this->hooks[] = 'scm_browser_page';
		$this->hooks[] = 'scm_update_repolist' ;
		$this->hooks[] = 'scm_generate_snapshots' ;
		$this->hooks[] = 'scm_create_subrepo' ;

		$this->register () ;
	}

	function getDefaultServer() {
		return forge_get_config('default_server', 'scmgit') ;
	}

        function printShortStats ($params) {
                $project = $this->checkParams ($params) ;
                if (!$project) {
                        return false ;
                }

                if ($project->usesPlugin($this->name)) {
                        $result = db_query_params('SELECT sum(commits) AS commits, sum(adds) AS adds FROM stats_cvs_group WHERE group_id=$1',
                                                  array ($project->getID())) ;
                        $commit_num = db_result($result,0,'commits');
                        $add_num    = db_result($result,0,'adds');
                        if (!$commit_num) {
                                $commit_num=0;
                        }
                        if (!$add_num) {
                                $add_num=0;
                        }
                        echo ' (Git: '.sprintf(_('<strong>%1$s</strong> commits, <strong>%2$s</strong> adds'), number_format($commit_num, 0), number_format($add_num, 0)).")";
                }
        }

	function getBlurb () {
		return '<p>' .
		    _('Documentation for Git is available at <a href="http://git-scm.com/">http://git-scm.com/</a>.') .
		    '</p><p>' .
		    _('<strong>Note:</strong> On hosted git setups, receiving non-fastforward pushes is always disabled. Furthermore, our project repositories disallow deleting branches to prevent data loss and history corruption; if you want to use temporary branches, please apply for a personal repository below, which <em>does</em> allow deleting branches (but still no non-fastforward pushes).') .
		    '</p>';
	}

	function getInstructionsForAnon ($project) {
		global $HTML;

		$b = $HTML->boxMiddle(_('Anonymous Git Access'), '', false, 'scmgit_anon');
		$b .= '<p>';
		$b .= _('This project\'s Git repository can be checked out through anonymous access with the following command.');
		$b .= '</p>';

		$b .= '<p>' ;
		$b .= '<tt>git clone '.util_make_url ('/anonscm/git/'.$project->getUnixName().'/'.$project->getUnixName().'.git').'</tt><br />';
		$b .= '</p>';

		$result = db_query_params ('SELECT u.user_id, u.user_name, u.realname FROM plugin_scmgit_personal_repos p, users u WHERE p.group_id=$1 AND u.user_id=p.user_id AND u.unix_status=$2',
					   array ($project->getID(),
						  'A')) ;
		$rows = db_numrows ($result) ;

		if ($rows > 0) {
			$b .= $HTML->boxMiddle(_('Developer\'s repository'), '', false, 'scmgit_persoanon');
			$b .= '<p>';
			$b .= ngettext ('One of this project\'s members also has a personal Git repository that can be checked out anonymously.',
					'Some of this project\'s members also have personal Git repositories that can be checked out anonymously.',
				$rows);
			$b .= '</p>';
			$b .= '<p>' ;
			for ($i=0; $i<$rows; $i++) {
				$user_id = db_result($result,$i,'user_id');
				$user_name = db_result($result,$i,'user_name');
				$real_name = db_result($result,$i,'realname');
				$b .= '<tt>git clone '.util_make_url ('/anonscm/git/'.$project->getUnixName().'/users/'.$user_name.'.git').'</tt> ('.util_make_link_u ($user_name, $user_id, $real_name).')<br />';
			}
			$b .= '</p>';
		}

		return $b ;
	}

	function getInstructionsForRW ($project) {
		global $HTML;

		if (session_loggedin()) {
			$u =& user_get_object(user_getid()) ;
			$d = $u->getUnixName() ;
			if (forge_get_config('use_ssh', 'scmgit')) {
				$b = $HTML->boxMiddle(_('Developer GIT Access via SSH'), '', false, 'scmgit_dev');
				$b .= '<p>';
				$b .= _('Only project developers can access the GIT tree via this method. SSH must be installed on your client machine. Enter your site password when prompted.');
				$b .= '</p>';
				$b .= '<p><tt>git clone git+ssh://'.$d.'@' . $this->getBoxForProject($project) . forge_get_config('repos_path', 'scmgit') .'/'. $project->getUnixName() .'/'. $project->getUnixName() .'.git</tt></p>' ;
			} elseif (forge_get_config('use_dav', 'scmgit')) {
				$protocol = forge_get_config('use_ssl', 'scmgit')? 'https' : 'http';
				$b = $HTML->boxMiddle(_('Developer GIT Access via HTTP'), '', false, 'scmgit_dev');
				$b .= '<p>';
				$b .= _('Only project developers can access the GIT tree via this method. Enter your site password when prompted.');
				$b .= '</p>';
				$b .= '<p><tt>git clone '.$protocol.'://'.$d.'@' . $this->getBoxForProject($project) . forge_get_config('repos_path', 'scmgit') .'/'. $project->getUnixName() .'/'. $project->getUnixName() .'.git</tt></p>' ;
			}
		} else {
			if (forge_get_config('use_ssh', 'scmgit')) {
				$b = $HTML->boxMiddle(_('Developer GIT Access via SSH'), '', false, 'scmgit_dev');
				$b .= '<p>';
				$b .= _('Only project developers can access the GIT tree via this method. SSH must be installed on your client machine. Substitute <i>developername</i> with the proper value. Enter your site password when prompted.');
				$b .= '</p>';
				$b .= '<p><tt>git clone git+ssh://<i>'._('developername').'</i>@' . $this->getBoxForProject($project) . forge_get_config('repos_path', 'scmgit') .'/'. $project->getUnixName() .'/'. $project->getUnixName() .'.git</tt></p>' ;
			} elseif (forge_get_config('use_dav', 'scmgit')) {
				$protocol = forge_get_config('use_ssl', 'scmgit')? 'https' : 'http';
				$b = $HTML->boxMiddle(_('Developer GIT Access via HTTP'), '', false, 'scmgit_dev');
				$b .= '<p>';
				$b .= _('Only project developers can access the GIT tree via this method. Enter your site password when prompted.');
				$b .= '</p>';
				$b .= '<p><tt>git clone '.$protocol.'://<i>'._('developername').'</i>@' . $this->getBoxForProject($project) . forge_get_config('repos_path', 'scmgit') .'/'. $project->getUnixName() .'/'. $project->getUnixName() .'.git</tt></p>' ;
			}
		}

		if (session_loggedin()) {
                        $u =& user_get_object(user_getid()) ;
			if ($u->getUnixStatus() == 'A') {
				$result = db_query_params ('SELECT * FROM plugin_scmgit_personal_repos p WHERE p.group_id=$1 AND p.user_id=$2',
							   array ($project->getID(),
								  $u->getID())) ;
				if ($result && db_numrows ($result) > 0) {
					$b .= $HTML->boxMiddle(_('Access to your personal repository'));
					$b .= '<p>';
					$b .= _('You have a personal repository for this project, accessible through SSH with the following method. Enter your site password when prompted.');
					$b .= '</p>';
					$b .= '<p><tt>git clone git+ssh://'.$u->getUnixName().'@' . $this->getBoxForProject($project) . forge_get_config('repos_path', 'scmgit') .'/'. $project->getUnixName() .'/users/'. $u->getUnixName() .'.git</tt></p>' ;
				} else {
					$glist = $u->getGroups();
					foreach ($glist as $g) {
						if ($g->getID() == $project->getID()) {
							$b .= $HTML->boxMiddle(_('Request a personal repository'), '', false, 'scmgit_persoreq');
							$b .= '<p>';
							$b .= _('You can clone the project repository into a personal one into which you alone will be able to write.  Other members of the project will only have read access.  Access for non-members will follow the same rules as for the project\'s main repository.  Note that the personal repository may take some time before it is created (less than an hour in most situations).');
							$b .= '</p>';
							$b .= '<p>';
							$b .= sprintf (_('<a href="%s">Request a personal repository</a>.'),
								       util_make_url ('/plugins/scmgit/index.php?func=request-personal-repo&amp;group_id='.$project->getID()));
							$b .= '</p>';
						}
					}
				}
			}
		}

		return $b ;
	}

	function getSnapshotPara($g) {
		global $HTML;

		$has_snap = file_exists(forge_get_config('scm_snapshots_path') .
		    '/' . $g->getUnixName() . '-scm-latest.tar' .
		    util_get_compressed_file_extension());
		$has_tar = file_exists(forge_get_config('scm_tarballs_path') .
		    '/' . $g->getUnixName() . '.tar' .
		    util_get_compressed_file_extension());

		if (!$has_snap && !$has_tar) {
			return "";
		}
		$rv = $HTML->boxMiddle(_('Snapshots'), '', false, 'scm_snap');
		if ($has_snap) {
			$rv .= html_e('p', array(),
			    util_html_encode(_('SCM snapshots are nightly generated git tar archives (tarballs) of an export (like checkout, but without the VCS metadata) of the “HEAD” reference of the project’s source code tree (primary Git repository only, without any additional Git repositories). They are unversioned but made available for download here as snapshot distfiles. Beware that the git tar format is not fully compatible with POSIX ustar and may additionally leave a spurious extra file upon extraction. Also, git exports can have files removed and will not include any submodules.')));
			$rv .= html_e('p', array(), '[' .
			    util_make_link('/snapshots.php?group_id=' .
			    $g->getID(),
			    _('Download the nightly snapshot')) . ']');
		}
		if ($has_tar) {
			$rv .= html_e('p', array(),
			    util_html_encode(_('SCM tarballs are nightly generated ustar archives of the project’s version control trees, for cloning the entirety of the SCM repositories.')));
			$rv .= html_e('p', array(), '[' .
			    util_make_link('/tarballs.php?group_id=' .
			    $g->getID(),
			    _('Download the nightly tarball')) . ']');
		}
		return $rv;
	}

	function printBrowserPage($params) {
		if (($project = $this->checkParams($params)) &&
		    $project->usesPlugin($this->name) &&
		    $this->browserDisplayable($project)) {
			$url = "/plugins/scmgit/cgi-bin/gitweb.cgi?pf=" .
			    $project->getUnixName();
			session_redirect($url);
		}
	}

	function getBrowserLinkBlock ($project) {
		global $HTML ;
		$b = $HTML->boxMiddle(_('Git Repository Browser'));
		$b .= '<p>';
		$b .= _('Browsing the Git tree gives you a view into the current status of this project\'s code. You may also view the complete histories of any file in the repository.');
		$b .= '</p>';

		$rootdir = forge_get_config('repos_path', 'scmgit');
		$rootslashlen = strlen($rootdir) + 1;
		$unxname = $project->getUnixName();
		$xrepos = $this->getRepositories($rootdir . "/" . $unxname);
		$repos = array();
		$prepo = false;

		foreach ($xrepos as $repo) {
			$repo = substr($repo, $rootslashlen);
			if ($repo == ($unxname . "/" . $unxname . ".git")) {
				$prepo = true;
			} else {
				$repos[] = $repo;
			}
		}

		if ($prepo) {
			$b .= '<p>[' .
			    util_make_link("/plugins/scmgit/cgi-bin/gitweb.cgi?p=" .
			    $unxname . "/" . $unxname . ".git",
			    _('Browse primary Git repository')) . "]</p>\n";
		}

		if (!$repos) {
			return $b;
		}

		$b .= '<p>' . _('Browse additional Git repositories') .
		    "</p><ul>\n";
		foreach ($repos as $repo) {
			$b .= '<li>' .
			    util_make_link("/plugins/scmgit/cgi-bin/gitweb.cgi?p=" .
			    $repo, substr($repo, 0, -4)) . "</li>\n";
		}
		$b .= "</ul>\n";

		return $b ;
	}

	function getStatsBlock($project) {
		return "";
	}

	static function createUserRepo($params) {
		$project = $params['project'];
		$project_name = $project->getUnixName();
		$user_name = $params['user_name'];
		$unix_group = $params['unix_group'];
		$main_repo = $params['main_repo'];
		$root = $params['root'];

		$repodir = $root . '/users/' .  $user_name . '.git' ;
		chgrp($repodir, $unix_group);
		if ($project->enableAnonSCM()) {
			chmod ($repodir, 02755);
		} else {
			chmod ($repodir, 02750);
		}
		if (!is_file ("$repodir/HEAD") && !is_dir("$repodir/objects") && !is_dir("$repodir/refs")) {
			system ("git clone --bare $main_repo $repodir") ;
			system ("GIT_DIR=\"$repodir\" git config receive.denyNonFastforwards true") ;
			system ("GIT_DIR=\"$repodir\" git config receive.denyDeletes false") ;
			system ("GIT_DIR=\"$repodir\" git update-server-info") ;
			if (is_file ("$repodir/hooks/post-update.sample")) {
				rename ("$repodir/hooks/post-update.sample",
					"$repodir/hooks/post-update") ;
			}
			if (!is_file ("$repodir/hooks/post-update")) {
				$f = fopen ("$repodir/hooks/post-update","x+") ;
				fwrite ($f, "exec git-update-server-info\n") ;
				fclose ($f) ;
			}
			if (is_file ("$repodir/hooks/post-update")) {
				system ("chmod +x $repodir/hooks/post-update") ;
			}
			system("echo \"Git repository for user $user_name in project $project_name\" > $repodir/description");
		}
	}

	function createOrUpdateRepo ($params) {
		$params['newrepo'] = false;
		/* invert logic, subrepo returns false on success */
		return !$this->createOrUpdateSubRepo($params);
	}

	function createOrUpdateSubRepo ($params) {
		$project = $this->checkParams ($params) ;
		if (!$project) {
			return true;
		}

		if (! $project->usesPlugin ($this->name)) {
			return true;
		}

		$project_name = $project->getUnixName() ;
		$root = forge_get_config('repos_path', 'scmgit') . '/' . $project_name ;
		$unix_group = 'scm_' . $project_name ;
                system ("mkdir -p $root") ;

		$sys_name = forge_get_config('forge_name');
		if ($params['newrepo'] === false) {
			/* create main repository */
			$repo_name = $project_name;
			$repo_desc = "Main git repository" .
			    " for $sys_name project $project_name";
		} else {
			/* create subrepository */
			$repo_name = $params['newrepo'];
			$repo_desc = "Supplemental git repository $repo_name" .
			    " for $sys_name project $project_name";
		}
		$main_repo = $root . '/' .  $repo_name . '.git';
		if (!is_dir($main_repo) || (!is_file("$main_repo/HEAD") &&
		    !is_dir("$main_repo/objects") && !is_dir("$main_repo/refs"))) {
			$tmp_repo = util_mkdtemp('.git', $project_name);
			if ($tmp_repo == false) {
				return true;
			}
			system ("GIT_DIR=\"$tmp_repo\" git init --bare --shared=group") ;
			system ("GIT_DIR=\"$tmp_repo\" git config receive.denyNonFastforwards true") ;
			system ("GIT_DIR=\"$tmp_repo\" git config receive.denyDeletes true") ;
			system ("GIT_DIR=\"$tmp_repo\" git update-server-info") ;
			if (is_file ("$tmp_repo/hooks/post-update.sample")) {
				rename ("$tmp_repo/hooks/post-update.sample",
					"$tmp_repo/hooks/post-update") ;
			}
			if (!is_file ("$tmp_repo/hooks/post-update")) {
				$f = fopen ("$tmp_repo/hooks/post-update") ;
				fwrite ($f, "exec git-update-server-info\n") ;
				fclose ($f) ;
			}
			if (is_file ("$tmp_repo/hooks/post-update")) {
				system ("chmod +x $tmp_repo/hooks/post-update") ;
			}
			system ("ln -sf /etc/gforge/plugins/scmgit/post-receive-email.sh $tmp_repo/hooks/post-receive") ;
			system ("echo \"$repo_desc\" >$tmp_repo/description") ;
			system ("find $tmp_repo -type d | xargs chmod g+s") ;
			system ("chgrp -R $unix_group $tmp_repo") ;
			system ("chmod -R g+rwX,o+rX-w $tmp_repo") ;
			if ($project->enableAnonSCM()) {
				system ("chmod g+rwX,o+rX-w $root") ;
			} else {
				system ("chmod g+rwX,o-rwx $root") ;
			}
			$ret = true;
			/*
			 * $main_repo can already exist, for example if it’s
			 * not a directory or doesn’t contain a HEAD file or
			 * an objects or refs subdirectory… move it out of
			 * the way in these cases
			 */
			system("if test -e $main_repo || test -h $main_repo; then d=\$(mktemp -d $main_repo.scmgit-moved.XXXXXXXXXX) && mv -f $main_repo \$d/; fi");
			/* here’s still a TOCTOU but we check $ret below */
			system("mv $tmp_repo $main_repo", $ret);
			if ($ret != 0) {
				return true;
			}
		}
		system ("chgrp $unix_group $root") ;
		system ("chmod g+s $root") ;
		if ($project->enableAnonSCM()) {
			system ("chmod g+rwX,o+rX-w $root") ;
		} else {
			system ("chmod g+rwX,o-rwx $root") ;
		}

		util_create_file_with_contents("$root/ffgroup.nfo", $project_name);
		system("chown 0 $root/ffgroup.nfo; chmod 644 $root/ffgroup.nfo");

		$result = db_query_params ('SELECT u.user_name FROM plugin_scmgit_personal_repos p, users u WHERE p.group_id=$1 AND u.user_id=p.user_id AND u.unix_status=$2',
					   array ($project->getID(),
						  'A')) ;
		$rows = db_numrows ($result) ;
		for ($i=0; $i<$rows; $i++) {
			system ("mkdir -p $root/users") ;
			$user_name = db_result($result,$i,'user_name');
			$repodir = $root . '/users/' .  $user_name . '.git' ;

			if (!is_dir($repodir) && mkdir ($repodir, 0700)) {
				chown ($repodir, $user_name) ;

				$params = array();
				$params['project'] = $project;
				$params['user_name'] = $user_name;
				$params['unix_group'] = $unix_group;
				$params['root'] = $root;
				$params['main_repo'] = $main_repo;

				util_sudo_effective_user($user_name,
							 array("GitPlugin", "createUserRepo"),
							 $params);
			}
		}
		if (is_dir ("$root/users")) {
			system ("chmod g+rX-w,o+rX-w $root/users") ;
		}

		/* success */
		return false;
	}

	function updateRepositoryList ($params) {
		$groups = $this->getGroups(); // ordered by group_id
		$list = array () ;
		foreach ($groups as $project) {
			if ($this->browserDisplayable ($project)) {
				$list[] = $project ;
			}
		}

		$box = forge_get_config('scm_box');
		if ($box == '')
			$box = forge_get_config('default_server', 'scmgit');

		$gwlistfn = forge_get_config('data_path') . '/gitweb.list';

		$gwconffn = forge_get_config('data_path') . '/gitweb.conf';
		$config_f = fopen($gwconffn . '.new', 'w');
		$rootdir = forge_get_config('repos_path', 'scmgit');
		$rootslashlen = strlen($rootdir) + 1;
		fwrite($config_f, "\$projectroot = '$rootdir';\n");
		fwrite($config_f, "\$projects_list = '$gwlistfn';\n");
		fwrite($config_f, "@stylesheets = (\n\t'" .
		    util_make_uri('/plugins/scmgit/gitweb.css') . "',\n\t'" .
		    util_make_uri('/css/ffcgi.css') . "',\n);\n");
		fwrite($config_f, "\$logo = '" . util_make_uri('/plugins/scmgit/git-logo.png') . "';\n");
		fwrite($config_f, "\$favicon = '" . util_make_uri('/plugins/scmgit/git-favicon.png') . "';\n");
		fwrite($config_f, "\$javascript = '" . util_make_uri('/plugins/scmgit/gitweb.js') . "';\n");
		fwrite($config_f, "our @extra_breadcrumbs = (\n\t[ '" .
		    forge_get_config('forge_name') . "' => '" .
		    util_make_uri('/') . "' ],\n);\n");
		fwrite($config_f, "\$site_name = 'gitweb @ " . forge_get_config('web_host') . "';\n");
		fwrite($config_f, "\$projects_list_description_width = 66;\n");
		fwrite($config_f, "\$default_text_plain_charset = 'UTF-8';\n");
		fwrite($config_f, "\$fallback_encoding = 'cp1252';\n");
		fwrite($config_f, "@diff_opts = ();\n");
		fwrite($config_f, "@git_base_url_list = (\n\t'" .
		    util_make_url('/anonscm/git') . "',\n\t'git+ssh://" . $box .
		    forge_get_config('repos_path', 'scmgit') . "',\n);\n");
		fwrite($config_f, "\$prevent_xss = 1;\n");
		fwrite($config_f, "\$maxload = 12;\n");
		fwrite($config_f, "\$omit_owner = 1;\n");
		fwrite($config_f, "our \$version .= ' (EvolvisForge)';\n");
		fwrite($config_f, "\$feature{'snapshot'}{'default'} = [];\n");
		fwrite($config_f, "\$feature{'remote_heads'}{'override'} = 1;\n");
		fwrite($config_f, "\$feature{'actions'}{'default'} = [\n\t('project home', '" .
		    util_make_uri('/plugins/scmgit/?func=grouppage/%n') .
		    "', 'summary'),\n];\n");
		fwrite($config_f, "\$feature{'timed'}{'default'} = [1];\n");
		fwrite($config_f, "\$feature{'extra-branch-refs'}{'override'} = 1;\n");
		fwrite($config_f, "\n1;\n");
		fclose($config_f);
		chmod($gwconffn . '.new', 0644);
		rename($gwconffn . '.new', $gwconffn);

		$f = fopen($gwlistfn . '.new', 'w');
		foreach ($list as $project) {
			$project_name = $project->getUnixName();
			$repos = $this->getRepositories($rootdir . "/" .  $project_name);
			$unix_group = 'scm_' . $project_name;
			foreach ($repos as $repo) {
				$reldir = substr($repo, $rootslashlen);
				fwrite($f, urlencode($reldir) .
				    " " . urlencode($unix_group) . "\n");
			}
		}
		fclose($f);
		chmod($gwlistfn . '.new', 0644);
		rename($gwlistfn . '.new', $gwlistfn);
	}

	function getRepositories($path) {
		if (!is_dir($path))
			return array();
		$sublist = array();
		$entries = scandir($path); // defaults to SCANDIR_SORT_ASCENDING
		foreach ($entries as $entry) {
			if (($entry == ".") or ($entry == ".."))
				continue;
			if ($entry == "HEAD")
				return array($path);
			$fullname = $path . "/" . $entry;
			if (is_dir($fullname)) {
				if (!is_link($fullname))
					$sublist[] = $fullname;
			}
		}
		$list = array();
		foreach ($sublist as $fullname) {
			$result = $this->getRepositories($fullname);
			$list = array_merge($list, $result);
		}
		return $list;
	}

	function generateSnapshots ($params) {
		$project = $this->checkParams ($params) ;
		if (!$project) {
			return false ;
		}

		$group_name = $project->getUnixName() ;

		$snapshot = forge_get_config('scm_snapshots_path').'/'.$group_name.'-scm-latest.tar'.util_get_compressed_file_extension();
		$tarball = forge_get_config('scm_tarballs_path').'/'.$group_name.'.tar'.util_get_compressed_file_extension();

		if (! $project->usesPlugin ($this->name)) {
			return false;
		}

		if (! $project->enableAnonSCM()) {
			if (is_file($snapshot)) {
				unlink ($snapshot) ;
			}
			if (is_file($tarball)) {
				unlink ($tarball) ;
			}
			return false;
		}

		// TODO: ideally we generate one snapshot per git repository
		$toprepo = forge_get_config('repos_path', 'scmgit') ;
		$repo = $toprepo . '/' . $project->getUnixName() . '/' .  $project->getUnixName() . '.git' ;

		if (!is_dir ($repo)) {
			if (is_file($snapshot)) {
				unlink ($snapshot) ;
			}
			if (is_file($tarball)) {
				unlink ($tarball) ;
			}
			return false ;
		}

		$tmp = trim(`mktemp -d /var/tmp/ffscmgit.XXXXXXXXXX`);
		if ($tmp == '') {
			return false ;
		}
		system("GIT_DIR=\"$repo\" git archive --format=tar --prefix=$group_name-scm-latest/ HEAD | ".forge_get_config('compression_method')." >$tmp/snapshot");
		chmod ("$tmp/snapshot", 0644) ;
		rpl_copy("$tmp/snapshot", $snapshot);
		unlink ("$tmp/snapshot") ;

		system("(cd $toprepo; paxtar -M set -cf - " .
		    $project->getUnixName() . ") | " .
		    forge_get_config('compression_method') . " >$tmp/tarball");
		chmod ("$tmp/tarball", 0644) ;
		rpl_copy("$tmp/tarball", $tarball);
		unlink ("$tmp/tarball") ;
		system ("rm -rf $tmp") ;
	}

	function printAdminPage($params) {
		$rv = parent::printAdminPage($params);
		$group = $this->checkParams($params);
		if ($group && $group->usesPlugin($this->name)) {
			print "\n<div style=\"border:1px solid black; margin:12px; padding:12px;\">\n";
			print '<input type="checkbox" name="scm_git_addsubrepo" value="1" /> ';
			print _('Create new subrepository with name:')."\n";
			print '<input type="text" size="32" maxlength="255" ' .
			    'name="scm_git_addsubreponame" value="" />';
			print "\n</div>\n";
		}
		return $rv;
	}

	function adminUpdate($params) {
		global $feedback, $error_msg;

		$rv = parent::adminUpdate($params);
		$group = $this->checkParams($params);
		if ($group && $group->usesPlugin($this->name) &&
		    util_ifsetor($params['scm_git_addsubrepo']) == 1 &&
		    ($p_reponame = util_ifsetor($params['scm_git_addsubreponame']))) {
			/* copied from gforge/utils/scm-newsubrepo.php */

			/* ltrim and rtrim, strip leading/trailing slash and ".git" */
			$p_reponame = ltrim(preg_replace('#(/|.git)*$#', '', trim($p_reponame)), "/");

			$e = false;
			$p_reponamefull = "";
			foreach (explode("/", $p_reponame) as $p_reponamepart) {
				if (!account_groupnamevalid($p_reponamepart,
				    60, 1)) {
					$e = "Invalid subrepo name! " . $GLOBALS['register_error'] . "\n";
					break;
				}
				$p_reponamefull .= $p_reponamepart . "/";
			}
			if (substr($p_reponamefull, 0, strlen("users/")) ==
			    "users/") {
				$e = "Invalid subrepo name! May not begin with users/\n";
			}

			if (!$e) {
				$res = db_query_params('INSERT INTO scm_subrepos
					(group_id, newrepo)
				VALUES	($1, $2)',
				    array($group->getID(), $p_reponame));
				if (!$res || db_affected_rows($res) < 1) {
					$e = "Could not queue for creation! " . db_error();
				}
			}

			if (!$e) {
				$feedback = sprintf(_('Subrepository %s scheduled for creation.'),
				    $p_reponame);
			} else {
				$error_msg = htmlspecialchars($e);
			}
		}
		return $rv;
	}
}

// Local Variables:
// mode: php
// c-file-style: "bsd"
// End:
