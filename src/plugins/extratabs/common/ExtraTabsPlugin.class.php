<?php
/**
 * FusionForge extratabs plugin
 *
 * Copyright 2005, Raphaël Hertzog
 * Copyright 2009, Roland Mas
 * Copyright (C) 2010 Alain Peyrat - Alcatel-Lucent
 * Copyright © 2011 Thorsten Glaser <t.glaser@tarent.de>
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

class ExtraTabsPlugin extends Plugin {
	function __construct() {
		parent::__construct();
		$this->name = "extratabs" ;
		$this->text = "Extra tabs";
		$this->hooks[] = "project_admin_plugins" ;
		$this->hooks[] = "groupisactivecheckbox" ; // The "use ..." checkbox in editgroupinfo
		$this->hooks[] = "groupisactivecheckboxpost" ; //
		$this->hooks[] = "groupmenu" ;  // To put into the project tabs
		$this->hooks[] = "outermenu" ;  // To put into the global tabs
		$this->hooks[] = "clone_project_from_template" ;
	}

	function CallHook ($hookname, &$params) {
		global $HTML;

		if ($hookname == "project_admin_plugins") {
			$group_id=$params['group_id'];
			$group = &group_get_object($group_id);
			if ($group->usesPlugin($this->name))
				echo '<p>' . util_make_link('/plugins/extratabs/index.php?group_id=' . $group_id,
				    _('Extra Tabs Admin')) . '</p>';
		} elseif ($hookname == "groupisactivecheckbox") {
			$group_id=$params['group_id'];
			//Check if the group is active
			// this code creates the checkbox in the project edit public info page to activate/deactivate the plugin
			$group = &group_get_object($group_id);
			echo "<tr>";
			echo "<td>";
			echo ' <input type="checkbox" name="use_extratabsplugin" value="1" ';
			// CHECKED OR UNCHECKED?
			if ( $group->usesPlugin ( $this->name ) ) {
				echo 'checked="checked"';
			}
			echo " /><br/>";
			echo "</td>";
			echo "<td>";
			echo "<strong>Use ".$this->text." Plugin</strong>";
			echo "</td>";
			echo "</tr>";
		} elseif ($hookname == "groupisactivecheckboxpost") {
			$group_id=$params['group_id'];
			// this code actually activates/deactivates the plugin after the form was submitted in the project edit public info page
			$group = &group_get_object($group_id);
			$use_extratabsplugin = getStringFromRequest('use_extratabsplugin');
			if ( $use_extratabsplugin == 1 ) {
				$group->setPluginUse ( $this->name );
			} else {
				$group->setPluginUse ( $this->name, false );
			}
		} elseif ($hookname == "groupmenu" || $hookname == "outermenu") {
			$group_id = util_ifsetor($params['group'], 0);
			if ($hookname == "outermenu") {
				/* global tabs belong to Site Admin group */
				$group_id = 1;
			} else if ($group_id == 1) {
				/* we don't display them on the group too */
				return;
			}
			$project = group_get_object($group_id);
			if (!$project || !is_object($project))
				return;
			if ($project->isError())
				return;
			if (!$project->isProject())
				return;
			$res_tabs = db_query_params('SELECT tab_name, tab_url, type, tooltip FROM plugin_extratabs_main WHERE group_id=$1 ORDER BY index',
						     array ($group_id)) ;
			if (!isset($params['TOOLTIPS'])) {
				$tooltips_array = array();
				$params['TOOLTIPS'] = &$tooltips_array;
			}
			while ($row_tab = db_fetch_array($res_tabs)) {
				$params['TITLES'][] = $row_tab['tab_name'];
				$params['TOOLTIPS'][count($params['TITLES']) - 1] = $row_tab['tooltip'];
				switch ($row_tab['type']) {
					case 0: // Link
						$params['DIRS'][] = $row_tab['tab_url'];
						break;

					case 1: // Iframe
						$params['DIRS'][] = '/plugins/'.$this->name.'/iframe.php?group_id='.$group_id.'&amp;tab_name='.urlencode($row_tab['tab_name']);
						if (isset($params['toptab']) && ($params['toptab'] == $row_tab['tab_name'])) {
							$params['selected'] = count($params['TITLES']) - 1;
						}
						break;

					default:
						return;
				}
			}
		} elseif ($hookname == "clone_project_from_template") {
			$tabs = array () ;
			$res = db_query_params('SELECT tab_name, tab_url, index, tooltip FROM plugin_extratabs_main WHERE group_id=$1 ORDER BY index',
						     array ($params['template']->getID())) ;
			while ($row = db_fetch_array($res)) {
				$data = array () ;
				$data['tab_url'] = $params['project']->replaceTemplateStrings ($row['tab_url']) ;
				$data['tab_name'] = $params['project']->replaceTemplateStrings ($row['tab_name']) ;
				$data['tooltip'] = $params['project']->replaceTemplateStrings ($row['tooltip']) ;
				$data['index'] = $row['index'] ;
				$tabs[] = $data ;
			}

			foreach ($tabs as $tab) {
				db_query_params('INSERT INTO plugin_extratabs_main (tab_url, tab_name, index, group_id, tooltip) VALUES ($1,$2,$3,$5,$4)',
						 array ($data['tab_url'],
							$data['tab_name'],
							$data['index'],
							$data['tooltip'],
							$params['project']->getID())) ;
			}
		}
	}
}

// Local Variables:
// mode: php
// c-file-style: "bsd"
// End:

?>
