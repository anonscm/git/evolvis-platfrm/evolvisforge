<?php

/**
 * Extra tabs plugin
 * Copyright 2005, Raphaël Hertzog
 * Copyright 2006-2009, Roland Mas
 * Copyright 2009-2010, Alain Peyrat
 * Copyright 2010, Franck Villaume
 * Copyright (C) 2010 Alain Peyrat - Alcatel-Lucent
 * Copyright © 2011 Thorsten Glaser <t.glaser@tarent.de>
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

$feedback = '';
$error_msg = '';

require_once ('../../../www/env.inc.php');
require_once $gfcommon.'include/pre.php';
require_once $gfwww.'project/admin/project_admin_utils.php';

$group_id = getIntFromRequest ('group_id') ;
$index = getIntFromRequest ('index') ;

$tab_name = htmlspecialchars(trim(getStringFromRequest ('tab_name')));
$tab_rename = htmlspecialchars(trim(getStringFromRequest ('tab_rename')));
$tab_url = htmlspecialchars(trim(getStringFromRequest ('tab_url', 'http://')));
$tab_new_url = htmlspecialchars(trim(getStringFromRequest ('tab_new_url')));
$tab_tooltip = htmlspecialchars(trim(getStringFromRequest ('tab_tooltip')));
$tab_new_tooltip = htmlspecialchars(trim(getStringFromRequest ('tab_new_tooltip')));
$type = getIntFromRequest('type', 0);
$new_type = getIntFromRequest('new_type', -1);

if ($type === '0') {
	$type = 0;
}
if ($new_type === '0') {
	$new_type = 0;
}

session_require_perm ('project_admin', $group_id) ;

// get current information
$group = group_get_object($group_id);
if (!$group || !is_object($group)) {
	exit_no_group();
} elseif ($group->isError()) {
	exit_error($group->getErrorMessage(),'home');
}

db_begin();

// Calculate new index field
$res = db_query_params ('SELECT COUNT(*) as c FROM plugin_extratabs_main WHERE group_id = $1',
			array ($group_id)) ;
$row = db_fetch_array($res);
$newid = $row['c'] + 1;

$selected = 0; // No item selected by default

// Do work before displaying so that the result is immediately visible
if (getStringFromRequest ('addtab') != '') {
	if ($tab_name == '' || $tab_url == '' || $tab_url == 'http://') {
		$error_msg = _('ERROR: Missing Name or URL for the new tab');
	} else if (!util_check_url($tab_url)) {
		$error_msg = _('ERROR: Malformed URL (only http, https and ftp allowed)');
	} else {
		if ($type === 0) {
			$new_type2 = 'Link';
		} else if ($type == -1) {
			$new_type2 = 'unchanged';
		} else if ($type == 1) {
			$new_type2 = 'IFRAME';
		} else {
			$new_type2 = 'unknown';
		}
		$new_type2 .= '(' . $type . ')';
		$res = db_query_params('SELECT * FROM plugin_extratabs_main WHERE group_id=$1 AND tab_name=$2',
			array($group_id, $tab_name));
		if ($res && db_numrows($res) > 0) {
			$error_msg = _('ERROR: Name for tab is already used.');
		} else if (!($res = $group->addHistory('extratab-new',
		    $newid . '=' . $new_type2 . ':' . $tab_name . '(' .
		    $tab_tooltip . ')', $tab_url)) ||
		    db_affected_rows($res) < 1) {
			$error_msg = sprintf (_('Cannot insert audit trail entry: %s'),
			    db_error());
		} else {
			$res = db_query_params('INSERT INTO plugin_extratabs_main (group_id, index, tab_name, tab_url, type, tooltip) VALUES ($1,$2,$3,$4,$6,$5)',
						array ($group_id,
						       $newid,
						       $tab_name,
						       $tab_url,
						       $tab_tooltip,
						       $type)) ;
			if (!$res || db_affected_rows($res) < 1) {
				$error_msg = sprintf (_('Cannot insert new tab entry: %s'),
						      db_error());
			} else {
				$tab_name = '';
				$tab_url = 'http://';
				$tab_tooltip = '';
				$feedback = _('Tab successfully added');
			}
		}
	}
} elseif (getStringFromRequest ('delete') != '') {
	$res = db_query_params('SELECT * FROM plugin_extratabs_main WHERE group_id=$1 AND index=$2',
	    array($group_id, $index));
	if ($res && db_numrows($res)) {
		$old_tab_url = db_result($res, 0, 'tab_url');
		$old_tab_name = db_result($res, 0, 'tab_name');
		$old_tab_type = db_result($res, 0, 'type');
		$old_tab_tooltip = db_result($res, 0, 'tooltip');
		if ($old_tab_type === 0) {
			$old_tab_type2 = 'Link';
		} else if ($old_tab_type == -1) {
			$old_tab_type2 = 'unchanged';
		} else if ($old_tab_type == 1) {
			$old_tab_type2 = 'IFRAME';
		} else {
			$old_tab_type2 = 'unknown';
		}
		$old_tab_type2 .= '(' . $old_tab_type . ')';
	} else {
		$old_tab_url = '?';
		$old_tab_name = '?';
		$old_tab_type2 = '?';
		$old_tab_tooltip = '?';
	}
	$res = db_query_params ('DELETE FROM plugin_extratabs_main WHERE group_id=$1 AND index=$2',
				array ($group_id,
				       $index)) ;
	if (!$res || db_affected_rows($res) < 1) {
		$error_msg = sprintf (_('Cannot delete tab entry: %s'), db_error());
	} else {
		$group->addHistory('extratab-del', $index . '=' .
		    $old_tab_type2 . ':' . $old_tab_name . '(' .
		    $old_tab_tooltip . ')', $old_tab_url);
		$res = db_query_params ('SELECT index FROM plugin_extratabs_main WHERE group_id=$1 AND index > $2 ORDER BY index ASC',
					array ($group_id,
					       $index)) ;
		if (db_numrows($res) > 0) {
			$todo = array () ;
			while ($row = db_fetch_array($res)) {
				$todo[] = $row['index'] ;
			}
			foreach ($todo as $i) {
				$res = db_query_params ('UPDATE plugin_extratabs_main SET index = index - 1 WHERE group_id = $1 AND index = $2',
							array ($group_id,
							       $i)) ;
			}
		}
		if ($res) {
			$feedback = _('Tab successfully deleted');
		} else {
			$error_msg = sprintf (_('Cannot delete tab entry: %s'), db_error());
		}
	}
} elseif (getStringFromRequest ('up') != '') {
	if ($index > 1) {
		$previous = $index - 1;
		$res = db_query_params('UPDATE plugin_extratabs_main SET index=0 WHERE group_id=$1 AND index=$2',
				       array ($group_id,
					      $index)) ;
		$res = db_query_params('UPDATE plugin_extratabs_main SET index=$1 WHERE group_id=$2 AND index=$3',
				       array ($index,
					      $group_id,
					      $previous)) ;
		$res = db_query_params('UPDATE plugin_extratabs_main SET index=$1 WHERE group_id=$2 AND index=0',
				       array ($previous,
					      $group_id)) ;
		$selected = $previous;
		$feedback = _('Tab successfully moved');
	} else {
		$warning_msg = _('Tab not moved, already at first position');
		$selected = $index;
	}
} elseif (getStringFromRequest ('down') != '') {
	if ($index < $newid - 1) {
		$next = $index + 1;
		$res = db_query_params('UPDATE plugin_extratabs_main SET index=0 WHERE group_id=$1 AND index=$2',
				       array ($group_id,
					      $index)) ;
		$res = db_query_params('UPDATE plugin_extratabs_main SET index=$1 WHERE group_id=$2 AND index=$3',
				       array ($index,
					      $group_id,
					      $next)) ;
		$res = db_query_params('UPDATE plugin_extratabs_main SET index=$1 WHERE group_id=$2 AND index=0',
				       array ($next,
					      $group_id)) ;
		$feedback = _('Tab successfully moved');
		$selected = $next;
	} else {
		$warning_msg = _('Tab not moved, already at last position');
		$selected = $index;
	}
} elseif (getStringFromRequest ('modify') != '') {
	$done = 0;
	if ($tab_new_url && $tab_new_url != 'http://') {
		$tab_new_url2 = $tab_new_url;
	} else {
		$tab_new_url2 = '(unchanged)';
	}
	if ($new_type === 0) {
		$new_type2 = 'Link';
	} else if ($new_type == -1) {
		$new_type2 = 'unchanged';
	} else if ($new_type == 1) {
		$new_type2 = 'IFRAME';
	} else {
		$new_type2 = 'unknown';
	}
	$new_type2 .= '(' . $new_type . ')';
	if ($tab_rename) {
		$tab_name2 = ':' . $tab_rename;
	} else {
		$tab_name2 = '-unchanged';
	}
	$res = db_query_params('SELECT * FROM plugin_extratabs_main WHERE group_id=$1 AND index=$2',
	    array($group_id, $index));
	if ($res && db_numrows($res)) {
		$old_tab_url = db_result($res, 0, 'tab_url');
		$old_tab_name = db_result($res, 0, 'tab_name');
		$old_tab_type = db_result($res, 0, 'type');
		$old_tab_tooltip = db_result($res, 0, 'tooltip');
		if ($tab_new_url && $tab_new_url != 'http://') {
			$tab_new_url2 = $old_tab_url . ' -> ' . $tab_new_url;
		} else {
			$tab_new_url2 = '(unchanged) ' . $old_tab_url;
		}
		if ($old_tab_type === 0) {
			$old_tab_type2 = 'Link';
		} else if ($old_tab_type == -1) {
			$old_tab_type2 = 'unchanged';
		} else if ($old_tab_type == 1) {
			$old_tab_type2 = 'IFRAME';
		} else {
			$old_tab_type2 = 'unknown';
		}
		$old_tab_type2 .= '(' . $old_tab_type . ')';
		$new_type2 = $old_tab_type2 . '->' . $new_type2;
		if ($tab_rename) {
			$tab_name2 = '<' . $old_tab_name . '>' . $tab_rename;
		} else {
			$tab_name2 = '-unchanged<' . $old_tab_name;
		}
		if ($tab_new_tooltip) {
			$tab_name2 = '{' . $old_tab_tooltip . '|' . $tab_new_tooltip . '}';
		}
	}
  if (!($res = $group->addHistory('extratab-mod', $index . '=' . $new_type2 .
    $tab_name2, $tab_new_url2)) || db_affected_rows($res) < 1) {
	$error_msg = sprintf (_('Cannot insert audit trail entry: %s'),
	    db_error());
  } else {
	if ($tab_rename) {
		$res = db_query_params ('UPDATE plugin_extratabs_main SET tab_name=$1 WHERE group_id=$2 AND index=$3',
					array ($tab_rename,
						   $group_id,
						   $index));
		if (!$res || db_affected_rows($res) < 1) {
			$error_msg = sprintf (_('Cannot rename the tab: %s'), db_error());
		} else {
			$feedback .= ($feedback ? '. ' : '') . _('Tab successfully renamed');
			$done = 1;
		}
	}
	if ($tab_new_tooltip) {
		$res = db_query_params('UPDATE plugin_extratabs_main SET tooltip=$1 WHERE group_id=$2 AND index=$3',
					array ($tab_new_tooltip,
						   $group_id,
						   $index));
		if (!$res || db_affected_rows($res) < 1) {
			$error_msg = sprintf(_('Cannot change the tab tooltip: %s'), db_error());
		} else {
			$feedback .= ($feedback ? '. ' : '') . _('Tooltip successfully changed');
			$done = 1;
		}
	}
	if ($tab_new_url && $tab_new_url != 'http://') {
		if (!util_check_url($tab_new_url)) {
			$error_msg = _('ERROR: Malformed URL (only http, https and ftp allowed)');
		} else {
			$res = db_query_params ('UPDATE plugin_extratabs_main SET tab_url=$1 WHERE group_id=$2 AND index=$3',
					array ($tab_new_url,
						   $group_id,
						   $index));
			if (!$res || db_affected_rows($res) < 1) {
				$error_msg .= ($error_msg ? '. ' : '') . sprintf (_('Cannot change URL: %s'), db_error());
			} else {
				$feedback .= ($feedback ? '. ' : '') . _('URL successfully changed');
				$done = 1;
			}
		}
	}
	if ($new_type != -1) {
		$res = db_query_params ('UPDATE plugin_extratabs_main SET type=$1 WHERE group_id=$2 AND index=$3',
					array ($new_type,
						   $group_id,
						   $index));
		if (!$res || db_affected_rows($res) < 1) {
			$error_msg .= ($error_msg ? '. ' : '') . sprintf (_('Cannot set type: %s'), db_error());
		} else {
			$feedback .= ($feedback ? '. ' : '') . _('Type successfully changed');
			$done = 1;
		}
	}
  }
	if (!$error_msg && !$done) {
		$warning_msg .= ($warning_msg ? '. ' : '') . _('Nothing done');
	}
}
if (!$res) {
	db_rollback();
} else  {
	db_commit();
}

$adminheadertitle=sprintf(_('Manage extra tabs for project %1$s'), $group->getPublicName() );
project_admin_header(array('title'=>$adminheadertitle, 'group'=>$group->getID()));

?>

<div style="border:5px red groove; padding:12px; margin:12px;">
<h2><?php echo _('Legally important warning!'); ?></h2>
<p><?php echo _('Do <em>not</em> choose the type IFRAME for external content, that is, content not part of the Forge and/or outside of your control!'); ?></p>
<p><?php echo _('Doing so otherwise implies taking the content as if it were our own, opening the door for possible lawsuits, both over illicit content (the content embedded changed and was outside of our control) and copyright/trademark infringement (embedding external content is misrepresenting authorship, at the very least)! This responsibility cannot be disclaimed!'); ?></p>
<p><?php echo _('By submitting an IFRAME you hereby accept legal responsibility for the embedded content. This action <strong>will</strong> be logged.'); ?></p>
</div>

<h2><?php echo _('Add new tab'); ?></h2>

<p><?php echo _('You can add your own tabs in the menu bar with the form below.') ?></p>

<form name="new_tab" action="<?php echo util_make_uri ('/plugins/extratabs/'); ?>" method="post">
<fieldset>
<legend><?php echo _('Add new tab'); ?></legend>
<p>
<input type="hidden" name="group_id" value="<?php echo $group->getID() ?>" />
<input type="hidden" name="addtab" value="1" />
<strong><?php echo _('Name of the tab:') ?></strong><?php echo utils_requiredField(); ?>
<br />
<input type="text" size="20" maxlength="20" name="tab_name" value="<?php echo $tab_name ?>" /><br />
</p>
<p>
<strong><?php echo _('Tooltip:') ?></strong>
<br />
<input type="text" size="60" name="tab_tooltip" value="<?php echo util_html_secure($tab_tooltip) ?>" /><br />
</p>
<p>
<strong><?php echo _('URL of the tab:') ?></strong><?php echo utils_requiredField(); ?>
<br />
<input type="text" size="60" name="tab_url" value="<?php echo $tab_url ?>" /><br/>
</p>
<p>
<strong><?php echo _('Type of the tab:') ?></strong><?php echo utils_requiredField(); ?>
<br />
<input type="radio" name="type" value="0" checked="checked"/><?php echo _('Link') ?>
<input type="radio" name="type" value="1" /><?php echo _('IFRAME') ?>
</p>
<p>
<input type="submit" value="<?php echo _('Add tab') ?>" />
</p>
</fieldset>
</form>

<?php
	$res = db_query_params ('SELECT * FROM plugin_extratabs_main WHERE group_id=$1 ORDER BY index ASC', array ($group_id)) ;
$nbtabs = db_numrows($res) ;
if ($nbtabs > 0) {

?>

<h2><?php echo _('Modify extra tabs'); ?></h2>
<p>
<?php echo _('You can modify the tabs that you already added.');
?>
</p>

<form name="modify_tab" action="<?php echo util_make_uri('/plugins/extratabs/'); ?>" method="post">
<fieldset>
<legend><?php echo _('Modify tab'); ?></legend>
<input type="hidden" name="group_id" value="<?php echo $group->getID() ?>" />
<table>
<tr>
	<td><?php echo _('Tab to modify:') ?></td>
	<td><select name="index">
<?php
$options = '';
while ($row = db_fetch_array($res)) {
    if ($row['index'] == $selected) {
	$options .= "<option selected=\"selected\" value='" . $row['index'] . "'>" . util_html_secure($row['tab_name']) .  "</option>";
    } else {
	$options .= "<option value='" . $row['index'] . "'>" . util_html_secure($row['tab_name']) .  "</option>";
    }
}
echo $options;
?>
	</select></td>
</tr><tr>
	<td><?php echo _('Rename to:'); ?></td>
	<td><input type="text" size="20" maxlength="20" name="tab_rename" value="" /></td>
</tr><tr>
	<td><?php echo _('New tooltip (set to “-” to delete):'); ?></td>
	<td><input type="text" size="60" name="tab_new_tooltip" value="" /></td>
</tr><tr>
	<td><?php echo _('New URL:'); ?></td>
	<td><input type="text" size="60" name="tab_new_url" value="http://" /></td>
</tr><tr>
	<td><?php echo _('New type:'); ?></td>
	<td>
		<input type="radio" name="new_type" value="0" /><?php echo _('Link') ?>
		<input type="radio" name="new_type" value="1" /><?php echo _('IFRAME') ?>
	</td>
</tr><tr><td colspan="2">
<input type="submit" name="modify" value="<?php echo _('Modify tab') ?>" />
</td></tr>
</table>
</fieldset>
</form>

<h2><?php echo _('Move or delete extra tabs') ;?></h2>
<p>
<?php echo _('You can move and delete the tabs that you already added. Please note that those extra tabs can only appear after the standard tabs. And you can only move them inside the set of extra tabs.');
?>
</p>

<form name="change_tab" action="<?php echo util_make_uri('/plugins/extratabs/'); ?>" method="post">
<fieldset>
<legend><?php echo _('Move or delete tab'); ?></legend>
<p>
<input type="hidden" name="group_id" value="<?php echo $group->getID() ?>" />
<?php
	echo _('Tab to modify:')
?>
<select name="index">
<?php
echo $options;
?>
</select>
</p>
<p>
	  <?php if ($nbtabs > 1) { ?>
<input type="submit" name="up" value="<?php echo _('Move tab before') ?>" />
<input type="submit" name="down" value="<?php echo _('Move tab after') ?>" />
		  <?php } ?>
<input type="submit" name="delete" value="<?php echo _('Delete tab') ?>" />
</p>
</fieldset>
</form>

<?php
	  }
project_admin_footer(array());

// Local Variables:
// mode: php
// c-file-style: "bsd"
// End:

?>
