<?php

	/*
	 * NOTE: This file is world-readable for technical
	 * reasons, and, as such, MUST do its own auth
	 * stuff if used for private groups!
	 */

	$projectpage = "";
	if (isset($_SERVER['SCRIPT_NAME'])) {
		$projectpage = preg_replace('#^/www/([^/]+)/index.php$#',
		    'https://' . $_SERVER['HTTP_HOST'] .
		    '/projects/\1/?subpage=home',
		    $_SERVER['SCRIPT_NAME']);
		if ($projectpage == $_SERVER['SCRIPT_NAME'])
			$projectpage = "";
	}
	if (!$projectpage)
		$projectpage = preg_replace('#^([^\.]*)\.(.*)$#',
		    'https://\2/projects/\1/?subpage=home',
		    $_SERVER['HTTP_HOST']);

	header("Status: 301 Moved Permanently", true, 301);
	header("Location: ${projectpage}", true);
	echo "\nPlease go to ${projectpage} instead!\n";
	exit;
?>
