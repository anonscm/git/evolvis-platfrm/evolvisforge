<?php

require_once(dirname(__FILE__) . '/../phpFUnit.php');

/**
 * Syntax test class.
 *
 * @package	SyntaxTests
 * @author	Alain Peyrat <aljeux@free.fr>
 * @copyright	2009 Alain Peyrat. All rights reserved.
 * @license	http://www.opensource.org/licenses/gpl-license.php  GPL License
 */
class SyntaxTest extends phpFUnit_TestCase {
	/**
	 * First, make sure pcregrep is installed
	 */
	public function testPcRegrepInstalled() {
		$output = `type pcregrep >/dev/null; echo $?`;
		$rc = trim($output);
		if ($rc != '0') {
			$output = `type pcregrep`;
			$this->fail('You should probably install "pcregrep" : `type pcregrep` reports "'.$output);
		}
		$this->assertEquals('0', $rc);
	}

	/**
	 * Validate all php code with php -l.
	 */
	public function testPhpSyntax() {
		$output = `find . -name '*.php' -type f -exec php -l {} \; | grep -v '^No syntax errors detected'`;
		$this->assertEquals('', $output);
	}

	/**
	 * Validate all scripts with isutf8.
	 */
	public function testUTF8Chars() {
		$output = `find . -type f -a \( -name '*.php' -o -name '*.sql' -o -name '*.sh' -o -name '*.pl' \) | xargs isutf8`;
		$this->assertEquals('', $output);
	}

	/**
	 * Ensure all scripts use Unix-style line endings
	 */
	public function testUnixLineEndings() {
		$output = `find . -type f -a \( -name '*.php' -o -name '*.sql' -o -name '*.sh' -o -name '*.pl' \) | xargs pcregrep -l '\r$'`;
		$this->assertEquals('', $output);
	}

	/**
	 * Ensure no scripts have SVN conflicts markers
	 */
	public function testSVNConflicts() {
		$output = `find . -type f | xargs grep -l '^<<<<<<'`;
		$this->assertEquals('', $output);
		$output = `find . -type f | xargs grep -l '^>>>>>>'`;
		$this->assertEquals('', $output);
	}

	/**
	 * Ensure no script has an empty last line
	 */
	public function testEmptyLastLine() {
		$output = `find . -name '*.php' -type f | while read i ; do [ -s \$i ] && [ -z "\$(tail -n 1 \$i)" ] && echo \$i ; done`;
		$this->assertEquals('', $output);
	}

	/**
	 * Validate syntax of gettextfiles
	 */
	public function testGettextSyntax() {
		$output = `./utils/manage-translations.sh check 2>&1`;
		$this->assertEquals('', $output);
	}
}

// Local Variables:
// mode: php
// c-file-style: "bsd"
// End:
