ServerName {core/lists_host}

Alias /robots.txt /etc/gforge/custom/robots.txt

RedirectMatch permanent ^/$ https://{core/web_host}/mailman/listinfo
RedirectMatch permanent ^/(pipermail/.*) https://{core/web_host}/$1
RedirectMatch permanent ^/(mailman/.*) https://{core/web_host}/$1
RedirectMatch permanent ^/cgi-bin/(mailman/.*) https://{core/web_host}/$1
RedirectMatch permanent ^/(images/mailman/.*) https://{core/web_host}/mailman/$1
