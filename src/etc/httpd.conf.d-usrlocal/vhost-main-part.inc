DocumentRoot /usr/local/share/gforge/www
DirectoryIndex index.htm index.html index.php

<IfModule mod_userdir.c>
  UserDir disabled
</IfModule>

# Define configuration/env variables for passing passwords and other secrets to PHP
<Directory /usr/local/share/gforge/www>
  Include /etc/gforge/httpd.conf.d/secrets.inc
</Directory>

<Location /projects>
  ForceType application/x-httpd-php
</Location>
<Location /users>
  ForceType application/x-httpd-php
</Location>

IncludeOptional /etc/gforge/httpd.conf.d/plugin-*.inc

Alias /images/ /usr/local/share/gforge/www/images/
Alias /robots.txt /etc/gforge/custom/robots.txt

Include /etc/gforge/httpd.conf.d/projects-in-mainvhost.inc

AddHandler cgi-script .cgi
ScriptAlias /mailman/ /usr/lib/cgi-bin/mailman/

Alias /pipermail /var/lib/mailman/archives/public
<Directory /var/lib/mailman/archives/public>
    AllowOverride Options
    Options FollowSymLinks Indexes
</Directory>

RedirectMatch 301 ^/wsdl(/.*)*$ https://evolvis.org/plugins/scmgit/cgi-bin/gitweb.cgi?p=evolvis-platfrm/evolvisforge.git;a=tree;f=src/www/soap;hb=refs/heads/oldstable
