
# We are inside <VirtualHost ...>

ServerName {core/web_host}
ServerAlias www.{core/web_host}
ServerAdmin webmaster@{core/web_host}

RedirectMatch permanent ^/cgi-bin/(mailman/.*) https://{core/web_host}/$1

RedirectMatch 301 ^/wsdl.xml https://{core/web_host}/soap/index.php?wsdl

Include /etc/gforge/httpd.conf.d/vhost-main-part.inc
