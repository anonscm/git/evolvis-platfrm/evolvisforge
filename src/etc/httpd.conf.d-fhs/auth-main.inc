# HTTP Basic Auth hook for the forge itself

# By default, allow everything
Require all granted

# Sample: use PAM for forge user authentification
# as backend for HTTP Basic Auth (just uncomment)
#AuthType Basic
#AuthName "Forge Login"
#AuthPAM_Enabled on
#AuthBasicAuthoritative off
#AuthUserFile /dev/null
#require valid-user
#Satisfy All
