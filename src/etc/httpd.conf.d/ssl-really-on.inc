Header always set Strict-Transport-Security "max-age=31536000; includeSubDomains"

<IfModule mod_ssl.c>
  SSLEngine on

  SSLCertificateFile {core/config_path}/ssl-cert.pem
  SSLCertificateKeyFile {core/config_path}/ssl-cert.key
  SSLCertificateChainFile {core/config_path}/ssl-cert.ca
  # Add extra SSL configuration (e.g. SSLCACertificatePath) here

  SSLProtocol All -SSLv2 -SSLv3
  # Protect from the CRIME attack:
  SSLCompression off
  SSLCipherSuite kEECDH+aRSA:kEDH+aRSA:!COMPLEMENTOFDEFAULT:!ADH:!AECDH:-MEDIUM:!LOW:!EXPORT:!aNULL:!eNULL:!PSK:!aECDH:!aDSS:!DES:!MD5
  SSLHonorCipherOrder on
  # This is not valid inside a VirtualHost, do it globally:
  #SSLSessionCache "nonenotnull"

  <Files ~ "\.(cgi|shtml)$">
    SSLOptions +StdEnvVars
  </Files>

  <Directory "/usr/lib/cgi-bin">
    SSLOptions +StdEnvVars
  </Directory>

  SetEnvIf User-Agent ".*MSIE.*" nokeepalive ssl-unclean-shutdown
</IfModule>

<IfModule apache_ssl.c>
  SSLEnable

  SetEnvIf User-Agent ".*MSIE.*" nokeepalive ssl-unclean-shutdown
</IfModule>
