#!/usr/bin/php
<?php
/*-
 * Delete a forge user from the command line
 *
 * Copyright © 2011
 *	Thorsten Glaser <t.glaser@tarent.de>
 * Copyright © 2010
 *	Roland Mas
 * All rights reserved.
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *-
 * Mark a forge user as deleted, including things like group memberships.
 */

function usage($rc=1) {
	echo "Usage: .../user-del.php username username\n";
	exit($rc);
}

if (count($argv) != 3) {
	usage();
}
$argv0 = array_shift($argv);
$u_name = array_shift($argv);
$u_check = array_shift($argv);

if ($u_name == '-h') {
	usage(0);
}

if ($u_name !== $u_check) {
	usage();
}

require dirname(__FILE__).'/../common/include/env.inc.php';
require_once $gfwww.'include/squal_pre.php';
require_once $gfcommon.'include/cron_utils.php';

// Fake admin user login
$G_SESSION = user_get_object_by_name("admin");
$G_SESSION->is_logged_in = true;
$G_SESSION->is_super_user = true;

// Plugins subsystem
require_once $gfcommon.'include/Plugin.class.php';
require_once $gfcommon.'include/PluginManager.class.php';

// SCM-specific plugins subsystem
require_once $gfcommon.'include/SCMPlugin.class.php';

setup_plugin_manager();

$u_obj = user_get_object_by_name($u_name);
if (!$u_obj || $u_obj->isError()) {
	echo "Wrong user! ";
	if ($u_obj) {
		echo $u_obj->getErrorMessage();
	}
	echo "\n";
	usage();
}

if (!$u_obj->delete(true)) {
	echo "Error! " . $u_obj->getErrorMessage() . "\n";
	exit(1);
}
