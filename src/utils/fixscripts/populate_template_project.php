#! /usr/bin/php
<?php
/**
 * Copyright 2010 Roland Mas
 * Copyright © 2012, 2013
 *	Thorsten Glaser <t.glaser@tarent.de>
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

require dirname(__FILE__).'/../common/include/env.inc.php';
require_once $gfcommon.'include/pre.php';

$err='';

// Plugins subsystem
require_once('common/include/Plugin.class.php') ;
require_once('common/include/PluginManager.class.php') ;

setup_plugin_manager () ;
session_set_admin () ;

function usage($rc=1) {
	echo "Usage:\n";
	echo "\t.../populate_template_project.php new unixname groupname\n";
	exit($rc);
}

function hasmailinglist($project, $listname) {
	$mlFactory = new MailingListFactory($project);
	if (!$mlFactory || !is_object($mlFactory) || $mlFactory->isError()) {
		return false;
	}
	$mlArray = $mlFactory->getMailingLists();
	if ($mlFactory->isError()) {
		return false;
	}
	$listname = $project->getUnixName() . '-' . $listname;
	foreach ($mlArray as $mlEntry) {
		if ($mlEntry->getName() == $listname) {
			return true;
		}
	}
	return false;
}

function populateProject($project) {
	db_begin();

	$role = new Role($project);
	$todo = array_keys($role->defaults);
	for ($c=0; $c<count($todo); $c++) {
		if (!($role_id = $role->createDefault($todo[$c]))) {
			if ($todo[$c] == 'Admin') {
				/*
				 * silently ignore this error for the one
				 * default role always created, even for
				 * emptily started projects
				 */
				continue;
			}
			$project->setError(sprintf('R%d (%s): %s',
			    $c, $todo[$c], $role->getErrorMessage()));
			db_rollback();
			setup_gettext_from_context();
			return false;
		}
	}

	if (forge_get_config ('use_tracker')) {
		$ats = new ArtifactTypes($project);
		if (!$ats || !is_object($ats)) {
			$project->setError(_('Error creating ArtifactTypes object'));
			db_rollback();
			setup_gettext_from_context();
			return false;
		} else if ($ats->isError()) {
			$project->setError(sprintf (_('ATS%d: %s'), 1, $ats->getErrorMessage()));
			db_rollback();
			setup_gettext_from_context();
			return false;
		}
		if (!$ats->createTrackers()) {
			$project->setError(sprintf (_('ATS%d: %s'), 2, $ats->getErrorMessage()));
			db_rollback();
			setup_gettext_from_context();
			return false;
		}
	}

	$ra = RoleAnonymous::getInstance() ;
	$rl = RoleLoggedIn::getInstance() ;
	$ra->linkProject ($project) ;
	$rl->linkProject ($project) ;

	$ra->setSetting ('project_read', $project->getID(), 1) ;
	$rl->setSetting ('project_read', $project->getID(), 1) ;

	$ra->setSetting ('frs', $project->getID(), 1) ;
	$rl->setSetting ('frs', $project->getID(), 1) ;

	$ra->setSetting ('docman', $project->getID(), 1) ;
	$rl->setSetting ('docman', $project->getID(), 1) ;

	$ra->setSetting('scm', $project->getID(), 1);
	$rl->setSetting('scm', $project->getID(), 1);

	$atf = new ArtifactTypeFactory ($project) ;
	foreach ($atf->getAllArtifactTypeIds() as $atid) {
		$at = artifactType_get_object ($atid) ;
		if ($at->isPublic()) {
			$ra->setSetting ('tracker', $atid, 1) ;
			$rl->setSetting ('tracker', $atid, 1) ;
		}
	}

	if (forge_get_config('use_mail')) {
		$mlist = new MailingList($project);
		if (!hasmailinglist($project, 'commits') &&
		    !$mlist->create('commits',_('Commits'),1,session_get_user()->getID())) {
			$project->setError(sprintf(_('ML: %s'),$mlist->getErrorMessage()));
			db_rollback();
			setup_gettext_from_context();
			return false;
		}
		if (!hasmailinglist($project, 'discuss') &&
		    !$mlist->create('discuss',_('Discussion'),1,session_get_user()->getID())) {
			$project->setError(sprintf(_('ML: %s'),$mlist->getErrorMessage()));
			db_rollback();
			setup_gettext_from_context();
			return false;
		}
	}
	$project->normalizeAllRoles () ;

	db_commit();
	return true;
}

if (count($argv) < 2) {
	usage();
} else if (in_array($argv[1], array('-h', '-?', '--help'))) {
	usage(0);
} else if (count($argv) == 4 && $argv[1] == "new") {
	db_begin();
	$project = new Group();
	$desc = sprintf("Template project %s (%s) populated on %s",
	    $argv[2], $argv[3], date("r"));
	if (!$project->create(session_get_user(), $argv[3], $argv[2],
	    $desc, $desc)) {
		db_rollback();
		printf("Error: could not create group: %s\n",
		    $project->getErrorMessage());
		exit(1);
	}
	if (!db_query_params('UPDATE groups SET
			use_forum=0,
			use_tracker=1,
			use_mail=1,
			use_pm=0,
			use_docman=0,
			use_news=0,
			use_scm=1,
			use_frs=0,
			use_stats=0,
			use_survey=0,
			use_ftp=0,
			use_webdav=0
		WHERE group_id=$1',
	    array($project->getID()))) {
		printf("Error updating project information: %s\n",
		    db_error());
		db_rollback();
		exit(1);
	}

	if (!$project->setAsTemplate(true)) {
		printf("Error: could not mark group as template: %s\n",
		    db_error());
		db_rollback();
		exit(1);
	}
	if (!populateProject($project)) {
		printf("Error: could not populate new group: %s\n",
		    $project->getErrorMessage());
		exit(1);
	}
	db_commit();
} else {
	usage();
}

printf("Group #%d %s (%s) populated successfully.\n", $project->getID(),
    $project->getUnixName(), $project->getPublicName(true));
exit(0);
