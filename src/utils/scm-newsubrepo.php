#!/usr/bin/php
<?php
/*-
 * Create new SCM subrepository for an existing group.
 *
 * Copyright © 2011
 *	Thorsten Glaser <t.glaser@tarent.de>
 * Copyright © 2010
 *	Roland Mas
 * All rights reserved.
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *-
 * Create new SCM subrepository for an existing group, if the SCM
 * supports this (svn doesn’t but git does).
 */

function usage($rc=1) {
	echo "Usage: .../scm-newsubrepo.php groupname newreponame\n";
	exit($rc);
}

if (count($argv) != 3) {
	usage();
}
$argv0 = array_shift($argv);
$p_groupname = array_shift($argv);
$p_reponame = array_shift($argv);

require_once(dirname(__FILE__).'/../common/include/env.inc.php');
require_once $gfwww.'include/squal_pre.php';
require_once $gfcommon.'include/cron_utils.php';

// Plugins subsystem
require_once $gfcommon.'include/Plugin.class.php';
require_once $gfcommon.'include/PluginManager.class.php';

// SCM-specific plugins subsystem
require_once $gfcommon.'include/SCMPlugin.class.php';

// Fake admin user login
session_set_admin();

setup_plugin_manager();

$group = group_get_object_by_name($p_groupname);
if (!$group || $group->isError()) {
	echo "Wrong group! ";
	if ($group) {
		echo $group->getErrorMessage();
	}
	echo "\n";
	usage();
}

/* ltrim and rtrim, strip leading/trailing slash and ".git" */
$p_reponame = ltrim(preg_replace('#(/|.git)*$#', '', trim($p_reponame)), "/");

foreach (explode("/", $p_reponame) as $p_reponamepart) {
	if (!account_groupnamevalid($p_reponamepart)) {
		echo "Invalid subrepo name! " . $GLOBALS['register_error'] . "\n";
		usage();
	}
}

$hook_params = array(
	'group_id' => $group->getID(),
	'newrepo' => $p_reponame,
    );
if (plugin_hook('scm_create_subrepo', $hook_params)) {
	echo "Failed! Maybe the plugin doesn't support this?\n";
	usage();
}

$hook_params = array();
plugin_hook('scm_update_repolist', $hook_params);
