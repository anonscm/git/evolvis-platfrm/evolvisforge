#! /usr/bin/php
<?php
/**
 * Copyright 2010 Roland Mas
 * Copyright © 2010, 2012
 *      Thorsten “mirabilos” Glaser <t.glaser@tarent.de>
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

require (dirname(__FILE__).'/../common/include/env.inc.php');
require_once $gfcommon.'include/pre.php';

$err='';

// Plugins subsystem
require_once('common/include/Plugin.class.php') ;
require_once('common/include/PluginManager.class.php') ;

setup_plugin_manager () ;
session_set_admin () ;

$res = db_query_params ('SELECT role_id FROM pfo_role ORDER BY role_id',
			array ());

$rows=db_numrows($res);

for ($i=0; $i<$rows; $i++) {
	$role = RBACEngine::getInstance()->getRoleById (db_result($res,$i,'role_id')) ;
	echo "Normalising role #" . ($i + 1) . "/" . $rows . ": " .
	    $role->getDisplayableName() . "\n";
	$role->normalizeData() ;
}

$params = array();
plugin_hook("normalise_roles", $params);

$res = db_query_params('SELECT group_id FROM groups', array());
$rows = db_numrows($res);
for ($i = 0; $i < $rows; ++$i) {
	$group_id = db_result($res, $i, 'group_id');
	$g =& group_get_object($group_id);
	echo "Normalising group #" . ($i + 1) . "/" . $rows . ": " .
	    $g->getPublicName(true) . "... ";
	if (!$SYS->sysCheckCreateGroup($group_id)) {
		echo "failed! " . $SYS->getErrorMessage() . "\n";
	} else {
		echo "done\n";
	}
}
