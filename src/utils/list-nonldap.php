#!/usr/bin/php
<?php
/*-
 * List all users that do *not* have the LDAP role, for FusionForge
 *
 * Copyright © 2014
 *	Thorsten “mirabilos” Glaser <t.glaser@tarent.de>
 * All rights reserved.
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *-
 * Suggest to pipe the output through sort | less for better viewing.
 */

require (dirname (__FILE__).'/../common/include/env.inc.php');
require_once $gfcommon.'include/pre.php';

$res = db_query_params('SELECT user_id FROM users ORDER BY user_name', array());
if (!$res) {
	echo "E: could not fetch users: " . db_error() . "\n";
	exit(1);
}
$users = user_get_objects(util_result_column_to_array($res, 0));
foreach ($users as $u) {
	/* skip the none and admin user */
	if ($u->getID() == 100 || $u->getID() == 101)
		continue;

	$is_ldap = false;
	foreach ($u->getRoles() as $r) {
		if (!($r instanceof Evolvis_RoleExternal))
			continue;
		if ($r->getName() != 'LDAP users')
			continue;
		$is_ldap = true;
	}
	if (!$is_ldap)
		echo $u->getStatus() . $u->getUnixStatus() . " " .
		    $u->getUnixName() . ' ' .
		    util_make_uri('/admin/useredit.php?user_id=' .
		    $u->getID()) . "\n";
}
