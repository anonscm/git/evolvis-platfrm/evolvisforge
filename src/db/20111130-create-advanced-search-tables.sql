CREATE TABLE advancedquery (
	id SERIAL PRIMARY KEY,
	name VARCHAR(56) NOT NULL,
	query TEXT NOT NULL
);

-- This table identifies all system wide queries (appearing in every tracker)
CREATE TABLE advancedquery_is_system_query (
	query_id INT PRIMARY KEY REFERENCES advancedquery(id)
);

-- This table identifies all queries that are private by one user.
CREATE TABLE user_has_query (
	user_id INT REFERENCES users(user_id),
	query_id INT REFERENCES advancedquery(id),
	group_artifact_id INT NOT NULL REFERENCES artifact_group_list(group_artifact_id),
	PRIMARY KEY (user_id, query_id, group_artifact_id)
);

-- This table identifies all queries that are global for one specific tracker.
CREATE TABLE artifact_group_has_query (
	group_artifact_id INT NOT NULL REFERENCES artifact_group_list(group_artifact_id),
	query_id INT REFERENCES advancedquery(id),
	PRIMARY KEY (group_artifact_id, query_id)
);
