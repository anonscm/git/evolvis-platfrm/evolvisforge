--
-- Add new processor and file types to the FRS
--

-- Rename some ancient weird ones
UPDATE frs_processor SET name='PowerPC' WHERE processor_id=2000;
UPDATE frs_processor SET name='SPARC' WHERE processor_id=4000;

-- Move sparc64 right under sparc
INSERT INTO frs_processor (processor_id, name)
	VALUES (4001, 'UltraSPARC');
UPDATE frs_file SET processor_id=4001 WHERE processor_id=5000;
DELETE FROM frs_processor WHERE processor_id=5000;

-- Add new ones
INSERT INTO frs_processor (processor_id, name)
	VALUES (1001, 'amd64');
INSERT INTO frs_processor (processor_id, name)
	VALUES (5000, 'ARM');
INSERT INTO frs_processor (processor_id, name)
	VALUES (5001, 'ARM64');

-- Change some filetypes
UPDATE frs_filetype SET name='Text' WHERE type_id=8100;
UPDATE frs_filetype SET name='HTML' WHERE type_id=8200;
UPDATE frs_filetype SET name='PDF' WHERE type_id=8300;

-- Add new filetypes
INSERT INTO frs_filetype (type_id, name)
	VALUES (2100, 'DOS COM/EXE');
INSERT INTO frs_filetype (type_id, name)
	VALUES (2101, '16-bit Windows');
INSERT INTO frs_filetype (type_id, name)
	VALUES (2102, '16-bit OS/2');
INSERT INTO frs_filetype (type_id, name)
	VALUES (2103, '32-bit OS/2');
INSERT INTO frs_filetype (type_id, name)
	VALUES (2104, '32-bit Windows');
INSERT INTO frs_filetype (type_id, name)
	VALUES (2105, '64-bit Windows');
INSERT INTO frs_filetype (type_id, name)
	VALUES (2106, 'Windows CE EXE');
INSERT INTO frs_filetype (type_id, name)
	VALUES (2107, 'Windows CE CAB');
INSERT INTO frs_filetype (type_id, name)
	VALUES (2200, 'APK');
INSERT INTO frs_filetype (type_id, name)
	VALUES (2300, 'Macintosh');
INSERT INTO frs_filetype (type_id, name)
	VALUES (2301, 'MacOS X');
INSERT INTO frs_filetype (type_id, name)
	VALUES (2302, 'DMG');
INSERT INTO frs_filetype (type_id, name)
	VALUES (2303, 'iPhoneOS');
INSERT INTO frs_filetype (type_id, name)
	VALUES (3101, '.tar.bz2');
INSERT INTO frs_filetype (type_id, name)
	VALUES (3102, '.cpio.bz2');
INSERT INTO frs_filetype (type_id, name)
	VALUES (3111, '.tar.gz');
INSERT INTO frs_filetype (type_id, name)
	VALUES (3112, '.cpio.gz');
INSERT INTO frs_filetype (type_id, name)
	VALUES (3120, '.xz');
INSERT INTO frs_filetype (type_id, name)
	VALUES (3121, '.tar.xz');
INSERT INTO frs_filetype (type_id, name)
	VALUES (3122, '.cpio.xz');
INSERT INTO frs_filetype (type_id, name)
	VALUES (3900, 'Other Archive');
INSERT INTO frs_filetype (type_id, name)
	VALUES (5011, 'Source .tar.bz2');
INSERT INTO frs_filetype (type_id, name)
	VALUES (5012, 'Source .cpio.bz2');
INSERT INTO frs_filetype (type_id, name)
	VALUES (5021, 'Source .tar.gz');
INSERT INTO frs_filetype (type_id, name)
	VALUES (5022, 'Source .cpio.gz');
INSERT INTO frs_filetype (type_id, name)
	VALUES (5031, 'Source .tar.xz');
INSERT INTO frs_filetype (type_id, name)
	VALUES (5032, 'Source .cpio.xz');
INSERT INTO frs_filetype (type_id, name)
	VALUES (8010, '.png');
INSERT INTO frs_filetype (type_id, name)
	VALUES (8011, '.mng');
INSERT INTO frs_filetype (type_id, name)
	VALUES (8020, '.gif');
INSERT INTO frs_filetype (type_id, name)
	VALUES (8030, 'Photoshop PSD');
INSERT INTO frs_filetype (type_id, name)
	VALUES (8031, 'Gimp XCF');
INSERT INTO frs_filetype (type_id, name)
	VALUES (8040, 'SVG');
INSERT INTO frs_filetype (type_id, name)
	VALUES (8301, 'PostScript');
INSERT INTO frs_filetype (type_id, name)
	VALUES (8302, 'EPS');
INSERT INTO frs_filetype (type_id, name)
	VALUES (8400, 'Audio/Video');
INSERT INTO frs_filetype (type_id, name)
	VALUES (8410, 'MPEG-4/DivX');
INSERT INTO frs_filetype (type_id, name)
	VALUES (8420, 'MPEG/MP3');
INSERT INTO frs_filetype (type_id, name)
	VALUES (8430, 'Vorbis/Theora');
INSERT INTO frs_filetype (type_id, name)
	VALUES (8440, 'FLV');
INSERT INTO frs_filetype (type_id, name)
	VALUES (8450, 'QuickTime');
INSERT INTO frs_filetype (type_id, name)
	VALUES (8460, 'M-JPEG');
INSERT INTO frs_filetype (type_id, name)
	VALUES (8500, 'Any Office Suite');
INSERT INTO frs_filetype (type_id, name)
	VALUES (8510, 'OpenDocument');
INSERT INTO frs_filetype (type_id, name)
	VALUES (8511, 'old OpenOffice');
INSERT INTO frs_filetype (type_id, name)
	VALUES (8520, 'OOXML');
INSERT INTO frs_filetype (type_id, name)
	VALUES (8521, 'MS Office');
INSERT INTO frs_filetype (type_id, name)
	VALUES (8530, 'Softmaker Office');
INSERT INTO frs_filetype (type_id, name)
	VALUES (8540, 'Lotus SmartSuite');
INSERT INTO frs_filetype (type_id, name)
	VALUES (8900, 'Other Documentation');
