-- We disabled all nōn-Evolvis themes, so migrate everyone to ours.

UPDATE users SET theme_id=(SELECT theme_id FROM themes WHERE dirname='evolvis');
