-- mediawiki
DROP TABLE IF EXISTS interwiki;

-- no longer written to *or* rotated
TRUNCATE activity_log;
TRUNCATE frs_dlstats_file;

-- old migration functions
DROP FUNCTION IF EXISTS migrate_rbac_permissions_to_pfo_rbac();
DROP FUNCTION IF EXISTS pfo_rbac_permissions_from_old(int, text, int);

-- mediawiki perms
DELETE FROM pfo_role_setting WHERE section_name LIKE 'plugin\_mediawiki\_%';
DELETE FROM role_setting WHERE section_name LIKE 'plugin\_mediawiki\_%';
