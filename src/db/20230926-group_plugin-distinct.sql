-- fix duplicates in GitPlugin.class.php $this->getGroups();

ALTER TABLE group_plugin
   ADD CONSTRAINT group_plugin_distinct UNIQUE (group_id, plugin_id);
