-- Copyright © 2011
--	Mike Esser <m.esser@tarent.de>
--	Thorsten Glaser <t.glaser@tarent.de>
--	Roland Mas <roland@gnurandal.com>
-- All rights reserved.
--
-- This file is part of FusionForge. FusionForge is free software;
-- you can redistribute it and/or modify it under the terms of the
-- GNU General Public License as published by the Free Software
-- Foundation; either version 2 of the Licence, or (at your option)
-- any later version.
--
-- FusionForge is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License along
-- with FusionForge; if not, write to the Free Software Foundation, Inc.,
-- 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

CREATE OR REPLACE FUNCTION std_search_new_tracker(integer) RETURNS void AS '
DECLARE
	curBugTracker ALIAS FOR $1;
	valuesToSort  RECORD;
	valueString   TEXT;
BEGIN
	valueString := '''';
	FOR valuesToSort IN SELECT * FROM artifact_extra_field_elements
	WHERE extra_field_id = (
	    SELECT extra_field_id FROM artifact_extra_field_list
	    WHERE field_name = ''Resolution'' AND group_artifact_id=curBugTracker)
	LOOP
		-- Check if this is relevant for Quality Assurance
		IF valuesToSort.element_name = ''Fixed''
		    OR valuesToSort.element_name LIKE ''Won%t Fix''
		    OR valuesToSort.element_name = ''Invalid''
		    OR valuesToSort.element_name = ''Works For Me''
		    OR valuesToSort.element_name = ''Duplicate''
		THEN
			valueString := (valueString || (valuesToSort.element_id || '',''));
		END IF;
	END LOOP;
	IF valueString != '''' THEN
		INSERT INTO artifact_query(group_artifact_id, user_id, query_name) VALUES (curBugTracker, 100, ''STD Quality Assurance'');
		INSERT INTO artifact_query_fields VALUES ((SELECT max(artifact_query_id) FROM artifact_query), ''2'', 0, ''1'');
		INSERT INTO artifact_query_fields VALUES ((SELECT max(artifact_query_id) FROM artifact_query), ''1'', 0, ''0'');
		INSERT INTO artifact_query_fields VALUES ((SELECT max(artifact_query_id) FROM artifact_query), ''3'', 0, '''');
		INSERT INTO artifact_query_fields VALUES ((SELECT max(artifact_query_id) FROM artifact_query), ''7'', 0, '''');
		INSERT INTO artifact_query_fields VALUES ((SELECT max(artifact_query_id) FROM artifact_query), ''8'', 0, '''');
		INSERT INTO artifact_query_fields VALUES ((SELECT max(artifact_query_id) FROM artifact_query), ''5'', 0, ''artifact_id'');
		INSERT INTO artifact_query_fields VALUES ((SELECT max(artifact_query_id) FROM artifact_query), ''6'', 0, ''ASC'');
		valueString := SUBSTRING(valueString, 0, LENGTH(valueString) - 1);
		RAISE NOTICE ''Current String = %'', valueString;
		INSERT INTO artifact_query_fields VALUES ((SELECT max(artifact_query_id) FROM artifact_query), ''4'',
		    (SELECT extra_field_id FROM artifact_extra_field_list WHERE field_name LIKE ''Resolution'' AND group_artifact_id=curBugTracker), valueString);
	END IF;

	valueString := '''';
	FOR valuesToSort IN SELECT * FROM artifact_extra_field_elements
	WHERE extra_field_id = (
	    SELECT extra_field_id FROM artifact_extra_field_list
	    WHERE field_name = ''Resolution'' AND group_artifact_id=curBugTracker)
	LOOP
		-- Check if this is relevant for Quality Assurance
		IF valuesToSort.element_name = ''Accepted As Bug''
		    OR valuesToSort.element_name = ''Awaiting Response''
		THEN
			valueString := (valueString || (valuesToSort.element_id || '',''));
		END IF;
	END LOOP;
	IF valueString != '''' THEN
		INSERT INTO artifact_query(group_artifact_id, user_id, query_name) VALUES (curBugTracker, 100, ''STD Development'');
		INSERT INTO artifact_query_fields VALUES ((SELECT max(artifact_query_id) FROM artifact_query), ''2'', 0, ''1'');
		INSERT INTO artifact_query_fields VALUES ((SELECT max(artifact_query_id) FROM artifact_query), ''1'', 0, ''0'');
		INSERT INTO artifact_query_fields VALUES ((SELECT max(artifact_query_id) FROM artifact_query), ''3'', 0, '''');
		INSERT INTO artifact_query_fields VALUES ((SELECT max(artifact_query_id) FROM artifact_query), ''7'', 0, '''');
		INSERT INTO artifact_query_fields VALUES ((SELECT max(artifact_query_id) FROM artifact_query), ''8'', 0, '''');
		INSERT INTO artifact_query_fields VALUES ((SELECT max(artifact_query_id) FROM artifact_query), ''5'', 0, ''artifact_id'');
		INSERT INTO artifact_query_fields VALUES ((SELECT max(artifact_query_id) FROM artifact_query), ''6'', 0, ''ASC'');
		valueString := SUBSTRING(valueString, 0, LENGTH(valueString) - 1);
		RAISE NOTICE ''Current String = %'', valueString;
		INSERT INTO artifact_query_fields VALUES ((SELECT max(artifact_query_id) FROM artifact_query), ''4'',
		    (SELECT extra_field_id FROM artifact_extra_field_list WHERE field_name LIKE ''Resolution'' AND group_artifact_id=curBugTracker), valueString);
	END IF;

	valueString := '''';
	FOR valuesToSort IN SELECT * FROM artifact_extra_field_elements
	WHERE extra_field_id = (
	    SELECT extra_field_id FROM artifact_extra_field_list
	    WHERE field_name = ''Severity'' AND group_artifact_id=curBugTracker)
	LOOP
		-- Check if this is relevant for Quality Assurance
		IF valuesToSort.element_name = ''blocker''
		    OR valuesToSort.element_name = ''critical''
		THEN
			valueString := (valueString || (valuesToSort.element_id || '',''));
		END IF;
	END LOOP;
	IF valueString != '''' THEN
		INSERT INTO artifact_query(group_artifact_id, user_id, query_name) VALUES (curBugTracker, 100, ''STD Blocker/Critical'');
		INSERT INTO artifact_query_fields VALUES ((SELECT max(artifact_query_id) FROM artifact_query), ''2'', 0, ''1'');
		INSERT INTO artifact_query_fields VALUES ((SELECT max(artifact_query_id) FROM artifact_query), ''1'', 0, ''0'');
		INSERT INTO artifact_query_fields VALUES ((SELECT max(artifact_query_id) FROM artifact_query), ''3'', 0, '''');
		INSERT INTO artifact_query_fields VALUES ((SELECT max(artifact_query_id) FROM artifact_query), ''7'', 0, '''');
		INSERT INTO artifact_query_fields VALUES ((SELECT max(artifact_query_id) FROM artifact_query), ''8'', 0, '''');
		INSERT INTO artifact_query_fields VALUES ((SELECT max(artifact_query_id) FROM artifact_query), ''5'', 0, ''artifact_id'');
		INSERT INTO artifact_query_fields VALUES ((SELECT max(artifact_query_id) FROM artifact_query), ''6'', 0, ''ASC'');
		valueString := SUBSTRING(valueString, 0, LENGTH(valueString) - 1);
		RAISE NOTICE ''Current String = %'', valueString;
		INSERT INTO artifact_query_fields VALUES ((SELECT max(artifact_query_id) FROM artifact_query), ''4'',
		    (SELECT extra_field_id FROM artifact_extra_field_list WHERE field_name LIKE ''Severity'' AND group_artifact_id=curBugTracker), valueString);
	END IF;
END;'
	LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION Iu7quah7() RETURNS void
    LANGUAGE plpgsql
    AS $_$
BEGIN
    BEGIN
	ALTER TABLE artifact_history ADD COLUMN new_value text;
    EXCEPTION
	WHEN duplicate_column THEN
	     RETURN;
    END;
END;$_$;
SELECT Iu7quah7();
CREATE OR REPLACE FUNCTION Iu7quah7() RETURNS void
    LANGUAGE plpgsql
    AS $_$
BEGIN
    BEGIN
	ALTER TABLE project_history ADD COLUMN new_value text;
    EXCEPTION
	WHEN duplicate_column THEN
	     RETURN;
    END;
END;$_$;
SELECT Iu7quah7();
CREATE OR REPLACE FUNCTION Iu7quah7() RETURNS void
    LANGUAGE plpgsql
    AS $_$
BEGIN
    BEGIN
	ALTER TABLE group_history ADD COLUMN new_value text;
    EXCEPTION
	WHEN duplicate_column THEN
	     RETURN;
    END;
END;$_$;
SELECT Iu7quah7();

CREATE OR REPLACE VIEW artifact_history_user_vw AS SELECT ah.id, ah.artifact_id, ah.field_name, ah.old_value, ah.entrydate, users.user_name, ah.new_value FROM artifact_history ah, users WHERE (ah.mod_by = users.user_id);
CREATE OR REPLACE VIEW project_history_user_vw AS SELECT users.realname, users.email, users.user_name, project_history.project_history_id, project_history.project_task_id, project_history.field_name, project_history.old_value, project_history.mod_by, project_history.mod_date, project_history.new_value FROM users, project_history WHERE (project_history.mod_by = users.user_id);

CREATE OR REPLACE FUNCTION Iu7quah7() RETURNS void
    LANGUAGE plpgsql
    AS $_$
BEGIN
    BEGIN
	CREATE TABLE scm_subrepos (
		did serial PRIMARY KEY,
		group_id int not null CONSTRAINT group_id REFERENCES groups(group_id) ON DELETE CASCADE,
		newrepo text
	);
    EXCEPTION
	WHEN OTHERS THEN
	     RETURN;
    END;
END;$_$;
SELECT Iu7quah7();

CREATE OR REPLACE FUNCTION Iu7quah7() RETURNS void
    LANGUAGE plpgsql
    AS $_$
BEGIN
    BEGIN
	CREATE VIEW tasktracker_ids AS
		SELECT artifact.artifact_id AS id, 'aid' AS is_a,
		    artifact_group_list.is_public AS subproject_is_public,
		    groups.is_public AS group_is_public,
		    artifact.summary AS summary
		FROM artifact
		INNER JOIN artifact_group_list ON
		    artifact.group_artifact_id = artifact_group_list.group_artifact_id
		INNER JOIN groups ON
		    artifact_group_list.group_id = groups.group_id
		UNION
		SELECT project_task.project_task_id AS id, 'tid' AS is_a,
		    project_group_list.is_public AS subproject_is_public,
		    groups.is_public AS group_is_public,
		    project_task.summary AS summary
		FROM project_task
		INNER JOIN project_group_list ON
		    project_task.group_project_id = project_group_list.group_project_id
		INNER JOIN groups ON
		    project_group_list.group_id = groups.group_id
	    ;
	CREATE TABLE tasktracker_scm (
		id		BIGINT	NOT NULL,
		group_name	TEXT	NOT NULL,
		revision	BIGINT	NOT NULL,
		href		TEXT	NOT NULL,
		CONSTRAINT tasktracker_scm_pk
			PRIMARY KEY (id, group_name, revision),
		-- CONSTRAINT tasktracker_scm_fk_id
		--	FOREIGN KEY (id) REFERENCES tasktracker_ids (id),
		CONSTRAINT tasktracker_scm_fk_group
			FOREIGN KEY (group_name) REFERENCES groups (unix_group_name)
	    );

    EXCEPTION
	WHEN OTHERS THEN
	     RETURN;
    END;
END;$_$;
SELECT Iu7quah7();

DROP FUNCTION Iu7quah7();
