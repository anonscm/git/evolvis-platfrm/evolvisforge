--
-- Add new file types to the FRS
--

INSERT INTO frs_filetype (type_id, name)
	VALUES (1100, 'Python 2.6+ runnable PKZIP archive');
INSERT INTO frs_filetype (type_id, name)
	VALUES (1101, 'Python3 runnable PKZIP archive');
INSERT INTO frs_filetype (type_id, name)
	VALUES (2201, 'Java JAR');
INSERT INTO frs_filetype (type_id, name)
	VALUES (2202, 'Java WAR');
INSERT INTO frs_filetype (type_id, name)
	VALUES (2203, 'Java EAR');
INSERT INTO frs_filetype (type_id, name)
	VALUES (2204, 'Java RAR');
INSERT INTO frs_filetype (type_id, name)
	VALUES (2205, 'Liferay LAR');
INSERT INTO frs_filetype (type_id, name)
	VALUES (2206, 'Sources JAR');
INSERT INTO frs_filetype (type_id, name)
	VALUES (2207, 'Javadoc JAR');
INSERT INTO frs_filetype (type_id, name)
	VALUES (2400, 'Unix executable or (shared) object');
INSERT INTO frs_filetype (type_id, name)
	VALUES (2401, 'a.out executable or object');
INSERT INTO frs_filetype (type_id, name)
	VALUES (2402, 'ELF executable or object');
INSERT INTO frs_filetype (type_id, name)
	VALUES (2403, '(E)COFF executable or object');
INSERT INTO frs_filetype (type_id, name)
	VALUES (2410, 'Unix kernel');
INSERT INTO frs_filetype (type_id, name)
	VALUES (2411, 'Unix kernel (bootable)');
INSERT INTO frs_filetype (type_id, name)
	VALUES (2420, 'Bootloader');
INSERT INTO frs_filetype (type_id, name)
	VALUES (3001, '.arj');
INSERT INTO frs_filetype (type_id, name)
	VALUES (3002, '.lzh');
INSERT INTO frs_filetype (type_id, name)
	VALUES (3003, '.rar');
INSERT INTO frs_filetype (type_id, name)
	VALUES (3200, 'ISO 9660');
INSERT INTO frs_filetype (type_id, name)
	VALUES (3201, 'raw HDD image');
INSERT INTO frs_filetype (type_id, name)
	VALUES (3202, 'VMware HDD image');
INSERT INTO frs_filetype (type_id, name)
	VALUES (3203, 'qcow2 HDD image');
INSERT INTO frs_filetype (type_id, name)
	VALUES (3204, 'VirtualBox appliance');
INSERT INTO frs_filetype (type_id, name)
	VALUES (3205, 'Floppy image');
INSERT INTO frs_filetype (type_id, name)
	VALUES (3300, '.a Unix archiver ar(1)');
INSERT INTO frs_filetype (type_id, name)
	VALUES (3301, '.a for a.out');
INSERT INTO frs_filetype (type_id, name)
	VALUES (3302, '.a for ELF');
INSERT INTO frs_filetype (type_id, name)
	VALUES (3303, '.a for (E)COFF');
INSERT INTO frs_filetype (type_id, name)
	VALUES (7999, 'Other Binary (distinguished)');
INSERT INTO frs_filetype (type_id, name)
	VALUES (8050, 'WebP');
INSERT INTO frs_filetype (type_id, name)
	VALUES (8099, 'Other Image (distinguished)');
INSERT INTO frs_filetype (type_id, name)
	VALUES (8101, 'Licencing Information');
INSERT INTO frs_filetype (type_id, name)
	VALUES (8102, 'Configuration');
INSERT INTO frs_filetype (type_id, name)
	VALUES (8103, 'SQL');
INSERT INTO frs_filetype (type_id, name)
	VALUES (8104, 'JSON');
INSERT INTO frs_filetype (type_id, name)
	VALUES (8110, 'Script (Install/Documentation)');
INSERT INTO frs_filetype (type_id, name)
	VALUES (8111, 'DOS Batch');
INSERT INTO frs_filetype (type_id, name)
	VALUES (8112, 'Bourne/POSIX Shell');
INSERT INTO frs_filetype (type_id, name)
	VALUES (8113, 'C Shell (csh, tcsh)');
INSERT INTO frs_filetype (type_id, name)
	VALUES (8114, 'Korn Shell');
INSERT INTO frs_filetype (type_id, name)
	VALUES (8115, 'GNU Bourne-Argain Shell (bash)');
INSERT INTO frs_filetype (type_id, name)
	VALUES (8116, 'Z Shell (zsh)');
INSERT INTO frs_filetype (type_id, name)
	VALUES (8199, 'Other Text (distinguished)');
INSERT INTO frs_filetype (type_id, name)
	VALUES (8210, 'CSS');
INSERT INTO frs_filetype (type_id, name)
	VALUES (8220, 'XML');
INSERT INTO frs_filetype (type_id, name)
	VALUES (8221, 'XSLT');
INSERT INTO frs_filetype (type_id, name)
	VALUES (8470, 'WebM');
INSERT INTO frs_filetype (type_id, name)
	VALUES (9810, 'checksum/hash file');
INSERT INTO frs_filetype (type_id, name)
	VALUES (9811, 'detached PGP signature');
INSERT INTO frs_filetype (type_id, name)
	VALUES (9812, 'PGP non-cleartext file');
