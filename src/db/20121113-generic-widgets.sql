--
-- Generic widget preferences/settings table
--
-- Shamelessly stolen from 20100506-add-widgets.sql
-- and the widget_rss code, except our data is, to
-- the DB, opaque and not of interest, so that not
-- every new widget needs a DB change.
--
-- Note:
-- 15:23⎜«Lo-lan-do:#fusionforge» SERIAL is NOT NULL by default.
--

CREATE TABLE widget_prefs (
	id SERIAL,
	owner_id INTEGER NOT NULL DEFAULT 0,
	owner_type CHARACTER VARYING(1) NOT NULL DEFAULT 'u',
	d TEXT NOT NULL,
	PRIMARY KEY (id)
);
