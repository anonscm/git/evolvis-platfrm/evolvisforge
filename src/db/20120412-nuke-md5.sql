-- remove the stored MD5-hashed unsalted password for all users
-- to avoid rainbow table attacks, should the DB ever be leaked

DROP VIEW nss_passwd;
DROP VIEW nss_shadow;
ALTER TABLE users
	ALTER COLUMN unix_pw TYPE character varying(128),
	DROP COLUMN user_pw;
CREATE VIEW nss_passwd AS SELECT unix_uid AS uid, unix_gid AS gid, user_name AS login, CASE unix_pw WHEN '' THEN 'x' WHEN ':' THEN 'x' ELSE unix_pw END AS passwd, realname AS gecos, shell, user_name AS homedir, status FROM users WHERE unix_status = 'A';
CREATE VIEW nss_shadow AS SELECT user_name AS login, CASE unix_pw WHEN '' THEN 'x' WHEN ':' THEN 'x' ELSE unix_pw END AS passwd, 'n'::character(1) AS expired, 'n'::character(1) AS pwchange FROM users WHERE unix_status = 'A';
GRANT SELECT ON nss_passwd TO gforge_nss;
