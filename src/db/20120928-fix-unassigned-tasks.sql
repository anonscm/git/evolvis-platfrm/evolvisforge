INSERT INTO project_assigned_to
	(project_task_id, assigned_to_id)
    SELECT
	project_task.project_task_id,
	100 AS assigned_to_id
    FROM project_task
    LEFT OUTER JOIN project_assigned_to
	ON project_task.project_task_id = project_assigned_to.project_task_id
    WHERE project_assigned_to.assigned_to_id IS NULL;
