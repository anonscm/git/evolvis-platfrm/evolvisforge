CREATE FUNCTION upgrade_default_role_to_admin () RETURNS void AS $$
DECLARE
	g groups%ROWTYPE ;
BEGIN
	FOR g IN SELECT * FROM groups
	LOOP
		UPDATE user_group SET role_id=(
		       SELECT min(r.role_id)
		       FROM role r JOIN role_setting rs USING (role_id)
		       WHERE r.group_id=g.group_id
		       	     AND rs.section_name='projectadmin'
			     AND rs.value='A'
		       )
		WHERE role_id=1
		      AND group_id=g.group_id;
	END LOOP ;

END ;
$$ LANGUAGE plpgsql ;

SELECT upgrade_default_role_to_admin () ;

DROP FUNCTION upgrade_default_role_to_admin () ;
