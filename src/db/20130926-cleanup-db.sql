--
-- Remove PFO RBAC settings, user<->role assignments, and roles
-- for which no group exists, i.e. which had been deleted badly.
--
-- TODO: add FK constraints into pfo_role to prevent that.
--

DELETE FROM pfo_role_setting WHERE role_id IN (
	SELECT role_id FROM pfo_role
		LEFT OUTER JOIN groups
		    ON pfo_role.home_group_id = groups.group_id
	    WHERE groups.group_id IS NULL
		AND pfo_role.home_group_id IS NOT NULL
);
DELETE FROM pfo_user_role WHERE role_id IN (
	SELECT role_id FROM pfo_role
		LEFT OUTER JOIN groups
		    ON pfo_role.home_group_id = groups.group_id
	    WHERE groups.group_id IS NULL
		AND pfo_role.home_group_id IS NOT NULL
);
DELETE FROM pfo_role WHERE role_id IN (
	SELECT role_id FROM pfo_role
		LEFT OUTER JOIN groups
		    ON pfo_role.home_group_id = groups.group_id
	    WHERE groups.group_id IS NULL
		AND pfo_role.home_group_id IS NOT NULL
);
