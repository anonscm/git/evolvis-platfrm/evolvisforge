CREATE OR REPLACE FUNCTION get_tasktracker_minid() RETURNS bigint AS $$
DECLARE
	mid bigint;
BEGIN
	IF EXISTS (SELECT 1
	    FROM pg_class c
		JOIN pg_catalog.pg_namespace n ON c.relnamespace=n.oid
	    WHERE n.nspname='pg_catalog'
	    AND c.relname='pg_sequence') THEN
		SELECT seqmin INTO STRICT mid FROM pg_sequence
		    WHERE seqrelid='tasktracker_seq'::regclass;
	ELSE
		SELECT min_value INTO STRICT mid FROM tasktracker_seq;
	END IF;
	RETURN mid;
END;
$$ LANGUAGE plpgsql;
